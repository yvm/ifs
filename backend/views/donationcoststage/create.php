<?php

use yii\helpers\Html;

$this->title = 'Создание таблица';
$this->params['breadcrumbs'][] = ['label' => 'Donationcoststages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcoststage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
