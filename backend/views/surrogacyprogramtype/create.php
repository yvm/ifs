<?php

use yii\helpers\Html;

$this->title = 'Создание программа';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyprogramtypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacyprogramtype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
