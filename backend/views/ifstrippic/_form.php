<?php

use kartik\file\FileInput;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Ifstrippic */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ifstrippic-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php
    echo $form->field($model, 'image[]')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
    ]);
    ?>

    <?= $form->field($model, 'ifstrip_id')->dropDownList(\common\models\Ifstrip::getList()) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
