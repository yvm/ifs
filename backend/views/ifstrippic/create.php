<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ifstrippic */

$this->title = 'Создание картинка';
$this->params['breadcrumbs'][] = ['label' => 'Ifstrippics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ifstrippic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
