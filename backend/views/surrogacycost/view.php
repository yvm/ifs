<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Surrogacycosts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycost-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            'countStar',
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
            [
                'attribute' => 'price',
                'value' => $model->price,
                'format' => 'raw',
            ],
            [
                'attribute' => 'statusPriceGuest',
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceGuest);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPriceUser',
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceUser);
                },
                'format' => 'raw',
            ],
            'package_title',
            'package_subtitle',
            [
                'attribute' => 'package_content',
                'value' => $model->package_content,
                'format' => 'raw',
            ],
            'package_tabletitle'


        ],
    ]) ?>

</div>
