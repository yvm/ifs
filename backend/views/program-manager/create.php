<?php

use yii\helpers\Html;

$this->title = 'Создание Program Manager';
$this->params['breadcrumbs'][] = ['label' => 'Program Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-manager-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
