<?php

use yii\helpers\Html;

$this->title = 'Редактирование Program Manager: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Program Managers', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="program-manager-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
