<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ifs Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ifs-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'firstname',
                'value' => $model->firstname,
                'format' => 'raw',
            ],
            [
                'attribute' => 'lastname',
                'value' => $model->lastname,
                'format' => 'raw',
            ],
            'role',
            'type',
            [
                'attribute' => 'mobile_phone_number',
                'value' => $model->mobile_phone_number,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'telegram',
                'value' => $model->telegram,
                'format' => 'raw',
            ],
            'country',
            [
                'attribute' => 'WatsApp',
                'value' => $model->WatsApp,
                'format' => 'raw',
            ],
            [
                'attribute' => 'Skype',
                'value' => $model->Skype,
                'format' => 'raw',
            ],
            'city',
            [
                'attribute' => 'subject',
                'value' => $model->subject,
                'format' => 'raw',
            ],
            [
                'attribute' => 'message',
                'value' => $model->message,
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
