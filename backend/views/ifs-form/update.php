<?php

use yii\helpers\Html;

$this->title = 'Редактирование Ifs Form: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ifs Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="ifs-form-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
