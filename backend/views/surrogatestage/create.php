<?php

use yii\helpers\Html;

$this->title = 'Создание этапа суррогатного материнства';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatestages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatestage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
