<?php

use yii\helpers\Html;

$this->title = 'Создание Donorforeigncon';
$this->params['breadcrumbs'][] = ['label' => 'Donorforeigncons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorforeigncon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
