<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\GetAConsultation */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="get-aconsultation-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'firstname') ?>

    <?= $form->field($model, 'lastname') ?>

    <?= $form->field($model, 'role') ?>

    <?= $form->field($model, 'type') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'WatsApp') ?>

    <?php // echo $form->field($model, 'Skype') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'DesiredConsultLocation') ?>

    <?php // echo $form->field($model, 'data') ?>

    <?php // echo $form->field($model, 'time') ?>

    <?php // echo $form->field($model, 'QuestionsComments') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
