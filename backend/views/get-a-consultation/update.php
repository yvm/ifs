<?php

use yii\helpers\Html;

$this->title = 'Редактирование Get A Consultation: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Get A Consultations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="get-aconsultation-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
