<?php

use yii\helpers\Html;

$this->title = 'Создание Get A Consultation';
$this->params['breadcrumbs'][] = ['label' => 'Get A Consultations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="get-aconsultation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
