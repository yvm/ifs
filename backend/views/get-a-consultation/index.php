<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Get A Consultations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="get-aconsultation-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать Get A Consultation', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'firstname',
                'value' => $model->firstname,
                'format' => 'raw',
            ],
            [
                'attribute' => 'lastname',
                'value' => $model->lastname,
                'format' => 'raw',
            ],
            'role',
            'type',

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
