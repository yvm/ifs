<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Get A Consultations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="get-aconsultation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'firstname',
                'value' => $model->firstname,
                'format' => 'raw',
            ],
            [
                'attribute' => 'lastname',
                'value' => $model->lastname,
                'format' => 'raw',
            ],
            'role',
            'type',
            'country',
            [
                'attribute' => 'WatsApp',
                'value' => $model->WatsApp,
                'format' => 'raw',
            ],
            [
                'attribute' => 'Skype',
                'value' => $model->Skype,
                'format' => 'raw',
            ],
            'city',
            'DesiredConsultLocation',
            [
                'attribute' => 'data',
                'value' => $model->data,
                'format' => 'raw',
            ],
            [
                'attribute' => 'time',
                'value' => $model->time,
                'format' => 'raw',
            ],
            [
                'attribute' => 'QuestionsComments',
                'value' => $model->QuestionsComments,
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
