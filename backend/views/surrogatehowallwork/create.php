<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogatehowallwork';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatehowallworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatehowallwork-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
