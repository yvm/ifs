<?php

use yii\helpers\Html;

$this->title = 'Редактирования контентa';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyprogramcons', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="surrogacyprogramcon-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
