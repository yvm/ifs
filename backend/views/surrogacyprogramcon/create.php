<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogacyprogramcon';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyprogramcons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacyprogramcon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
