<?php

use yii\helpers\Html;

$this->title = 'Создания вида';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacycosttypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycosttype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
