<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Виды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycosttype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'content',
//                'value' => $model->content,
//                'format' => 'raw',
//            ],
            ['attribute'=>'surrogacycost_id', 'value'=>function($model){ return $model->surrogacyCostName;},'filter'=>\common\models\Surrogacycost::getList()],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
