<?php

use yii\helpers\Html;

$this->title = 'Создание Ethnic Origin Person';
$this->params['breadcrumbs'][] = ['label' => 'Ethnic Origin People', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ethnic-origin-person-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
