<?php

use yii\helpers\Html;

$this->title = 'Редактирование Ethnic Origin Person: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Ethnic Origin People', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="ethnic-origin-person-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
