<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

if($_GET['type'])
    $model->type = $_GET['type'];
?>
<div class="registration-text-form">
    <?php $form = ActiveForm::begin(); ?>

    <?if($model->type == 3 || $model->type == 4){?>
        <?= $form->field($model, 'role')->dropDownList([1 => 'Intended Parent', 2 => 'Surrogate', 3 => 'Egg Donor',
            4 => 'Surrogate & Egg Donor', 5 => 'Biotransportation Client', 20 => 'Admin'], ['prompt' => '']) ?>
    <?}?>

    <?= $form->field($model, 'type')->input('hidden')->label(false) ?>

    <?= $form->field($model, 'text')->widget(CKEditor::className(), [
        'editorOptions' => [
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
