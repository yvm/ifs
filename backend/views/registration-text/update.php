<?php

use yii\helpers\Html;

if($model->type == 1)
    $name = "Terms and Condition";

if($model->type == 2)
    $name = "Registration form";

if($model->type == 3)
    $name = "Registration form checkbox";

$this->title = 'Редактирование '.$name;
$this->params['breadcrumbs'][] = ['label' => 'Registration Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="registration-text-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
