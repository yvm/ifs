<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Registration Texts';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registration-text-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?if($_GET['type'] == 3){?>
        <p>
            <?= Html::a('Создать Registration Text', ['create?type=3'], ['class' => 'btn btn-success']) ?>
        </p>
    <?}?>
    <?if($_GET['type'] == 4){?>
        <p>
            <?= Html::a('Создать Registration Text', ['create?type=4'], ['class' => 'btn btn-success']) ?>
        </p>
    <?}?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'role',
            'type',
            [
                'attribute' => 'text',
                'value' => $model->text,
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
