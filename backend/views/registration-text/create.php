<?php

use yii\helpers\Html;

$this->title = 'Создание Registration Text';
$this->params['breadcrumbs'][] = ['label' => 'Registration Texts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="registration-text-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
