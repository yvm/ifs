<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Школа для Суррогатов';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyschools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
        //            'class' => 'btn btn-danger',
        //            'data' => [
        //                'confirm' => 'Are you sure you want to delete this item?',
        //                'method' => 'post',
        //            ],
        //        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'contenta',
                'value' => $model->contenta,
                'format' => 'raw',
            ],

            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>200]);
                }
            ],
            [
                'attribute' => 'contentb',
                'value' => $model->contentb,
                'format' => 'raw',
            ],
            [
                'attribute' => 'contentc',
                'value' => $model->contentc,
                'format' => 'raw',
            ],
        ],
    ]) ?>

</div>
