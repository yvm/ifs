<?php

use yii\helpers\Html;

$this->title = 'Редактирование школа для Суррогатов';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyschools', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="surrogacyschool-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>
</div>
