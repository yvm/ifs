<?php

use yii\helpers\Html;

$this->title = 'Создание FAQ';
$this->params['breadcrumbs'][] = ['label' => 'Donorfaqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorfaq-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
