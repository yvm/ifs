<?php

use yii\helpers\Html;

$this->title = 'Редактирование FAQ: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Donorfaqs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="donorfaq-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
