<?php

use yii\helpers\Html;

$this->title = 'Редактирование текста: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Translation Modals', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="translation-modal-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
