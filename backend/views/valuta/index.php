<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Валюты';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valuta-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать валюта', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'text',
                'value' => $model->text,
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => Label::statusList(),
                'value' => function ($model) {
                    return Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],
                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
