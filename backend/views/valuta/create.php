<?php

use yii\helpers\Html;

$this->title = 'Создание Valuta';
$this->params['breadcrumbs'][] = ['label' => 'Valutas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="valuta-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
