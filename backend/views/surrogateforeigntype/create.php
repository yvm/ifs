<?php

use yii\helpers\Html;

$this->title = 'Создание вида';
$this->params['breadcrumbs'][] = ['label' => 'Surrogateforeigntypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogateforeigntype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
