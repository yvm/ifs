<?php

use yii\helpers\Html;

$this->title = 'Создание Donorrequire';
$this->params['breadcrumbs'][] = ['label' => 'Donorrequires', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorrequire-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
