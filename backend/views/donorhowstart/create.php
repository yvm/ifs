<?php

use yii\helpers\Html;

$this->title = 'Создание пункта';
$this->params['breadcrumbs'][] = ['label' => 'Donorhowstarts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorhowstart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
