<?php

use yii\helpers\Html;

$this->title = 'Создание Physician';
$this->params['breadcrumbs'][] = ['label' => 'Physicians', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="physician-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
