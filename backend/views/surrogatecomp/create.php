<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogatecomp';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatecomps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatecomp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
