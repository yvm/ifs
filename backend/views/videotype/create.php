<?php

use yii\helpers\Html;

$this->title = 'Создание видео';
$this->params['breadcrumbs'][] = ['label' => 'Videotypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="videotype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
