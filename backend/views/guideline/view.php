<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Guidelines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$roles = [1 =>'Родитель', 2=>'Суррогатные',3 => 'Донор'];
$file = [1 => 'Фото',2 =>'Документы',3 =>'Образец почерка',4 =>'Голосовое сообщение',5 =>'Видео']
?>
<div class="guideline-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
            [
                'attribute' => 'file',
                'value' => $file[$model->file],
                'format' => 'text',
            ],
            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
            ],
        ],
    ]) ?>

</div>
