<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Guidelines';
$this->params['breadcrumbs'][] = $this->title;

$files = [1 => 'Фото',2 =>'Документы',3 =>'Образец почерка',4 =>'Голосовое сообщение',5 =>'Видео']
?>
<div class="guideline-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],

            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
                'filter' => \common\models\Role::getList(),
            ],
            [
                'attribute' => 'file',
                'value' => function($model){ return $model->files;},
                'filter' => $files,
            ],


                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
