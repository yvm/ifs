<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
$roles = [1 =>'Родитель', 2=>'Суррогатные',3 => 'Донор'];
$file = [1 => 'Фото',2 =>'Документы',3 =>'Образец почерка',4 =>'Голосовое сообщение',5 =>'Видео']

?>
<div class="guideline-form">

    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'editorOptions' => [
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false,
        ],
    ]) ?>

    <?= $form->field($model, 'role')->dropDownList($roles) ?>

    <?= $form->field($model, 'file')->dropDownList($file) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
