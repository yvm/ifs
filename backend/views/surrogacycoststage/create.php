<?php

use yii\helpers\Html;

$this->title = 'Создание таблица';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacycoststages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycoststage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
