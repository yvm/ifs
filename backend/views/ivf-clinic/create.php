<?php

use yii\helpers\Html;

$this->title = 'Создание Ivf Clinic';
$this->params['breadcrumbs'][] = ['label' => 'Ivf Clinics', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ivf-clinic-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
