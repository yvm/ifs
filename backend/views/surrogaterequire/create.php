<?php

use yii\helpers\Html;

$this->title = 'Создание суррогатные требования';
$this->params['breadcrumbs'][] = ['label' => 'Surrogaterequires', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogaterequire-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
