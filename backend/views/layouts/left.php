<aside class="main-sidebar">
    <section class="sidebar">
        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    [
                        'label' => 'Переводы',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'По сайту', 'icon' => 'fa fa-user', 'url' => ['translation/index'],'active' => $this->context->id == 'translation'],
                            ['label' => 'Модальные окна', 'icon' => 'fa fa-user', 'url' => ['translation-modal/index'],'active' => $this->context->id == 'translation-modal'],
                            ['label' => 'Сообщение', 'icon' => 'fa fa-user', 'url' => ['translation-message/index'],'active' => $this->context->id == 'translation-message'],
                            ['label' => 'Terms and Condition', 'icon' => 'fa fa-user', 'url' => ['registration-text/index?type=1'],'active' => $this->context->id == 'registration-text'],
                            ['label' => 'Registration form checkbox', 'icon' => 'fa fa-user', 'url' => ['registration-text/index?type=3'],'active' => $this->context->id == 'registration-text'],
                            ['label' => 'Step 1', 'icon' => 'fa fa-user', 'url' => ['registration-text/index?type=4'],'active' => $this->context->id == 'registration-text'],
                        ],
                    ],

                    ['label' => 'E-mail админов', 'icon' => 'fa fa-user', 'url' => ['admin-email/index'],'active' => $this->context->id == 'admin-email'],
                    [
                        'label' => 'Пользователи',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Информация о пользователе', 'icon' => 'fa fa-user', 'url' => ['user/index'],'active' => $this->context->id == 'user'],
                            ['label' => 'История профиля', 'icon' => 'fa fa-user', 'url' => ['profile-history/index'],'active' => $this->context->id == 'profile-history'],
                            ['label' => 'Программы пользователей', 'icon' => 'fa fa-user', 'url' => ['program/index'],'active' => $this->context->id == 'program'],
                            ['label' => 'Program Manager', 'icon' => 'fa fa-user', 'url' => ['program-manager/index'],'active' => $this->context->id == 'program-manager'],
                            ['label' => 'IVF Clinic', 'icon' => 'fa fa-user', 'url' => ['ivf-clinic/index'],'active' => $this->context->id == 'ivf-clinic'],
                            ['label' => 'Physician', 'icon' => 'fa fa-user', 'url' => ['physician/index'],'active' => $this->context->id == 'physician'],
                        ],
                    ],

                    ['label' => 'Guidelines', 'icon' => 'fa fa-user', 'url' => ['guideline/index'],'active' => $this->context->id == 'guideline'],

                    [
                        'label' => 'Поля для сайта',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Этническое происхождение', 'icon' => 'fa fa-user', 'url' => ['ethnic-origin-person/index'],'active' => $this->context->id == 'ethnic-origin-person'],
                            ['label' => 'Страны', 'icon' => 'fa fa-user', 'url' => ['countrys/index'],'active' => $this->context->id == 'countrys'],
                            ['label' => 'Желаемое место консультации', 'icon' => 'fa fa-user', 'url' => ['desired-consult-location/index'],'active' => $this->context->id == 'desired-consult-location'],

                        ],
                    ],
                    [
                        'label' => 'Поля для регистрация',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Валюты', 'icon' => 'fa fa-user', 'url' => ['valuta/index'],'active' => $this->context->id == 'valuta'],
                            ['label' => 'Компенсация для суррогат', 'icon' => 'fa fa-user', 'url' => ['surrogate-compensation/index'],'active' => $this->context->id == 'surrogate-compensation'],
                            ['label' => 'Компенсация для донор яйцеклеток', 'icon' => 'fa fa-user', 'url' => ['donor-compensation/index'],'active' => $this->context->id == 'donor-compensation'],
                        ],
                    ],
                    [
                        'label' => 'Заявки',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Консультации', 'icon' => 'fa fa-user', 'url' => ['get-a-consultation/index'],'active' => $this->context->id == 'get-a-consultation'],
							['label' => 'IFS контакт', 'icon' => 'fa fa-user', 'url' => ['ifs-form/index'],'active' => $this->context->id == 'ifs-form'],
							['label' => 'Biotransportation Form', 'icon' => 'fa fa-user', 'url' => ['biotransportation-form/index'],'active' => $this->context->id == 'biotransportation-form'],
                        ],
                    ],
                    [
                        'label' => 'Cтраны и языки',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Все языки', 'icon' => 'fa fa-user', 'url' => ['language/index'],'active' => $this->context->id == 'language'],
                            ['label' => 'Все cтраны', 'icon' => 'fa fa-user', 'url' => ['country/index'],'active' => $this->context->id == 'country'],
                            ['label' => 'Страны с языком', 'icon' => 'fa fa-user', 'url' => ['countrylang/index'],'active' => $this->context->id == 'countrylang'],
                            ['label' => 'Предвыбор', 'icon' => 'fa fa-user', 'url' => ['preselect/index'],'active' => $this->context->id == 'preselect'],
                        ],
                    ],


                    [
                        'label' => 'Меню',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Меню', 'icon' => 'fa fa-user', 'url' => ['menu/index'],'active' => $this->context->id == 'menu'],
                            ['label' => 'Категории', 'icon' => 'fa fa-user', 'url' => ['submenu/index'],'active' => $this->context->id == 'submenu'],
                        ],
                    ],
                    ['label' => 'Баннер страницы ', 'icon' => 'fa fa-user', 'url' => ['banner/index'],'active' => $this->context->id == 'banner'],
                    ['label' => 'Кнопки в баннере', 'icon' => 'fa fa-user', 'url' => ['buttons/index'],'active' => $this->context->id == 'buttons'],
                    [
                        'label' => 'Главная страница',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Баннер ', 'icon' => 'fa fa-user', 'url' => ['mainslide/index'],'active' => $this->context->id == 'mainslide'],
                            ['label' => 'Успехи', 'icon' => 'fa fa-user', 'url' => ['mainprogress/index'],'active' => $this->context->id == 'mainprogress'],
                        ],
                    ],


                    [
                        'label' => 'IFS',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'О нас', 'icon' => 'fa fa-user', 'url' => ['ifsabout/index'],'active' => $this->context->id == 'ifsabout'],
                            ['label' => 'Десять фактов о нас', 'icon' => 'fa fa-user', 'url' => ['ifsfacts/index'],'active' => $this->context->id == 'ifsfacts'],
                            ['label' => 'Наши услуги ', 'icon' => 'fa fa-user', 'url' => ['ifsservice/index'],'active' => $this->context->id == 'ifsservice'],
                            ['label' => 'Наши отличия ', 'icon' => 'fa fa-user', 'url' => ['ifsdiff/index'],'active' => $this->context->id == 'ifsdiff'],
                            ['label' => 'Наша медицинская команда ', 'icon' => 'fa fa-user', 'url' => ['ifsmedteam/index'],'active' => $this->context->id == 'ifsmedteam'],
                            ['label' => 'Наша команда', 'icon' => 'fa fa-user', 'url' => ['ifsteam/index'],'active' => $this->context->id == 'ifsteam'],
                            ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['ifscontact/index'],'active' => $this->context->id == 'ifscontact'],
                        ],
                    ],


                    [
                        'label' => 'Родители',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Как все это работает', 'icon' => 'fa fa-user', 'url' => ['parenthowallwork/index'],'active' => $this->context->id == 'parenthowallwork'],
                            ['label' => 'Программы', 'icon' => 'fa fa-user', 'url' => ['parentprogram/index'],'active' => $this->context->id == 'parentprogram'],
                            ['label' => 'Выгоды ', 'icon' => 'fa fa-user', 'url' => ['parentbenefit/index'],'active' => $this->context->id == 'parentbenefit'],
                            ['label' => 'Почему казахстан', 'icon' => 'fa fa-user', 'url' => ['parentwhykazak/index'],'active' => $this->context->id == 'parentwhykazak'],
                            [
                                'label' => 'Твоя поездка в алматы',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контенты', 'icon' => 'fa fa-user', 'url' => ['ifstrip/index'],'active' => $this->context->id == 'ifstrip'],
                                    ['label' => 'Картинки', 'icon' => 'fa fa-user', 'url' => ['ifstrippic/index'],'active' => $this->context->id == 'ifstrippic'],
                                ],
                            ],
                            ['label' => 'Наши ведущие врачи ', 'icon' => 'fa fa-user', 'url' => ['parentdoctor/index'],'active' => $this->context->id == 'parentdoctor'],
                            ['label' => 'Законы о суррогатном материнстве', 'icon' => 'fa fa-user', 'url' => ['parentlaw/index'],'active' => $this->context->id == 'parentlaw'],
                            ['label' => 'Истории и отзывы', 'icon' => 'fa fa-user', 'url' => ['parentstory/index'],'active' => $this->context->id == 'parentstory'],
                            ['label' => 'FAQ', 'icon' => 'fa fa-user', 'url' => ['parentfaq/index'],'active' => $this->context->id == 'parentfaq'],
                            ['label' => 'Как начать', 'icon' => 'fa fa-user', 'url' => ['parenthowstart/index'],'active' => $this->context->id == 'parenthowstart'],
                        ],
                    ],

                    [
                        'label' => 'Cуррогатное',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Программы ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['surrogacyprogramcon/index'],'active' => $this->context->id == 'surrogacyprogramcon'],
                                    ['label' => 'Программы', 'icon' => 'fa fa-user', 'url' => ['surrogacyprogramtype/index'],'active' => $this->context->id == 'surrogacyprogramtype'],
                                ],
                            ],
                            ['label' => 'Процессы ', 'icon' => 'fa fa-user', 'url' => ['surrogacyprocess/index'],'active' => $this->context->id == 'surrogacyprocess'],
                            [
                                'label' => 'Цены ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['surrogacycostcon/index'],'active' => $this->context->id == 'surrogacycostcon'],
                                    ['label' => 'Программы', 'icon' => 'fa fa-user', 'url' => ['surrogacycost/index'],'active' => $this->context->id == 'surrogacycost'],
                                    ['label' => 'Виды', 'icon' => 'fa fa-user', 'url' => ['surrogacycosttype/index'],'active' => $this->context->id == 'surrogacycosttype'],
                                    ['label' => 'Таблицы', 'icon' => 'fa fa-user', 'url' => ['surrogacycoststage/index'],'active' => $this->context->id == 'surrogacycoststage'],
                                ],
                            ],
                            ['label' => 'Баннер ', 'icon' => 'fa fa-user', 'url' => ['surrogacydatabase/index'],'active' => $this->context->id == 'surrogacydatabase'],
                            ['label' => 'Школа для Суррогатов ', 'icon' => 'fa fa-user', 'url' => ['surrogacyschool/index'],'active' => $this->context->id == 'surrogacyschool'],
                            ['label' => 'Сервисы ', 'icon' => 'fa fa-user', 'url' => ['surrogacyservice/index'],'active' => $this->context->id == 'surrogacyservice'],
                        ],
                    ],

                    [
                        'label' => 'Донорство яйцеклеток',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Программы ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['donationprogramcon/index'],'active' => $this->context->id == 'donationprogramcon'],
                                    ['label' => 'Программы', 'icon' => 'fa fa-user', 'url' => ['donationprogramtype/index'],'active' => $this->context->id == 'donationprogramtype'],
                                ],
                            ],
                            ['label' => 'Как начать ', 'icon' => 'fa fa-user', 'url' => ['donationhowstart/index'],'active' => $this->context->id == 'donationhowstart'],
                            [
                                'label' => 'Цены ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['donationcostcon/index'],'active' => $this->context->id == 'donationcostcon'],
                                    ['label' => 'Программы', 'icon' => 'fa fa-user', 'url' => ['donationcost/index'],'active' => $this->context->id == 'donationcost'],
                                    ['label' => 'Виды', 'icon' => 'fa fa-user', 'url' => ['donationcosttype/index'],'active' => $this->context->id == 'donationcosttype'],
                                    ['label' => 'Таблицы', 'icon' => 'fa fa-user', 'url' => ['donationcoststage/index'],'active' => $this->context->id == 'donationcoststage'],
                                ],
                            ],
                            ['label' => 'Баннер ', 'icon' => 'fa fa-user', 'url' => ['donationlast/index'],'active' => $this->context->id == 'donationlast'],

                        ],
                    ],



                    [
                        'label' => 'Био Транспорт',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Транспортировка биоматериала ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Содержания', 'icon' => 'fa fa-user', 'url' => ['biotranscon/index'],'active' => $this->context->id == 'biotranscon'],
                                    ['label' => 'Факты', 'icon' => 'fa fa-user', 'url' => ['biotransfact/index'],'active' => $this->context->id == 'biotransfact'],
                                ],
                            ],
                            ['label' => 'Процессы', 'icon' => 'fa fa-user', 'url' => ['biotransprocess/index'],'active' => $this->context->id == 'biotransprocess'],
                            ['label' => 'Costs', 'icon' => 'fa fa-user', 'url' => ['biotranscost/index'],'active' => $this->context->id == 'biotranscost'],
                        ],
                    ],

                    [
                        'label' => 'суррогат',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Как все это работает', 'icon' => 'fa fa-user', 'url' => ['surrogatehowallwork/index'],'active' => $this->context->id == 'surrogatehowallwork'],
                            ['label' => 'О донорстве яйцеклеток ', 'icon' => 'fa fa-user', 'url' => ['surrogateabout/index'],'active' => $this->context->id == 'surrogateabout'],
                            ['label' => 'Преимущества ', 'icon' => 'fa fa-user', 'url' => ['surrogatebeneft/index'],'active' => $this->context->id == 'surrogatebeneft'],
                            ['label' => 'Суррогатные требования ', 'icon' => 'fa fa-user', 'url' => ['surrogaterequire/index'],'active' => $this->context->id == 'surrogaterequire'],

                            [
                                'label' => 'Для иностранных суррогатов ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['surrogateforeigncon/index'],'active' => $this->context->id == 'surrogateforeigncon'],
                                    ['label' => 'Виды', 'icon' => 'fa fa-user', 'url' => ['surrogateforeigntype/index'],'active' => $this->context->id == 'surrogateforeigntype'],
                                ],
                            ],

                            [
                                'label' => 'Этапы суррогатного материнства ',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['surrogatestagecon/index'],'active' => $this->context->id == 'surrogatestagecon'],
                                    ['label' => 'Этапы', 'icon' => 'fa fa-user', 'url' => ['surrogatestage/index'],'active' => $this->context->id == 'surrogatestage'],
                                ],
                            ],

                            ['label' => 'Компенсация ', 'icon' => 'fa fa-user', 'url' => ['surrogatecomp/index'],'active' => $this->context->id == 'surrogatecomp'],
                            ['label' => 'Школа для Суррогатов ', 'icon' => 'fa fa-user', 'url' => ['surrogateschool/index'],'active' => $this->context->id == 'surrogateschool'],
                            ['label' => 'Сервисы ', 'icon' => 'fa fa-user', 'url' => ['surrogateservice/index'],'active' => $this->context->id == 'surrogateservice'],
                            ['label' => 'Психолого-правовое обеспечение ', 'icon' => 'fa fa-user', 'url' => ['surrogatepsyc/index'],'active' => $this->context->id == 'surrogatepsyc'],
                            ['label' => 'Истории и отзывы ', 'icon' => 'fa fa-user', 'url' => ['surrogatestory/index'],'active' => $this->context->id == 'surrogatestory'],
                            ['label' => 'FAQ ', 'icon' => 'fa fa-user', 'url' => ['surrogatefaq/index'],'active' => $this->context->id == 'surrogatefaq'],
                            ['label' => 'Как начать ', 'icon' => 'fa fa-user', 'url' => ['surrogatehowstart/index'],'active' => $this->context->id == 'surrogatehowstart'],
                        ],
                    ],


                    [
                        'label' => 'Донор яйцеклеток',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Как все это работает', 'icon' => 'fa fa-user', 'url' => ['donorhowallwork/index'],'active' => $this->context->id == 'donorhowallwork'],
                            ['label' => 'О донорстве яйцеклеток ', 'icon' => 'fa fa-user', 'url' => ['donorabout/index'],'active' => $this->context->id == 'donorabout'],
                            ['label' => 'Преимущества ', 'icon' => 'fa fa-user', 'url' => ['donorbeneft/index'],'active' => $this->context->id == 'donorbeneft'],
                            ['label' => 'Требования к донорам яйцеклеток ', 'icon' => 'fa fa-user', 'url' => ['donorrequire/index'],'active' => $this->context->id == 'donorrequire'],

                            [
                                'label' => 'Для иностранных доноров яйцеклеток',
                                'icon' => 'fa fa-user',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Контент', 'icon' => 'fa fa-user', 'url' => ['donorforeigncon/index'],'active' => $this->context->id == 'donorforeigncon'],
                                    ['label' => 'Виды', 'icon' => 'fa fa-user', 'url' => ['donorforeigntype/index'],'active' => $this->context->id == 'donorforeigntype'],
                                ],
                            ],
                            ['label' => 'Этапы иностранных доноров яйцеклеток ', 'icon' => 'fa fa-user', 'url' => ['donorstage/index'],'active' => $this->context->id == 'donorstage'],
                            ['label' => 'Компенсация ', 'icon' => 'fa fa-user', 'url' => ['donorcomp/index'],'active' => $this->context->id == 'donorcomp'],
                            ['label' => 'Истории и отзывы ', 'icon' => 'fa fa-user', 'url' => ['donorstory/index'],'active' => $this->context->id == 'donorstory'],
                            ['label' => 'FAQ ', 'icon' => 'fa fa-user', 'url' => ['donorfaq/index'],'active' => $this->context->id == 'donorfaq'],
                            ['label' => 'Как начать ', 'icon' => 'fa fa-user', 'url' => ['donorhowstart/index'],'active' => $this->context->id == 'donorhowstart'],
                        ],
                    ],
                    [
                        'label' => 'База данных',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Доступы', 'icon' => 'fa fa-user', 'url' => ['admission/index'],'active' => $this->context->id == 'admission'],
                            ['label' => 'Ограничение возраста', 'icon' => 'fa fa-user', 'url' => ['age/index'],'active' => $this->context->id == 'age'],
                            ['label' => 'Ограничение роста', 'icon' => 'fa fa-user', 'url' => ['height/index'],'active' => $this->context->id == 'height'],
                            ['label' => 'Ограничение веса', 'icon' => 'fa fa-user', 'url' => ['weight/index'],'active' => $this->context->id == 'weight'],
                        ],
                    ],

                    ['label' => 'месяцы', 'icon' => 'fa fa-user', 'url' => ['month/index'],'active' => $this->context->id == 'month'],
                    ['label' => 'Новости и события', 'icon' => 'fa fa-user', 'url' => ['blog/index'],'active' => $this->context->id == 'blog'],
                    [
                        'label' => 'Фотогалерея',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Темы', 'icon' => 'fa fa-user', 'url' => ['photo/index'],'active' => $this->context->id == 'photo'],
                            ['label' => 'Фото', 'icon' => 'fa fa-user', 'url' => ['phototype/index'],'active' => $this->context->id == 'phototype'],
                        ],
                    ],

                    [
                        'label' => 'Видео',
                        'icon' => 'fa fa-user',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Темы', 'icon' => 'fa fa-user', 'url' => ['video/index'],'active' => $this->context->id == 'video'],
                            ['label' => 'Видео', 'icon' => 'fa fa-user', 'url' => ['videotype/index'],'active' => $this->context->id == 'videotype'],
                        ],
                    ],

                    ['label' => 'Статья', 'icon' => 'fa fa-user', 'url' => ['article/index'],'active' => $this->context->id == 'article'],

                    ['label' => 'Контакты', 'icon' => 'fa fa-user', 'url' => ['contact/index'],'active' => $this->context->id == 'contact'],

                    ['label' => '404', 'icon' => 'fa fa-user', 'url' => ['pagenotfound/index'],'active' => $this->context->id == 'pagenotfound'],
                ],
            ]
        ) ?>
    </section>

</aside>