<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = ' Ограничение возраста для роли :'.$model->roles->name;
$this->params['breadcrumbs'][] = ['label' => 'Ages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="age-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            'from',
            'to',
            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
            ],
        ],
    ]) ?>

</div>
