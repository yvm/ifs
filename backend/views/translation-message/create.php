<?php

use yii\helpers\Html;

$this->title = 'Создание текста';
$this->params['breadcrumbs'][] = ['label' => 'Translation Messages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="translation-message-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
