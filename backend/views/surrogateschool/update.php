<?php

use yii\helpers\Html;

$this->title = 'Редактирование школа для Суррогатов';
$this->params['breadcrumbs'][] = ['label' => 'Surrogateschools', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="surrogateschool-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
    'model' => $model, 'new_model' => $new_model, 'id' => $id,
    ]) ?>
</div>
