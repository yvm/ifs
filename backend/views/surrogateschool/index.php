<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Школа для Суррогатов';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogateschool-index">
    <h1><?= Html::encode($this->title) ?></h1>
    <!--        <p>-->
    <!--            --><?//= Html::a('Создать ', ['create'], ['class' => 'btn btn-success']) ?>
    <!--        </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [

            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'contenta',
                'value' => $model->contenta,
                'format' => 'raw',
            ],
            [
                'format' => 'html',
                'filter' => '',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
        ],
    ]); ?>
</div>
