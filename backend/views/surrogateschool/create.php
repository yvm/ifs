<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogateschool';
$this->params['breadcrumbs'][] = ['label' => 'Surrogateschools', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogateschool-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
