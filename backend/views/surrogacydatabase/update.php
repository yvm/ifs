<?php

use yii\helpers\Html;

$this->title = 'Редактирование баннера';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacydatabases', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="surrogacydatabase-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
