<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogacydatabase';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacydatabases', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacydatabase-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
