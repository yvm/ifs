<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Баннер';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacydatabase-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],
            [
                'attribute' => 'buttonName',
                'value' => $model->buttonName,
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->status);
                },
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
