<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<!--<script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>-->
<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Доступы</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="profile" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">


                <?= $form->field($model, 'status_profile')->dropDownList([0 => 'Suspended Profile', 1 => 'Active Profile']) ?>

                <?= $form->field($model, 'active')->dropDownList([1 => 'Available', 2 => 'Not Available ', 3 => 'Currently family helping'], ['prompt' => ''])  ?>

                <?= $form->field($model, 'status_edit_profile')->dropDownList([1 => 'Включен', 0 => 'Отключен'])  ?>

                <?= $form->field($model, 'status_comp')->dropDownList([1 => 'Включен', 0 => 'Отключен'])  ?>

                <?= $form->field($model, 'profi')->dropDownList([0 => 'Отключен', 1 => 'Включен']) ?>

                <?= $form->field($model, 'proven')->dropDownList([0 => 'Отключен', 1 => 'Включен']) ?>

            </div>
            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">


                <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'password')->input('password') ?>

                <?= $form->field($model, 'password_repeat')->input('password') ?>

                <?= $form->field($model, 'role')->dropDownList([1 => 'Intended Parent', 2 => 'Surrogate', 3 => 'Egg Donor',
                    4 => 'Surrogate & Egg Donor', 5 => 'Biotransportation Client', 20 => 'Admin'], ['prompt' => '']) ?>


                <?= $form->field($model, 'type')->dropDownList([1 => 'Surrogacy', 2 => 'Egg', 3 => 'Surrogacy and Egg'], ['prompt' => '']) ?>

                <?= $form->field($model, 'step')->textInput() ?>

                <?$model->created_at = date('Y-m-d', $model->created_at)?>
                <?= $form->field($model, 'created_at')->input('date', ['disabled' => 'disabled']) ?>

            </div>
        </div>
    </div>

    <?php ActiveForm::end(); ?>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>

    if($('#user-role').val() != 1){
        $('.field-user-type').hide();
    }else{
        $('.field-user-type').show();
    }
    $('#user-role').change(function(){
        if($(this).val() != 1){
            $('.field-user-type').hide();
        }else{
            $('.field-user-type').show();
        }

    });

</script>


