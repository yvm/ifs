<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?
            if($model->role != 20){
        ?>
        <?= Html::a('Просмотр и редактирование профиля', ['profile', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?}?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'username',
                'value' => $model->username,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'auth_key',
//                'value' => $model->auth_key,
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'password_hash',
//                'value' => $model->password_hash,
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'password_reset_token',
//                'value' => $model->password_reset_token,
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'access_token',
//                'value' => $model->access_token,
//                'format' => 'raw',
//            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'status_profile',
                'filter' => \backend\controllers\LabelStatusUser::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelStatusUser::statusLabel($model->status_profile);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'status_comp',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->status_comp);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'status_edit_profile',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->status_edit_profile);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'active',
                'filter' => \backend\controllers\LabelActiveUser::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\LabelActiveUser::statusLabel($model->active);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'profi',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->profi);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'proven',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->proven);
                },
                'format' => 'raw',
            ],
            'role',
            'type',
            'step',
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
