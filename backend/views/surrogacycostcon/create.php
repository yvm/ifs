<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogacycostcon';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacycostcons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycostcon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
