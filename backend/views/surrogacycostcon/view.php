<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Контент';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacycostcons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacycostcon-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],

        ],
    ]) ?>

</div>
