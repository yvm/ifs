<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Surrogatestagecon */

$this->title = 'Create Surrogatestagecon';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatestagecons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatestagecon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
