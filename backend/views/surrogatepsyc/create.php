<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogatepsyc';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatepsycs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatepsyc-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
