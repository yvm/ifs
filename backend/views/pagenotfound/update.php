<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pagenotfound */

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'Pagenotfounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pagenotfound-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
