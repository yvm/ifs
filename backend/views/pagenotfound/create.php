<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Pagenotfound */

$this->title = 'Create Pagenotfound';
$this->params['breadcrumbs'][] = ['label' => 'Pagenotfounds', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pagenotfound-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
