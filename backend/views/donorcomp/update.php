<?php

use yii\helpers\Html;

$this->title = 'Редактирование ';
$this->params['breadcrumbs'][] = ['label' => 'Компенсация', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="donorcomp-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
