<?php

use yii\helpers\Html;

$this->title = 'Создание Donorcomp';
$this->params['breadcrumbs'][] = ['label' => 'Donorcomps', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorcomp-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
