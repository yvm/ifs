<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Компенсация';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorcomp-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <!--    <p>-->
    <!--        --><?//= Html::a('Создать Surrogatecomp', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',

            [
                'attribute' => 'contenta',
                'value' => $model->contenta,
                'format' => 'raw',
            ],
            [
                'attribute' => 'contentb',
                'value' => $model->contentb,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'contentc',
//                'value' => $model->contentc,
//                'format' => 'raw',
//            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
        ],
    ]); ?>
</div>
