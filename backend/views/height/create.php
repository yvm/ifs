<?php

use yii\helpers\Html;

$this->title = 'Создание Height';
$this->params['breadcrumbs'][] = ['label' => 'Heights', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="height-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
