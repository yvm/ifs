<?php

use yii\helpers\Html;

$this->title = 'Редактирование ограничение роста для роли : ' . $model->roles->name;;
$this->params['breadcrumbs'][] = ['label' => 'Heights', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="height-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
