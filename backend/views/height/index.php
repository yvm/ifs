<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Ограничение роста';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="height-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'from',
            'to',
            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
                'filter' => \common\models\Role::getList(),
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
