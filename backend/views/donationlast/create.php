<?php

use yii\helpers\Html;

$this->title = 'Создания баннера';
$this->params['breadcrumbs'][] = ['label' => 'Donationlasts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationlast-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
