<?php

use yii\helpers\Html;

$this->title = 'Создание история и отзыв';
$this->params['breadcrumbs'][] = ['label' => 'Donorstories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorstory-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
