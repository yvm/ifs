<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = ' Компенсация для донор яйцеклеток';
$this->params['breadcrumbs'][] = ['label' => 'Donor Compensations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-compensation-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>

    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [

            'from',
            'to',
            'move',
        ],
    ]) ?>

</div>
