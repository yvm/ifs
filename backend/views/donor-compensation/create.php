<?php

use yii\helpers\Html;

$this->title = 'Создание Donor Compensation';
$this->params['breadcrumbs'][] = ['label' => 'Donor Compensations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-compensation-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
