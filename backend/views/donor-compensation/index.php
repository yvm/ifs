<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Компенсация для донор яйцеклеток';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donor-compensation-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'from',
            'to',
            'move',

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
