<?php

use yii\helpers\Html;

$this->title = 'Редактирование компенсация для донор яйцеклеток';
$this->params['breadcrumbs'][] = ['label' => 'Donor Compensations', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="donor-compensation-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
