<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogacyservice';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyservices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacyservice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
