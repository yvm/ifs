<?php

use yii\helpers\Html;

$this->title = 'Создание Donorhowallwork';
$this->params['breadcrumbs'][] = ['label' => 'Donorhowallworks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorhowallwork-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
