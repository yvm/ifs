<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Blog */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Blogs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="blog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>300]);
                }
            ],
            [
                'format' => 'raw',
                'attribute' => 'content',
                'value' => $model->content,
            ],
            'date',
            [
                'format' => 'raw',
                'attribute' => 'tag',
                'value' => $model->content,
            ],
            [
                'format' => 'raw',
                'attribute' => 'metaName',
                'value' => $model->metaName,
            ],
            [
                'format' => 'raw',
                'attribute' => 'metaDesc',
                'value' => $model->metaDesc,
            ],
            [
                'format' => 'raw',
                'attribute' => 'metaKey',
                'value' => $model->metaKey,
            ],

        ],
    ]) ?>

</div>
