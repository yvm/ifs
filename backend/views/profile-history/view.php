<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->first_name." ".$model->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Profile Histories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-history-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
//            'user_id',
            [
                'attribute' => 'first_name',
                'value' => $model->first_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'last_name',
                'value' => $model->last_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'country',
                'value' => $model->getCountryName(),
                'format' => 'raw',
            ],
            [
                'attribute' => 'state',
                'value' => $model->getStateName(),
                'format' => 'raw',
            ],
            [
                'attribute' => 'city',
                'value' => $model->getCityName(),
                'format' => 'raw',
            ],
            [
                'attribute' => 'address',
                'value' => $model->address,
                'format' => 'raw',
            ],
            [
                'attribute' => 'zip_code',
                'value' => $model->zip_code,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'skype',
                'value' => $model->skype,
                'format' => 'raw',
            ],
            [
                'attribute' => 'vatzup',
                'value' => $model->vatzup,
                'format' => 'raw',
            ],
            [
                'attribute' => 'telegram',
                'value' => $model->telegram,
                'format' => 'raw',
            ],
            [
                'attribute' => 'facebook',
                'value' => $model->facebook,
                'format' => 'raw',
            ],
            [
                'attribute' => 'instagram',
                'value' => $model->instagram,
                'format' => 'raw',
            ],
            [
                'attribute' => 'twitter',
                'value' => $model->twitter,
                'format' => 'raw',
            ],
            [
                'attribute' => 'vk',
                'value' => $model->vk,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'marital_status',
                'value' => $model->getMaritalStatus(),
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_status',
                'value' => $model->getProgramStatus(),
                'format' => 'raw',
            ],

            [
                'attribute' => 'how_many_children',
                'value' => $model->getCountChildren(),
                'format' => 'raw',
            ],
            [
                'attribute' => 'previous',
                'value' => $model->getMaritalPrevious(),
                'format' => 'raw',
            ],
            'updated',
        ],
    ]) ?>

</div>
