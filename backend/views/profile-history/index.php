<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' История профиля';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profile-history-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать Profile History', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',
            'user_id',
            [
                'attribute' => 'first_name',
                'value' => $model->first_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'last_name',
                'value' => $model->last_name,
                'format' => 'raw',
            ],
            'updated',

            ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
            ],
    ]); ?>
</div>
