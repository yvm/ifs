<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Процессы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacyprocess-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'day',
                'value' => $model->day,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'content',
//                'value' => $model->content,
//                'format' => 'raw',
//            ],


                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
