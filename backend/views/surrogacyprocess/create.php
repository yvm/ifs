<?php

use yii\helpers\Html;

$this->title = 'Создания процесса';
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyprocesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogacyprocess-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
