<?php

use yii\helpers\Html;

$this->title = 'Редактирования процесса: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Surrogacyprocesses', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="surrogacyprocess-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
