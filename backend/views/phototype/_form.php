<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="phototype-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?php
    echo $form->field($model, 'image[]')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload' => false ,
        ] ,
        'options' => [
            'multiple' => true,
            'accept' => 'image/*',
        ],
    ]);
    ?>

    <?= $form->field($model, 'content')->textarea() ?>

    <?= $form->field($model, 'photo_id')->dropDownList(\common\models\Photo::getList()) ?>

    <div class="form-group">
        <?=Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
