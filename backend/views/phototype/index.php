<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Фото';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phototype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать ', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($data){
                    return Html::img($data->getImage(), ['width'=>100]);
                }
            ],
            ['attribute'=>'photo_id', 'value'=>function($model){ return $model->photoName;},'filter'=>\common\models\Photo::getList()],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
