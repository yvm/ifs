<?php

use yii\helpers\Html;

$this->title = 'Создание Phototype';
$this->params['breadcrumbs'][] = ['label' => 'Phototypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="phototype-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
