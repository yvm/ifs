<?php

use yii\helpers\Html;

$this->title = 'Создание Surrogateservice';
$this->params['breadcrumbs'][] = ['label' => 'Surrogateservices', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogateservice-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
