<?php

use yii\helpers\Html;

$this->title = 'Создание Donorstage';
$this->params['breadcrumbs'][] = ['label' => 'Donorstages', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donorstage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
