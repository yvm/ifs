<?php

use yii\helpers\Html;

$this->title = 'Редактирование Countrys: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Countrys', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id1]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="countrys-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
