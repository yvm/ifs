<?php

use yii\helpers\Html;

$this->title = 'Создание Desired Consult Location';
$this->params['breadcrumbs'][] = ['label' => 'Desired Consult Locations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="desired-consult-location-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
