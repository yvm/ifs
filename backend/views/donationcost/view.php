<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Donationcosts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcost-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],

            [
                'attribute' => 'price',
                'value' => $model->price,
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPriceGuest',
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceGuest);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPriceUser',
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceUser);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'buttonStart',
                'value' => $model->buttonStart,
                'format' => 'raw',
            ],
            [
                'attribute' => 'buttonGetCost',
                'value' => $model->buttonGetCost,
                'format' => 'raw',
            ],
            [
                'attribute' => 'content',
                'value' => $model->content,
                'format' => 'raw',
            ],

        ],
    ]) ?>

</div>
