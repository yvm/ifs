<?php

use backend\controllers\Label;
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Costs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcost-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'price',
                'value' => $model->price,
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPriceGuest',
                'filter' => Label::statusList(),
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceGuest);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'statusPriceUser',
                'filter' => Label::statusList(),
                'value' => function ($model) {
                    return Label::statusLabel($model->statusPriceUser);
                },
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'content',
//                'value' => $model->content,
//                'format' => 'raw',
//            ],
//            [
//                'attribute' => 'buttonStart',
//                'value' => $model->buttonStart,
//                'format' => 'raw',
//            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
