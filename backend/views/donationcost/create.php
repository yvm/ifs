<?php

use yii\helpers\Html;

$this->title = 'Создания';
$this->params['breadcrumbs'][] = ['label' => 'Donationcosts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcost-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
