<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Ifstrip */

$this->title = 'Создание контента';
$this->params['breadcrumbs'][] = ['label' => 'Ifstrips', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ifstrip-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
