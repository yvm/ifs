<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Donationcostcon */

$this->title = 'Create Donationcostcon';
$this->params['breadcrumbs'][] = ['label' => 'Donationcostcons', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcostcon-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
