<?php

use yii\helpers\Html;

$this->title = 'Создание преимущества';
$this->params['breadcrumbs'][] = ['label' => 'Surrogatebenefts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="surrogatebeneft-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
