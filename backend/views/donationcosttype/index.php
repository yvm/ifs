<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = ' Виды';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="donationcosttype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name',
                'value' => $model->name,
                'format' => 'raw',
            ],
//            [
//                'attribute' => 'content',
//                'value' => $model->content,
//                'format' => 'raw',
//            ],

            ['attribute'=>'donationcost_id', 'value'=>function($model){ return $model->donationCostName;},'filter'=>\common\models\Donationcost::getList()],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
