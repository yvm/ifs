<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="program-form">
    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Основное</a>
            </li>
            <?if(!$model->isNewRecord){?>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#stage" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Шаги</a>
            </li>
            <?}?>
            <li class="nav-item">
                <a id="profile-tab" data-toggle="tab" href="#new" role="tab" aria-controls="profile" aria-selected="false" class="nav-link">Новый шаг</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <?if(!$model->isNewRecord){?>
            <div id="stage" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">
                <?foreach($ProgramStages as $k => $v){?>
                    <?php $form = ActiveForm::begin(); ?>
                        <?= $form->field($v, 'id')->input('hidden')->label(false) ?>
                        <?= $form->field($v, 'stage_number')->dropDownList(['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5',
                            '6' => '6', '7' => '7', '8' => '8', '91' => '9', '10' => '10']) ?>

                        <?= $form->field($v, 'start_date')->input('date') ?>

                        <?= $form->field($v, 'end_date')->input('date') ?>

                        <?= $form->field($v, 'stage_name')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($v, 'stage_description')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($v, 'completed_work')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($v, 'user_actions')->textInput(['maxlength' => true]) ?>

                        <div class="row">
                            <div class="" style="text-align: center">
                                <?$images = unserialize($v->images);?>
                                <div style="margin-top:20px;">
                                    <?if($images) foreach($images as $v1){?>
                                        <img src="/backend/web/<?=$v->path.$v1?>" alt="" style="margin-left:15px;width:400px;height:400px;float:left;">
                                    <?}?>
                                </div>
                            </div>
                        </div>

                        <?= $form->field($v, 'files'.$k.'[]')->label(false)->widget(FileInput::className(), [
                            'options' => [
                                'accept' => 'image/*',
                                'multiple' => true,
                            ]
                        ]) ?>

                        <?= $form->field($v, 'notes')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($v, 'next_scheduled_stage')->textInput(['maxlength' => true]) ?>

                        <?= $form->field($v, 'scheduled_start')->input('date') ?>

                        <?= $form->field($v, 'scheduled_end')->input('date') ?>

                        <?= $form->field($v, 'scheduled_user')->textInput(['maxlength' => true]) ?>

                        <div class="form-group">
                            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                        </div>
                    <?php ActiveForm::end(); ?>
                <?}?>
            </div>
            <?}?>
            <div id="new" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade">
                    <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($ProgramStagesNew, 'stage_number')->dropDownList(['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5',
                        '6' => '6', '7' => '7', '8' => '8', '91' => '9', '10' => '10']) ?>

                    <?= $form->field($ProgramStagesNew, 'start_date')->input('date') ?>

                    <?= $form->field($ProgramStagesNew, 'end_date')->input('date') ?>

                    <?= $form->field($ProgramStagesNew, 'stage_name')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($ProgramStagesNew, 'stage_description')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($ProgramStagesNew, 'completed_work')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($ProgramStagesNew, 'user_actions')->textInput(['maxlength' => true]) ?>

                    <div class="row">
                        <div class="" style="text-align: center">
                            <?$images = unserialize($ProgramStagesNew->images)?>
                            <div style="margin-top:20px;">
                                <?if($images)foreach($images as $v){?>
                                    <img src="/backend/web/<?=$v->path.$v?>" alt="" style="margin-left:15px;width:400px;height:400px;float:left;">
                                <?}?>
                            </div>
                        </div>
                    </div>

                    <?= $form->field($ProgramStagesNew, 'files[]')->label(false)->widget(FileInput::className(), [
                    'options' => [
                        'accept' => 'image/*',
                        'multiple' => true,
                    ]
                    ]) ?>

                    <?= $form->field($ProgramStagesNew, 'notes')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($ProgramStagesNew, 'next_scheduled_stage')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($ProgramStagesNew, 'scheduled_start')->input('date') ?>

                    <?= $form->field($ProgramStagesNew, 'scheduled_end')->input('date') ?>

                    <?= $form->field($ProgramStagesNew, 'scheduled_user')->textInput(['maxlength' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
            </div>
            <div id="home" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade show active in">
                <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, 'user_id')->hiddenInput()->label(false) ?>

                <?
                    if($user->role == 1 || $user->role == 5)
                        $array = [2 => 'Surrogacy', 3 => 'Egg Donation', 5 => 'Biotransportation'];
                    else
                        $array = [2 => 'Surrogacy', 3 => 'Egg Donation'];
                //        if($model->program_type != 'Surrogacy' && $model->program_type != 'Egg Donation' && $model->program_type != 'Biotransportation'){
                //            $array = [$model->program_type => $model->program_type, 'Surrogacy' => 'Surrogacy', 'Egg Donation' => 'Egg Donation',
                //                'Biotransportation' => 'Biotransportation'];
                //        }
                if($model->program_type === null)
                    $model->program_type = $user->role;
                ?>

                <?= $form->field($model, 'program_type')->checkboxList($array) ?>

                <!--    --><?//= $form->field($model, 'program_type2')->textInput(); ?>

                <?
                $array = ['Economy' => 'Economy', 'Essential' => 'Essential', 'VIP' => 'VIP',
                    'Special' => 'Special', 'Only Egg Donation' => 'Only Egg Donation', 'Egg Donation & Transportation' => 'Egg Donation & Transportation',
                    'Egg Donation & IVF' => 'Egg Donation & IVF', 'Within Kazakhstan' => 'Within Kazakhstan', 'To/From Europe' => 'To/From Europe',
                    'To/From Asia' => 'To/From Asia', 'To/From America' => 'To/From America'];
                if(!in_array($model->program_name, $array) && $model->program_name !== null){
                    $array = [$model->program_name => $model->program_name, 'Economy' => 'Economy', 'Essential' => 'Essential', 'VIP' => 'VIP',
                        'Special' => 'Special', 'Only Egg Donation' => 'Only Egg Donation', 'Egg Donation & Transportation' => 'Egg Donation & Transportation',
                        'Egg Donation & IVF' => 'Egg Donation & IVF', 'Within Kazakhstan' => 'Within Kazakhstan', 'To/From Europe' => 'To/From Europe',
                        'To/From Asia' => 'To/From Asia', 'To/From America' => 'To/From America'];
                }
                ?>

                <?= $form->field($model, 'program_name')->dropDownList($array) ?>

                <?= $form->field($model, 'program_name2')->textInput() ?>

                <?= $form->field($model, 'program_status')->dropDownList(['Active' => 'Active', 'Suspended' => 'Suspended', 'Finished' => 'Finished']) ?>

                <?= $form->field($model, 'beginning_program')->input('date') ?>

                <?= $form->field($model, 'completion_program')->input('date') ?>

                <?
                if($model->program_country === null)
                    $model->program_country = $user->personal_info->country;
                ?>

                <?= $form->field($model, 'program_country')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\Countrys::find()->orderBy('sort')->all(), 'id', 'name')) ?>

                <?
                $array = ['Almaty' => 'Almaty', 'Astana' => 'Astana', 'Bishkek' => 'Bishkek',
                    'Kiev' => 'Kiev', 'Minsk' => 'Minsk', 'Moscow' => 'Moscow', 'Tashkent' => 'Tashkent', 'Tbilisi' => 'Tbilisi'];
                if(!in_array($model->program_city, $array)){
                    $array = [$model->program_city => $model->program_city, 'Almaty' => 'Almaty', 'Astana' => 'Astana', 'Bishkek' => 'Bishkek',
                        'Kiev' => 'Kiev', 'Minsk' => 'Minsk', 'Moscow' => 'Moscow', 'Tashkent' => 'Tashkent', 'Tbilisi' => 'Tbilisi'];
                }
                ?>

                <?
                if($model->program_city === null){
                    $model->program_city = $user->personal_info->city;

                    $region = \common\models\City::findOne($user->personal_info->city);
                    ?>

                    <?= $form->field($model, 'program_city')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\City::findAll(['region_id' => $region->region_id]), 'id', 'name')) ?>
                <?}
                else{
                    $region = \common\models\City::findOne($model->program_city);
                    ?>
                    <?= $form->field($model, 'program_city')->dropDownList(\yii\helpers\ArrayHelper::map(\common\models\City::findAll(['region_id' => $region->region_id]), 'id', 'name')) ?>
                <?}?>
                <?= $form->field($model, 'program_city2')->textInput() ?>

                <?= $form->field($model, 'program_cost')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'program_cost_valuta')->dropDownList(['€' => '€', '$' => '$', 'Тенге' => 'Тенге']) ?>

                <?
                $array = \yii\helpers\ArrayHelper::map(\common\models\ProgramManager::find()->all(), 'name', 'name');
                if(!in_array($model->program_manager, $array)){
                    $array[$model->program_manager] = $model->program_manager;
                }
                ?>

                <?= $form->field($model, 'program_manager')->dropDownList($array) ?>

                <?= $form->field($model, 'program_manager2')->textInput() ?>

                <?
                $array = \yii\helpers\ArrayHelper::map(\common\models\IvfClinic::find()->all(), 'name', 'name');
                if(!in_array($model->ivf_clinic, $array)){
                    $array[$model->ivf_clinic] = $model->ivf_clinic;
                }
                ?>

                <?= $form->field($model, 'ivf_clinic')->dropDownList($array) ?>

                <?= $form->field($model, 'ivf_clinic2')->textInput() ?>

                <?
                $array = \yii\helpers\ArrayHelper::map(\common\models\Physician::find()->all(), 'name', 'name');
                if(!in_array($model->physician, $array)){
                    $array[$model->physician] = $model->physician;
                }
                ?>

                <?= $form->field($model, 'physician')->dropDownList($array) ?>

                <?= $form->field($model, 'physician2')->textInput() ?>

                <?$array_surrogate = \common\models\User::find()->where('type = 1 OR type = 3')->all();?>

                <?= $form->field($model, 'surrogate')->dropDownList(\yii\helpers\ArrayHelper::map($array_surrogate, 'id', 'profiles')) ?>

                <?$array_surrogate = \common\models\User::find()->where('type = 2 OR type = 3')->all();?>

                <?= $form->field($model, 'egg_donor')->dropDownList(\yii\helpers\ArrayHelper::map($array_surrogate, 'id', 'profiles')) ?>

                <?= $form->field($model, 'program_notes')->textInput(['maxlength' => true]) ?>

                <div class="form-group">
                    <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>
