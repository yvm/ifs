<?php

use yii\helpers\Html;

$this->title = 'Создание Program';
$this->params['breadcrumbs'][] = ['label' => 'Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'ProgramStagesNew' => $ProgramStagesNew,
        'user'  => $user
    ]) ?>

</div>
