<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ProgramSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="program-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'name_program') ?>

    <?= $form->field($model, 'program_type') ?>

    <?= $form->field($model, 'program_name') ?>

    <?php // echo $form->field($model, 'program_status') ?>

    <?php // echo $form->field($model, 'beginning_program') ?>

    <?php // echo $form->field($model, 'completion_program') ?>

    <?php // echo $form->field($model, 'program_country') ?>

    <?php // echo $form->field($model, 'program_city') ?>

    <?php // echo $form->field($model, 'program_cost') ?>

    <?php // echo $form->field($model, 'program_manager') ?>

    <?php // echo $form->field($model, 'ivf_clinic') ?>

    <?php // echo $form->field($model, 'physician') ?>

    <?php // echo $form->field($model, 'surrogate') ?>

    <?php // echo $form->field($model, 'egg_donor') ?>

    <?php // echo $form->field($model, 'program_notes') ?>

    <?php // echo $form->field($model, 'stage_number') ?>

    <?php // echo $form->field($model, 'start_and_end_date') ?>

    <?php // echo $form->field($model, 'stage_name') ?>

    <?php // echo $form->field($model, 'stage_description') ?>

    <?php // echo $form->field($model, 'completed_work') ?>

    <?php // echo $form->field($model, 'user_actions') ?>

    <?php // echo $form->field($model, 'images') ?>

    <?php // echo $form->field($model, 'notes') ?>

    <?php // echo $form->field($model, 'next_scheduled_stage') ?>

    <?php // echo $form->field($model, 'scheduled_start') ?>

    <?php // echo $form->field($model, 'scheduled_user') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
