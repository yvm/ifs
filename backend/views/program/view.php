<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update-program', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'user_id',
            [
                'attribute' => 'program_type',
                'value' => $model->program_type,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_name',
                'value' => $model->program_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_status',
                'value' => $model->program_status,
                'format' => 'raw',
            ],
            [
                'attribute' => 'beginning_program',
                'value' => $model->beginning_program,
                'format' => 'raw',
            ],
            [
                'attribute' => 'completion_program',
                'value' => $model->completion_program,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_country',
                'value' => $model->program_country,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_city',
                'value' => $model->program_city,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_cost',
                'value' => $model->program_cost,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_manager',
                'value' => $model->program_manager,
                'format' => 'raw',
            ],
            [
                'attribute' => 'ivf_clinic',
                'value' => $model->ivf_clinic,
                'format' => 'raw',
            ],
            [
                'attribute' => 'physician',
                'value' => $model->physician,
                'format' => 'raw',
            ],
            [
                'attribute' => 'surrogate',
                'value' => $model->surrogate,
                'format' => 'raw',
            ],
            [
                'attribute' => 'egg_donor',
                'value' => $model->egg_donor,
                'format' => 'raw',
            ],
            [
                'attribute' => 'program_notes',
                'value' => $model->program_notes,
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
