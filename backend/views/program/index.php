<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Programs';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="program-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать Program', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            [
//                'attribute' => 'id',
//                'value' => $model->profile->first_name.' '.$model->profile->last_name,
//                'format' => 'raw',
//            ],
                [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{update}'
                ],
            ],
    ]); ?>
</div>
