<?php

use yii\helpers\Html;

if($type == 1)
    $this->title = 'Редактирование Program: ' . $model->profile->first_name.' '.$model->profile->last_name;
else
    $this->title = 'Редактирование Program: ' . $model->user->profile->first_name.' '.$model->user->profile->last_name;
$this->params['breadcrumbs'][] = ['label' => 'Programs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="program-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form'.$type, [
        'model' => $model,
        'ProgramStages' => $ProgramStages,
        'ProgramStagesNew' => $ProgramStagesNew,
        'id'    => $id
    ]) ?>
</div>
