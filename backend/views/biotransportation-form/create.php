<?php

use yii\helpers\Html;

$this->title = 'Создание Biotransportation Form';
$this->params['breadcrumbs'][] = ['label' => 'Biotransportation Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biotransportation-form-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
