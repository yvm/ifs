<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\BiotransportationForm */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="biotransportation-form-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'name_pick_up') ?>

    <?= $form->field($model, 'date') ?>

    <?= $form->field($model, 'contact_person') ?>

    <?= $form->field($model, 'phone') ?>

    <?php // echo $form->field($model, 'adress') ?>

    <?php // echo $form->field($model, 'country') ?>

    <?php // echo $form->field($model, 'state') ?>

    <?php // echo $form->field($model, 'city') ?>

    <?php // echo $form->field($model, 'name_deliver') ?>

    <?php // echo $form->field($model, 'contact_deliver') ?>

    <?php // echo $form->field($model, 'phone_deliver') ?>

    <?php // echo $form->field($model, 'adress_deliver') ?>

    <?php // echo $form->field($model, 'country_deliver') ?>

    <?php // echo $form->field($model, 'state_deliver') ?>

    <?php // echo $form->field($model, 'city_deliver') ?>

    <?php // echo $form->field($model, 'first_name') ?>

    <?php // echo $form->field($model, 'last_name') ?>

    <?php // echo $form->field($model, 'mobile_phone') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'country_transportation') ?>

    <?php // echo $form->field($model, 'state_transportation') ?>

    <?php // echo $form->field($model, 'city_transportation') ?>

    <?php // echo $form->field($model, 'adress_transportation') ?>

    <?php // echo $form->field($model, 'biomaterial_type') ?>

    <?php // echo $form->field($model, 'tube_type') ?>

    <?php // echo $form->field($model, 'tube_number') ?>

    <?php // echo $form->field($model, 'biomaterial_infected') ?>

    <?php // echo $form->field($model, 'additional_information') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
