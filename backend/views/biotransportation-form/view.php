<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Biotransportation Forms', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biotransportation-form-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'name_pick_up',
                'value' => $model->name_pick_up,
                'format' => 'raw',
            ],
            [
                'attribute' => 'date',
                'value' => $model->date,
                'format' => 'raw',
            ],
            [
                'attribute' => 'contact_person',
                'value' => $model->contact_person,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'adress',
                'value' => $model->adress,
                'format' => 'raw',
            ],
            'country',
            'state',
            'city',
            [
                'attribute' => 'name_deliver',
                'value' => $model->name_deliver,
                'format' => 'raw',
            ],
            [
                'attribute' => 'contact_deliver',
                'value' => $model->contact_deliver,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone_deliver',
                'value' => $model->phone_deliver,
                'format' => 'raw',
            ],
            [
                'attribute' => 'adress_deliver',
                'value' => $model->adress_deliver,
                'format' => 'raw',
            ],
            'country_deliver',
            'state_deliver',
            'city_deliver',
            [
                'attribute' => 'first_name',
                'value' => $model->first_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'last_name',
                'value' => $model->last_name,
                'format' => 'raw',
            ],
            [
                'attribute' => 'mobile_phone',
                'value' => $model->mobile_phone,
                'format' => 'raw',
            ],
            [
                'attribute' => 'email',
                'value' => $model->email,
                'format' => 'raw',
            ],
            [
                'attribute' => 'country_transportation',
                'value' => $model->country_transportation,
                'format' => 'raw',
            ],
            [
                'attribute' => 'state_transportation',
                'value' => $model->state_transportation,
                'format' => 'raw',
            ],
            [
                'attribute' => 'city_transportation',
                'value' => $model->city_transportation,
                'format' => 'raw',
            ],
            [
                'attribute' => 'adress_transportation',
                'value' => $model->adress_transportation,
                'format' => 'raw',
            ],
            [
                'attribute' => 'biomaterial_type',
                'value' => $model->biomaterial_type,
                'format' => 'raw',
            ],
            [
                'attribute' => 'tube_type',
                'value' => $model->tube_type,
                'format' => 'raw',
            ],
            'tube_number',
            'biomaterial_infected',
            [
                'attribute' => 'additional_information',
                'value' => $model->additional_information,
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
