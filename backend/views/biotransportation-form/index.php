<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Biotransportation Forms';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="biotransportation-form-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <p>
        <?= Html::a('Создать Biotransportation Form', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'name_pick_up',
                'value' => $model->name_pick_up,
                'format' => 'raw',
            ],
            [
                'attribute' => 'date',
                'value' => $model->date,
                'format' => 'raw',
            ],
            [
                'attribute' => 'contact_person',
                'value' => $model->contact_person,
                'format' => 'raw',
            ],
            [
                'attribute' => 'phone',
                'value' => $model->phone,
                'format' => 'raw',
            ],

                ['class' => 'yii\grid\ActionColumn'],
            ],
    ]); ?>
</div>
