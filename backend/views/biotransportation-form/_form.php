<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="biotransportation-form-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'name_pick_up')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country')->textInput() ?>

    <?= $form->field($model, 'state')->textInput() ?>

    <?= $form->field($model, 'city')->textInput() ?>

    <?= $form->field($model, 'name_deliver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_deliver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_deliver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress_deliver')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_deliver')->textInput() ?>

    <?= $form->field($model, 'state_deliver')->textInput() ?>

    <?= $form->field($model, 'city_deliver')->textInput() ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'mobile_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'country_transportation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'state_transportation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'city_transportation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'adress_transportation')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'biomaterial_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tube_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'tube_number')->textInput() ?>

    <?= $form->field($model, 'biomaterial_infected')->textInput() ?>

    <?= $form->field($model, 'additional_information')->widget(CKEditor::className(), [
        'editorOptions' => [
            'options' => ['rows' => 6],
            'allowedContent' => true,
            'preset' => 'full',
            'inline' => false,
        ],
    ]) ?>

    <?= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
    </div>
