<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;

?>
<div class="admission-form">
    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'visibility')->dropDownList([1=>'Активно', 0=>'Закрыто']) ?>

    <?= $form->field($model, 'compensation')->dropDownList([1=>'Активно', 0=>'Закрыто']) ?>

    <?= $form->field($model, 'photo')->dropDownList([1=>'Активно', 0=>'Закрыто']) ?>

    <?= $form->field($model, 'video')->dropDownList([1=>'Активно', 0=>'Закрыто']) ?>

    <?= $form->field($model, 'role')->dropDownList(\common\models\Role::getList(), ['disabled' => 'disabled']) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
