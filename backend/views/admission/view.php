<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

$this->title = 'Доступы';
$this->params['breadcrumbs'][] = ['label' => 'Admissions', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
<!--        --><?//= Html::a('Удалить', ['delete', 'id' => $model->id], [
//            'class' => 'btn btn-danger',
//            'data' => [
//                'confirm' => 'Вы уверены, что хотите удалить этот элемент?',
//                'method' => 'post',
//            ],
//        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
//            'id',
            [
                'attribute' => 'visibility',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->visibility);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'compensation',
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->compensation);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'photo',
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->photo);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'video',
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->video);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
            ],
        ],
    ]) ?>

</div>
