<?php
use yii\helpers\Html;
use yii\grid\GridView;

$this->title = 'Доступы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admission-index">

    <h1><?= Html::encode($this->title) ?></h1>
<!--    <p>-->
<!--        --><?//= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

//            'id',

            [
                'attribute' => 'visibility',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->visibility);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'compensation',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->compensation);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'photo',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->photo);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'video',
                'filter' => \backend\controllers\Label::statusList(),
                'value' => function ($model) {
                    return \backend\controllers\Label::statusLabel($model->video);
                },
                'format' => 'raw',
            ],

            [
                'attribute' => 'role',
                'value' => function($model){ return $model->roles->name;},
                'filter' => \common\models\Role::getList(),
            ],

                ['class' => 'yii\grid\ActionColumn','template'=>'{update} {view}'],
            ],
    ]); ?>
</div>
