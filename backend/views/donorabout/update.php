<?php

use yii\helpers\Html;

$this->title = 'Редактирование';
$this->params['breadcrumbs'][] = ['label' => 'О донорстве яйцеклеток', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="donorabout-update">
    <h1><?= Html::encode($this->title) ?></h1>
    <?= $this->render('_form', [
    'model' => $model, 'new_model' => $new_model, 'id' => $id,
    ]) ?>
</div>
