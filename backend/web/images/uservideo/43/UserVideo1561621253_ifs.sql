-- phpMyAdmin SQL Dump
-- version 4.7.3
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 27 2019 г., 07:24
-- Версия сервера: 5.6.37
-- Версия PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `ifs`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admission`
--

CREATE TABLE `admission` (
  `id` int(11) NOT NULL,
  `visibility` int(11) NOT NULL,
  `compensation` int(11) NOT NULL,
  `video` int(11) NOT NULL,
  `photo` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admission`
--

INSERT INTO `admission` (`id`, `visibility`, `compensation`, `video`, `photo`, `role`) VALUES
(1, 0, 1, 1, 0, 1),
(2, 0, 1, 0, 1, 2),
(3, 0, 1, 1, 1, 3);

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `status_profile` int(11) NOT NULL,
  `active` int(11) DEFAULT NULL,
  `status_edit_profile` int(11) DEFAULT NULL,
  `status_comp` int(11) NOT NULL,
  `profi` int(11) DEFAULT NULL,
  `proven` int(11) DEFAULT NULL,
  `role` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `step` int(11) DEFAULT '0',
  `got_it` int(11) DEFAULT NULL,
  `providers_step1_id` int(11) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `access_token`, `email`, `status`, `status_profile`, `active`, `status_edit_profile`, `status_comp`, `profi`, `proven`, `role`, `type`, `step`, `got_it`, `providers_step1_id`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'C9R-Y7LuGogb4gwkpt1uIcESqWKkGaQS', '$2y$13$U/ftGJM7w7yyekQ3neFwN.fMEsMxEGQPNZ8OSkvdHgwk0OBfuLwgK', NULL, NULL, 'admin', 1, 1, NULL, 1, 0, NULL, NULL, 20, 0, 20, 1, NULL, 1554438274, 1561002932),
(43, 'nurzat.kaz@gmail.com', 'gVT2yjUqxhegPqM8YXhJq_cqA1o6I6sW', '$2y$13$pXoVlQhwzsvGB78Xb1OZ0OLBti7OdWbvvcWsVY1FMEk8wuMsW.H6u', NULL, 'rrXoujW2130-BDSSZMVbOKnhAFzgMuHC_1558609856', 'nurzat.kaz@gmail.com', 1, 0, 1, 1, 0, 1, 1, 1, 3, 20, 0, NULL, 1557331604, 1561544746),
(44, 'it_yvm@mail.ru33', 'v0e6A0H7QMCovr1_4PLvk843Kybgxc2R', '$2y$13$CodnEShAVTi0uUAjXDQ73.TDQV.PSKPEsBbPaoofx8nDGBHxvaTL.', NULL, NULL, 'it_yvm@mail.ru33', 0, 1, 1, 1, 0, NULL, NULL, 2, NULL, 20, NULL, NULL, 1557718506, 1557719921),
(49, 'a_okas97@mail.ru2', 'CVT31VgSkRRJ5QpPnGdFi--INPSsL9KR', '$2y$13$cQJ8q7fGdpZMJELXBIMg1ezpjm9A4QPnj/v/3t/02dgigeHiFC9uW', NULL, NULL, 'a_okas97@mail.ru2', 0, 1, 1, 1, 0, NULL, NULL, 2, NULL, 20, NULL, NULL, 1557805903, 1557808655),
(51, 'a_okas97@mail.ru3', '7kv_H5-ck0ngmcf6Yee9vts892gdAbJZ', '$2y$13$buDpkZnn4.EmymZyC4BuYuU5p0m2fAGmc/nQj..9m2a5vhlyTaAQm', NULL, NULL, 'a_okas97@mail.ru3', 1, 1, 1, 1, 0, NULL, NULL, 1, 3, 20, NULL, NULL, 1557809940, 1557810128),
(53, 'a_okas97@mail.ru444', 'Pdsi-oIaGwtItFyV30hkTHeutTtKjJ1s', '$2y$13$ruzgLA2D1edBWhU.VQoNO.q/EYPad6jlLXT7YrkHRTUFjgKo8hVyS', NULL, NULL, 'a_okas97@mail.ru444', 1, 1, 1, 1, 0, NULL, NULL, 1, 2, 20, NULL, NULL, 1557813103, 1557817588),
(57, 'a_okas97@mail.ruaa', '3qQRXL80KAQA4_xLLpq3D5icOzWlOBua', '$2y$13$T1.gVjMjgcVNAE5ZmFXZu.llKKYq8BAXvr48f1CZ1mELhUHYSODta', NULL, NULL, 'a_okas97@mail.ruaa', 1, 1, 1, 1, 0, NULL, NULL, 3, NULL, 20, NULL, NULL, 1557822691, 1557823385),
(58, 'it_yvm@mail.ru22', 'y5Onj2PqDWc7MrRxNYWpW-ekO0CxUjGZ', '$2y$13$1P7QPyl4NKAu/nRXQD5ckO3kHl84xp/tCvKdSgX8Pj8ZkEoHEh5Qu', NULL, NULL, 'it_yvm@mail.ru22', 1, 1, 1, 1, 1, NULL, NULL, 4, NULL, 20, NULL, NULL, 1557822705, 1560240270),
(59, 'a_okas97@mail.ru111', 'xygJYCFDGu6woIXpTKDeSEFdt_FBzYIs', '$2y$13$DvGJ6yo0x7WAJ1VVy9UrV.zTjmgLVsiuxM4GeL5WcPQTLHfO6HCy.', NULL, NULL, 'a_okas97@mail.ru111', 1, 1, 1, 1, 0, 1, 1, 1, 1, 20, NULL, NULL, 1557824193, 1558000134),
(62, 'Info@a-lux.kz', 'dmhPtN8ht_BWr_4jJuMjiQxkdQT_-00C', '$2y$13$pxeVJNPrAiAv9doPeFcy3OvJuYR2AD5P/VIrSFJYh/QvUkakYlF3C', NULL, 'ObIz-gInHdPHNBSFkCeSASoZhlfLqWAq_1557904135', 'Info@a-lux.kz', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 0, NULL, NULL, 1557904134, 1557904134),
(63, 'it_yvm@mail.ru1', '1wT09sdlDw5SIoK4vXCnxTVjZ6oq20Bf', '$2y$13$KIuNw3NRj2Uo44XO8GY6s.n9615Xhj5p6EOIktAyyGGNV19dH6Ul6', NULL, 'PyEnF5UHHkNmu3hYcZ1-F08MOURcqxeb_1558003870', 'it_yvm@mail.ru1', 0, 1, NULL, 1, 0, NULL, NULL, 1, 1, 0, NULL, NULL, 1558003869, 1558003869),
(64, 'it_yvm@mail.ruff', 'Zm2_9Hl5o3z9VIHqT56qMkPVisJg9bRh', '$2y$13$vtHTf13Mu6sM8zB8UvPpsOb92/RmhzRrWdKYuB5QxqDwxXlJ6UuSe', NULL, NULL, 'it_yvm@mail.ruff', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 3, NULL, NULL, 1558071242, 1558071454),
(66, 'ps_v@bk.ru11', '7K1fWPLHtzlOk8hgn-DT3w21UAPxC6Dg', '$2y$13$rcICjbd4pgpw7VRHTTHLZ.kAT3NTHezgYLWcj2f.LDQ8ztIM/rc82', NULL, NULL, 'ps_v@bk.ru111', 1, 1, 1, 1, 0, NULL, NULL, 4, NULL, 20, NULL, NULL, 1558073132, 1558609940),
(67, 'a_okas97@mail.ru555', 'M7zw1RUepB1VF2NqDVTq57fvZk1tXAjp', '$2y$13$HeAAvxScCNlo/YRE3fLa5uEeADPV4h2Mu7x0NTBF9Q1FBHQ8OK4r.', NULL, NULL, 'a_okas97@mail.ru555', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 1, NULL, NULL, 1558502543, 1558502591),
(68, 'a_okas97@mail.ru1112', 's1dQj1NSlDGUFmqYK431-FQwhGW0Bmnk', '$2y$13$Ut6CEblQCCCUW72dDx5QjO0Veck5nWe.L5MzTTUlbQxY.ZF6H1c7i', NULL, NULL, 'a_okas97@mail.ru1112', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 3, NULL, NULL, 1558509291, 1558514797),
(76, 'bauka.abdi@gmail.com', 'Sfc6O9hbHi0GGY0TpAhCNhvicF3T2I8T', '$2y$13$FnahMhXRbBeUGcDj7KBp4u6TlR6zwbHkDYHO0iKB4yoMh2R/txV8y', NULL, NULL, 'bauka.abdi@gmail.com', 0, 1, NULL, 1, 0, NULL, NULL, 3, NULL, 8, NULL, NULL, 1558670619, 1558697772),
(77, '150104007@stu.sdu.edu.kz', '3pb3MXuQVanVQPU_GoSm2fNDdTs0EHy9', '$2y$13$QA2UVVWLIhEIvxtaQg591eO//JVlqntzyGU2z.9dQrtUQxORP7ATq', NULL, NULL, '150104007@stu.sdu.edu.kz', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 4, NULL, NULL, 1558692876, 1558694511),
(85, 'it_yvm@mail.rudddd', 'jBNh0ht0ATomW5VmY8BbjjD0fUeez7t1', '$2y$13$p0UbJMl39yE5hSjen7yzSuEt2a0F4CDKUmV71vaW1HhYJPwEbIUAS', NULL, NULL, 'it_yvm@mail.rudddd', 1, 1, NULL, 1, 0, NULL, NULL, 3, NULL, 2, NULL, NULL, 1559300032, 1559300361),
(86, 'baurzhan0502@gmail.com', 'KvKlBT94AkDXebX7qKpaOijTi0fc9J3m', '$2y$13$Wv1zq3SQVyylc.Il/qSfeu/lCShnuRAPVet4GBBkYbzhmWGsK9Pge', NULL, NULL, 'baurzhan0502@gmail.com', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 10, NULL, NULL, 1559300352, 1560153392),
(87, 'is345english@mail.com', 'qMk_eoLTjFU9BrdeYysJykM_wvf92HME', '$2y$13$BdTNiqJNKL1NQ8j5uemLEO4oM94p0iOPlfYHA/Ez0Pynq5lrj.dOS', NULL, 'PJm-7F0UN45fjZsc2omQuOtD1VbczYZw_1559301384', 'is345english@mail.com', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 0, NULL, NULL, 1559301383, 1559301383),
(89, 'is345english@gmail.com', '6w8Uau5IFRe4O-AgFiFsaZ-l52UK3Uvx', '$2y$13$cxayPIIgtX09O3hEFtK/T.jZoE7ONZBe.E990YAdNWWFsge9alGqq', NULL, NULL, 'is345english@gmail.com', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 20, 0, NULL, 1559301601, 1560847000),
(90, 'manager@a-lux.kz', '2yyxLmLSgdfKpaX4hkT83xBwilKu0ngM', '$2y$13$C/M1caxorfwg0Bo7Esah9uRt28Ig4zTKNFLsKpGrbohUwjSYj5F5u', NULL, NULL, 'manager@a-lux.kz', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 3, NULL, NULL, 1559302616, 1559304184),
(91, 'primakaz@yandex.ru', 'mFAHtCTeF8fwQIiBUcwGWaRy3tBkmhpp', '$2y$13$fi2PQqQYf7evv2./qbRZo.S.TlfTUCHbsaTTRwauFXHm1QhWtO13G', NULL, NULL, 'primakaz@yandex.ru', 1, 1, NULL, 1, 0, NULL, NULL, 1, 2, 1, NULL, NULL, 1559392699, 1559392756),
(93, 'is1708gr@gmail.com', '957-iGTUdcNO9xoisjMKeBtjdRzQKy1P', '$2y$13$FXTuRox5gZyQhMjdpmbQgeIQe96TIAIedL2mQs7Dv8.DK2xQcqm16', NULL, NULL, 'is1708gr@gmail.com', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 6, NULL, NULL, 1559537011, 1559541032),
(95, 'dauren.maikenov@mail.ru', 'e4-ptD0buqOZHCW01kui2vkkbQ-Ti8A6', '$2y$13$HFbcyKKh1mJsQDsFwJef6O1SYn8rmphKT7iKg1tcKcueeLAKgM6De', NULL, NULL, 'dauren.maikenov@mail.ru', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 4, NULL, NULL, 1559542338, 1559543203),
(106, 'polina3b7@gmail.com', 'IQLId66q0aVkR0O2GhFUUNtEBv975xq9', '$2y$13$AzaYBE69VGTEKkgqqnOMPuRbwSsXeKRt01XmJG89YWprV981nw39K', NULL, 'Zx0DHYrkIyoPDZ2loLOzGlZHJFDK_pMq_1559642348', 'polina3b7@gmail.com', 0, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 0, NULL, NULL, 1559642347, 1559642347),
(127, 'info@zoomapps.kz', 'u1DzXULGrkkkjte9dDTEoTkaqhVmw4fB', '$2y$13$U0ugqH0EyIsB87u6ymrBnuhflNI1H.pxGDwG/WbENr2pQb6uWhiFy', NULL, 'h-8-PLfXOhNtD7xO-7JWd2akN7j74gIH_1559815230', 'info@zoomapps.kz', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 3, NULL, NULL, 1559815229, 1559819003),
(129, 'mirkormanbek@gmail.com', 'iviVQNN9UfkiYKHjOkErAWlx7K28Rm7v', '$2y$13$fO/hK9kyiFPuKGSoBs.j8O0/gnY.XeUfPfwX2wk..p0bp2GujAFSe', NULL, 'ed4RKHCI1WZ9bSEqfk8rUVFfAyku_9f9_1559818953', 'mirkormanbek@gmail.com', 1, 1, NULL, 1, 0, NULL, NULL, 3, NULL, 10, NULL, NULL, 1559818953, 1559820533),
(190, 'ifs.team@mail.ru', '0f52BsyM378Dup0h2vrz3y3HrCZHoWVo', '$2y$13$6G/Gou8FoVTjBAdy7yrMOeTSLd2bpu1IwftSHoP1VWhXqvHzv4.PC', NULL, 'ybMsC4t3ou7EjRJEOI82TW8pOGhlgA5z_1560167638', 'ifs.team@mail.ru', 1, 1, NULL, 1, 0, NULL, NULL, 2, NULL, 20, NULL, NULL, 1560167637, 1560235434),
(202, 'triplegg@mail.ru', 'qHXPWKimv28vBEdutmwtvjYV_QOFn9hW', '$2y$13$MWGn3sSgU5AEmEgB5ChRLOxhmbpOdknDAeInURaip.x.HnHK0npFy', NULL, 'sjJ160xfAMit9GEVcHktpS8xYDm8PhqX_1560239035', 'triplegg@mail.ru', 1, 1, NULL, 1, 0, NULL, NULL, 4, NULL, 20, NULL, NULL, 1560239034, 1560240114),
(212, 'ki_ba77@mail.ru', '6i-9l2L0riNMXpFZc5efxScMgolZ9_IV', '$2y$13$JekvblOMWOAuonnnZd.tFOIAsr8o2VCS.KPml.IHdEX3fgo5mjZyC', NULL, 'E7w8dvA9MWOgfgumr8Vp7AyKcORgt3lb_1560326569', 'ki_ba77@mail.ru', 1, 1, NULL, 1, 0, NULL, NULL, 4, NULL, 10, NULL, NULL, 1560326568, 1560327055),
(213, 'a_okas97@mail.ru', 'ObQnFw-ljywHYjUnKJ3gbAFChDsfw6S1', '$2y$13$Lt9w6MvO1ksoFsJtmS4YOerhYs0wnW76UZrwGDxw5FUBcrPzHvkT6', NULL, 'NnhE69OMxsPYy1XYjO6rpUo4o9_Rt78S_1560337493', 'a_okas97@mail.ru', 1, 1, NULL, 1, 0, NULL, NULL, 1, 1, 20, NULL, NULL, 1560337492, 1560337978),
(214, 'it_yvm@mail.ru', 'n8GLQ1awRv7BPYPM047OhUWmIETu8qN-', '$2y$13$AkoDR9DcpDGhlaWlXIE8CeAMNkNmdp.aJNtbPgkWsKcMv6YBpr2U2', NULL, '4S22soLESoZPfXayf8COigq8HBKwbx4a_1560426196', 'it_yvm@mail.ru', 1, 1, 2, 1, 0, NULL, NULL, 2, NULL, 20, 0, NULL, 1560426195, 1560511634),
(215, 'ps_v@bk.ru', '6_LeFda98DExoo26GdxLrSNjkKWNyH0i', '$2y$13$cLlIdDE0ZJ8aTyE.Rnkyq.wXMvmtAXAhIkrxsOJUlN2LxdpZ/ifpq', NULL, 'QGx2TWvwv0NcSUNno7ORHkHgzEAVtm1J_1560509011', 'ps_v@bk.ru', 1, 1, 1, 1, 0, NULL, NULL, 2, NULL, 20, NULL, NULL, 1560509010, 1560511684);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admission`
--
ALTER TABLE `admission`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admission`
--
ALTER TABLE `admission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
