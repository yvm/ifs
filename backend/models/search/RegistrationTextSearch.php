<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RegistrationText;

class RegistrationTextSearch extends RegistrationText
{
    public $this_type;

    public function __construct($type, array $config = [])
    {
        parent::__construct($config);

        $this->this_type = $type;
    }

    public function rules()
    {
        return [
            [['id', 'role', 'type'], 'integer'],
            [['text', 'this_type'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = RegistrationText::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'type' => $this->this_type,
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        return $dataProvider;
    }
}
