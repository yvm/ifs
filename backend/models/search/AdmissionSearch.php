<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Admission;

class AdmissionSearch extends Admission
{
    public function rules()
    {
        return [
            [['id','visibility', 'compensation','photo','video','role'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Admission::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'visibility' => $this->visibility,
            'compensation' => $this->compensation,
            'role' => $this->role,
            'photo' => $this->photo,
            'video' => $this->video,
        ]);

        return $dataProvider;
    }
}
