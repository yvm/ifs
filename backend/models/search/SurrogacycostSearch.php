<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Surrogacycost;

class SurrogacycostSearch extends Surrogacycost
{
    public function rules()
    {
        return [
            [['id', 'countStar', 'statusPriceGuest', 'statusPriceUser', 'country_id', 'language_id'], 'integer'],
            [['name', 'content', 'price'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Surrogacycost::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'countStar' => $this->countStar,
            'statusPriceGuest' => $this->statusPriceGuest,
            'statusPriceUser' => $this->statusPriceUser,
            'country_id' => $this->country_id,
            'language_id' => $this->language_id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'price', $this->price]);

        return $dataProvider;
    }
}
