<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Valuta;

class ValutaSearch extends Valuta
{
    public function rules()
    {
        return [
            [['id', 'sort','status'], 'integer'],
            [['text'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = Valuta::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sort' => $this->sort,
            'status' => $this->status
        ]);

        $query->andFilterWhere(['like', 'text', $this->text]);

        $query->orderBy('sort ASC');

        return $dataProvider;
    }
}
