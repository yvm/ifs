<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\IfsForm as IfsFormModel;

class IfsForm extends IfsFormModel
{
    public function rules()
    {
        return [
            [['id', 'role', 'type', 'country', 'city'], 'integer'],
            [['firstname', 'lastname', 'mobile_phone_number', 'email', 'telegram', 'WatsApp', 'Skype', 'subject', 'message', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = IfsFormModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'type' => $this->type,
            'country' => $this->country,
            'city' => $this->city,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'mobile_phone_number', $this->mobile_phone_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'telegram', $this->telegram])
            ->andFilterWhere(['like', 'WatsApp', $this->WatsApp])
            ->andFilterWhere(['like', 'Skype', $this->Skype])
            ->andFilterWhere(['like', 'subject', $this->subject])
            ->andFilterWhere(['like', 'message', $this->message]);

        return $dataProvider;
    }
}
