<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ProfileHistory;

class ProfileHistorySearch extends ProfileHistory
{
    public function rules()
    {
        return [
            [['id', 'user_id', 'country', 'state', 'city', 'marital_status', 'program_status', 'previous'], 'integer'],
            [['first_name', 'last_name', 'address', 'zip_code', 'phone', 'skype', 'vatzup', 'telegram', 'facebook', 'instagram', 'twitter', 'vk', 'email', 'how_many_children'], 'safe'],
            [['updated'],'date']
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = ProfileHistory::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'country' => $this->country,
            'state' => $this->state,
            'city' => $this->city,
            'marital_status' => $this->marital_status,
            'program_status' => $this->program_status,
            'previous' => $this->previous,
        ]);

        $query->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'zip_code', $this->zip_code])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'skype', $this->skype])
            ->andFilterWhere(['like', 'vatzup', $this->vatzup])
            ->andFilterWhere(['like', 'telegram', $this->telegram])
            ->andFilterWhere(['like', 'facebook', $this->facebook])
            ->andFilterWhere(['like', 'instagram', $this->instagram])
            ->andFilterWhere(['like', 'twitter', $this->twitter])
            ->andFilterWhere(['like', 'vk', $this->vk])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'how_many_children', $this->how_many_children])
            ->andFilterWhere(['like', 'updated', $this->updated]);

        $query->orderBy('id DESC');

        return $dataProvider;
    }
}
