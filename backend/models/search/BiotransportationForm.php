<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BiotransportationForm as BiotransportationFormModel;

class BiotransportationForm extends BiotransportationFormModel
{
    public function rules()
    {
        return [
            [['id', 'country', 'state', 'city', 'country_deliver', 'state_deliver', 'city_deliver', 'tube_number', 'biomaterial_infected'], 'integer'],
            [['name_pick_up', 'date', 'contact_person', 'phone', 'adress', 'name_deliver', 'contact_deliver', 'phone_deliver', 'adress_deliver', 'first_name', 'last_name', 'mobile_phone', 'email', 'country_transportation', 'state_transportation', 'city_transportation', 'adress_transportation', 'biomaterial_type', 'tube_type', 'additional_information', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = BiotransportationFormModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'country' => $this->country,
            'state' => $this->state,
            'city' => $this->city,
            'country_deliver' => $this->country_deliver,
            'state_deliver' => $this->state_deliver,
            'city_deliver' => $this->city_deliver,
            'tube_number' => $this->tube_number,
            'biomaterial_infected' => $this->biomaterial_infected,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'name_pick_up', $this->name_pick_up])
            ->andFilterWhere(['like', 'date', $this->date])
            ->andFilterWhere(['like', 'contact_person', $this->contact_person])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'adress', $this->adress])
            ->andFilterWhere(['like', 'name_deliver', $this->name_deliver])
            ->andFilterWhere(['like', 'contact_deliver', $this->contact_deliver])
            ->andFilterWhere(['like', 'phone_deliver', $this->phone_deliver])
            ->andFilterWhere(['like', 'adress_deliver', $this->adress_deliver])
            ->andFilterWhere(['like', 'first_name', $this->first_name])
            ->andFilterWhere(['like', 'last_name', $this->last_name])
            ->andFilterWhere(['like', 'mobile_phone', $this->mobile_phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'country_transportation', $this->country_transportation])
            ->andFilterWhere(['like', 'state_transportation', $this->state_transportation])
            ->andFilterWhere(['like', 'city_transportation', $this->city_transportation])
            ->andFilterWhere(['like', 'adress_transportation', $this->adress_transportation])
            ->andFilterWhere(['like', 'biomaterial_type', $this->biomaterial_type])
            ->andFilterWhere(['like', 'tube_type', $this->tube_type])
            ->andFilterWhere(['like', 'additional_information', $this->additional_information]);

        return $dataProvider;
    }
}
