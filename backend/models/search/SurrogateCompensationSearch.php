<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\SurrogateCompensation;

class SurrogateCompensationSearch extends SurrogateCompensation
{
    public function rules()
    {
        return [
            [['id', 'from', 'to', 'move'], 'integer'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = SurrogateCompensation::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'from' => $this->from,
            'to' => $this->to,
            'move' => $this->move,
        ]);

        return $dataProvider;
    }
}
