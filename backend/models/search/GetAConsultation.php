<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\GetAConsultation as GetAConsultationModel;

class GetAConsultation extends GetAConsultationModel
{
    public function rules()
    {
        return [
            [['id', 'role', 'type', 'country', 'city', 'DesiredConsultLocation'], 'integer'],
            [['firstname', 'lastname', 'WatsApp', 'Skype', 'data', 'time', 'QuestionsComments', 'created_at'], 'safe'],
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function search($params)
    {
        $query = GetAConsultationModel::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'role' => $this->role,
            'type' => $this->type,
            'country' => $this->country,
            'city' => $this->city,
            'DesiredConsultLocation' => $this->DesiredConsultLocation,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'firstname', $this->firstname])
            ->andFilterWhere(['like', 'lastname', $this->lastname])
            ->andFilterWhere(['like', 'WatsApp', $this->WatsApp])
            ->andFilterWhere(['like', 'Skype', $this->Skype])
            ->andFilterWhere(['like', 'data', $this->data])
            ->andFilterWhere(['like', 'time', $this->time])
            ->andFilterWhere(['like', 'QuestionsComments', $this->QuestionsComments]);

        return $dataProvider;
    }
}
