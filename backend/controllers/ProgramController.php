<?php

namespace backend\controllers;

use Yii;
use common\models\User;
use common\models\Profiles;
use common\models\Program;
use common\models\ProgramStages;
use backend\models\search\ProgramSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;


class ProgramController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new ProgramSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function bfsave($model)
    {
        if(!empty($model->program_type2))
            $model->program_type = $model->program_type2;
        if(!empty($model->program_name2))
            $model->program_name = $model->program_name2;
        if(!empty($model->program_city2))
            $model->program_city = $model->program_city2;
        if(!empty($model->program_manager2))
            $model->program_manager = $model->program_manager2;
        if(!empty($model->ivf_clinic2))
            $model->ivf_clinic = $model->ivf_clinic2;
        if(!empty($model->physician2))
            $model->physician = $model->physician2;

        return $model;
    }

    public function actionCreate($id)
    {
        $model = new Program();
        $ProgramStagesNew = new ProgramStages;
        $user  = User::findOne($id);

        $model->program_type = unserialize($model->program_type);
        $model->user_id = $id;

        if ($model->load(Yii::$app->request->post())) {

            $model->program_type = serialize($model->program_type);

            $model = $this->bfsave($model);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($ProgramStagesNew->load(Yii::$app->request->post())) {
            $ProgramStagesNew->files = UploadedFile::getInstances($ProgramStagesNew, 'files');

            $files = [];
            foreach($ProgramStagesNew->files as $file) {
                $time = time();
                $file->saveAs($ProgramStagesNew->path . $time . $file->baseName . '.' . $file->extension);

                $files[] = $time . $file->baseName . '.' . $file->extension;
            }

            if(count($files))
                $ProgramStagesNew->images = serialize($files);

            $ProgramStagesNew->got_it = 0;
            $ProgramStagesNew->save(false);
        }

        return $this->render('create', [
            'model' => $model,
            'ProgramStagesNew' => $ProgramStagesNew,
            'user'  => $user
        ]);
    }

    public function actionUpdate($id)
    {
        $model = Program::findAll(['user_id' => $id]);

        return $this->render('update', [
            'model' => $model,
            'type'  => 1,
            'id'    => $id
        ]);
    }

    public function actionUpdateProgram($id)
    {
        $model = Program::findOne($id);
        $ProgramStagesNew = new ProgramStages;
        $model->program_type = unserialize($model->program_type);

        if ($model->load(Yii::$app->request->post())) {
            $model->program_type = serialize($model->program_type);

            $model = $this->bfsave($model);
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        if ($ProgramStagesNew->load(Yii::$app->request->post())) {
            $new = 1;
            if(!empty($ProgramStagesNew->id)) {
                $ProgramStagesNew = ProgramStages::findOne($ProgramStagesNew->id);
                $ProgramStagesNew->load(Yii::$app->request->post());
                $new = 0;
            }
            $ProgramStagesNew->program_id = $id;
            if($new)
                $stage = 'files';
            else {
                $stage = $ProgramStagesNew->stage_number - 1;
                $stage = 'files' . $stage;
            }
            $ProgramStagesNew->$stage = UploadedFile::getInstances($ProgramStagesNew, $stage);

            $files = [];
            foreach($ProgramStagesNew->$stage as $file) {
                $time = time();
                $file->saveAs($ProgramStagesNew->path . $time . $file->baseName . '.' . $file->extension);

                $files[] = $time . $file->baseName . '.' . $file->extension;
            }

            if(!empty($ProgramStagesNew->images)) {
                $images_old = unserialize($ProgramStagesNew->images);
                foreach ($images_old as $v)
                    $files[] = $v;
            }

            if(count($files))
                $ProgramStagesNew->images = serialize($files);

            $ProgramStagesNew->got_it = 0;
            if($ProgramStagesNew->save(false) && $new == 1){
                $profile = Profiles::findOne($model->user->id);
                $this->actionNewProgramStages($profile);
            }
        }

        $ProgramStages = ProgramStages::findAll(['program_id' => $id]);

        return $this->render('update', [
            'model' => $model,
            'ProgramStages' => $ProgramStages,
            'ProgramStagesNew' => $ProgramStagesNew,
            'type'  => '',
            'id'    => $id
        ]);
    }

    public function actionDelete($id)
    {

        $model = Program::findOne($id);
        $models = Program::find()->where('sort > '.$model->sort)->all();

        foreach($models as $v){
            $v->sort--;
            $v->save(false);
        }


        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Program::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
