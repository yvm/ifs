<?php

namespace backend\controllers;

use common\models\Country;
use common\models\Countrylang;
use Yii;
use common\models\Language;
use backend\models\search\LanguageSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
class LanguageController extends BackendController
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $searchModel = new LanguageSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionCreate()
    {
        $model = new Language();

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            if ($model->country_id == 0) {

                $countries = Country::find()->all();
                $language_id = $model->id;
                $status = $model->status;
                foreach ($countries as $v) {
                    $model = new Countrylang();
                    $lastlang = Countrylang::find()->where('country_id=' . $v->id)->limit(1)->orderBy('sort DESC')->one();
                    $model->country_id = $v->id;
                    $model->language_id = $language_id;
                    $model->status = $status;
                    if ($lastlang == null) {
                        $model->sort = 1;
                    } else {
                        $model->sort = $lastlang->sort + 1;
                    }
                    if ($model->save()) {
                        Countrylang::createLanguageContent($v->id, $model->language_id);
                        Countrylang::createMonthContent($model->id);
                    }
                }
                return $this->redirect(['index']);

            } else {

                $countryLang = new Countrylang();
                $countryLang->country_id = $model->country_id;
                $countryLang->language_id = $model->id;
                $lastlang = Countrylang::find()->where('country_id=' . $model->country_id)->limit(1)->orderBy('sort DESC')->one();
                if ($lastlang == null) {
                    $countryLang->sort = 1;
                } else {
                    $countryLang->sort = $lastlang->sort + 1;
                }
                if ($countryLang->save(false)) {
                    Yii::$app->session->set('country', $model->country_id);
                    Yii::$app->session->set('lang', $model->id);
                    Countrylang::createLanguageContent($model->country_id, $model->id);
                    Countrylang::createMonthContent($model->id);

                }

                return $this->redirect(['index']);
            }

        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionDelete($id)
    {
        Countrylang::deleteAll('language_id='.$id);
        Countrylang::deleteLangContent($id);
        Countrylang::deleteMonthContent($id);

        if($this->findModel($id)->delete()){
            Yii::$app->session->set('country',1);
            Yii::$app->session->set('lang',1);
        }

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = Language::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
