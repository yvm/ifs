<?php
namespace common\models;

use DateTime;
use Yii;

class Profiles extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'profiles';
    }

    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name', 'email', 'fio'], 'required'],
            [['user_id'], 'integer'],
            [['email'], 'email'],
            [['status'], 'string'],
            [['created_at'], 'safe'],
            [['first_name', 'last_name', 'email', 'vatzup', 'skype', 'telegram', 'facebook', 'work_contact', 'twitter', 'vk', 'home_contact', 'instagram'], 'string', 'max' => 100],
            [['mobile'], 'string', 'max' => 30],
            [['user_id'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'first_name' => Yii::t('app', 'First Name'),
            'last_name' => Yii::t('app', 'Last Name'),
            'email' => Yii::t('app', 'Email'),
            'vatzup' => Yii::t('app', 'Vatzup'),
            'skype' => Yii::t('app', 'Skype'),
            'telegram' => Yii::t('app', 'Telegram'),
            'mobile' => Yii::t('app', 'Mobile'),
            'status' => Yii::t('app', 'Status'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function getCreatedDate(){
        try {
            $dt = new DateTime($this->created_at);
        } catch (\Exception $e) {
        }
        return $dt->format('d.m.Y');

    }

    public function getCreatedDateWithoutDot(){
        try {
            $dt = new DateTime($this->created_at);
        } catch (\Exception $e) {
        }
        return $dt->format('dmY');
    }

}
