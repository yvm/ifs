<?php
namespace common\models;

use Yii;

class SurrogateAvailability extends \yii\db\ActiveRecord
{

    const eggDonor = 'eggDonor';
    const surrogate = 'surrogate';
    const surrogate_and_eggDonor = 'surrogate_and_eggDonor';

    public static function tableName()
    {
        return 'surrogate_availability';
    }

    public function rules()
    {
        return [
            [['user_id', 'willing_to_travel', 'would_you_allow', 'i_am_willing_as','are_you_willing_to_be_completely',
                'willing_to_self_in_cycle_sync',
                'willing_to_self_egg_donor_cycle','do_you_have_flexible_schedule'], 'required'],
            [['user_id', 'willing_to_travel', 'would_you_allow', 'i_am_willing_as','are_you_willing_to_be_completely',
                'are_you_willing_to_meet_recipient','are_you_willing_to_talk_with_potential','i_am_willing_as_eggdonor',
                'are_you_willing_to_do_anonymous','can_you_travel', 'have_passport', 'willing_to_self_in_cycle_sync',
                'willing_to_self_egg_donor_cycle','do_you_have_flexible_schedule'], 'integer'],
            [['created_at'], 'safe'],
            [['willing_to_travel'], 'string', 'max' => 10],
            ['are_you_willing_to_be_completely', 'yes_no', 'on' => self::surrogate],
            [['have_passport', 'are_you_willing_to_do_anonymous', 'can_you_travel'], 'required', 'on' => self::surrogate],
            [['have_passport', 'are_you_willing_to_do_anonymous', 'can_you_travel'], 'required', 'on' => self::eggDonor],
            ['are_you_willing_to_be_completely', 'yes_no_surrogate_and_eggDonor', 'on' => self::surrogate_and_eggDonor],
        ];
    }

    public function yes_no($attribute,$params)
    {
        if($this->are_you_willing_to_be_completely == 0) {
            if (empty($this->are_you_willing_to_meet_recipient))
                $this->addError("are_you_willing_to_meet_recipient", "Необходимо указать are_you_willing_to_meet_recipient.");
            if (empty($this->are_you_willing_to_talk_with_potential))
                $this->addError("are_you_willing_to_talk_with_potential", "Необходимо указать are_you_willing_to_talk_with_potential.");
            if (empty($this->i_am_willing_as_eggdonor))
                $this->addError("i_am_willing_as_eggdonor", "Необходимо указать i_am_willing_as_eggdonor.");
        }
    }

    public function yes_no_surrogate_and_eggDonor($attribute,$params)
    {
        if($this->are_you_willing_to_be_completely == 2) {
            if (empty($this->are_you_willing_to_meet_recipient))
                $this->addError("are_you_willing_to_meet_recipient", "Необходимо указать are_you_willing_to_meet_recipient.");
            if (empty($this->are_you_willing_to_talk_with_potential))
                $this->addError("are_you_willing_to_talk_with_potential", "Необходимо указать are_you_willing_to_talk_with_potential.");
            if (empty($this->i_am_willing_as_eggdonor))
                $this->addError("i_am_willing_as_eggdonor", "Необходимо указать i_am_willing_as_eggdonor.");
            if (empty($this->are_you_willing_to_do_anonymous))
                $this->addError("are_you_willing_to_do_anonymous", "Необходимо указать are_you_willing_to_do_anonymous.");
            if (empty($this->can_you_travel))
                $this->addError("can_you_travel", "Необходимо указать can_you_travel.");
            if (empty($this->have_passport))
                $this->addError("have_passport", "Необходимо указать have_passport.");
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'willing_to_travel' => Yii::t('app', 'Willing To Travel'),
            'would_you_allow' => Yii::t('app', 'Would You Allow'),
            'i_am_willing_as' => Yii::t('app', 'I Am Willing As'),
            'can_you_travel' => Yii::t('app', 'Can You Travel'),
            'have_passport' => Yii::t('app', 'Have Passport'),
            'willing_to_self' => Yii::t('app', 'Willing To Self'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function scenarios()
    {
        return [
            self::surrogate => ['would_you_allow','i_am_willing_as','can_you_travel',
                'have_passport','can_you_travel', 'willing_to_self_in_cycle_sync'],

            self::eggDonor => ['are_you_willing_to_be_completely',
                'are_you_willing_to_meet_recipient','are_you_willing_to_talk_with_potential','i_am_willing_as_eggdonor',
                'are_you_willing_to_do_anonymous','can_you_travel', 'have_passport',
                'willing_to_self_egg_donor_cycle','do_you_have_flexible_schedule'],

            self::surrogate_and_eggDonor => ['would_you_allow','i_am_willing_as', 'willing_to_self_in_cycle_sync',
                'are_you_willing_to_be_completely','are_you_willing_to_meet_recipient','are_you_willing_to_talk_with_potential',
                'i_am_willing_as','are_you_willing_to_do_anonymous','can_you_travel', 'have_passport', 'i_am_willing_as_eggdonor',
                'willing_to_self_egg_donor_cycle','do_you_have_flexible_schedule'],


        ];
    }

}
