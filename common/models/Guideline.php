<?php
namespace common\models;

use Yii;

class Guideline extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'guideline';
    }

    public function rules()
    {
        return [
            [['content', 'role', 'file', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['role', 'file', 'country_id', 'language_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'role' => 'Роль',
            'file' => 'Тип файла',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getRoles()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }

    public function getFiles()
    {
        $files = [1 => 'Фото',2 =>'Документы',3 =>'Образец почерка',4 =>'Голосовое сообщение',5 =>'Видео'];
        return $files[$this->file];
    }
}
