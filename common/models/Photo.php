<?php
namespace common\models;

use Yii;

class Photo extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/photo/';

    public static function tableName()
    {
        return 'photo';
    }

    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'image' => Yii::t('app', 'Картинка'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/photo/' . $this->image : '/no-image.png';
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Photo::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

    public function getImages(){
        return $this->hasMany(Phototype::className(), ['photo_id' => 'id']);
    }

    public function getPreviuosId(){
        $photo = Photo::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $preId = "";
        foreach ($photo as $v){
            if($v->id == $this->id){
                break;
            }
            $preId = $v->id;
        }
        return $preId;
    }

    public function getNextId(){
        $photo = Photo::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $nextId = "";
        $check = false;
        foreach ($photo as $v){
            if($check){
                $nextId = $v->id;
                break;
            }
            if($v->id == $this->id){
                $check = true;
            }
        }
        return $nextId;
    }
}
