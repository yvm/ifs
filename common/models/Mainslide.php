<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "mainslide".
 *
 * @property int $id
 * @property string $littleTitle
 * @property string $bigTitle
 * @property string $slogan
 * @property string $content
 * @property string $image
 * @property int $cssNumber
 */
class Mainslide extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mainslide';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['littleTitle', 'bigTitle', 'slogan', 'content','status'], 'required'],
            [['content'], 'string'],
            [['cssNumber','status'], 'integer'],
            [['littleTitle', 'bigTitle', 'slogan', 'image','url'], 'string', 'max' => 255],
            [['image'],'file','extensions'=>'png,jpg']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'littleTitle' => 'Маленький заголовок ',
            'bigTitle' => 'Большой заголовок ',
            'slogan' => 'Девиз',
            'content' => 'Содержания',
            'image' => 'Картинка',
            'url' => 'Ссылка',
            'cssNumber' => 'Css Number',
            'status'=>'Статус'
        ];
    }

    public function saveImage($filename)
    {
        $this->image = $filename;
        return $this->save(false);
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/' . $this->image : '/no-image.png';
    }
}
