<?php
namespace common\models;

use Yii;

class HealthDetails extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'health_details';
    }

    public function rules()
    {
        return [
            [['user_id', 'type', 'relation', 'details'], 'required'],
            [['user_id'], 'integer'],
            [['details'], 'string'],
            [['created_at'], 'safe'],
            [['type', 'relation'], 'string', 'max' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'type' => Yii::t('app', 'Type'),
            'relation' => Yii::t('app', 'Relation'),
            'details' => Yii::t('app', 'Details'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
