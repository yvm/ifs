<?php
namespace common\models;

use Yii;

class City extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'city';
    }

    public function rules()
    {
        return [
            [['region_id', 'name'], 'required'],
            [['region_id'], 'integer'],
            [['crt_date'], 'safe'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'region_id' => Yii::t('app', 'Region ID'),
            'name' => 'Название',
            'crt_date' => Yii::t('app', 'Crt Date'),
        ];
    }
}
