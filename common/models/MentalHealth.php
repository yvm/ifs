<?php
namespace common\models;

use Yii;

class MentalHealth extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'mental_health';
    }

    public function rules()
    {
        return [
            [['user_id', 'postpartum_depression', 'condition_or_illness', 'attempted_suicide', 'health_professional'], 'required'],
            [['user_id', 'postpartum_depression', 'condition_or_illness', 'attempted_suicide', 'health_professional'], 'integer'],
            [['describe'], 'string'],
            [['created_at'], 'safe'],
            [['user_id'], 'unique'],
            ['health_professional', 'yes_no']
        ];
    }

    public function yes_no($attribute,$params)
    {
        if ($this->health_professional)
            if (empty($this->describe))
                $this->addError("describe", "Необходимо указать describe.");
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'postpartum_depression' => Yii::t('app', 'Postpartum Depression'),
            'condition_or_illness' => Yii::t('app', 'Condition Or Illness'),
            'attempted_suicide' => Yii::t('app', 'Attempted Suicide'),
            'health_professional' => Yii::t('app', 'Health Professional'),
            'describe' => Yii::t('app', 'Describe'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
