<?php
namespace common\models;

use Yii;

class SexualHistory extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'sexual_history';
    }

    public function rules()
    {
        return [
            [['user_id', 'many_sexual_partners', 'sexually_active'], 'required'],
            [['user_id', 'many_sexual_partners', 'sexually_active'], 'integer'],
            [['created_at'], 'safe'],
            [['user_id'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'many_sexual_partners' => Yii::t('app', 'Many Sexual Partners'),
            'sexually_active' => Yii::t('app', 'Sexually Active'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
