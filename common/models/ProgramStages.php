<?php
namespace common\models;

use Yii;

class ProgramStages extends \yii\db\ActiveRecord
{
    public $files;
    public $files0;
    public $files1;
    public $files2;
    public $files3;
    public $files4;
    public $files5;
    public $files6;
    public $files7;
    public $files8;
    public $path = 'images/programstages/';

    public static function tableName()
    {
        return 'program_stages';
    }

    public function rules()
    {
        return [
            [['program_id', 'stage_number', 'start_date', 'end_date', 'stage_name', 'stage_description', 'completed_work', 'user_actions', 'images', 'notes', 'next_scheduled_stage', 'scheduled_start', 'scheduled_end', 'scheduled_user', 'got_it'], 'required'],
            [['program_id', 'stage_number', 'got_it', 'id'], 'integer'],
            [['images'], 'string'],
            [['created_at'], 'safe'],
            [['start_date', 'end_date', 'scheduled_start', 'scheduled_end'], 'string', 'max' => 20],
            [['stage_name', 'stage_description', 'completed_work', 'user_actions', 'notes', 'next_scheduled_stage', 'scheduled_user'], 'string', 'max' => 255],
            [['files', 'files0', 'files1', 'files2', 'files3', 'files4', 'files5', 'files6', 'files7', 'files8'], 'each', 'rule' => ['image']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'program_id' => 'Program ID',
            'stage_number' => 'Stage Number',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'stage_name' => 'Stage Name',
            'stage_description' => 'Stage Description',
            'completed_work' => 'Completed Work',
            'user_actions' => 'User Actions',
            'images' => 'Images',
            'notes' => 'Notes',
            'next_scheduled_stage' => 'Next Scheduled Stage',
            'scheduled_start' => 'Scheduled Start',
            'scheduled_end' => 'Scheduled End',
            'scheduled_user' => 'Scheduled User',
            'got_it' => 'Got It',
            'created_at' => 'Created At',
        ];
    }

    public function upload()
    {
        $time = time();
        $this->images->saveAs($this->path. $time . $this->images->baseName . '.' . $this->images->extension);
        return $time . $this->images->baseName . '.' . $this->images->extension;
    }
	
    public function getProgram(){
        return $this->hasOne(Program::className(), ['id' => 'program_id']);
    }
}
