<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "translation".
 *
 * @property int $id
 * @property string $name
 * @property int $page_id
 * @property string $pageNama
 * @property int $country_id
 * @property int $language_id
 */
class Translation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'translation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['page_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'page_id' => 'Страница',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getPage()
    {
        return $this->hasOne(Menu::className(), ['id' => 'page_id']);
    }

    public function getPageName(){
        return (isset($this->page))? $this->page->text:'Шапка, футер';
    }
}
