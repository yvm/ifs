<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ifstrip".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property int $country_id
 * @property int $language_id
 */
class Ifstrip extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ifstrip';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'content' => 'Содержание',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Ifstrip::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

    public function getTripImages(){
        return $this->hasMany(Ifstrippic::className(), ['ifstrip_id' => 'id']);
    }
}
