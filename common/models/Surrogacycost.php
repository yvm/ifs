<?php
namespace common\models;

use Yii;

class Surrogacycost extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacycost';
    }

    public function rules()
    {
        return [
            [['name', 'countStar', 'content', 'price', 'statusPriceGuest', 'statusPriceUser', 'package_title',
                'package_subtitle','package_content','package_tabletitle','country_id', 'language_id'], 'required'],
            [['countStar', 'statusPriceGuest', 'statusPriceUser', 'country_id', 'language_id'], 'integer'],
            [['content','package_content'], 'string'],
            [['name', 'price','package_title','package_subtitle','package_tabletitle'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'countStar' => Yii::t('app', 'Количества звездочка'),
            'content' => Yii::t('app', 'Содержание'),
            'price' => Yii::t('app', 'Цена'),
            'statusPriceGuest' => Yii::t('app', 'Статус цена для незарегистрированых пользователей'),
            'statusPriceUser' => Yii::t('app', 'Статус цена для зарегистрированных пользователей'),
            'package_title' => Yii::t('app', 'Заголовок пакета'),
            'package_subtitle' => Yii::t('app', 'Подзаголовок пакета'),
            'package_content' => Yii::t('app', 'Описание'),
            'package_tabletitle' => Yii::t('app', 'Заголовок таблица '),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Surrogacycost::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

    public function getTypes(){
        return $this->hasMany(Surrogacycosttype::className(), ['surrogacycost_id' => 'id']);
    }

    public function getGeturl(){
        return $this->id;
    }

    public function getGetname(){
        return $this->name;
    }
}
