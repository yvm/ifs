<?php
namespace common\models;

use Yii;

class Region extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'region';
    }

    public function rules()
    {
        return [
            [['id', 'country_id', 'name'], 'required'],
            [['id', 'country_id'], 'integer'],
            [['crt_date'], 'safe'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id1' => Yii::t('app', 'Id1'),
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Country ID'),
            'name' => 'Название',
            'crt_date' => Yii::t('app', 'Crt Date'),
        ];
    }
}
