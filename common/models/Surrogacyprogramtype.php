<?php
namespace common\models;

use Yii;

class Surrogacyprogramtype extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacyprogramtype';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id','countStar'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание'),
            'countStar' => 'Количества звездочка',
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
