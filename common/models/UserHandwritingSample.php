<?php
namespace common\models;

use Yii;

class UserHandwritingSample extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_handwriting_sample';
    }

    public function rules()
    {
        return [
            [['user_id', 'video'], 'required'],
            [['user_id'], 'integer'],
            [['video'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'video' => 'Video',
        ];
    }
}
