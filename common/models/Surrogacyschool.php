<?php
namespace common\models;

use Yii;

class Surrogacyschool extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/surrogacyschool/';

    public static function tableName()
    {
        return 'surrogacyschool';
    }

    public function rules()
    {
        return [
            [['contenta', 'contentb', 'contentc', 'country_id', 'language_id'], 'required'],
            [['contenta', 'contentb', 'contentc'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contenta' => Yii::t('app', 'Верхний контент'),
            'image' => Yii::t('app', 'Картинка'),
            'contentb' => Yii::t('app', 'Средний контент'),
            'contentc' => Yii::t('app', 'Нижний контент'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogacy/' . $this->image : '/no-image.png';
    }


}
