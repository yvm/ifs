<?php
namespace common\models;

use Yii;

class GetAConsultation extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'get_a_consultation';
    }

    public function rules()
    {
        return [
            [['firstname', 'lastname', 'role', 'mobile_phone_number', 'email', 'country', 'city', 'DesiredConsultLocation', 'data', 'time', 'GMT', 'QuestionsComments'], 'required'],
            [['role', 'type', 'country', 'city', 'DesiredConsultLocation'], 'integer'],
            [['QuestionsComments'], 'string'],
            [['email'], 'email'],
            [['created_at'], 'safe'],
            [['firstname', 'lastname', 'mobile_phone_number', 'email', 'telegram', 'WatsApp', 'Skype', 'data', 'time'], 'string', 'max' => 255],
            [['GMT'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'role' => 'Role',
            'type' => 'Type',
            'mobile_phone_number' => 'Mobile Phone Number',
            'email' => 'Email',
            'telegram' => 'Telegram',
            'country' => 'Country',
            'WatsApp' => 'Wats App',
            'Skype' => 'Skype',
            'city' => 'City',
            'DesiredConsultLocation' => 'Desired Consult Location',
            'data' => 'Data',
            'time' => 'Time',
            'GMT' => 'Gmt',
            'QuestionsComments' => 'Questions Comments',
            'created_at' => 'Created At',
        ];
    }
}
