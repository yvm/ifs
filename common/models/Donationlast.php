<?php
namespace common\models;

use Yii;

class Donationlast extends \yii\db\ActiveRecord
{
    public $files;

    public static function tableName()
    {
        return 'donationlast';
    }

    public function rules()
    {
        return [
            [['name', 'buttonName', 'country_id', 'language_id'], 'required'],
            [['name'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['buttonName'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Содержание',
            'buttonName' => Yii::t('app', 'Название кнопка'),
            'image' => Yii::t('app', 'Картинка'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }


    public function getImage()
    {
        return ($this->image) ? '/uploads/eggdonation/' . $this->image : '/no-image.png';
    }


}
