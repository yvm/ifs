<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "month".
 *
 * @property int $id
 * @property string $name
 * @property int $language_id
 */
class Month extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'month';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'language_id'], 'required'],
            [['language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'language_id' => 'Language ID',
        ];
    }
}
