<?php
namespace common\models;

use backend\models\search\MainslideSearch;
use Yii;

class Countrylang extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'countrylang';
    }

    public function rules()
    {
        return [
            [['country_id', 'language_id', 'status', 'sort'], 'required'],
            [['country_id', 'language_id', 'status', 'sort'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'country_id' => Yii::t('app', 'Страна'),
            'language_id' => Yii::t('app', 'Язык'),
            'status' => 'Статус',
            'sort' => 'Сортировка',
        ];
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Countrylang::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }

    public function getLang()
    {
        return $this->hasOne(Language::className(), ['id' => 'language_id']);
    }

    public function getLangName(){
        return (isset($this->lang))? $this->lang->name:'Не задан';
    }


    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    public function getCountryName(){
        return (isset($this->country))? $this->country->name:'Не задан';
    }



    public static function createMonthContent($language_id)
    {
        $month = Month::find()->where('language_id=1')->all();
        foreach ($month as $v){
            $newMonth = new Month();
            $newMonth->name = $v->name;
            $newMonth->language_id = $language_id;
            $newMonth->save(false);
        }

    }


    public static function createLanguageContent($country_id, $language_id){
        $arrForMenu = array();
        $menu = Menu::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($menu as $v){
            $newMenu = new Menu();
            $newMenu->text = $v->text;
            $newMenu->status = $v->status;
            $newMenu->metaName = $v->metaName;
            $newMenu->metaDesc = $v->metaDesc;
            $newMenu->metaKey = $v->metaKey;
            $newMenu->url = $v->url;
            $newMenu->sort = $v->sort;
            $newMenu->country_id = $country_id;
            $newMenu->language_id = $language_id;
            $newMenu->save(false);
            $arrForMenu[$v->id] = $newMenu->id;
        }

        $submenu = Submenu::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($submenu as $v){
            $newSubmenu = new Submenu();
            $newSubmenu->name = $v->name;
            $newSubmenu->status = $v->status;
            $newSubmenu->menu_id = $arrForMenu[$v->menu_id];
            $newSubmenu->url = $v->url;
            $newSubmenu->sort = $v->sort;
            $newSubmenu->country_id = $country_id;
            $newSubmenu->language_id = $language_id;
            $newSubmenu->save(false);
        }


        $contact = Contact::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($contact as $v){
            $newContact = new Contact();
            $newContact->mobileTel = $v->mobileTel;
            $newContact->telegramTel = $v->telegramTel;
            $newContact->email = $v->email;
            $newContact->address = $v->address;
            $newContact->facebook = $v->facebook;
            $newContact->vk = $v->vk;
            $newContact->instagram = $v->instagram;
            $newContact->twitter = $v->twitter;
            $newContact->google = $v->google;
            $newContact->ok = $v->ok;
            $newContact->telegram = $v->telegram;
            $newContact->skype = $v->skype;
            $newContact->urlMap = $v->urlMap;
            $newContact->country_id = $country_id;
            $newContact->language_id = $language_id;
            $newContact->save(false);
        }

        $banner = Banner::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($banner as $v){
            $newBanner = new Banner();
            $newBanner->littleTitle = $v->littleTitle;
            $newBanner->bigTitle = $v->bigTitle;
            $newBanner->slogan = $v->slogan;
            $newBanner->image = $v->image;
            $newBanner->country_id = $country_id;
            $newBanner->language_id = $language_id;
            $newBanner->save(false);
        }

        $bannerButtons = Buttons::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($bannerButtons as $v){
            $newBannerButtons = new Buttons();
            $newBannerButtons->name = $v->name;
            $newBannerButtons->url = $v->url;
            $newBannerButtons->status = $v->status;
            $newBannerButtons->country_id = $country_id;
            $newBannerButtons->language_id = $language_id;
            $newBannerButtons->save(false);
        }





        //HOME
        $mainSlide = Mainslide::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($mainSlide as $v){
            $newSlide = new Mainslide();
            $newSlide->littleTitle = $v->littleTitle;
            $newSlide->bigTitle = $v->bigTitle;
            $newSlide->slogan = $v->slogan;
            $newSlide->image = $v->image;
            $newSlide->content = $v->content;
            $newSlide->url = $v->url;
            $newSlide->status = $v->status;
            $newSlide->cssNumber = $v->cssNumber;
            $newSlide->country_id = $country_id;
            $newSlide->language_id = $language_id;
            $newSlide->save(false);
        }

        $mainProgress = Mainprogress::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($mainProgress as $v){
            $newProgress = new Mainprogress();
            $newProgress->content = $v->content;
            $newProgress->country_id = $country_id;
            $newProgress->language_id = $language_id;
            $newProgress->save(false);
        }










        //IFS
        $ifsAbout = Ifsabout::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsAbout as $v){
            $newIfsAbout = new Ifsabout();
            $newIfsAbout->subtitlea = $v->subtitlea;
            $newIfsAbout->subtitleb = $v->subtitleb;
            $newIfsAbout->content = $v->content;
            $newIfsAbout->url = $v->url;
            $newIfsAbout->country_id = $country_id;
            $newIfsAbout->language_id = $language_id;
            $newIfsAbout->save(false);
        }

        $ifsContact = Ifscontact::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsContact as $v){
            $newIfsContact = new Ifscontact();
            $newIfsContact->name = $v->name;
            $newIfsContact->country_id = $country_id;
            $newIfsContact->language_id = $language_id;
            $newIfsContact->save(false);
        }

        $ifsDifference = Ifsdiff::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsDifference as $v){
            $newIfsDifference = new Ifsdiff();
            $newIfsDifference->content = $v->content;
            $newIfsDifference->country_id = $country_id;
            $newIfsDifference->language_id = $language_id;
            $newIfsDifference->save(false);
        }

        $ifsFacts = Ifsfacts::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsFacts as $v){
            $newIfsFacts = new Ifsfacts();
            $newIfsFacts->content = $v->content;
            $newIfsFacts->contentsvg = $v->contentsvg;
            $newIfsFacts->country_id = $country_id;
            $newIfsFacts->language_id = $language_id;
            $newIfsFacts->save(false);
        }

        $ifsMedTeam = Ifsmedteam::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsMedTeam as $v){
            $newIfsMedTeam = new Ifsmedteam();
            $newIfsMedTeam->name = $v->name;
            $newIfsMedTeam->image = $v->image;
            $newIfsMedTeam->position = $v->position;
            $newIfsMedTeam->experience = $v->experience;
            $newIfsMedTeam->country_id = $country_id;
            $newIfsMedTeam->language_id = $language_id;
            $newIfsMedTeam->save(false);
        }

        $ifsTeam = Ifsteam::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsTeam as $v){
            $newIfsTeam = new Ifsteam();
            $newIfsTeam->name = $v->name;
            $newIfsTeam->image = $v->image;
            $newIfsTeam->position = $v->position;
            $newIfsTeam->experience = $v->experience;
            $newIfsTeam->country_id = $country_id;
            $newIfsTeam->language_id = $language_id;
            $newIfsTeam->save(false);
        }


        $ifsService = Ifsservice::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($ifsService as $v){
            $newIfsService = new Ifsservice();
            $newIfsService->name = $v->name;
            $newIfsService->image = $v->image;
            $newIfsService->content = $v->content;
            $newIfsService->country_id = $country_id;
            $newIfsService->language_id = $language_id;
            $newIfsService->save(false);
        }









        //PARENT
        $parentBenefit = Parentbenefit::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentBenefit as $v){
            $newParentBenefit = new Parentbenefit();
            $newParentBenefit->name = $v->name;
            $newParentBenefit->content = $v->content;
            $newParentBenefit->isBig = $v->isBig;
            $newParentBenefit->country_id = $country_id;
            $newParentBenefit->language_id = $language_id;
            $newParentBenefit->save(false);
        }

        $parentDoctor = Parentdoctor::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentDoctor as $v){
            $newParentDoctor = new Parentdoctor();
            $newParentDoctor->name = $v->name;
            $newParentDoctor->image = $v->image;
            $newParentDoctor->contenta = $v->contenta;
            $newParentDoctor->contentb = $v->contentb;
            $newParentDoctor->country_id = $country_id;
            $newParentDoctor->language_id = $language_id;
            $newParentDoctor->save(false);
        }

        $parentFaq = Parentfaq::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentFaq as $v){
            $newParentFaq = new Parentfaq();
            $newParentFaq->name = $v->name;
            $newParentFaq->content = $v->content;
            $newParentFaq->country_id = $country_id;
            $newParentFaq->language_id = $language_id;
            $newParentFaq->save(false);
        }

        $parentHowAllWork = Parenthowallwork::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentHowAllWork as $v){
            $newParentHowAllWork = new Parenthowallwork();
            $newParentHowAllWork->url = $v->url;
            $newParentHowAllWork->status = $v->status;
            $newParentHowAllWork->country_id = $country_id;
            $newParentHowAllWork->language_id = $language_id;
            $newParentHowAllWork->save(false);
        }

        $parentHowStart = Parenthowstart::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentHowStart as $v){
            $newParentHowStart = new Parenthowstart();
            $newParentHowStart->name = $v->name;
            $newParentHowStart->country_id = $country_id;
            $newParentHowStart->language_id = $language_id;
            $newParentHowStart->save(false);
        }

        $parentLaw = Parentlaw::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentLaw as $v){
            $newParentLaw = new Parentlaw();
            $newParentLaw->contenta = $v->contenta;
            $newParentLaw->contentb = $v->contentb;
            $newParentLaw->country_id = $country_id;
            $newParentLaw->language_id = $language_id;
            $newParentLaw->save(false);
        }

        $parentProgram = Parentprogram::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentProgram as $v){
            $newParentProgram = new Parentprogram();
            $newParentProgram->name = $v->name;
            $newParentProgram->image = $v->image;
            $newParentProgram->buttonName = $v->buttonName;
            $newParentProgram->url = $v->url;
            $newParentProgram->status = $v->status;
            $newParentProgram->country_id = $country_id;
            $newParentProgram->language_id = $language_id;
            $newParentProgram->save(false);
        }

        $parentStory = Parentstory::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentStory as $v){
            $newParentStory = new Parentstory();
            $newParentStory->name = $v->name;
            $newParentStory->city = $v->city;
            $newParentStory->contenta = $v->contenta;
            $newParentStory->contentb = $v->contentb;
            $newParentStory->country_id = $country_id;
            $newParentStory->language_id = $language_id;
            $newParentStory->save(false);
        }


        $parentWhyKazak = Parentwhykazak::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($parentWhyKazak as $v){
            $newParentWhyKazak = new Parentwhykazak();
            $newParentWhyKazak->name = $v->name;
            $newParentWhyKazak->image = $v->image;
            $newParentWhyKazak->country_id = $country_id;
            $newParentWhyKazak->language_id = $language_id;
            $newParentWhyKazak->save(false);
        }







        //Surrogacy
        $arrForSurrogacy = array();
        $surrogacyCost= Surrogacycost::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyCost as $v){
            $newSurrogacyCost = new Surrogacycost();
            $newSurrogacyCost->name = $v->name;
            $newSurrogacyCost->countStar = $v->countStar;
            $newSurrogacyCost->content = $v->content;
            $newSurrogacyCost->price = $v->price;
            $newSurrogacyCost->statusPriceGuest = $v->statusPriceGuest;
            $newSurrogacyCost->statusPriceUser = $v->statusPriceUser;
            $newSurrogacyCost->package_title = $v->package_title;
            $newSurrogacyCost->package_subtitle = $v->package_subtitle;
            $newSurrogacyCost->package_content = $v->package_content;
            $newSurrogacyCost->package_tabletitle = $v->package_tabletitle;
            $newSurrogacyCost->country_id = $country_id;
            $newSurrogacyCost->language_id = $language_id;
            $newSurrogacyCost->save(false);
            $arrForSurrogacy[$v->id] = $newSurrogacyCost->id;
        }



        $surrogacyCostContent= Surrogacycostcon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyCostContent as $v){
            $newSurrogacyCostContent = new Surrogacycostcon();
            $newSurrogacyCostContent->content = $v->content;
            $newSurrogacyCostContent->country_id = $country_id;
            $newSurrogacyCostContent->language_id = $language_id;
            $newSurrogacyCostContent->save(false);
        }


        $surrogacyCostType= Surrogacycosttype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyCostType as $v){
            $newSurrogacyCostType = new Surrogacycosttype();
            $newSurrogacyCostType->name = $v->name;
            $newSurrogacyCostType->content = $v->content;
            $newSurrogacyCostType->surrogacycost_id = $arrForSurrogacy[$v->surrogacycost_id];
            $newSurrogacyCostType->country_id = $country_id;
            $newSurrogacyCostType->language_id = $language_id;
            $newSurrogacyCostType->save(false);
        }

        $arrForSurrogacyStage = array();
        $surrogacyCostStage= Surrogacycoststage::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyCostStage as $v){
            $newSurrogacyCostStage = new Surrogacycoststage();
            $newSurrogacyCostStage->name = $v->name;
            $newSurrogacyCostStage->content = $v->content;
            $newSurrogacyCostStage->cost_id = $arrForSurrogacy[$v->cost_id];
            $newSurrogacyCostStage->country_id = $country_id;
            $newSurrogacyCostStage->language_id = $language_id;
            $newSurrogacyCostStage->save(false);
            $arrForSurrogacyStage[$v->id] = $newSurrogacyCostStage->id;
        }




        $surrogacyDatabase= Surrogacydatabase::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyDatabase as $v){
            $newSurrogacyDatabase = new Surrogacydatabase();
            $newSurrogacyDatabase->content = $v->content;
            $newSurrogacyDatabase->buttonName = $v->buttonName;
            $newSurrogacyDatabase->status = $v->status;
            $newSurrogacyDatabase->country_id = $country_id;
            $newSurrogacyDatabase->language_id = $language_id;
            $newSurrogacyDatabase->save(false);
        }

        $surrogacyProcess= Surrogacyprocess::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyProcess as $v){
            $newSurrogacyProcess = new Surrogacyprocess();
            $newSurrogacyProcess->name = $v->name;
            $newSurrogacyProcess->day = $v->day;
            $newSurrogacyProcess->content = $v->content;
            $newSurrogacyProcess->country_id = $country_id;
            $newSurrogacyProcess->language_id = $language_id;
            $newSurrogacyProcess->save(false);
        }

        $surrogacyProgramContent= Surrogacyprogramcon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyProgramContent as $v){
            $newSurrogacyProgramContent = new Surrogacyprogramcon();
            $newSurrogacyProgramContent->content = $v->content;
            $newSurrogacyProgramContent->country_id = $country_id;
            $newSurrogacyProgramContent->language_id = $language_id;
            $newSurrogacyProgramContent->save(false);
        }

        $surrogacyProgramType= Surrogacyprogramtype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyProgramType as $v){
            $newSurrogacyProgramType= new Surrogacyprogramtype();
            $newSurrogacyProgramType->name = $v->name;
            $newSurrogacyProgramType->content = $v->content;
            $newSurrogacyProgramType->countStar = $v->countStar;
            $newSurrogacyProgramType->country_id = $country_id;
            $newSurrogacyProgramType->language_id = $language_id;
            $newSurrogacyProgramType->save(false);
        }

        $surrogacySchool= Surrogacyschool::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacySchool as $v){
            $newSurrogacySchool= new Surrogacyschool();
            $newSurrogacySchool->contenta = $v->contenta;
            $newSurrogacySchool->image = $v->image;
            $newSurrogacySchool->contentb = $v->contentb;
            $newSurrogacySchool->contentc = $v->contentc;
            $newSurrogacySchool->country_id = $country_id;
            $newSurrogacySchool->language_id = $language_id;
            $newSurrogacySchool->save(false);
        }

        $surrogacyService= Surrogacyservice::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogacyService as $v){
            $newSurrogacyService= new Surrogacyservice();
            $newSurrogacyService->content = $v->content;
            $newSurrogacyService->image = $v->image;
            $newSurrogacyService->country_id = $country_id;
            $newSurrogacyService->language_id = $language_id;
            $newSurrogacyService->save(false);
        }






        // EGG DONATION
        $arrForDonation = array();
        $donationcost= Donationcost::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationcost as $v){
            $newDonationcost= new Donationcost();
            $newDonationcost->name = $v->name;
            $newDonationcost->content = $v->content;
            $newDonationcost->price = $v->price;
            $newDonationcost->statusPriceGuest = $v->statusPriceGuest;
            $newDonationcost->statusPriceUser = $v->statusPriceUser;
            $newDonationcost->package_title = $v->package_title;
            $newDonationcost->package_subtitle = $v->package_subtitle;
            $newDonationcost->package_content = $v->package_content;
            $newDonationcost->package_tabletitle = $v->package_tabletitle;
            $newDonationcost->country_id = $country_id;
            $newDonationcost->language_id = $language_id;
            $newDonationcost->save(false);
            $arrForDonation[$v->id] = $newDonationcost->id;
        }


        $donationcostcon= Donationcostcon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationcostcon as $v){
            $newDonationcostcon= new Donationcostcon();
            $newDonationcostcon->content = $v->content;
            $newDonationcostcon->country_id = $country_id;
            $newDonationcostcon->language_id = $language_id;
            $newDonationcostcon->save(false);
        }

        $donationCostType= Donationcosttype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationCostType as $v){
            $newDonationCostType= new Donationcosttype();
            $newDonationCostType->name = $v->name;
            $newDonationCostType->content = $v->content;
            $newDonationCostType->donationcost_id = $arrForDonation[$v->donationcost_id];
            $newDonationCostType->country_id = $country_id;
            $newDonationCostType->language_id = $language_id;
            $newDonationCostType->save(false);
        }


        $arrForDonationStage = array();
        $donationCostStage= Donationcoststage::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationCostStage as $v){
            $newDonationCostStage = new Donationcoststage();
            $newDonationCostStage->name = $v->name;
            $newDonationCostStage->content = $v->content;
            $newDonationCostStage->cost_id = $arrForDonation[$v->cost_id];
            $newDonationCostStage->country_id = $country_id;
            $newDonationCostStage->language_id = $language_id;
            $newDonationCostStage->save(false);
            $arrForDonationStage[$v->id] = $newDonationCostStage->id;
        }




        $donationHowStart= Donationhowstart::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationHowStart as $v){
            $newDonationHowStart= new Donationhowstart();
            $newDonationHowStart->name = $v->name;
            $newDonationHowStart->country_id = $country_id;
            $newDonationHowStart->language_id = $language_id;
            $newDonationHowStart->save(false);
        }

        $donationLast= Donationlast::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationLast as $v){
            $newDonationLast= new Donationlast();
            $newDonationLast->name = $v->name;
            $newDonationLast->buttonName = $v->buttonName;
            $newDonationLast->image = $v->image;
            $newDonationLast->country_id = $country_id;
            $newDonationLast->language_id = $language_id;
            $newDonationLast->save(false);
        }

        $donationProgramContent = Donationprogramcon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationProgramContent as $v){
            $newDonationProgramContent =  new Donationprogramcon();
            $newDonationProgramContent->content = $v->content;
            $newDonationProgramContent->country_id = $country_id;
            $newDonationProgramContent->language_id = $language_id;
            $newDonationProgramContent->save(false);
        }

        $donationProgramType = Donationprogramtype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donationProgramType as $v){
            $newDonationProgramType =  new Donationprogramtype();
            $newDonationProgramType->name = $v->name;
            $newDonationProgramType->image = $v->image;
            $newDonationProgramType->content = $v->content;
            $newDonationProgramType->country_id = $country_id;
            $newDonationProgramType->language_id = $language_id;
            $newDonationProgramType->save(false);
        }










        // Biotransportation
        $biotransContent = Biotranscon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($biotransContent as $v){
            $newBiotransContent =  new Biotranscon();
            $newBiotransContent->title = $v->title;
            $newBiotransContent->content = $v->content;
            $newBiotransContent->country_id = $country_id;
            $newBiotransContent->language_id = $language_id;
            $newBiotransContent->save(false);
        }

        $biotransCost = Biotranscost::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($biotransCost as $v){
            $newBiotransCost =  new Biotranscost();
            $newBiotransCost->name = $v->name;
            $newBiotransCost->price = $v->price;
            $newBiotransCost->image = $v->image;
            $newBiotransCost->buttonName = $v->buttonName;
            $newBiotransCost->country_id = $country_id;
            $newBiotransCost->language_id = $language_id;
            $newBiotransCost->save(false);
        }

        $biotransFact = Biotransfact::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($biotransFact as $v){
            $newBiotransFact =  new Biotransfact();
            $newBiotransFact->content = $v->content;
            $newBiotransFact->image = $v->image;
            $newBiotransFact->country_id = $country_id;
            $newBiotransFact->language_id = $language_id;
            $newBiotransFact->save(false);
        }

        $biotransProcess = Biotransprocess::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($biotransProcess as $v){
            $newBiotransProcess =  new Biotransprocess();
            $newBiotransProcess->name = $v->name;
            $newBiotransProcess->imagea = $v->imagea;
            $newBiotransProcess->imageb = $v->imageb;
            $newBiotransProcess->country_id = $country_id;
            $newBiotransProcess->language_id = $language_id;
            $newBiotransProcess->save(false);
        }










        //Surrogate
        $surrogateabout = Surrogateabout::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateabout as $v){
            $newSurrogateAbout =  new Surrogateabout();
            $newSurrogateAbout->name = $v->name;
            $newSurrogateAbout->content = $v->content;
            $newSurrogateAbout->contenta = $v->contenta;
            $newSurrogateAbout->contentb = $v->contentb;
            $newSurrogateAbout->image = $v->image;
            $newSurrogateAbout->country_id = $country_id;
            $newSurrogateAbout->language_id = $language_id;
            $newSurrogateAbout->save(false);
        }

        $surrogateBenefit = Surrogatebeneft::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateBenefit as $v){
            $newSurrogateBenefit =  new Surrogatebeneft();
            $newSurrogateBenefit->name = $v->name;
            $newSurrogateBenefit->country_id = $country_id;
            $newSurrogateBenefit->language_id = $language_id;
            $newSurrogateBenefit->save(false);
        }

        $surrogateComp = Surrogatecomp::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateComp as $v){
            $newSurrogateComp =  new Surrogatecomp();
            $newSurrogateComp->name = $v->name;
            $newSurrogateComp->contenta = $v->contenta;
            $newSurrogateComp->contentb = $v->contentb;
            $newSurrogateComp->contentc = $v->contentc;
            $newSurrogateComp->country_id = $country_id;
            $newSurrogateComp->language_id = $language_id;
            $newSurrogateComp->save(false);
        }

        $surrogateFAQ = Surrogatefaq::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateFAQ as $v){
            $newSurrogateFAQ =  new Surrogatefaq();
            $newSurrogateFAQ->name = $v->name;
            $newSurrogateFAQ->content = $v->content;
            $newSurrogateFAQ->country_id = $country_id;
            $newSurrogateFAQ->language_id = $language_id;
            $newSurrogateFAQ->save(false);
        }

        $surrogateForeignContent = Surrogateforeigncon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateForeignContent as $v){
            $newSurrogateForeignContent =  new Surrogateforeigncon();
            $newSurrogateForeignContent->content = $v->content;
            $newSurrogateForeignContent->country_id = $country_id;
            $newSurrogateForeignContent->language_id = $language_id;
            $newSurrogateForeignContent->save(false);
        }

        $surrogateForeignType = Surrogateforeigntype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateForeignType as $v){
            $newSurrogateForeignType =  new Surrogateforeigntype();
            $newSurrogateForeignType->image = $v->image;
            $newSurrogateForeignType->content = $v->content;
            $newSurrogateForeignType->country_id = $country_id;
            $newSurrogateForeignType->language_id = $language_id;
            $newSurrogateForeignType->save(false);
        }


        $surrogateHowAllWork = Surrogatehowallwork::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateHowAllWork as $v){
            $newSurrogateHowAllWork =  new Surrogatehowallwork();
            $newSurrogateHowAllWork->url = $v->url;
            $newSurrogateHowAllWork->status = $v->status;
            $newSurrogateHowAllWork->country_id = $country_id;
            $newSurrogateHowAllWork->language_id = $language_id;
            $newSurrogateHowAllWork->save(false);
        }


        $surrogateHowStart = Surrogatehowstart::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateHowStart as $v){
            $newSurrogateHowStart =  new Surrogatehowstart();
            $newSurrogateHowStart->name = $v->name;
            $newSurrogateHowStart->country_id = $country_id;
            $newSurrogateHowStart->language_id = $language_id;
            $newSurrogateHowStart->save(false);
        }

        $surrogatePsyc = Surrogatepsyc::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogatePsyc as $v){
            $newSurrogatePsyc =  new Surrogatepsyc();
            $newSurrogatePsyc->image = $v->image;
            $newSurrogatePsyc->content = $v->content;
            $newSurrogatePsyc->contenta = $v->contenta;
            $newSurrogatePsyc->contentb = $v->contentb;
            $newSurrogatePsyc->country_id = $country_id;
            $newSurrogatePsyc->language_id = $language_id;
            $newSurrogatePsyc->save(false);
        }

        $surrogateRequire = Surrogaterequire::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateRequire as $v){
            $newSurrogateRequire =  new Surrogaterequire();
            $newSurrogateRequire->name = $v->name;
            $newSurrogateRequire->content = $v->content;
            $newSurrogateRequire->country_id = $country_id;
            $newSurrogateRequire->language_id = $language_id;
            $newSurrogateRequire->save(false);
        }

        $surrogateSchool = Surrogateschool::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateSchool as $v){
            $newSurrogateSchool =  new Surrogateschool();
            $newSurrogateSchool->image = $v->image;
            $newSurrogateSchool->contenta = $v->contenta;
            $newSurrogateSchool->contentb = $v->contentb;
            $newSurrogateSchool->contentc = $v->contentc;
            $newSurrogateSchool->contentd = $v->contentd;
            $newSurrogateSchool->contente = $v->contente;
            $newSurrogateSchool->country_id = $country_id;
            $newSurrogateSchool->language_id = $language_id;
            $newSurrogateSchool->save(false);
        }


        $surrogateService = Surrogateservice::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateService as $v){
            $newSurrogateService =  new Surrogateservice();
            $newSurrogateService->image = $v->image;
            $newSurrogateService->contenta = $v->contenta;
            $newSurrogateService->contentb = $v->contentb;
            $newSurrogateService->content = $v->content;
            $newSurrogateService->country_id = $country_id;
            $newSurrogateService->language_id = $language_id;
            $newSurrogateService->save(false);
        }


        $surrogateStage = Surrogatestage::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateStage as $v){
            $newSurrogateStage =  new Surrogatestage();
            $newSurrogateStage->name = $v->name;
            $newSurrogateStage->content = $v->content;
            $newSurrogateStage->country_id = $country_id;
            $newSurrogateStage->language_id = $language_id;
            $newSurrogateStage->save(false);
        }


        $surrogateStageCon = Surrogatestagecon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateStageCon as $v){
            $newSurrogateStageCon =  new Surrogatestagecon();
            $newSurrogateStageCon->content = $v->content;
            $newSurrogateStageCon->country_id = $country_id;
            $newSurrogateStageCon->language_id = $language_id;
            $newSurrogateStageCon->save(false);
        }



        $surrogateStory = Surrogatestory::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($surrogateStory as $v){
            $newSurrogateStory =  new Surrogatestory();
            $newSurrogateStory->name = $v->name;
            $newSurrogateStory->city = $v->city;
            $newSurrogateStory->contenta = $v->contenta;
            $newSurrogateStory->contentb = $v->contentb;
            $newSurrogateStory->country_id = $country_id;
            $newSurrogateStory->language_id = $language_id;
            $newSurrogateStory->save(false);
        }










        // EGG DONOR
        $donorabout = Donorabout::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorabout as $v){
            $newDonorabout =  new Donorabout();
            $newDonorabout->content = $v->content;
            $newDonorabout->image = $v->image;
            $newDonorabout->contenta = $v->contenta;
            $newDonorabout->contentb = $v->contentb;
            $newDonorabout->country_id = $country_id;
            $newDonorabout->language_id = $language_id;
            $newDonorabout->save(false);
        }

        $donorBenefit = Donorbeneft::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorBenefit as $v){
            $newDonorBenefit =  new Donorbeneft();
            $newDonorBenefit->name = $v->name;
            $newDonorBenefit->country_id = $country_id;
            $newDonorBenefit->language_id = $language_id;
            $newDonorBenefit->save(false);
        }

        $donorComp = Donorcomp::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorComp as $v){
            $newDonorComp =  new Donorcomp();
            $newDonorComp->contenta = $v->contenta;
            $newDonorComp->contentb = $v->contentb;
            $newDonorComp->country_id = $country_id;
            $newDonorComp->language_id = $language_id;
            $newDonorComp->save(false);
        }

        $donorFAQ = Donorfaq::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorFAQ as $v){
            $newDonorFAQ =  new Donorfaq();
            $newDonorFAQ->name = $v->name;
            $newDonorFAQ->content = $v->content;
            $newDonorFAQ->country_id = $country_id;
            $newDonorFAQ->language_id = $language_id;
            $newDonorFAQ->save(false);
        }

        $donorForeignContent = Donorforeigncon::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorForeignContent as $v){
            $newDonorForeignContent =  new Donorforeigncon();
            $newDonorForeignContent->content = $v->content;
            $newDonorForeignContent->country_id = $country_id;
            $newDonorForeignContent->language_id = $language_id;
            $newDonorForeignContent->save(false);
        }

        $donorForeignType = Donorforeigntype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorForeignType as $v){
            $newDonorForeignType =  new Donorforeigntype();
            $newDonorForeignType->image = $v->image;
            $newDonorForeignType->content = $v->content;
            $newDonorForeignType->country_id = $country_id;
            $newDonorForeignType->language_id = $language_id;
            $newDonorForeignType->save(false);
        }

        $donorHowAllWork = Donorhowallwork::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorHowAllWork as $v){
            $newDonorHowAllWork =  new Donorhowallwork();
            $newDonorHowAllWork->url = $v->url;
            $newDonorHowAllWork->status = $v->status;
            $newDonorHowAllWork->country_id = $country_id;
            $newDonorHowAllWork->language_id = $language_id;
            $newDonorHowAllWork->save(false);
        }


        $donorHowStart = Donorhowstart::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorHowStart as $v){
            $newDonorHowStart =  new Donorhowstart();
            $newDonorHowStart->name = $v->name;
            $newDonorHowStart->country_id = $country_id;
            $newDonorHowStart->language_id = $language_id;
            $newDonorHowStart->save(false);
        }

        $donorRequire = Donorrequire::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorRequire as $v){
            $newDonorRequire =  new Donorrequire();
            $newDonorRequire->name = $v->name;
            $newDonorRequire->content = $v->content;
            $newDonorRequire->country_id = $country_id;
            $newDonorRequire->language_id = $language_id;
            $newDonorRequire->save(false);
        }

        $donorStage = Donorstage::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorStage as $v){
            $newDonorStage =  new Donorstage();
            $newDonorStage->name = $v->name;
            $newDonorStage->content = $v->content;
            $newDonorStage->country_id = $country_id;
            $newDonorStage->language_id = $language_id;
            $newDonorStage->save(false);
        }

        $donorStory = Donorstory::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($donorStory as $v){
            $newDonorStory =  new Donorstory();
            $newDonorStory->name = $v->name;
            $newDonorStory->city =  $v->city;
            $newDonorStory->contenta = $v->contenta;
            $newDonorStory->contentb = $v->contentb;
            $newDonorStory->country_id = $country_id;
            $newDonorStory->language_id = $language_id;
            $newDonorStory->save(false);
        }











        //NEWS & EVENTS
        $blog = Blog::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($blog as $v){
            $newBlog =  new Blog();
            $newBlog->name = $v->name;
            $newBlog->image = $v->image;
            $newBlog->content = $v->content;
            $newBlog->date = $v->date;
            $newBlog->tag = $v->tag;
            $newBlog->metaName = $v->metaName;
            $newBlog->metaDesc = $v->metaDesc;
            $newBlog->metaKey = $v->metaKey;
            $newBlog->country_id = $country_id;
            $newBlog->language_id = $language_id;
            $newBlog->save(false);
        }










        //ARTICLES
        $article = Article::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($article as $v){
            $newArticle =  new Article();
            $newArticle->name = $v->name;
            $newArticle->image = $v->image;
            $newArticle->content = $v->content;
            $newArticle->date = $v->date;
            $newArticle->tag = $v->tag;
            $newArticle->metaName = $v->metaName;
            $newArticle->metaDesc = $v->metaDesc;
            $newArticle->metaKey = $v->metaKey;
            $newArticle->country_id = $country_id;
            $newArticle->language_id = $language_id;
            $newArticle->save(false);
        }








        //PHOTO GALLERY
        $arrForPhoto = array();
        $photo = Photo::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($photo as $v){
            $newPhoto =  new Photo();
            $newPhoto->name = $v->name;
            $newPhoto->image = $v->image;
            $newPhoto->country_id = $country_id;
            $newPhoto->language_id = $language_id;
            $newPhoto->save(false);
            $arrForPhoto[$v->id] = $newPhoto->id;
        }

        $photoType = Phototype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($photoType as $v){
            $newPhotoType =  new Phototype();
            $newPhotoType->name = $v->name;
            $newPhotoType->image = $v->image;
            $newPhotoType->content = $v->content;
            $newPhotoType->photo_id = $arrForPhoto[$v->photo_id];
            $newPhotoType->country_id = $country_id;
            $newPhotoType->language_id = $language_id;
            $newPhotoType->save(false);
        }











        //VIDEO GALLERY
        $arrForVideo = array();
        $video = Video::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($video as $v){
            $newVideo =  new Video();
            $newVideo->name = $v->name;
            $newVideo->image = $v->image;
            $newVideo->country_id = $country_id;
            $newVideo->language_id = $language_id;
            $newVideo->save(false);
            $arrForVideo[$v->id] = $newVideo->id;
        }

        $videoType = Videotype::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($videoType as $v){
            $newVideoType =  new Videotype();
            $newVideoType->name = $v->name;
            $newVideoType->url = $v->url;
            $newVideoType->content = $v->content;
            $newVideoType->video_id = $arrForVideo[$v->video_id];
            $newVideoType->country_id = $country_id;
            $newVideoType->language_id = $language_id;
            $newVideoType->save(false);
        }





        //YOUR TRIP TO ALMATY
        $arrForTrip = array();
        $trip = Ifstrip::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($trip as $v){
            $newTrip =  new Ifstrip();
            $newTrip->name = $v->name;
            $newTrip->content = $v->content;
            $newTrip->country_id = $country_id;
            $newTrip->language_id = $language_id;
            $newTrip->save(false);
            $arrForTrip[$v->id] = $newTrip->id;
        }

        $tripPictures = Ifstrippic::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($tripPictures as $v){
            $newTripPictures =  new Ifstrippic();
            $newTripPictures->image = $v->image;
            $newTripPictures->ifstrip_id = $arrForTrip[$v->ifstrip_id];
            $newTripPictures->country_id = $country_id;
            $newTripPictures->language_id = $language_id;
            $newTripPictures->save(false);
        }





        // Translation
        $translation = Translation::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($translation as $v){
            $newTranslation =  new Translation();
            $newTranslation->name = $v->name;
            if($arrForMenu[$v->page_id] == null){
                $arrForMenu[$v->page_id] = 0;
            }
            $newTranslation->page_id = $arrForMenu[$v->page_id];
            $newTranslation->country_id = $country_id;
            $newTranslation->language_id = $language_id;
            $newTranslation->save(false);
        }




        // PAGE NOT FOUND
        $pageNotFound = Pagenotfound::find()->where('country_id=1 AND language_id=1')->all();
        foreach ($pageNotFound as $v){
            $newPageNotFound =  new Pagenotfound();
            $newPageNotFound->name = $v->name;
            $newPageNotFound->content = $v->content;
            $newPageNotFound->country_id = $country_id;
            $newPageNotFound->language_id = $language_id;
            $newPageNotFound->save(false);
        }


    }






    public static function deleteLanguageContent($country_id, $language_id){

        Menu::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Submenu::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Contact::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Banner::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Buttons::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        //HOME
        Mainslide::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Mainprogress::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // IFS
        Ifsdiff::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifsfacts::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifsmedteam::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifsteam::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifscontact::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifsabout::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifsservice::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // PARENT
        Parentbenefit::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentdoctor::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentfaq::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parenthowallwork::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parenthowstart::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentlaw::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentprogram::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentstory::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Parentwhykazak::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // Surrogacy
        Surrogacycost::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacycostcon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacycosttype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacydatabase::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacyprocess::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacyprogramcon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacyprogramtype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacyschool::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacyservice::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogacycoststage::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);




        // EGG DONATION
        Donationcost::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationcostcon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationcosttype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationhowstart::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationlast::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationprogramcon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationprogramtype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donationcoststage::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // BIOTRANSPORTATION
        Biotranscon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Biotranscost::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Biotransfact::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Biotransprocess::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // SURROGATE
        Surrogateabout::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatebeneft::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatecomp::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatefaq::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogateforeigncon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogateforeigntype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatehowallwork::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatehowstart::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatepsyc::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogaterequire::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogateschool::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogateservice::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatestage::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatestagecon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Surrogatestory::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // EGG DONOR
        Donorabout::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorbeneft::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorcomp::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorfaq::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorforeigncon::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorforeigntype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorhowallwork::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorhowstart::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorrequire::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorstage::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Donorstory::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // NEWS & EVENTS
        Blog::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // ARTICLES
        Article::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // PHOTO GALLERY
        Photo::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Phototype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // Video GALLERY
        Video::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Videotype::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);



        // YOUR TRIP TO ALMATY
        Ifstrip::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);
        Ifstrippic::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // Translation
        Translation::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);


        // PAGE NOT FOUND
        Pagenotfound::deleteAll('country_id='.$country_id.' AND language_id='.$language_id);

    }


    public static function deleteCountryContent($country_id){

        Menu::deleteAll('country_id='.$country_id);
        Submenu::deleteAll('country_id='.$country_id);
        Contact::deleteAll('country_id='.$country_id);
        Banner::deleteAll('country_id='.$country_id);
        Buttons::deleteAll('country_id='.$country_id);



        // HOME
        Mainslide::deleteAll('country_id='.$country_id);
        Mainprogress::deleteAll('country_id='.$country_id);



        // IFS
        Ifsdiff::deleteAll('country_id='.$country_id);
        Ifsfacts::deleteAll('country_id='.$country_id);
        Ifsmedteam::deleteAll('country_id='.$country_id);
        Ifsteam::deleteAll('country_id='.$country_id);
        Ifscontact::deleteAll('country_id='.$country_id);
        Ifsabout::deleteAll('country_id='.$country_id);
        Ifsservice::deleteAll('country_id='.$country_id);



        // PARENT
        Parentbenefit::deleteAll('country_id='.$country_id);
        Parentdoctor::deleteAll('country_id='.$country_id);
        Parentfaq::deleteAll('country_id='.$country_id);
        Parenthowallwork::deleteAll('country_id='.$country_id);
        Parenthowstart::deleteAll('country_id='.$country_id);
        Parentlaw::deleteAll('country_id='.$country_id);
        Parentprogram::deleteAll('country_id='.$country_id);
        Parentstory::deleteAll('country_id='.$country_id);
        Parentwhykazak::deleteAll('country_id='.$country_id);



        // Surrogacy
        Surrogacycost::deleteAll('country_id='.$country_id);
        Surrogacycostcon::deleteAll('country_id='.$country_id);
        Surrogacycosttype::deleteAll('country_id='.$country_id);
        Surrogacydatabase::deleteAll('country_id='.$country_id);
        Surrogacyprocess::deleteAll('country_id='.$country_id);
        Surrogacyprogramcon::deleteAll('country_id='.$country_id);
        Surrogacyprogramtype::deleteAll('country_id='.$country_id);
        Surrogacyschool::deleteAll('country_id='.$country_id);
        Surrogacyservice::deleteAll('country_id='.$country_id);
        Surrogacycoststage::deleteAll('country_id='.$country_id);



        // EGG DONATION
        Donationcost::deleteAll('country_id='.$country_id);
        Donationcostcon::deleteAll('country_id='.$country_id);
        Donationcosttype::deleteAll('country_id='.$country_id);
        Donationhowstart::deleteAll('country_id='.$country_id);
        Donationlast::deleteAll('country_id='.$country_id);
        Donationprogramcon::deleteAll('country_id='.$country_id);
        Donationprogramtype::deleteAll('country_id='.$country_id);
        Donationcoststage::deleteAll('country_id='.$country_id);



        // Biotransportation
        Biotranscon::deleteAll('country_id='.$country_id);
        Biotranscost::deleteAll('country_id='.$country_id);
        Biotransfact::deleteAll('country_id='.$country_id);
        Biotransprocess::deleteAll('country_id='.$country_id);



        // SURROGATE
        Surrogateabout::deleteAll('country_id='.$country_id);
        Surrogatebeneft::deleteAll('country_id='.$country_id);
        Surrogatecomp::deleteAll('country_id='.$country_id);
        Surrogatefaq::deleteAll('country_id='.$country_id);
        Surrogateforeigncon::deleteAll('country_id='.$country_id);
        Surrogateforeigntype::deleteAll('country_id='.$country_id);
        Surrogatehowallwork::deleteAll('country_id='.$country_id);
        Surrogatehowstart::deleteAll('country_id='.$country_id);
        Surrogatepsyc::deleteAll('country_id='.$country_id);
        Surrogaterequire::deleteAll('country_id='.$country_id);
        Surrogateschool::deleteAll('country_id='.$country_id);
        Surrogateservice::deleteAll('country_id='.$country_id);
        Surrogatestage::deleteAll('country_id='.$country_id);
        Surrogatestagecon::deleteAll('country_id='.$country_id);
        Surrogatestory::deleteAll('country_id='.$country_id);



        // EGG DONOR
        Donorabout::deleteAll('country_id='.$country_id);
        Donorbeneft::deleteAll('country_id='.$country_id);
        Donorcomp::deleteAll('country_id='.$country_id);
        Donorfaq::deleteAll('country_id='.$country_id);
        Donorforeigncon::deleteAll('country_id='.$country_id);
        Donorforeigntype::deleteAll('country_id='.$country_id);
        Donorhowallwork::deleteAll('country_id='.$country_id);
        Donorhowstart::deleteAll('country_id='.$country_id);
        Donorrequire::deleteAll('country_id='.$country_id);
        Donorstage::deleteAll('country_id='.$country_id);
        Donorstory::deleteAll('country_id='.$country_id);



        // NEWS & EVENTS
        Blog::deleteAll('country_id='.$country_id);


        // ARTICLES
        Article::deleteAll('country_id='.$country_id);


        // PHOTO GALLERY
        Photo::deleteAll('country_id='.$country_id);
        Phototype::deleteAll('country_id='.$country_id);


        // Video GALLERY
        Video::deleteAll('country_id='.$country_id);
        Videotype::deleteAll('country_id='.$country_id);


        // YOUR TRIP TO ALMATY
        Ifstrip::deleteAll('country_id='.$country_id);
        Ifstrippic::deleteAll('country_id='.$country_id);


        // Translation
        Translation::deleteAll('country_id='.$country_id);


        // PAGE NOT FOUND
        Pagenotfound::deleteAll('country_id='.$country_id);

    }







    public static function deleteLangContent($language_id){

        Menu::deleteAll('language_id='.$language_id);
        Submenu::deleteAll('language_id='.$language_id);
        Contact::deleteAll('language_id='.$language_id);
        Banner::deleteAll('language_id='.$language_id);
        Buttons::deleteAll('language_id='.$language_id);



        // HOME
        Mainslide::deleteAll('language_id='.$language_id);
        Mainprogress::deleteAll('language_id='.$language_id);



        // IFS
        Ifsdiff::deleteAll('language_id='.$language_id);
        Ifsfacts::deleteAll('language_id='.$language_id);
        Ifsmedteam::deleteAll('language_id='.$language_id);
        Ifsteam::deleteAll('language_id='.$language_id);
        Ifscontact::deleteAll('language_id='.$language_id);
        Ifsabout::deleteAll('language_id='.$language_id);
        Ifsservice::deleteAll('language_id='.$language_id);



        // PARENT
        Parentbenefit::deleteAll('language_id='.$language_id);
        Parentdoctor::deleteAll('language_id='.$language_id);
        Parentfaq::deleteAll('language_id='.$language_id);
        Parenthowallwork::deleteAll('language_id='.$language_id);
        Parenthowstart::deleteAll('language_id='.$language_id);
        Parentlaw::deleteAll('language_id='.$language_id);
        Parentprogram::deleteAll('language_id='.$language_id);
        Parentstory::deleteAll('language_id='.$language_id);
        Parentwhykazak::deleteAll('language_id='.$language_id);



        // Surrogacy
        Surrogacycost::deleteAll('language_id='.$language_id);
        Surrogacycostcon::deleteAll('language_id='.$language_id);
        Surrogacycosttype::deleteAll('language_id='.$language_id);
        Surrogacydatabase::deleteAll('language_id='.$language_id);
        Surrogacyprocess::deleteAll('language_id='.$language_id);
        Surrogacyprogramcon::deleteAll('language_id='.$language_id);
        Surrogacyprogramtype::deleteAll('language_id='.$language_id);
        Surrogacyschool::deleteAll('language_id='.$language_id);
        Surrogacyservice::deleteAll('language_id='.$language_id);
        Surrogacycoststage::deleteAll('language_id='.$language_id);



        // EGG DONATION
        Donationcost::deleteAll('language_id='.$language_id);
        Donationcostcon::deleteAll('language_id='.$language_id);
        Donationcosttype::deleteAll('language_id='.$language_id);
        Donationhowstart::deleteAll('language_id='.$language_id);
        Donationlast::deleteAll('language_id='.$language_id);
        Donationprogramcon::deleteAll('language_id='.$language_id);
        Donationprogramtype::deleteAll('language_id='.$language_id);
        Donationcoststage::deleteAll('language_id='.$language_id);



        // Biotransportation
        Biotranscon::deleteAll('language_id='.$language_id);
        Biotranscost::deleteAll('language_id='.$language_id);
        Biotransfact::deleteAll('language_id='.$language_id);
        Biotransprocess::deleteAll('language_id='.$language_id);



        // SURROGATE
        Surrogateabout::deleteAll('language_id='.$language_id);
        Surrogatebeneft::deleteAll('language_id='.$language_id);
        Surrogatecomp::deleteAll('language_id='.$language_id);
        Surrogatefaq::deleteAll('language_id='.$language_id);
        Surrogateforeigncon::deleteAll('language_id='.$language_id);
        Surrogateforeigntype::deleteAll('language_id='.$language_id);
        Surrogatehowallwork::deleteAll('language_id='.$language_id);
        Surrogatehowstart::deleteAll('language_id='.$language_id);
        Surrogatepsyc::deleteAll('language_id='.$language_id);
        Surrogaterequire::deleteAll('language_id='.$language_id);
        Surrogateschool::deleteAll('language_id='.$language_id);
        Surrogateservice::deleteAll('language_id='.$language_id);
        Surrogatestage::deleteAll('language_id='.$language_id);
        Surrogatestagecon::deleteAll('language_id='.$language_id);
        Surrogatestory::deleteAll('language_id='.$language_id);



        // EGG DONOR
        Donorabout::deleteAll('language_id='.$language_id);
        Donorbeneft::deleteAll('language_id='.$language_id);
        Donorcomp::deleteAll('language_id='.$language_id);
        Donorfaq::deleteAll('language_id='.$language_id);
        Donorforeigncon::deleteAll('language_id='.$language_id);
        Donorforeigntype::deleteAll('language_id='.$language_id);
        Donorhowallwork::deleteAll('language_id='.$language_id);
        Donorhowstart::deleteAll('language_id='.$language_id);
        Donorrequire::deleteAll('language_id='.$language_id);
        Donorstage::deleteAll('language_id='.$language_id);
        Donorstory::deleteAll('language_id='.$language_id);



        // NEWS & EVENTS
        Blog::deleteAll('language_id='.$language_id);


        // ARTICLES
        Article::deleteAll('language_id='.$language_id);


        // PHOTO GALLERY
        Photo::deleteAll('language_id='.$language_id);
        Phototype::deleteAll('language_id='.$language_id);


        // Video GALLERY
        Video::deleteAll('language_id='.$language_id);
        Videotype::deleteAll('language_id='.$language_id);


        // YOUR TRIP TO ALMATY
        Ifstrip::deleteAll('language_id='.$language_id);
        Ifstrippic::deleteAll('language_id='.$language_id);


        // Translation
        Translation::deleteAll('language_id='.$language_id);


        // PAGE NOT FOUND
        Pagenotfound::deleteAll('language_id='.$language_id);

    }

    public static function deleteMonthContent($language_id)
    {
        Month::deleteAll('language_id='.$language_id);
    }
}
