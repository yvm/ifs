<?php
namespace common\models;

use Yii;

class Countrys extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'countrys';
    }

    public function rules()
    {
        return [
            [['id', 'name', 'sort'], 'required'],
            [['id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id1' => 'Id1',
            'id' => 'ID',
            'name' => 'Название',
            'sort' => 'Сортировка',
        ];
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Countrys::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }
}
