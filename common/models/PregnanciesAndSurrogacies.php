<?php
namespace common\models;

use Yii;

class PregnanciesAndSurrogacies extends \yii\db\ActiveRecord
{

    const surrogate = 'surrogate';
    const surrogate_name_of_clinic = 'surrogate_name_of_clinic';
    const surrogate_number_of_Miscarriage = 'surrogate_number_of_Miscarriage';
    const surrogate_numberMiscarriage_nameOfClinic = 'surrogate_numberMiscarriage_nameOfClinic';

    const eggDonor = 'eggDonor';
    const eggDonor_number_of_Miscarriage = 'eggDonor_number_of_Miscarriage';
    const eggDonor_name_of_clinic = 'eggDonor_name_of_clinic';
    const eggDonor_numberMiscarriage_nameOfClinic = 'eggDonor_numberMiscarriage_nameOfClinic';

    const surrogate_and_eggDonor = 'surrogate_and_eggDonor';
    const surrogate_and_eggDonor_number_of_Miscarriage = 'eggDonor_number_of_Miscarriage';
    const surrogate_and_eggDonor_name_of_clinic = 'eggDonor_name_of_clinic';
    const surrogate_and_eggDonor_numberMiscarriage_nameOfClinic = 'eggDonor_numberMiscarriage_nameOfClinic';
	
	 const profile = 'profile';


    public static function tableName()
    {
        return 'pregnancies_and_surrogacies';
    }

    public function rules()
    {
        return [
            [['user_id', 'are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies', 'how_many_children',
                'number_miscarriages','number_stillbirths','number_abortions','number_live_births','children_now','have_you_ever_egg_donor'], 'required'],
            [['user_id', 'are_you_pregnant', 'are_you_currently','have_you_ever_applied', 'have_you_ever', 'number_pregnancies', 'how_many_children', 'number_miscarriages', 'number_stillbirths', 'number_abortions', 'number_live_births', 'children_now'], 'integer'],
            [['list_each', 'if_any_of_your', 'please_list'], 'string'],
            [['created_at'], 'safe'],
            [['name_of_IVF','name_of_IVF_egg_donor'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['have_you_ever', 'number_pregnancies','have_you_ever_egg_donor'], 'yes_no'],
            [['number_pregnancies'], 'number_pregnancies'],
        ];
    }

    public function number_pregnancies($attribute,$params)
    {
        if($this->number_pregnancies != 0) {
            if($this->how_many_children + $this->number_miscarriages + $this->number_stillbirths +
                $this->children_now + $this->number_abortions + $this->number_live_births != $this->number_pregnancies)
                $this->addError("number_stillbirths", "В сумме должно быть ".$this->number_pregnancies.". У Вас получилось "
                    .($this->how_many_children + $this->number_miscarriages + $this->number_stillbirths +
                        $this->children_now + $this->number_abortions + $this->number_live_births));
        }
    }

    public function yes_no($attribute,$params)
    {
        if($this->have_you_ever != 0) {
            if (empty($this->name_of_IVF))
                $this->addError("name_of_IVF", "Необходимо указать name_of_IVF.");
        }

        if($this->have_you_ever_egg_donor != 0) {
            if (empty($this->name_of_IVF_egg_donor))
                $this->addError("name_of_IVF_egg_donor", "Необходимо указать name_of_IVF_egg_donor.");
        }

        if($this->number_pregnancies != 0) {
            if (empty($this->list_each))
                $this->addError("list_each", "Необходимо указать list_each.");
        }

    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'are_you_pregnant' => Yii::t('app', 'Are You Pregnant'),
            'are_you_currently' => Yii::t('app', 'Are You Currently'),
            'have_you_ever' => Yii::t('app', 'Have You Ever'),
            'name_of_IVF' => Yii::t('app', 'Name Of  Ivf'),
            'number_pregnancies' => Yii::t('app', 'Number Pregnancies'),
            'how_many_children' => Yii::t('app', 'How Many Children'),
            'number_miscarriages' => Yii::t('app', 'Number Miscarriages'),
            'number_stillbirths' => Yii::t('app', 'Number Stillbirths'),
            'number_abortions' => Yii::t('app', 'Number Abortions'),
            'number_live_births' => Yii::t('app', 'Number Live Births'),
            'children_now' => Yii::t('app', 'Children Now'),
            'list_each' => Yii::t('app', 'List Each'),
            'if_any_of_your' => Yii::t('app', 'If Any Of Your'),
            'please_list' => Yii::t('app', 'Please List'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function scenarios()
    {
        return [

            self::surrogate => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',  'list_each','name_of_IVF',
                'how_many_children', 'number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now'],
            self::surrogate_number_of_Miscarriage => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',
                'how_many_children', 'number_miscarriages','number_stillbirths','number_abortions','number_live_births',
                'children_now','list_each'],
            self::surrogate_name_of_clinic => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',
                'how_many_children', 'number_miscarriages','number_stillbirths','number_abortions','number_live_births',
                'children_now','name_of_IVF'],
            self::surrogate_numberMiscarriage_nameOfClinic => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',
                'how_many_children', 'number_miscarriages','number_stillbirths','number_abortions','number_live_births',
                'children_now'],


            self::eggDonor => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies', 'how_many_children',
                'number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now', 'list_each','name_of_IVF'],
            self::eggDonor_number_of_Miscarriage => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever','how_many_children',
                'number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now', 'number_pregnancies','name_of_IVF'],
            self::eggDonor_name_of_clinic => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',
                'how_many_children','number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now','list_each'],
            self::eggDonor_numberMiscarriage_nameOfClinic => ['user_id','are_you_pregnant', 'are_you_currently', 'have_you_ever', 'number_pregnancies',
                'how_many_children', 'number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now'],


            self::surrogate_and_eggDonor => ['user_id','are_you_pregnant', 'are_you_currently','have_you_ever_applied', 'have_you_ever', 'have_you_ever_egg_donor', 'number_pregnancies', 'how_many_children',
                'number_miscarriages','number_stillbirths','number_abortions','number_live_births', 'children_now', 'list_each','name_of_IVF', 'name_of_IVF_egg_donor'],
			
			self::profile => ['children_now'],


        ];
    }



}
