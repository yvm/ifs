<?php
namespace common\models;

use Yii;

class Surrogacyprocess extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacyprocess';
    }

    public function rules()
    {
        return [
            [['name', 'day', 'content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name', 'day'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'day' => Yii::t('app', 'Промежуток времени'),
            'content' => Yii::t('app', 'Содержания'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
