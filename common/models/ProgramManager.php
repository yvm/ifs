<?php
namespace common\models;

use Yii;

class ProgramManager extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'program_manager';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
