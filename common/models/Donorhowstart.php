<?php
namespace common\models;

use Yii;

class Donorhowstart extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'donorhowstart';
    }

    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Содержание',
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
