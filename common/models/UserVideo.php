<?php
namespace common\models;

use Yii;

class UserVideo extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_video';
    }

    public function rules()
    {
        return [
            [['user_id', 'video'], 'required'],
            [['user_id'], 'integer'],
            [['video'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'video' => Yii::t('app', 'Video'),
        ];
    }
}
