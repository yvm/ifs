<?php
namespace common\models;

use Yii;

class IfsForm extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'ifs_form';
    }

    public function rules()
    {
        return [
            [['firstname', 'lastname', 'role', 'mobile_phone_number', 'email', 'country', 'city', 'subject', 'message'], 'required'],
            [['role', 'type', 'country', 'city'], 'integer'],
            [['message'], 'string'],
            [['created_at'], 'safe'],
            [['firstname', 'lastname', 'mobile_phone_number', 'email', 'telegram', 'WatsApp', 'Skype', 'subject'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'firstname' => 'Firstname',
            'lastname' => 'Lastname',
            'role' => 'Role',
            'type' => 'Type',
            'mobile_phone_number' => 'Mobile Phone Number',
            'email' => 'Email',
            'telegram' => 'Telegram',
            'country' => 'Country',
            'WatsApp' => 'Wats App',
            'Skype' => 'Skype',
            'city' => 'City',
            'subject' => 'Subject',
            'message' => 'Message',
            'created_at' => 'Created At',
        ];
    }
}
