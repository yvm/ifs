<?php
namespace common\models;

use Yii;

class UserPhoto extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_photo';
    }

    public function rules()
    {
        return [
            [['user_id', 'photo'], 'required'],
            [['user_id'], 'integer'],
            [['photo'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'photo' => Yii::t('app', 'Photo'),
        ];
    }
}
