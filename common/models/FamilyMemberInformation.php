<?php
namespace common\models;

use Yii;

class FamilyMemberInformation extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'family_member_information';
    }

    public function rules()
    {
        return [
            [['user_id', 'relation', 'date_of_birth', 'living', 'body_type', 'other_details'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['relation', 'date_of_birth'], 'string', 'max' => 30],
            [['living'], 'string', 'max' => 15],
            [['body_type', 'other_details'], 'string', 'max' => 20],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'relation' => Yii::t('app', 'Relation'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'living' => Yii::t('app', 'Living'),
            'body_type' => Yii::t('app', 'Body Type'),
            'other_details' => Yii::t('app', 'Other Details'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
