<?php
namespace common\models;

use Yii;

class IfsFormFiles extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'ifs_form_files';
    }

    public function rules()
    {
        return [
            [['file'], 'required'],
            [['parent_id'], 'integer'],
            [['file'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file' => 'File',
            'parent_id' => 'Parent ID',
        ];
    }
}
