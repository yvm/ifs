<?php
namespace common\models;

use Yii;

class Surrogateschool extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogateschool';
    }

    public function rules()
    {
        return [
            [['contenta', 'contentb', 'contentc','contentd','contente', 'country_id', 'language_id'], 'required'],
            [['contenta', 'contentb', 'contentc','contentd','contente'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contenta' => Yii::t('app', 'Верхний контент'),
            'image' => Yii::t('app', 'Картинка'),
            'contentb' => Yii::t('app', 'Средний контент'),
            'contentc' => Yii::t('app', 'Нижний контент'),
            'contentd' => Yii::t('app', 'Видные содержания для мобильной версии'),
            'contente' => Yii::t('app', 'Скрытое содержания для мобильной версии'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogate/' . $this->image : '/no-image.png';
    }
}
