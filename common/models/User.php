<?php

namespace common\models;

use Illuminate\Database\Eloquent\Relations\HasOne;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\data\Pagination;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password
 * @property integer $isAdmin
 * @property string $photo
 *
 * @property Comment[] $comments
 */
class User extends \yii\db\ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = -1;
    const STATUS_READY   = 0;
    const STATUS_ACTIVE  = 1;

    const ROLE_GUEST     = 1;
    const ROLE_USER      = 2;
    const ROLE_AUTHOR    = 3;
    const ROLE_MANAGER   = 4;
    const ROLE_ADMIN     = 5;
    const ROLE_DEVELOPER = 20;

    const STEP1 = 'step1';
    const profile = 'profile';
    const update_password = 'update_password';
    const update_password_by_email = 'update_password_by_email';
    const admin = 'admin';
    const admin_no_psw = 'admin_no_psw';
	const GotIt = 'GotIt';
	
//    const RoleUser = [1 => 'Intended Parent', 2 => 'Surrogate', 3 => 'Egg Donor', 4 => 'Surrogate & Egg Donor',
//        5 => 'Biotransportation Client'];
//
//    const RoleIntendedParent = [1 => 'Surrogacy', 2 => 'Egg', 3 => 'Surrogacy & Egg'];

    public $password;
    public $old_password;
    public $password_repeat;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['active', 'got_it'], 'safe'],

            [['email'],'email','on'=>self::update_password_by_email],
            [['password', 'password_repeat'], 'required', 'on' => self::STEP1],
            [['password'], 'confirmpassword', 'on' => self::admin],
			[['password', 'password_repeat','old_password'], 'required', 'on' => self::update_password],
            ['password', 'string', 'min' => 8],
			['password', 'password_validate'],
            ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Password mismatch'],
        ];
    }

    public function password_validate($attribute,$params)
    {
        $pattern = '/^(?=.*[0-9])(?=.*[A-Z])(?=.*[a-z])[0-9A-Za-z!@$%^&*]{8,50}$/';
        preg_match($pattern, $this->password, $matches);
        if(!count($matches))
            $this->addError("password_repeat","Password must contains [0-9,[A-Z],[a-z]");
    }
	
    public function confirmpassword($attribute,$params)
    {
        if($this->password != $this->password_repeat)
            $this->addError("password_repeat","Пароли не совпадают");
    }

    public function scenarios()
    {
        return [
            self::STEP1 => ['password', 'password_repeat'],
            self::update_password => ['password', 'password_repeat','old_password'],
            self::profile => ['active'],
			self::GotIt => ['got_it'],
            self::update_password_by_email => ['email'],
            self::admin => ['username', 'email', 'status', 'active', 'role', 'type', 'step', 'password', 'password_repeat', 'profi', 'proven','status_edit_profile','status_comp','status_profile'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
            'email' => 'E-mail',
            'username' => 'Логин',
            'password' => 'Пароль',
            'password_repeat' => 'Подтвердите новый пароль',
            'status_profile' => 'Статус профиля',
            'active' => 'Статус для участие программу',
            'status_edit_profile'=> 'Статус для изменения профиля',
            'status_comp' => 'Компенсация',
            'role' => 'Роль',
            'step' => 'Шаг',
            'type' => 'Тип',
            'created_at' => 'Дата создание',
            'updated_at' => 'Дата редактирование',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if(isset($this->password)){
                    $this->setPassword($this->password);
                }elseif(empty($this->password_hash)) {
                    $this->setPassword(\Yii::$app->security->generateRandomString());
                }
                if ($this->status == self::STATUS_READY) {
                    $this->generateToken();
                }
                $this->generateAuthKey();
            }

            return true;
        }

        return false;
    }
    /**
     * Generates new password reset token
     */
    public function generateToken()
    {
        $this->access_token = \Yii::$app->security->generateRandomString().'_'.time();
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int) substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findByEmail($email)
    {
        return User::find()->where(['email'=>$email])->one();
    }

    public function create()
    {
        return $this->save(false);
    }

    public function saveFromVk($uid, $name, $photo)
    {
        $user = User::findOne($uid);
        if($user)
        {
            return Yii::$app->user->login($user);
        }

        $this->id = $uid;
        $this->name = $name;
        $this->photo = $photo;
        $this->create();

        return Yii::$app->user->login($this);
    }

    public function getImage()
    {
        return $this->photo;
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    public static function findByToken($token)
    {
//        if (!static::isTokenValid($token)) {
//            return null;
//        }

        return static::findOne(['access_token' => $token]);
    }

    public static function isTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire    = Settings::paramOf('user.password.reset.token.expire', 3600);

        return $timestamp + $expire >= time();
    }

    public function removeToken()
    {
        $this->access_token = null;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->status == self::STATUS_READY) {
                \Yii::$app->session->setFlash('success', 'Учетная запись зарегистрирована!');
                \Yii::$app->mailer
                    ->compose(['html' => 'signupConfirm-html', 'text' => 'signupConfirm-text'], ['user' => $this])
                    ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name.' robot'])
                    ->setTo($this->email)
                    ->setSubject('Подтверждение email для '.\Yii::$app->name)
                    ->send();
            }
        }
    }


    function randomPassword() {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $password = "";
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, strlen($alphabet)-1);
            $password.= $alphabet[$n];
        }
        return $password;
    }


   public function getShortRoleName(){
        $role = '';
        if($this->role == 1) $role = 'P';
        elseif ($this->role == 2) $role = 'S';
        elseif($this->role == 3) $role = 'ED';
        elseif($this->role == 4) $role = 'S&ED';
        elseif($this->role == 5) $role = 'BC';
        return $role;
    }

    public function getOrderID(){

        $id = $this->id;
        if($id < 9) $id = '00'.$id;
        elseif ($id > 9 && $id < 100) $id = '0'.$id;
        return $id;
    }

    public function getRoleName(){
        $role = '';
        if($this->role == 1 && $this->type == 3) $role = 'Intended Parent(Surrogacy & Egg Donation)';
        elseif($this->role == 1 && $this->type == 1) $role = 'Intended Parent(Surrogacy)';
        elseif($this->role == 1 && $this->type == 2) $role = 'Intended Parent(Egg Donation)';
        elseif($this->role == 2) $role = 'Surrogate';
        elseif($this->role == 3) $role = 'Egg Donor';
        elseif($this->role == 4) $role = 'Surrogate & Egg Donor';
        elseif($this->role == 5) $role = 'Biotransportation Сlient';
        return $role;
    }

    public function getTypeName(){
        $role = '';
        if($this->type == 3) $role = 'Surrogacy & Egg Donation';
        elseif($this->type == 1) $role = 'Surrogacy';
        elseif($this->type == 2) $role = 'Egg Donation';

        return $role;
    }

    public function getProfile(){
        return $this->hasOne(Profiles::className(), ['user_id' => 'id']);
    }

    public function getPersonal_info(){
        return $this->hasOne(PersonalInfo::className(), ['user_id' => 'id']);
    }

    public function getVideo(){
        return $this->hasOne(UserVideo::className(), ['user_id' => 'id']);
    }

    public function getEducation(){
        return $this->hasOne(UserEducation::className(), ['user_id' => 'id']);
    }

    public function getEthnic_origin(){
        return $this->hasOne(EthnicOrigin::className(), ['user_id' => 'id']);
    }

    public function getPersonal_history(){
        return $this->hasOne(PersonalHistory::className(), ['user_id' => 'id']);
    }

    public function getProfiles()
    {
        return $this->profile->first_name.' '.$this->profile->last_name.' '.$this->id.' '.$this->personal_info->country;
    }

    public function getPhoto(){
        return $this->hasOne(UserPhoto::className(), ['user_id' => 'id']);
    }

    public function getPhotos(){
        return $this->hasMany(UserPhoto::className(), ['user_id' => 'id']);
    }

    public function getMiscellaneous(){
        return $this->hasOne(Miscellaneous::className(), ['user_id' => 'id']);
    }

    public function getPregAndSurrogacies(){
        return $this->hasOne(PregnanciesAndSurrogacies::className(), ['user_id' => 'id']);
    }

    public function getFavorites(){
        return $this->hasOne(UserFavorites::className(), ['favorite_user_id' => 'id']);
    }


    public static function getSurrogateUsersWithPagination($pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_surrogate']){
            $query =  User::find()->where('(role = 2 OR role = 4) AND status = 1  AND step = 20');
        }else{
            $query =  User::find()->where('(role = 2 OR role = 4) AND status = 1 AND status_profile = 1 AND step = 20');
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }


    public static function getSurrogateSearchUsersWithPagination($id, $pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_surrogate']) {
            $query = User::find()->where('(role = 2 OR role = 4) AND status = 1  AND step = 20 AND id=' . $id);
        }else{
            $query = User::find()->where('(role = 2 OR role = 4) AND status = 1 AND status_profile = 1 AND step = 20 AND id=' . $id);
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }




    public static function getEggDonorUsersWithPagination($pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $query =  User::find()->where('(role = 3 OR role = 4) AND status = 1 AND step = 20');
        }else{
            $query =  User::find()->where('(role = 3 OR role = 4) AND status = 1 AND status_profile = 1 AND step = 20');
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }


    public static function getEggDonorSearchUsersWithPagination($id, $pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $query = User::find()->where('(role = 3 OR role = 4) AND status = 1 AND step = 20 AND id=' . $id);
        }else{
            $query = User::find()->where('(role = 3 OR role = 4) AND status = 1 AND status_profile = 1 AND step = 20 AND id=' . $id);
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }



    public static function getIndentedParentUsersWithPagination($pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $query = User::find()->where('role = 1 AND status = 1 AND step = 20');
        }else{
            $query = User::find()->where('role = 1 AND status = 1 AND status_profile = 1 AND step = 20');
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }


    public static function getIndentedParentSearchUsersWithPagination($id, $pageSize=9)
    {
        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $query = User::find()->where('role = 1 AND status = 1 AND step = 20 AND id=' . $id);
        }else{
            $query = User::find()->where('role = 1 AND status = 1 AND status_profile = 1 AND step = 20 AND id=' . $id);
        }
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
