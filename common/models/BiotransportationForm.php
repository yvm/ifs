<?php
namespace common\models;

use Yii;

class BiotransportationForm extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'biotransportation_form';
    }

    public function rules()
    {
        return [
            [['name_pick_up', 'date', 'contact_person', 'phone', 'adress', 'country', 'city', 'name_deliver', 'contact_deliver', 'phone_deliver', 'adress_deliver', 'country_deliver', 'city_deliver', 'first_name', 'last_name', 'mobile_phone', 'email', 'country_transportation', 'city_transportation', 'adress_transportation', 'biomaterial_type', 'tube_type', 'tube_number', 'biomaterial_infected', 'additional_information'], 'required'],
            [['country', 'state', 'city', 'country_deliver', 'state_deliver', 'city_deliver', 'country_transportation', 'state_transportation', 'city_transportation'], 'integer'],
            [['additional_information'], 'string'],
            [['created_at', 'price'], 'safe'],
            [['email'], 'email'],
            [['name_pick_up', 'contact_person', 'phone', 'adress', 'name_deliver', 'contact_deliver', 'phone_deliver', 'adress_deliver', 'first_name', 'last_name', 'mobile_phone', 'email', 'adress_transportation', 'biomaterial_type', 'tube_type'], 'string', 'max' => 255],
            [['date'], 'string', 'max' => 30],
            [['tube_number'], 'string', 'max' => 50],
            [['biomaterial_infected'], 'string', 'max' => 5],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name_pick_up' => 'Name Pick Up',
            'date' => 'Date',
            'contact_person' => 'Contact Person',
            'phone' => 'Phone',
            'adress' => 'Adress',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'name_deliver' => 'Name Deliver',
            'contact_deliver' => 'Contact Deliver',
            'phone_deliver' => 'Phone Deliver',
            'adress_deliver' => 'Adress Deliver',
            'country_deliver' => 'Country Deliver',
            'state_deliver' => 'State Deliver',
            'city_deliver' => 'City Deliver',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'mobile_phone' => 'Mobile Phone',
            'email' => 'Email',
            'country_transportation' => 'Country Transportation',
            'state_transportation' => 'State Transportation',
            'city_transportation' => 'City Transportation',
            'adress_transportation' => 'Adress Transportation',
            'biomaterial_type' => 'Biomaterial Type',
            'tube_type' => 'Tube Type',
            'tube_number' => 'Tube Number',
            'biomaterial_infected' => 'Biomaterial Infected',
            'additional_information' => 'Additional Information',
            'created_at' => 'Created At',
        ];
    }
}
