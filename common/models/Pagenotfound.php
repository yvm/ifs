<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pagenotfound".
 *
 * @property int $id
 * @property string $name
 * @property string $content
 * @property int $country_id
 * @property int $language_id
 */
class Pagenotfound extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pagenotfound';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок в баннере',
            'content' => 'Контент',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }
}
