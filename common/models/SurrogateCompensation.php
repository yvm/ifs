<?php
namespace common\models;

use Yii;

class SurrogateCompensation extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogate_compensation';
    }

    public function rules()
    {
        return [
            [['from', 'to', 'move'], 'required'],
            [['from', 'to', 'move'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'От',
            'to' => 'До',
            'move' => 'Шаг',
        ];
    }
}
