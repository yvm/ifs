<?php
namespace common\models;

use Yii;
use yii\data\Pagination;

class Phototype extends \yii\db\ActiveRecord
{


    public static function tableName()
    {
        return 'phototype';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'photo_id', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['photo_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg','maxFiles' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'image' => Yii::t('app', 'Картинка'),
            'content' => Yii::t('app', 'Текст'),
            'photo_id' => Yii::t('app', 'Тема'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/photo/' . $this->image : '/no-image.png';
    }

    public function getPhoto()
    {
        return $this->hasOne(Photo::className(), ['id' => 'photo_id']);
    }

    public function getPhotoName(){
        return (isset($this->photo))? $this->photo->name:'Не задан';
    }

    public static function getAll($pageSize=9,$id)
    {
        $query = Phototype::find()->where('photo_id = '.$id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
