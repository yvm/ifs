<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "donationcostcon".
 *
 * @property int $id
 * @property string $content
 * @property int $country_id
 * @property int $language_id
 */
class Donationcostcon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'donationcostcon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Содержание',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }
}
