<?php
namespace common\models;

use Yii;

class Surrogacycoststage extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacycoststage';
    }

    public function rules()
    {
        return [
            [['name', 'cost_id', 'country_id', 'language_id','content'], 'required'],
            [['cost_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['content'],'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'content' => 'Содержание',
            'cost_id' => 'Программа',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getCost()
    {
        return $this->hasOne(Surrogacycost::className(), ['id' => 'cost_id']);
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Surrogacycoststage::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }


}
