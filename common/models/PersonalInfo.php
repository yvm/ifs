<?php
namespace common\models;

use DateTime;
use Yii;

class PersonalInfo extends \yii\db\ActiveRecord
{
    const surrogate = 'surrogate';
    const step2 = 'step2';
    const eggDonor = 'eggDonor';
    const surrogate_and_eggDonor = 'surrogate_and_eggDonor';
    const profile_parent_biotrans = 'profile_parent_biotrans';
    const biotrans = 'biotrans';

    public static function tableName()
    {
        return 'personal_info';
    }

    public function rules()
    {
        return [
            [['user_id','height','weight','eye_color','natural_hair_color_now','natural_hair_color_as_child',
                'hair_type','complexion','freckles','do_you_have_any_dimples','were_you_overweight',
                'which_is_your_dominant_hand','gender', 'marital_status', 'citizenship', 'zip_postal_code', 'country',
                'city', 'height', 'weight','address'], 'required'],
            [['user_id','height','weight','eye_color','natural_hair_color_now','natural_hair_color_as_child',
                'hair_type','complexion','freckles','do_you_have_any_dimples','were_you_overweight',
                'which_is_your_dominant_hand','gender', 'marital_status', 'citizenship', 'zip_postal_code', 'country',
                'city', 'height', 'weight'], 'integer'],
            [['date_of_birth', 'created_at', 'state'], 'safe'],
            [['date_of_birth'], 'required', 'on' => self::step2],
            ['address', 'string'],
            [['user_id'], 'unique'],
            [['country'], 'yes_no'],
            [['date_of_birth'], 'date_of_birth'],
        ];
    }

    public function yes_no($attribute,$params)
    {
        if($this->country == 152)
            if(empty($this->state))
                $this->addError("state", "Необходимо указать state.");
    }

    public function date_of_birth($attribute,$params)
    {
        if(strtotime($this->date_of_birth) > time())
                $this->addError("date_of_birth", "Неправильная дата date_of_birth.");
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'gender' => Yii::t('app', 'Gender'),
            'marital_status' => Yii::t('app', 'Marital Status'),
            'date_of_birth' => Yii::t('app', 'Date Of Birth'),
            'citizenship' => Yii::t('app', 'Citizenship'),
            'zip_postal_code' => Yii::t('app', 'Zip Postal Code'),
            'country' => Yii::t('app', 'Country'),
            'state' => Yii::t('app', 'State'),
            'city' => Yii::t('app', 'City'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function scenarios()
    {
        return [
            self::surrogate => ['height','weight'],
            self::eggDonor => ['height','weight','eye_color','natural_hair_color_now','natural_hair_color_as_child',
                'hair_type','complexion','freckles','do_you_have_any_dimples','were_you_overweight',
                'which_is_your_dominant_hand'],
            self::surrogate_and_eggDonor => ['height','weight','eye_color','natural_hair_color_now','natural_hair_color_as_child',
                'hair_type','complexion','freckles','do_you_have_any_dimples','were_you_overweight',
                'which_is_your_dominant_hand'],

            self::step2 => ['gender','marital_status','date_of_birth','citizenship','country','city','zip_postal_code', 'state', 'address'],
            self::profile_parent_biotrans => ['country','state','city','marital_status','zip_postal_code','address'],
            self::biotrans => ['country','state','city','zip_postal_code'],

        ];
    }



    public function getDateOfBirth(){
        $birthDate = $this->date_of_birth;
        try {
            $date = new DateTime($birthDate);
        } catch (\Exception $e) {
        }
        try {
            $now = new DateTime();
        } catch (\Exception $e) {
        }
        $interval = $now->diff($date);
        return $interval->y;

    }

    public function getMaritalStatus(){
        $status = '';
        if($this->marital_status == 1) $status = 'Married';
        elseif ($this->marital_status == 2)$status = 'Single';
        return $status;
    }


    public function getCountryName(){
        $country = Countrys::find()->where('id='.$this->country)->one();
        return $country->name;
    }

    public function getStateName(){
        $state = Region::find()->where('country_id='.$this->country.' AND id='.$this->state)->one();
        return $state->name;
    }

    public function getCityName(){
        $city = City::find()->where('region_id='.$this->state.' AND id='.$this->city)->one();
        return $city->name;
    }

    public function getCitizenshipName(){
        $country = Countrys::find()->where('id='.$this->citizenship)->one();
        return $country->name;
    }



}
