<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "miscellaneous".
 *
 * @property int $id
 * @property int $user_id
 * @property string $how_know_about_us
 * @property string $please_describe
 * @property string $if_you_select_referral
 * @property string $have_ever_been
 * @property int $have_you_or_anyone
 * @property int $have_been_eggdonor
 * @property int $have_ever_applied
 * @property int $are_you_interested
 * @property int $willing_selectively_reduce
 * @property int $willing_have_amniocentesis
 * @property int $was_severe_medical
 * @property int $if_parents_requested
 * @property string $have_you_discussed
 * @property int $if_approved_as_ifs
 * @property int $desired_compensation
 */
class Miscellaneous extends \yii\db\ActiveRecord
{

    const eggDonor = 'eggDonor';
    const surrogate = 'surrogate';
    const surrogate_and_eggDonor = 'surrogate_and_eggDonor';
	const profile = 'profile';



    public static function tableName()
    {
        return 'miscellaneous';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'have_ever_been', 'have_you_or_anyone', 'have_been_eggdonor', 'have_ever_applied',
                'are_you_interested', 'willing_selectively_reduce', 'willing_have_amniocentesis', 'was_severe_medical',
                'if_parents_requested', 'have_you_discussed_become_surrogate', 'if_approved_as_ifs_surrogate',
                'have_you_discussed_desire_donate','if_approved_as_ifs_donor','desired_compensation',
                'how_know_about_us','compensation_currency','desired_compensation_egg_donor',], 'required'],
            [['user_id', 'have_you_or_anyone', 'have_been_eggdonor', 'have_ever_applied', 'are_you_interested', 'willing_selectively_reduce',
                'willing_have_amniocentesis', 'was_severe_medical', 'if_parents_requested', 'if_approved_as_ifs_surrogate','if_approved_as_ifs_donor',
                'desired_compensation','desired_compensation_egg_donor'], 'integer'],
            [['please_describe', 'if_you_select_referral', 'have_ever_been', 'have_you_discussed_become_surrogate','have_you_discussed_desire_donate',
            'compensation_currency'], 'string'],
            [['how_know_about_us'], 'string', 'max' => 255],
            [['other_compensation_currency','other_compensation_currency_egg_donor'],'string','max'=>10],
            [['how_know_about_us','other_compensation_currency'], 'yes_no'],
        ];
    }

    public function yes_no($attribute,$params)
    {
        if($this->how_know_about_us == "Other")
            if(empty($this->please_describe))
                $this->addError("please_describe", "Необходимо указать Please Describe.");

        if($this->how_know_about_us == "Referral")
            if(empty($this->if_you_select_referral))
                $this->addError("if_you_select_referral", "Необходимо указать If you select referral.");

    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'how_know_about_us' => 'How Know About Us',
            'please_describe' => 'Please Describe',
            'if_you_select_referral' => 'If You Select Referral',
            'have_ever_been' => 'Have Ever Been',
            'have_you_or_anyone' => 'Have You Or Anyone',
            'have_been_eggdonor' => 'Have Been Eggdonor',
            'have_ever_applied' => 'Have Ever Applied',
            'are_you_interested' => 'Are You Interested',
            'willing_selectively_reduce' => 'Willing Selectively Reduce',
            'willing_have_amniocentesis' => 'Willing Have Amniocentesis',
            'was_severe_medical' => 'Was Severe Medical',
            'if_parents_requested' => 'If Parents Requested',
            'desired_compensation' => 'Desired Compensation',
            'desired_compensation_egg_donor' => 'Desired Compensation as Egg Donor',
            'other_compensation_currency_egg_donor' => 'Other Compensation Currency as Egg Donor'
        ];
    }

    public function scenarios()
    {
        return [

            self::eggDonor => ['user_id', 'how_know_about_us', 'have_ever_been', 'have_you_or_anyone', 'have_been_eggdonor', 'have_ever_applied', 'are_you_interested', 'have_you_discussed_desire_donate', 'if_approved_as_ifs_donor', 'desired_compensation','compensation_currency','please_describe','if_you_select_referral','other_compensation_currency'],

            self::surrogate => ['user_id', 'how_know_about_us', 'have_ever_been', 'have_you_or_anyone', 'have_been_eggdonor', 'have_ever_applied', 'are_you_interested', 'willing_selectively_reduce', 'willing_have_amniocentesis', 'was_severe_medical', 'if_parents_requested', 'have_you_discussed_become_surrogate', 'if_approved_as_ifs_surrogate', 'desired_compensation','compensation_currency','please_describe','if_you_select_referral','other_compensation_currency'],

            self::surrogate_and_eggDonor => ['user_id', 'how_know_about_us', 'have_ever_been', 'have_you_or_anyone',  'willing_selectively_reduce', 'willing_have_amniocentesis', 'was_severe_medical', 'if_parents_requested', 'have_you_discussed_become_surrogate','have_you_discussed_desire_donate', 'if_approved_as_ifs_surrogate', 'if_approved_as_ifs_donor','desired_compensation','compensation_currency','compensation_currency','please_describe','if_you_select_referral','desired_compensation_egg_donor','other_compensation_currency','other_compensation_currency_egg_donor'],
			
			self::profile => ['have_been_eggdonor'],
        ];
    }


    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if($this->how_know_about_us == "Other"){
                $this->if_you_select_referral=null;
            }
            if($this->how_know_about_us == "Referral"){
                $this->please_describe=null;
            }
            if($this->how_know_about_us != "Other" && $this->how_know_about_us != "Referral"){
                $this->if_you_select_referral=null;
                $this->please_describe=null;
            }
            return true;
        }
        return false;
    }

    public function getValuta(){
        return $this->hasOne(Valuta::className(), ['id' => 'compensation_currency']);
    }
}
