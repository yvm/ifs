<?php
namespace common\models;

use Yii;

class Surrogateservice extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/surrogateservice/';

    public static function tableName()
    {
        return 'surrogateservice';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content','contenta','contentb'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Содержание для компьютерной версии'),
            'contenta' => Yii::t('app', 'Видные содержания для мобильной версии'),
            'contentb' => Yii::t('app', 'Скрытое содержания для мобильной версии'),
            'image' => Yii::t('app', 'Картинка'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogate/' . $this->image : '/no-image.png';
    }
}
