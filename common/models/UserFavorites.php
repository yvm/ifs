<?php
namespace common\models;

use Yii;
use yii\debug\models\search\Profile;

class UserFavorites extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'user_favorites';
    }

    public function rules()
    {
        return [
            [['user_id', 'favorite_user_id'], 'required'],
            [['user_id', 'favorite_user_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'favorite_user_id' => 'Favorite User ID',
        ];
    }


    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'favorite_user_id']);
    }

    public function getProfile()
    {
        return $this->hasOne(Profiles::className(), ['user_id' => 'favorite_user_id']);
    }


    public function getRoleName(){
        $role = '';
        if($this->user->role == 1){
            $role = 'intended-parent';
        }elseif ($this->user->role == 2){
            $role = 'surrogate';
        }elseif ($this->user->role == 3){
            $role = 'egg-donor';
        }

        return $role;

    }


}
