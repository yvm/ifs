<?php
namespace common\models;

use Yii;
use yii\data\Pagination;

class Videotype extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'videotype';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'url', 'video_id', 'country_id', 'language_id'], 'required'],
            [['content', 'url'], 'string'],
            [['video_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Текст'),
            'url' => Yii::t('app', 'Ссылка'),
            'video_id' => Yii::t('app', 'Тема'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getVideo()
    {
        return $this->hasOne(Video::className(), ['id' => 'video_id']);
    }

    public function getVideoName(){
        return (isset($this->video))? $this->video->name:'Не задан';
    }

    public static function getAll($pageSize=9,$id)
    {
        $query = Videotype::find()->where('video_id = '.$id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"]);
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }
}
