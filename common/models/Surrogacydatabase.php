<?php
namespace common\models;

use Yii;

class Surrogacydatabase extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacydatabase';
    }

    public function rules()
    {
        return [
            [['content', 'buttonName', 'status', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['status', 'country_id', 'language_id'], 'integer'],
            [['buttonName'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Содержания'),
            'buttonName' => Yii::t('app', 'Названия кнопка'),
            'status' => 'Статус',
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
