<?php

namespace common\models;

use DateTime;
use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "article".
 *
 * @property int $id
 * @property string $name
 * @property string $image
 * @property string $content
 * @property string $date
 * @property string $tag
 * @property int $country_id
 * @property int $language_id
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'content', 'date', 'tag', 'country_id', 'language_id'], 'required'],
            [['content', 'tag','metaDesc','metaKey'], 'string'],
            [['date'], 'safe'],
            [['country_id', 'language_id'], 'integer'],
            [['name','metaName'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'jpg,png']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'image' => 'Картинка',
            'content' => 'Содержание',
            'date' => 'Дата',
            'tag' => 'Теги',
            'metaName' => 'Мета Названия',
            'metaDesc' => 'Мета Описание',
            'metaKey' => 'Ключевые слова',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/article/' . $this->image : '/no-image.png';
    }

    public static function getAll($pageSize=15)
    {
        $query = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }

    public static function getBlogByMonth($month,$pageSize=15)
    {
        $arr = '(';
        $blog = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC')->all();
        foreach ($blog as $v){
            if((int)date('m', strtotime($v->date)) == $month){
                $arr.=$v->id.',';
            }
        }

        $arr = substr($arr,0,strlen($arr)-1);
        $arr.= ')';

        $query = Article::find()->where('id in '.$arr.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC');
        $count = $query->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $articles = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $data['data'] = $articles;
        $data['pagination'] = $pagination;

        return $data;
    }

    public function getDate()
    {
        return Yii::$app->formatter->asDate($this->date, 'dd/MM/yyyy');
    }

    public function getMonthNumber()
    {
        return date('m', $this->date);
    }

    public function getDateAsTime()
    {
        $d = DateTime::createFromFormat('Y-m-d', $this->date);
        return $d->getTimestamp();
    }


    public function getFirstDateAsTime(){

        $b = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC')->limit(1)->one();
        $firstDate = $b->date;
        $d = DateTime::createFromFormat('Y-m-d', $firstDate);
        return $d->getTimestamp();
    }

    public function getLastDateAsTime(){

        $b = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date ASC')->limit(1)->one();
        $lastDate = $b->date;
        $d = DateTime::createFromFormat('Y-m-d', $lastDate);
        return $d->getTimestamp();
    }

    public function getPreviuosBlogId(){
        $blog = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC')->all();
        $preId = "";
        foreach ($blog as $v){
            if($v->id == $this->id){
                break;
            }
            $preId = $v->id;
        }
        return $preId;
    }

    public function getNextBlogId(){
        $blog = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('date DESC')->all();
        $nextId = "";
        $check = false;
        foreach ($blog as $v){
            if($check){
                $nextId = $v->id;
                break;
            }
            if($v->id == $this->id){
                $check = true;
            }
        }
        return $nextId;
    }

    public function getLastBlogs(){
        $lastBlogs = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->limit(5)->orderBy('date DESC')->all();
        return $lastBlogs;
    }

    public function getArchives(){
        $thisYearBlog = array();
        $blog = Article::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        foreach ($blog as $v){
            if(date('y', strtotime($v->date)) == date('y', strtotime(date('Y-m-d')))){
                array_push($thisYearBlog,$v);
            }
        }

        $month = Month::find()->where('language_id='.Yii::$app->session["lang"])->all();
        $res = array();
        $m=0;
        foreach ($month as $mon){
            $m++;
            $count = 0;
            foreach ($thisYearBlog as $blog){
                if((int)date('m', strtotime($blog->date)) == $m){
                    $count++;
                }
            }

            array_push($res,$mon->name .' '. date('y', strtotime(date('Y-m-d'))).' ('.$count.')');
        }


        return $res;
    }
}
