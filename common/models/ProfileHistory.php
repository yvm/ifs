<?php
namespace common\models;

use Yii;

class ProfileHistory extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'profile_history';
    }

    public function rules()
    {
        return [
            [['user_id', 'first_name', 'last_name', 'country', 'state', 'city', 'address', 'zip_code',  'email', 'marital_status', 'previous'], 'required'],
            [['user_id', 'country', 'state', 'city', 'marital_status', 'program_status', 'previous'], 'integer'],
            [['first_name', 'last_name', 'address', 'zip_code', 'phone', 'skype', 'vatzup', 'telegram', 'facebook', 'instagram', 'twitter', 'vk', 'email', 'how_many_children'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID Пользователя',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'country' => 'Страна',
            'state' => 'Штат',
            'city' => 'Город',
            'address' => 'Адрес',
            'zip_code' => 'Почтовый индекс',
            'phone' => 'Телефон',
            'skype' => 'Скайп',
            'vatzup' => 'Ватсап',
            'telegram' => 'Телеграм',
            'facebook' => 'Facebook',
            'instagram' => 'Инстаграм',
            'twitter' => 'Твитер',
            'vk' => 'Вконтакте',
            'email' => 'E-mail',
            'marital_status' => 'Семейное положение',
            'program_status' => 'Статус участия в программе',
            'how_many_children' => 'Сколько у тебя детей сейчас?',
            'previous' => 'Предыдущий '.$this->role,
            'updated' => 'Дата изменение'
        ];
    }

    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getRole(){
        $role = '';
        if($this->user->role == 1){
            $role = 'Родитель';
        }elseif ($this->user->role == 2){
            $role = 'Суррогатное';
        }elseif ($this->user->role == 3){
            $role = 'донор яйцеклеток';
        }elseif ($this->user->role == 4){
            $role = 'Суррогатное & донор яйцеклеток';
        }elseif ($this->user->role == 5){
            $role = 'Био-транспорт клиент';
        }

        return $role;

    }


    public function getCountryName(){
        $country = Countrys::find()->where('id='.$this->country)->one();
        return $country->name;
    }

    public function getStateName(){
        $state = Region::find()->where('country_id='.$this->country.' AND id='.$this->state)->one();
        return $state->name;
    }

    public function getCityName(){
        $city = City::find()->where('region_id='.$this->state.' AND id='.$this->city)->one();
        return $city->name;
    }

    public function getMaritalStatus(){


        if($this->marital_status==1){
            $status = 'Замужен (Женат)';
        }else{
            $status = 'Не замужен (Не женат)';
        }
        return $status;
    }

    public function getProgramStatus(){

        if($this->program_status == 1){
            $status = 'Доступно ';
        }elseif($this->program_status){
            $status = 'Недоступно ';
        }else{
            if($this->user->role == 2 || $this->user->role ==3  || $this->user->role == 4){
                $status = 'Занят с суррогатом / донором яйцеклеток';
            }else{
                $status = 'в настоящее время семья помогает';
            }

        }
        return $status;
    }


    public function getMaritalPrevious(){


        if($this->previous == 1){
            $status = 'Да';
        }else{
            $status = 'Нет';
        }
        return $status;
    }

    public function getCountChildren(){


        if($this->how_many_children == 100){
            $sum = '10+';
        }else{
            $sum = $this->how_many_children;
        }
        return $sum;
    }
}
