<?php
namespace common\models;

use Yii;

class PersonalHistory extends \yii\db\ActiveRecord
{
    const Surrogate = 'Surrogate';
    const EggDonor = 'EggDonor';
    const SurrogateEggDonor = 'SurrogateEggDonor';

    public static function tableName()
    {
        return 'personal_history';
    }

    public function rules()
    {
        return [
            [['user_id', 'good_health', 'have_you_ever',  'blood_group', 'please_indicate', 'you_currently', 'have_you_taken', 'birth_control', 'ablation_procedure', 'currently_taking', 'menstrual_periods', 'recreational_drugs', 'drink_alcohol', 'smoking_cigarettes', 'alcohol_abuse', 'alcohol_addiction', 'sexual_abuse', 'had_any_cancer', 'tattoos', 'body_piercings', 'blood_transfusion2', 'blood_donor', 'hospitalizations', 'blood_transfusion', 'lenses_glasses', 'eyesight', 'work_done', 'genetic_conditions', 'fragile_x', 'tay_sachs', 'sickle_cell', 'cystic_fibrosis', 'sma', 'ablation_procedure2', 'c_t_birth_control', 'depo_provera'], 'required'],
            [['user_id', 'good_health', 'have_you_ever', 'you_currently', 'have_you_taken', 'birth_control', 'ablation_procedure', 'currently_taking', 'menstrual_periods', 'recreational_drugs', 'drink_alcohol', 'smoking_cigarettes', 'alcohol_abuse', 'alcohol_addiction', 'sexual_abuse', 'had_any_cancer', 'tattoos', 'body_piercings', 'blood_transfusion2', 'blood_donor', 'hospitalizations', 'blood_transfusion', 'lenses_glasses', 'eyesight', 'work_done', 'genetic_conditions', 'fragile_x', 'tay_sachs', 'sickle_cell', 'cystic_fibrosis', 'sma', 'ablation_procedure2', 'c_t_birth_control'], 'integer'],
            [['good_health_describe', 'have_you_ever_describe', 'you_currently_describe', 'please_list', 'ablation_procedure_describe', 'recreational_drugs_describe', 'smoking_cigarettes_d', 'alcohol_abuse_d', 'alcohol_addiction_d', 'sexual_abuse_d', 'had_any_cancer_d', 'body_piercings_date', 'blood_transfusion2_date', 'blood_donor_d', 'hospitalizations_d'], 'string'],
            [['please_indicate', 'method_birth_control', 'created_at', 'depo_provera_date'], 'safe'],
            [['blood_group'], 'string', 'max' => 10],
            [['date_fragile_x', 'date_tay_sachs', 'date_sickle_cell', 'date_cystic_fibrosis', 'date_sma', 'tattoos_date'], 'string', 'max' => 100],
            [['user_id'], 'unique'],
//            [['good_health_describe'], 'required', 'when' => function($model) {
//                return $model->good_health;
//            }],
            [['good_health_describe', 'have_you_ever_describe', 'you_currently_describe', 'ablation_procedure_describe', 'recreational_drugs_describe', 'smoking_cigarettes_d',
                'alcohol_abuse_d', 'alcohol_addiction_d', 'sexual_abuse_d', 'had_any_cancer_d', 'tattoos_date', 'body_piercings_date', 'blood_transfusion2_date',
                'blood_donor_d', 'hospitalizations_d', 'date_fragile_x', 'date_tay_sachs', 'date_sickle_cell', 'date_cystic_fibrosis', 'date_sma', 'please_list',
                'depo_provera', 'method_birth_control', 'lenses_glasses'], 'yes_no'],

            [['tattoos_date', 'date_fragile_x', 'date_sickle_cell', 'date_cystic_fibrosis', 'date_sma', 'date_tay_sachs',
                'depo_provera_date', 'body_piercings_date'], 'date_validate'],
        ];
    }

    public function date_validate($attribute,$params)
    {
        if(strtotime($this->tattoos_date) > time())
            $this->addError("tattoos_date", "Неправильная дата tattoos_date.");

        if(strtotime($this->date_fragile_x) > time())
            $this->addError("date_fragile_x", "Неправильная дата date_fragile_x.");

        if(strtotime($this->date_sickle_cell) > time())
            $this->addError("date_sickle_cell", "Неправильная дата date_sickle_cell.");

        if(strtotime($this->date_cystic_fibrosis) > time())
            $this->addError("date_cystic_fibrosis", "Неправильная дата date_cystic_fibrosis.");

        if(strtotime($this->date_sma) > time())
            $this->addError("date_sma", "Неправильная дата date_sma.");

        if(strtotime($this->date_tay_sachs) > time())
            $this->addError("date_tay_sachs", "Неправильная дата date_tay_sachs.");

        if(strtotime($this->depo_provera_date) > time())
            $this->addError("depo_provera_date", "Неправильная дата depo_provera_date.");

        if(strtotime($this->body_piercings_date) > time())
            $this->addError("body_piercings_date", "Неправильная дата body_piercings_date.");
    }

    public function yes_no($attribute,$params)
    {
        if(!empty($this->birth_control))
            if(empty($this->method_birth_control))
                $this->addError("method_birth_control", "Необходимо указать method_birth_control.");
        if(empty($this->good_health))
            if(empty($this->good_health_describe))
                $this->addError("good_health_describe", "Необходимо указать good_health_describe.");
        if(!empty($this->have_you_ever))
            if(empty($this->have_you_ever_describe))
                $this->addError("have_you_ever_describe", "Необходимо указать have_you_ever_describe.");
        if(!empty($this->you_currently))
            if(empty($this->you_currently_describe))
                $this->addError("you_currently_describe", "Необходимо указать you_currently_describe.");
        if(!empty($this->ablation_procedure))
            if(empty($this->ablation_procedure_describe))
                $this->addError("ablation_procedure_describe", "Необходимо указать ablation_procedure_describe.");
        if(!empty($this->recreational_drugs))
            if(empty($this->recreational_drugs_describe))
                $this->addError("recreational_drugs_describe", "Необходимо указать recreational_drugs_describe.");
//        if(!empty($this->smoking_cigarettes))
//            $this->addError("smoking_cigarettes_d", "Необходимо указать good_health_describe.");
        if(!empty($this->alcohol_abuse))
            if(empty($this->alcohol_abuse_d))
                $this->addError("alcohol_abuse_d", "Необходимо указать alcohol_abuse_d.");
        if(!empty($this->alcohol_addiction))
            if(empty($this->alcohol_addiction_d))
                $this->addError("alcohol_addiction_d", "Необходимо указать alcohol_addiction_d.");
        if(!empty($this->sexual_abuse))
            if(empty($this->sexual_abuse_d))
                $this->addError("sexual_abuse_d", "Необходимо указать sexual_abuse_d.");
        if(!empty($this->had_any_cancer))
            if(empty($this->had_any_cancer_d))
                $this->addError("had_any_cancer_d", "Необходимо указать had_any_cancer_d.");
        if(!empty($this->tattoos))
            if($this->tattoos_date == '')
                $this->addError("tattoos_date", "Необходимо указать tattoos_date.");
        if(!empty($this->body_piercings))
            if($this->body_piercings_date == '')
                $this->addError("body_piercings_date", "Необходимо указать body_piercings_date.");
//        if(!empty($this->blood_transfusion2))
//            if($this->blood_transfusion2_date == '')
//                $this->addError("blood_transfusion2_date", "Необходимо указать blood_transfusion2_date.");
        if(!empty($this->blood_donor))
            if(empty($this->blood_donor_d))
                $this->addError("blood_donor_d", "Необходимо указать blood_donor_d.");
        if(!empty($this->hospitalizations))
            if(empty($this->hospitalizations_d))
                $this->addError("hospitalizations_d", "Необходимо указать hospitalizations_d.");
        if(!empty($this->fragile_x))
            if($this->date_fragile_x == '')
                $this->addError("date_fragile_x", "Необходимо указать date_fragile_x.");
        if(!empty($this->sickle_cell))
            if($this->date_sickle_cell == '')
                $this->addError("date_sickle_cell", "Необходимо указать date_sickle_cell.");
        if(!empty($this->cystic_fibrosis))
            if($this->date_cystic_fibrosis == '')
                $this->addError("date_cystic_fibrosis", "Необходимо указать date_cystic_fibrosis.");
        if(!empty($this->sma))
            if($this->date_sma == '')
                $this->addError("date_sma", "Необходимо указать date_sma.");
        if(!empty($this->tay_sachs))
            if($this->date_tay_sachs == '')
                $this->addError("date_tay_sachs", "Необходимо указать date_tay_sachs.");
        if(!empty($this->have_you_taken))
            if(empty($this->please_list))
                $this->addError("please_list", "Необходимо указать please_list.");
        if(!empty($this->depo_provera))
            if(empty($this->depo_provera_date))
                $this->addError("depo_provera_date", "Необходимо указать depo_provera_date.");
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'good_health' => Yii::t('app', 'Good Health'),
            'good_health_describe' => Yii::t('app', 'Good Health Describe'),
            'have_you_ever' => Yii::t('app', 'Have You Ever'),
            'have_you_ever_describe' => Yii::t('app', 'Have You Ever Describe'),
            'blood_group' => Yii::t('app', 'Blood Group'),
            'please_indicate' => Yii::t('app', 'Please Indicate'),
            'you_currently' => Yii::t('app', 'You Currently'),
            'you_currently_describe' => Yii::t('app', 'You Currently Describe'),
            'have_you_taken' => Yii::t('app', 'Have You Taken'),
            'please_list' => Yii::t('app', 'Please List'),
            'birth_control' => Yii::t('app', 'Birth Control'),
            'method_birth_control' => Yii::t('app', 'Method Birth Control'),
            'ablation_procedure' => Yii::t('app', 'Ablation Procedure'),
            'ablation_procedure_describe' => Yii::t('app', 'Ablation Procedure Describe'),
            'currently_taking' => Yii::t('app', 'Currently Taking'),
            'menstrual_periods' => Yii::t('app', 'Menstrual Periods'),
            'recreational_drugs' => Yii::t('app', 'Recreational Drugs'),
            'recreational_drugs_describe' => Yii::t('app', 'Recreational Drugs Describe'),
            'drink_alcohol' => Yii::t('app', 'Drink Alcohol'),
            'smoking_cigarettes' => Yii::t('app', 'Smoking Cigarettes'),
            'smoking_cigarettes_d' => Yii::t('app', 'Smoking Cigarettes D'),
            'alcohol_abuse' => Yii::t('app', 'Alcohol Abuse'),
            'alcohol_abuse_d' => Yii::t('app', 'Alcohol Abuse D'),
            'alcohol_addiction' => Yii::t('app', 'Alcohol Addiction'),
            'alcohol_addiction_d' => Yii::t('app', 'Alcohol Addiction D'),
            'sexual_abuse' => Yii::t('app', 'Sexual Abuse'),
            'sexual_abuse_d' => Yii::t('app', 'Sexual Abuse D'),
            'had_any_cancer' => Yii::t('app', 'Had Any Cancer'),
            'had_any_cancer_d' => Yii::t('app', 'Had Any Cancer D'),
            'tattoos' => Yii::t('app', 'Tattoos'),
            'tattoos_date' => Yii::t('app', 'Tattoos Date'),
            'body_piercings' => Yii::t('app', 'Body Piercings'),
            'body_piercings_date' => Yii::t('app', 'Body Piercings Date'),
            'blood_transfusion2' => Yii::t('app', 'Blood Transfusion2'),
            'blood_transfusion2_date' => Yii::t('app', 'Blood Transfusion2 Date'),
            'blood_donor' => Yii::t('app', 'Blood Donor'),
            'blood_donor_d' => Yii::t('app', 'Blood Donor D'),
            'hospitalizations' => Yii::t('app', 'Hospitalizations'),
            'hospitalizations_d' => Yii::t('app', 'Hospitalizations D'),
            'blood_transfusion' => Yii::t('app', 'Blood Transfusion'),
            'lenses_glasses' => Yii::t('app', 'Lenses Glasses'),
            'eyesight' => Yii::t('app', 'Eyesight'),
            'work_done' => Yii::t('app', 'Work Done'),
            'genetic_conditions' => Yii::t('app', 'Genetic Conditions'),
            'fragile_x' => Yii::t('app', 'Fragile X'),
            'date_fragile_x' => Yii::t('app', 'Date Fragile X'),
            'tay_sachs' => Yii::t('app', 'Tay Sachs'),
            'date_tay_sachs' => Yii::t('app', 'Date Tay Sachs'),
            'sickle_cell' => Yii::t('app', 'Sickle Cell'),
            'date_sickle_cell' => Yii::t('app', 'Date Sickle Cell'),
            'cystic_fibrosis' => Yii::t('app', 'Cystic Fibrosis'),
            'date_cystic_fibrosis' => Yii::t('app', 'Date Cystic Fibrosis'),
            'sma' => Yii::t('app', 'Sma'),
            'date_sma' => Yii::t('app', 'Date Sma'),
            'ablation_procedure2' => Yii::t('app', 'Ablation Procedure2'),
            'c_t_birth_control' => Yii::t('app', 'C T Birth Control'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
    public function scenarios()
    {
        return [
            self::Surrogate => ['user_id', 'good_health', 'good_health_describe', 'have_you_ever', 'have_you_ever_describe', 'blood_group', 'please_indicate', 'you_currently', 'you_currently_describe', 'have_you_taken', 'please_list', 'birth_control', 'method_birth_control', 'ablation_procedure', 'ablation_procedure_describe', 'currently_taking', 'menstrual_periods', 'recreational_drugs', 'recreational_drugs_describe', 'drink_alcohol', 'smoking_cigarettes', 'alcohol_abuse', 'alcohol_abuse_d', 'alcohol_addiction', 'alcohol_addiction_d', 'sexual_abuse', 'sexual_abuse_d', 'had_any_cancer', 'had_any_cancer_d', 'blood_donor', 'blood_donor_d', 'hospitalizations', 'hospitalizations_d', 'blood_transfusion', 'depo_provera', 'depo_provera_date'],
            self::EggDonor => ['user_id', 'good_health', 'good_health_describe', 'have_you_ever', 'have_you_ever_describe', 'blood_group', 'please_indicate', 'you_currently', 'you_currently_describe', 'have_you_taken', 'please_list', 'lenses_glasses', 'eyesight', 'work_done', 'fragile_x', 'date_fragile_x', 'tay_sachs', 'date_tay_sachs', 'sickle_cell', 'date_sickle_cell', 'cystic_fibrosis', 'date_cystic_fibrosis', 'sma', 'date_sma', 'no_any_disiases', 'birth_control', 'method_birth_control', 'currently_taking', 'menstrual_periods', 'depo_provera', 'depo_provera_date', 'recreational_drugs', 'recreational_drugs_describe', 'smoking_cigarettes', 'drink_alcohol', 'alcohol_abuse', 'alcohol_abuse_d', 'alcohol_addiction', 'alcohol_addiction_d', 'sexual_abuse', 'sexual_abuse_d', 'had_any_cancer', 'had_any_cancer_d', 'tattoos', 'tattoos_date', 'body_piercings', 'body_piercings_date', 'blood_transfusion2', 'blood_donor', 'blood_donor_d', 'hospitalizations', 'hospitalizations_d'],
            self::SurrogateEggDonor => ['user_id', 'good_health', 'good_health_describe', 'have_you_ever', 'have_you_ever_describe', 'blood_group', 'please_indicate', 'you_currently', 'you_currently_describe', 'have_you_taken', 'please_list', 'lenses_glasses', 'eyesight', 'work_done', 'fragile_x', 'date_fragile_x', 'tay_sachs', 'date_tay_sachs', 'sickle_cell', 'date_sickle_cell', 'cystic_fibrosis', 'date_cystic_fibrosis', 'sma', 'date_sma', 'no_any_disiases', 'birth_control', 'method_birth_control', 'currently_taking', 'menstrual_periods', 'depo_provera', 'depo_provera_date', 'recreational_drugs', 'recreational_drugs_describe', 'smoking_cigarettes', 'drink_alcohol', 'alcohol_abuse', 'alcohol_abuse_d', 'alcohol_addiction', 'alcohol_addiction_d', 'sexual_abuse', 'sexual_abuse_d', 'had_any_cancer', 'had_any_cancer_d', 'tattoos', 'tattoos_date', 'body_piercings', 'body_piercings_date', 'blood_transfusion2', 'blood_donor', 'blood_donor_d', 'hospitalizations', 'hospitalizations_d', 'ablation_procedure2'],
        ];
    }
}
