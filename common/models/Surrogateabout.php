<?php
namespace common\models;

use Yii;

class Surrogateabout extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/surrogateabout/';

    public static function tableName()
    {
        return 'surrogateabout';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'country_id', 'language_id'], 'required'],
            [['content','contenta','contentb'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание для компьютерной версии'),
            'contenta' => Yii::t('app', 'Видные содержания для мобильной версии'),
            'contentb' => Yii::t('app', 'Скрытое содержания для мобильной версии'),
            'image' => Yii::t('app', 'Картинка'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogate/' . $this->image : '/no-image.png';
    }


}
