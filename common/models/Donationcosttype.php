<?php
namespace common\models;

use Yii;

class Donationcosttype extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'donationcosttype';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'donationcost_id', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['donationcost_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание'),
            'donationcost_id' => Yii::t('app', 'Категория'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getDonationCost()
    {
        return $this->hasOne(Donationcost::className(), ['id' => 'donationcost_id']);
    }

    public function getDonationCostName(){
        return (isset($this->donationCost))? $this->donationCost->name:'Не задан';
    }
}
