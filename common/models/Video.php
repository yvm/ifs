<?php
namespace common\models;

use Yii;

class Video extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/video/';

    public static function tableName()
    {
        return 'video';
    }

    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'image' => Yii::t('app', 'Картинка'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/video/' . $this->image : '/no-image.png';
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Video::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

    public function getImages(){
        return $this->hasMany(Videotype::className(), ['video_id' => 'id']);
    }

    public function getPreviuosId(){
        $video = Video::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $preId = "";
        foreach ($video as $v){
            if($v->id == $this->id){
                break;
            }
            $preId = $v->id;
        }
        return $preId;
    }

    public function getNextId(){
        $video = Video::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $nextId = "";
        $check = false;
        foreach ($video as $v){
            if($check){
                $nextId = $v->id;
                break;
            }
            if($v->id == $this->id){
                $check = true;
            }
        }
        return $nextId;
    }
}
