<?php
namespace common\models;

use Yii;

class Physician extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'physician';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
