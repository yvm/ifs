<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ifstrippic".
 *
 * @property int $id
 * @property string $image
 * @property int $ifstrip_id
 * @property int $country_id
 * @property int $language_id
 */
class Ifstrippic extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ifstrippic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ifstrip_id', 'country_id', 'language_id','image'], 'required'],
            [['ifstrip_id', 'country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg','maxFiles' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image' => 'Картинка',
            'ifstrip_id' => 'Категория',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/ifs/' . $this->image : '/no-image.png';
    }

    public function getIfstrip()
    {
        return $this->hasOne(Ifstrip::className(), ['id' => 'ifstrip_id']);
    }

    public function getIfstripName(){
        return (isset($this->ifstrip))? $this->ifstrip->name:'Не задан';
    }

}
