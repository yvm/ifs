<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "ethnic_origin".
 *
 * @property int $id
 * @property int $user_id
 * @property int $ethnicA
 * @property int $percentA
 * @property int $ethnicB
 * @property int $percentB
 * @property int $ethnicC
 * @property int $percentC
 * @property int $ethnicD
 * @property int $percentD
 */
class EthnicOrigin extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ethnic_origin';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'ethnicA', 'percentA'], 'required'],
            [['user_id', 'ethnicA', 'percentA', 'ethnicB', 'percentB', 'ethnicC', 'percentC', 'ethnicD', 'percentD'], 'integer'],
            [['percentA'], 'percent'],
            [['percentB', 'percentC', 'percentD'], 'need_validate_Select_Ethnic'],
            [['ethnicB', 'ethnicC', 'ethnicD'], 'need_validate_Select_percent'],
        ];
    }

    public function percent($attribute,$params)
    {
        $percentB = 0;
        $percentC = 0;
        $percentD = 0;
        if($this->percentB)
            $percentB = $this->percentB;
        if($this->percentC)
            $percentC = $this->percentC;
        if($this->percentD)
            $percentD = $this->percentD;
        if($this->percentA + $percentB + $percentC + $percentD != 100)
                $this->addError("percentA", "В сумме должно быть 100%. У Вас получилось "
                    .($this->percentA + $percentB + $percentC + $percentD));
    }

    public function need_validate_Select_Ethnic($attribute,$params)
    {
        if(!empty($this->percentB) && empty($this->ethnicB))
            $this->addError("ethnicB", "Нужно указать Select Ethnic");
        if(!empty($this->percentC) && empty($this->ethnicC))
            $this->addError("ethnicC", "Нужно указать Select Ethnic");
        if(!empty($this->percentD) && empty($this->ethnicD))
            $this->addError("ethnicD", "Нужно указать Select Ethnic");
    }

    public function need_validate_Select_percent($attribute,$params)
    {
        if(!empty($this->ethnicB) && empty($this->percentB))
            $this->addError("percentB", "Нужно указать percent");
        if(!empty($this->ethnicC) && empty($this->percentC))
            $this->addError("percentC", "Нужно указать percent");
        if(!empty($this->ethnicD) && empty($this->percentD))
            $this->addError("percentD", "Нужно указать percent");
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'ethnicA' => 'Ethnic A',
            'percentA' => 'Percent A',
            'ethnicB' => 'Ethnic B',
            'percentB' => 'Percent B',
            'ethnicC' => 'Ethnic C',
            'percentC' => 'Percent C',
            'ethnicD' => 'Ethnic D',
            'percentD' => 'Percent D',
        ];
    }

    public function getEthnicsA(){
        return $this->hasOne(EthnicOriginPerson::className(), ['id' => 'ethnicA']);
    }

    public function getEthnicsB(){
        return $this->hasOne(EthnicOriginPerson::className(), ['id' => 'ethnicB']);
    }

    public function getEthnicsC(){
        return $this->hasOne(EthnicOriginPerson::className(), ['id' => 'ethnicC']);
    }

    public function getEthnicsD(){
        return $this->hasOne(EthnicOriginPerson::className(), ['id' => 'ethnicD']);
    }
}
