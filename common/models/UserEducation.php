<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_education".
 *
 * @property int $id
 * @property int $user_id
 * @property int $educational_level
 * @property string $occupation
 * @property string $what_is_your_native_language
 * @property int $are_you_currently_employed
 * @property int $do_you_know_any_language
 * @property int $foreign_language
 */
class UserEducation extends \yii\db\ActiveRecord
{

    const eggDonor = 'eggDonor';
    const eggDonorForeignLanguage = 'eggDonorForeignLanguage';
    const surrogate = 'surrogate';
    const surrogateForeignLanguage = 'surrogateForeignLanguage';

    public static function tableName()
    {
        return 'user_education';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'educational_level', 'occupation', 'what_is_your_native_language', 'are_you_currently_employed', 'do_you_know_any_language', 'foreign_language','what_type_of_educational_goals'], 'required'],
            [['user_id', 'educational_level', 'are_you_currently_employed', 'do_you_know_any_language', 'foreign_language'], 'integer'],
            [['occupation', 'what_is_your_native_language'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'educational_level' => 'Educational Level',
            'occupation' => 'Occupation',
            'what_is_your_native_language' => 'What Is Your Native Language',
            'are_you_currently_employed' => 'Are You Currently Employed',
            'do_you_know_any_language' => 'Do You Know Any Language',
            'foreign_language' => 'Foreign Language',
        ];
    }

    public function scenarios()
    {
        return [
            self::eggDonor => ['user_id', 'educational_level', 'occupation', 'what_is_your_native_language', 'are_you_currently_employed', 'do_you_know_any_language', 'foreign_language','what_type_of_educational_goals'],
            self::eggDonorForeignLanguage => ['user_id', 'educational_level', 'occupation', 'what_is_your_native_language', 'are_you_currently_employed', 'do_you_know_any_language','what_type_of_educational_goals'],

            self::surrogate => ['user_id', 'educational_level', 'occupation', 'what_is_your_native_language', 'are_you_currently_employed', 'do_you_know_any_language', 'foreign_language'],
            self::surrogateForeignLanguage => ['user_id', 'educational_level', 'occupation', 'what_is_your_native_language', 'are_you_currently_employed', 'do_you_know_any_language'],
        ];
    }
}
