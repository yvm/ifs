<?php
namespace common\models;

use Yii;

class FamilyHealthHistory extends \yii\db\ActiveRecord
{
    const role1 = "role1";
    const surrogate = 'surrogate';
    const egg_donation = 'egg_donation';
    const surrogate_donation = 'surrogate_donation';

    public static function tableName()
    {
        return 'family_health_history';
    }

    public function rules()
    {
        return [
            [['user_id', 'did_you_have', 'you_adopted'], 'required'],
            [['user_id', 'did_you_have', 'you_adopted'], 'integer'],
            [['created_at', 'your_family', 'at_all'], 'safe'],
            [['user_id'], 'unique'],
            [['did_you_have'], 'did_you_haveValue', 'on' => self::role1],
            [['user_id'], 'FamilyMemberInformation'],
        ];
    }

    public function FamilyMemberInformation($attribute,$params)
    {
        $model = FamilyMemberInformation::findAll(['user_id' => Yii::$app->session['reg_user']]);
        if(empty($this->you_adopted) && !count($model))
            $this->addError("user_id", "Нужно указать хотя бы одного родственника(нужно заполнить все поля)");
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'did_you_have' => Yii::t('app', 'Did You Have'),
            'you_adopted' => Yii::t('app', 'You Adopted'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function did_you_haveValue($attribute,$params)
    {
        if(!empty($this->did_you_have)) {
            if(empty($this->your_family))
                $this->addError("your_family", "Необходимо указать your_family.");
            if(empty($this->at_all))
                $this->addError("at_all", "Необходимо указать at_all.");
        }
    }
}
