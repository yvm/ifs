<?php
namespace common\models;

use Yii;

class Surrogatefaq extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogatefaq';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
