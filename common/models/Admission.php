<?php
namespace common\models;

use Yii;

class Admission extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'admission';
    }

    public function rules()
    {
        return [
            [['compensation','role','photo','video','visibility'], 'required'],
            [['compensation','role','photo','video','visibility'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'visibility' => 'Видимость подвесной профиль',
            'compensation' => 'Компенсация',
            'photo' => 'Фото',
            'video' => 'Видео',
            'role' => 'Роль'
        ];
    }

    public function getRoles()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
}
