<?php
namespace common\models;

use Yii;

class TranslationModal extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'translation_modal';
    }

    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }
}
