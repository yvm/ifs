<?php
namespace common\models;

use Yii;

class Donationcoststage extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'donationcoststage';
    }

    public function rules()
    {
        return [
            [['name', 'cost_id', 'country_id', 'language_id','content'], 'required'],
            [['cost_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['content'],'string']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'cost_id' => 'Программа',
            'content' => 'Содержание',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }

    public function getCost()
    {
        return $this->hasOne(Donationcost::className(), ['id' => 'cost_id']);
    }

    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Donationcoststage::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

}
