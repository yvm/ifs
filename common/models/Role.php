<?php
namespace common\models;

use Yii;

class Role extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'role';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Role::find()->all(),'id','name');
    }
}
