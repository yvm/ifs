<?php
namespace common\models;

use Yii;

class Surrogacycosttype extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogacycosttype';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'surrogacycost_id', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['surrogacycost_id', 'country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание'),
            'surrogacycost_id' => Yii::t('app', 'Категория'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
    public function getSurrogacyCost()
    {
        return $this->hasOne(Surrogacycost::className(), ['id' => 'surrogacycost_id']);
    }

    public function getSurrogacyCostName(){
        return (isset($this->surrogacyCost))? $this->surrogacyCost->name:'Не задан';
    }
}
