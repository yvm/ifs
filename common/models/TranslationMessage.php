<?php
namespace common\models;

use Yii;

class TranslationMessage extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'translation_message';
    }

    public function rules()
    {
        return [
            [['name', 'country_id', 'language_id'], 'required'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'country_id' => 'Country ID',
            'language_id' => 'Language ID',
        ];
    }
}
