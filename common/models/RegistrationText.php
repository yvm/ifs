<?php
namespace common\models;

use Yii;

class RegistrationText extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'registration_text';
    }

    public function rules()
    {
        return [
            [['role', 'type', 'text'], 'required'],
            [['role', 'type'], 'integer'],
            [['text'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role' => 'Role',
            'type' => 'Type',
            'text' => 'Текст',
        ];
    }
}
