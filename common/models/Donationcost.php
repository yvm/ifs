<?php
namespace common\models;

use Yii;

class Donationcost extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'donationcost';
    }

    public function rules()
    {
        return [
            [['name', 'content', 'price', 'country_id','package_title',
                'package_subtitle','package_content','package_tabletitle', 'language_id'], 'required'],
            [['content','package_content'], 'string'],
            [['country_id', 'language_id','statusPriceGuest','statusPriceUser'], 'integer'],
            [['name', 'price','package_title','package_subtitle','package_tabletitle'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Название',
            'content' => Yii::t('app', 'Содержание'),
            'price' => Yii::t('app', 'Цена'),
            'statusPriceGuest' => 'Статус цена для незарегистрированых пользователей ',
            'statusPriceUser' => 'Статус цена для зарегистрированных пользователей',
            'package_title' => Yii::t('app', 'Заголовок пакета'),
            'package_subtitle' => Yii::t('app', 'Подзаголовок пакета'),
            'package_content' => Yii::t('app', 'Описание'),
            'package_tabletitle' => Yii::t('app', 'Заголовок таблица '),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }


    public static function getList(){
        return \yii\helpers\ArrayHelper::map(Donationcost::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all(),'id','name');
    }

    public function getTypes(){
        return $this->hasMany(Donationcosttype::className(), ['donationcost_id' => 'id']);
    }
}
