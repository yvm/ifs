<?php
namespace common\models;

use Yii;

class Consultation extends \yii\db\ActiveRecord
{

    const surrogate_donation = 'surrogate_donation';
    const biotrans = 'biotrans';

    public static function tableName()
    {
        return 'consultation';
    }

    public function rules()
    {
        return [
            [['user_id', 'ready_consultation'], 'required'],
            [['user_id', 'ready_consultation', 'location', 'time_from'], 'integer'],
            [['questions_comments'], 'string'],
            [['time_date', 'created_at'], 'safe'],
            [['time_time'], 'string', 'max' => 10],
            [['user_id'], 'unique'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'ready_consultation' => Yii::t('app', 'Ready Consultation'),
            'questions_comments' => Yii::t('app', 'Questions Comments'),
            'location' => Yii::t('app', 'Location'),
            'time_date' => Yii::t('app', 'Time Date'),
            'time_time' => Yii::t('app', 'Time Time'),
            'time_from' => Yii::t('app', 'Time From'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }


    public function scenarios()
    {
        return [

            self::surrogate_donation => ['user_id', 'ready_consultation','questions_comments','location','time_date','time_time','time_from'],
            self::biotrans => ['user_id', 'ready_consultation'],
        ];
    }
}
