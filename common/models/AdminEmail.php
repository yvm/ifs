<?php
namespace common\models;

use Yii;

class AdminEmail extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'admin_email';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
