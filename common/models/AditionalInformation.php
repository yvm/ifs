<?php
namespace common\models;

use Yii;

class AditionalInformation extends \yii\db\ActiveRecord
{
    const surrogate_donation = 'surrogate_donation';
    const surrogate = 'surrogate';
    const egg_donation = 'egg_donation';
    const biotrans = 'biotrans';

    public static function tableName()
    {
        return 'aditional_information';
    }

    public function rules()
    {
        return [
            [['user_id','reason_surrogacy','describe', 'timing_to_begin', 'know_about_us'
                ,'financially_prepared'], 'required'],
            [['user_id', 'reason_surrogacy', 'egg_donor_needed', 'surrogate_needed', 'timing_to_begin', 'financially_prepared', 'know_about_us'], 'integer'],
            [['describe',  'referal','message_to_surrogate_eggDonor'], 'string'],
            [['created_at'], 'safe'],
            [['who_recommended'], 'string', 'max' => 255],
            [['user_id'], 'unique'],
            [['know_about_us'], 'yes_no'],
			[['surrogate_needed'], 'yes_no_1'],
			[['egg_donor_needed'], 'yes_no_2'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'reason_surrogacy' => Yii::t('app', 'Reason Surrogacy'),
            'describe' => Yii::t('app', 'Describe'),
            'surrogate_needed' => Yii::t('app', 'Surrogate Needed'),
            'timing_to_begin' => Yii::t('app', 'Timing To Begin'),
            'financially_prepared' => Yii::t('app', 'Financially Prepared'),
            'know_about_us' => Yii::t('app', 'Know About Us'),
            'who_recommended' => Yii::t('app', 'Who Recommended'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public function yes_no($attribute,$params)
    {
       
        if($this->know_about_us == 16) {
            if (empty($this->referal))
                $this->addError("referal", "Необходимо заполнить referal.");
        }
        if($this->know_about_us == 17) {
            if (empty($this->who_recommended))
                $this->addError("who_recommended", "Необходимо заполнить who_recommended.");
        }
    }
	
	
	public function yes_no_1($attribute,$params)
    {
        if($this->surrogate_needed == 1) {
            if (empty($this->message_to_surrogate_eggDonor))
                $this->addError("message_to_surrogate_eggDonor", "Необходимо заполнить message_to_surrogate_eggDonor.");
        }
      
    }
	
	public function yes_no_2($attribute,$params)
    {
        if($this->egg_donor_needed == 1) {
            if (empty($this->message_to_surrogate_eggDonor))
                $this->addError("message_to_surrogate_eggDonor", "Необходимо заполнить message_to_surrogate_eggDonor.");
        }
    }

    public function scenarios()
    {
        return [

            self::surrogate_donation => ['user_id','reason_surrogacy','describe', 'timing_to_begin', 'know_about_us','surrogate_needed','egg_donor_needed',
                'message_to_surrogate_eggDonor','financially_prepared','referal','who_recommended'],

            self::surrogate => ['user_id','reason_surrogacy','describe', 'timing_to_begin', 'know_about_us','surrogate_needed',
                'message_to_surrogate_eggDonor','financially_prepared','referal','who_recommended'],

            self::egg_donation => ['user_id','reason_surrogacy','describe', 'timing_to_begin', 'know_about_us','egg_donor_needed',
                'message_to_surrogate_eggDonor','financially_prepared','referal','who_recommended'],

            self::biotrans => ['user_id','timing_to_begin','know_about_us'],
        ];
    }
}
