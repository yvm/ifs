<?php
namespace common\models;

use Yii;

class Donorabout extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/donorabout/';

    public static function tableName()
    {
        return 'donorabout';
    }

    public function rules()
    {
        return [
            [['content', 'country_id', 'language_id'], 'required'],
            [['content','contenta','contentb'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Содержание для компьютерной версии'),
            'contenta' => Yii::t('app', 'Видные содержания для мобильной версии'),
            'contentb' => Yii::t('app', 'Скрытое содержания для мобильной версии'),
            'image' => Yii::t('app', 'Картинка'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/eggdonor/' . $this->image : '/no-image.png';
    }
}
