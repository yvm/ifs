<?php
namespace common\models;

use Yii;

class Donorcomp extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'donorcomp';
    }

    public function rules()
    {
        return [
            [['contenta', 'contentb',  'country_id', 'language_id'], 'required'],
            [['contenta', 'contentb'], 'string'],
            [['country_id', 'language_id'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'contenta' => Yii::t('app', 'Содержание'),
            'contentb' => Yii::t('app', 'Содержание снизу'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
