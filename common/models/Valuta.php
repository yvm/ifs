<?php
namespace common\models;

use Yii;

class Valuta extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'valuta';
    }

    public function rules()
    {
        return [
            [['text', 'sort'], 'required'],
            [['sort','status'], 'integer'],
            [['text'], 'string', 'max' => 10],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'text' => 'Текст',
            'status' => 'Статус',
            'sort' => 'Сортировка',
        ];
    }

    public function beforeSave($insert){
        if($this->isNewRecord) {
            $model = Valuta::find()->orderBy('sort DESC')->one();
            if (!$model || $this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
        }
        return parent::beforeSave($insert);
    }
}
