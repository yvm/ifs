<?php
namespace common\models;

use Yii;

class Height extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'height';
    }

    public function rules()
    {
        return [
            [['from', 'to', 'role'], 'required'],
            [['from', 'to', 'role'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'От',
            'to' => 'До',
            'role' => 'Роль',
        ];
    }

    public function getRoles()
    {
        return $this->hasOne(Role::className(), ['id' => 'role']);
    }
}
