<?php
namespace common\models;

use Yii;

class Surrogacyservice extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/surrogacyservice/';

    public static function tableName()
    {
        return 'surrogacyservice';
    }

    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['image'], 'file', 'extensions' => 'png,jpg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'content' => Yii::t('app', 'Содержание'),
            'image' => Yii::t('app', 'Картинка'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogacy/' . $this->image : '/no-image.png';
    }

}
