<?php
namespace common\models;

use Yii;

class Program extends \yii\db\ActiveRecord
{
    public $program_type2;
    public $program_name2;
    public $program_city2;
    public $program_manager2;
    public $ivf_clinic2;
    public $physician2;

    public static function tableName()
    {
        return 'program';
    }

    public function rules()
    {
        return [
            [['user_id', 'program_type', 'program_name', 'program_status', 'beginning_program', 'completion_program', 'program_country', 'program_city', 'program_cost', 'program_cost_valuta', 'program_manager', 'ivf_clinic', 'physician', 'surrogate', 'egg_donor', 'program_notes'], 'required'],
            [['user_id'], 'integer'],
            [['created_at'], 'safe'],
            [['program_type', 'program_name', 'program_status', 'beginning_program', 'completion_program', 'program_country', 'program_city', 'program_cost', 'program_cost_valuta', 'program_manager', 'ivf_clinic', 'physician', 'surrogate', 'egg_donor', 'program_notes'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'program_type' => 'Program Type',
            'program_name' => 'Program Name',
            'program_status' => 'Program Status',
            'beginning_program' => 'Beginning Program',
            'completion_program' => 'Completion Program',
            'program_country' => 'Program Country',
            'program_city' => 'Program City',
            'program_cost' => 'Program Cost',
            'program_cost_valuta' => 'Program Cost Valuta',
            'program_manager' => 'Program Manager',
            'ivf_clinic' => 'Ivf Clinic',
            'physician' => 'Physician',
            'surrogate' => 'Surrogate',
            'egg_donor' => 'Egg Donor',
            'program_notes' => 'Program Notes',
            'created_at' => 'Created At',
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function getProgram_stages(){
        return $this->hasMany(ProgramStages::className(), ['program_id' => 'id']);
    }

    public function getProgram_stages_got_it(){
        return $this->hasMany(ProgramStages::className(), ['program_id' => 'id'])->where('got_it = 0');
    }
}
