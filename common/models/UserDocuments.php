<?php
namespace common\models;

use Yii;

class UserDocuments extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/userdocuments/';

    public static function tableName()
    {
        return 'user_documents';
    }

    public function rules()
    {
        return [
            [['user_id', 'img'], 'required'],
            [['user_id'], 'integer'],
//            [['user_id'], 'unique'],
//            ['files', 'each', 'rule' => ['image']],
//            [['img'], 'file', 'skipOnEmpty' => false, 'extensions' => ['png, jpg, jpeg'], 'maxFiles' => 10, 'maxSize' => 1024 * 1024 * 5],
//            [['img'], 'file', 'extensions' => ['png', 'jpg', 'jpeg'], 'maxSize' => 1024 * 1024 * 0.5],
        ];
    }

    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'img' => 'Картинка',
        ];
    }

    public function upload()
    {
        $time = time();
        $this->img->saveAs($this->path. $time . $this->img->baseName . '.' . $this->img->extension);
        return $time . $this->img->baseName . '.' . $this->img->extension;
    }
}
