<?php
namespace common\models;

use Yii;

class Surrogatepsyc extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/surrogatepsyc/';

    public static function tableName()
    {
        return 'surrogatepsyc';
    }

    public function rules()
    {
        return [
            [['content', 'country_id', 'language_id'], 'required'],
            [['content','contenta','contentb'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Картинка'),
            'content' => Yii::t('app', 'Содержание для компьютерной версии'),
            'contenta' => Yii::t('app', 'Видные содержания для мобильной версии'),
            'contentb' => Yii::t('app', 'Скрытое содержания для мобильной версии'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/surrogate/' . $this->image : '/no-image.png';
    }
}
