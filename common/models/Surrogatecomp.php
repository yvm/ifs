<?php
namespace common\models;

use Yii;

class Surrogatecomp extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'surrogatecomp';
    }

    public function rules()
    {
        return [
            [['name', 'contenta', 'contentb', 'contentc', 'country_id', 'language_id'], 'required'],
            [['contenta', 'contentb', 'contentc'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => 'Заголовок',
            'contenta' => Yii::t('app', 'Содержание слева'),
            'contentb' => Yii::t('app', 'Содержание справа'),
            'contentc' => Yii::t('app', 'Содержание снизу'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }
}
