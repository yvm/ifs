<?php
namespace common\models;

use Yii;

class Donorforeigntype extends \yii\db\ActiveRecord
{
    public $files;
    public $path = 'images/donorforeigntype/';

    public static function tableName()
    {
        return 'donorforeigntype';
    }

    public function rules()
    {
        return [
            [['content', 'country_id', 'language_id'], 'required'],
            [['content'], 'string'],
            [['country_id', 'language_id'], 'integer'],
            [['image'], 'file', 'extensions' => 'png,jpg,jpeg'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'image' => Yii::t('app', 'Картинка'),
            'content' => Yii::t('app', 'Содержание'),
            'country_id' => Yii::t('app', 'Country ID'),
            'language_id' => Yii::t('app', 'Language ID'),
        ];
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/eggdonor/' . $this->image : '/no-image.png';
    }
}
