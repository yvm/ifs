<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "user_characteristic".
 *
 * @property int $id
 * @property int $user_id
 * @property string $favorite_food
 * @property string $favorite_color
 * @property string $favorite_sport
 * @property string $favorite_book
 * @property string $favorite_singer_or_group
 * @property string $favorite_movie
 * @property string $favorite_hobby
 * @property string $artistic_talent
 * @property string $personality_character
 * @property string $achievements
 * @property string $goals
 * @property string $talents
 * @property string $in_five_years_where_do_you_see_yourself
 * @property string $in_ten_years_where_do_you_see_yourself
 * @property string $in_school_what_subject_did_you_enjoy
 * @property string $what_were_your_academic_strengths
 * @property string $what_do_you_like_about_yourself
 * @property string $if_you_could_change
 * @property string $what_does_family_mean
 * @property string $who_is_one_person
 * @property string $memorable_moments
 * @property string $message_to_intended_parents
 * @property string $why_are_you_interested
 * @property int $religion
 */
class UserCharacteristic extends \yii\db\ActiveRecord
{


    const eggDonor = 'eggDonor';
    const surrogate = 'surrogate';

    public static function tableName()
    {
        return 'user_characteristic';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'favorite_food','favorite_color','favorite_sport','favorite_book','favorite_singer_or_group','favorite_movie',
                'favorite_hobby','artistic_talent','personality_character','achievements','goals','talents','in_five_years_where_do_you_see_yourself',
                'in_ten_years_where_do_you_see_yourself','in_school_what_subject_did_you_enjoy','what_were_your_academic_strengths',
                'what_do_you_like_about_yourself', 'if_you_could_change', 'what_does_family_mean', 'who_is_one_person','memorable_moments',
                'message_to_intended_parents', 'why_are_you_interested','religion'], 'required'],
            [['user_id', 'religion'], 'integer'],
            [['favorite_food', 'favorite_color', 'favorite_sport', 'favorite_book', 'favorite_singer_or_group', 'favorite_movie', 'favorite_hobby', 'artistic_talent', 'personality_character', 'achievements', 'goals', 'talents', 'in_five_years_where_do_you_see_yourself', 'in_ten_years_where_do_you_see_yourself', 'in_school_what_subject_did_you_enjoy', 'what_were_your_academic_strengths', 'what_do_you_like_about_yourself', 'if_you_could_change', 'what_does_family_mean', 'who_is_one_person', 'memorable_moments', 'message_to_intended_parents', 'why_are_you_interested'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'favorite_food' => 'Favorite Food',
            'favorite_color' => 'Favorite Color',
            'favorite_sport' => 'Favorite Sport',
            'favorite_book' => 'Favorite Book',
            'favorite_singer_or_group' => 'Favorite Singer Or Group',
            'favorite_movie' => 'Favorite Movie',
            'favorite_hobby' => 'Favorite Hobby',
            'artistic_talent' => 'Artistic Talent',
            'personality_character' => 'Personality Character',
            'achievements' => 'Achievements',
            'goals' => 'Goals',
            'talents' => 'Talents',
            'in_five_years_where_do_you_see_yourself' => 'In Five Years Where Do You See Yourself',
            'in_ten_years_where_do_you_see_yourself' => 'In Ten Years Where Do You See Yourself',
            'in_school_what_subject_did_you_enjoy' => 'In School What Subject Did You Enjoy',
            'what_were_your_academic_strengths' => 'What Were Your Academic Strengths',
            'what_do_you_like_about_yourself' => 'What Do You Like About Yourself',
            'if_you_could_change' => 'If You Could Change',
            'what_does_family_mean' => 'What Does Family Mean',
            'who_is_one_person' => 'Who Is One Person',
            'memorable_moments' => 'Memorable Moments',
            'message_to_intended_parents' => 'Message To Intended Parents',
            'why_are_you_interested' => 'Why Are You Interested',
            'religion' => 'Religion',
        ];
    }

    public function scenarios()
    {
        return [
            self::eggDonor => ['user_id', 'favorite_food','favorite_color','favorite_sport','favorite_book','favorite_singer_or_group','favorite_movie',
                'favorite_hobby','artistic_talent','personality_character','achievements','goals','talents','in_five_years_where_do_you_see_yourself',
                'in_ten_years_where_do_you_see_yourself','in_school_what_subject_did_you_enjoy','what_were_your_academic_strengths',
                'what_do_you_like_about_yourself', 'if_you_could_change', 'what_does_family_mean', 'who_is_one_person','memorable_moments',
                'message_to_intended_parents', 'why_are_you_interested','religion'],

           self::surrogate => ['user_id','what_do_you_like_about_yourself', 'if_you_could_change', 'what_does_family_mean',
                'message_to_intended_parents', 'why_are_you_interested'],

        ];
    }
}
