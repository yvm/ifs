<?php
namespace frontend\models;

use common\models\Profiles;
use yii\base\Model;
use common\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $type;
    public $type1;
    public $password;
    public $captcha;
    public $captcha_old;
    public $captcha_repeat;

    const reg_user1 = 'reg_user1';
    const reg_user2 = 'reg_user2';
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            [['email', 'type', 'captcha_repeat'], 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => '<p><span>The email adress you entered is 
                    already registered with IFS.</span><br>
                  Please choose another email address or <span><a href="">login</a></span> to proceed</p>'],

//            ['password', 'required'],
            ['password', 'string', 'min' => 6],

            [['type1', 'captcha', 'captcha_old', 'captcha_repeat'], 'safe'],
            ['captcha_repeat', 'compare', 'compareAttribute' => 'captcha', 'message' => 'Неправильный проверочный код'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new User();
        $user->username = $this->email;
        $user->email = $this->email;
        $user->role = $this->type;
        if($user->role == 1) {
            if(count($this->type1)==2)
                $user->type = 3;
            else{
                $user->type = $this->type1[0];
            }
        }
//        $user->setPassword(12345678);
        $user->generateAuthKey();
        $user->status = $user::STATUS_READY;
        $user->save(false);

        $profile = new Profiles();
        $profile->user_id = $user->id;
        $profile->email = $user->email;
        $profile->save(false);

        return $user->id ? $user : null;
    }

    public function actionVipCapcha($length)
    {
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        $max = strlen($codeAlphabet);

        for ($i=0; $i < $length; $i++) {
            $token .= $codeAlphabet[$this->crypto_rand_secure(0, $max-1)];
        }

        $token = $this->createImg($token);

        return $token;
    }

    public function crypto_rand_secure($min, $max)
    {
        $range = $max - $min;
        if ($range < 1) return $min; // not so random...
        $log = ceil(log($range, 2));
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd > $range);
        return $min + $rnd;
    }

    public function createImg($str)
    {
        Header("Content-type: image/png");

        $im=imagecreatetruecolor(120,50);

        $white1 = imagecolorallocate($im, 255, 255, 255);
        $grey = imagecolorallocate($im, 128, 128, 128);

        $text = $str;
        $font = $_SERVER['DOCUMENT_ROOT']."/frontend/web/fonts/Lato-Regular.ttf";

        $white = imagecolorallocate($im,74, 94, 104);
        imagefill($im, 0, 0, $white);

        imagettftext($im, 14, 0, 11, 33, $white1, $font, $text);

        if($_SESSION['name_capcha'])
            unlink($_SERVER['DOCUMENT_ROOT']."/frontend/web/images/captcha/".$_SESSION['name_capcha'].".png");

        $_SESSION['captcha'] = $text;
        $_SESSION['name_capcha'] = time();

        imagepng($im, $_SERVER['DOCUMENT_ROOT']."/frontend/web/images/captcha/".$_SESSION['name_capcha'].".png");

        return $_SESSION['name_capcha'];
    }

    public function scenarios()
    {
        return [

            self::reg_user1 => ['email'],

            self::reg_user2 => ['email','captcha_repeat', 'type', 'type1'],

        ];
    }
}
