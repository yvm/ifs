<?if(empty($_GET['page'])) $page = 1; else $page = $_GET['page'];?>
<section class="blue-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="inner-title">
                    <h3>Search result</h3>
                </div>
            </div>
            <div class="bg-white">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="found-result">
                            <p>Found <?=$count?> results for </p>
                        </div>
                        <div class="found-result-country text-center">
                            <p><?=$_GET['text']?></p>
                        </div>
                        <?if($result)foreach($result[$page] as $v){?>
                            <div class="search-item">
                                <div class="found-result-country">
                                    <p><a href="<?=$v['url']?>"><?=$v['name']?></a></p>
                                </div>
                                <div class="search-result-text">
                                    <p><?=$v['text']?></p>
                                </div>
                            </div>
                        <?}?>
                        <div class="pagination">
                            <ul>
                                <?if($result)foreach($result as $k => $v){?>
                                    <li><a <?if($page == $k){?>class="active"<?}?> href="http://<?=$_SERVER['SERVER_NAME'].'/search/index?text='.$_GET['text'].'&page='.$k?>"><?=$k?></a></li>
                                <?}?>
<!--                                <li class="dots"><a href="">...</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>