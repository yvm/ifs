
<section class="blue-bg big-bottom-padding">
    <div class="container">
        <div class="row">
            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$video['main']->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$video['main']->bigTitle?></h3>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>
            <div class="bg-white ">
                <div class="row">
                    <? foreach ( $video['data'] as $v):?>
                        <div class="col-lg-4 col-md-6 col-sm-12">
                            <div class="gallery-item">
                                <a href="view?id=<?=$v->id?>">
                                    <img src="<?=$v->getImage();?>" class="img-flid" alt="" title="">
                                    <div class="caption-text">
                                        <p><?=$v->name;?> </p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    <? endforeach;?>
                </div>
            </div>

        </div>
    </div>

    </div>
</section>

