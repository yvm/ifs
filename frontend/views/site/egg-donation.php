<main class="egg-donation-main">
    <div class="container">

        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link inner-btn-brown'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text-brown">
                    <p><?=$eggdonation['main']->littleTitle?></p>
                </div>
                <div class="inner-title-brown">
                    <h3><?=$eggdonation['main']->bigTitle?></h3>
                </div>
                <div class="inner-title-description-brown">
                    <p><?=$eggdonation['main']->slogan?></p>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link-brown dash-bot-brown">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link-brown dash-bot-brown">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>
		</div>
		</div>
</main>
<section class="egg-donation">
    <div class="container">
        <div class="row">

            <div class="bg-white" id="egg-donation-programs" style="margin-top: 130px;">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$eggdonation['sub'][0]->name?></h3>
                        </div>
                        <div class="about-surrogacy-text">
                            <?=$eggdonation['programcon']->content?>
                        </div>
                    </div>

                    <? foreach ($eggdonation['programtype'] as $v):?>
                    <div class="col-lg-4">
                        <div class="egg-program-item">
                            <div class="egg-donation-img">
                                <img src="<?=$v->getImage();?>" alt="">
                            </div>
                                <p>
                                    <?=$v->name;?>
                                </p>
                            <div class="about-surrogacy-text">
                                <p><?=$v->content;?></p>
                            </div>
                        </div>
                    </div>
                    <? endforeach;?>
                </div>
            </div>

            <div class="bg-grey" id="egg-donation-how-to-start">
                <div class="desktop-version row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$eggdonation['sub'][1]->name?></h3>
                        </div>
                        <div class="row">
                            <? $m=0;?>
                            <? foreach ($eggdonation['howStart'] as $v):?>
                            <? $m++;?>
                            <div class="col-lg-3">
                                <div class="round-num">
                                    <span><?=$m?></span>
                                </div>
                                <div class="benefit-text">
                                    <p><?=$v->name?></p>
                                </div>
                            </div>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="mobile-version">
                    <div class="section-title">
                        <h3><?=$eggdonation['sub'][1]->name?></h3>
                    </div>
                    <div class="row">
                        <div class="col-sm-1 col-3">
                            <div class="circle">
                                <ul>
                                    <? $m=0;?>
                                    <? foreach ( $eggdonation['howStart'] as $v):?>
                                    <? $m++;?>
                                        <li><?=$m?></li>
                                    <? endforeach;?>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-11 col-9">
                            <div class="circle-text">
                                <ul>
                                    <? foreach ( $eggdonation['howStart'] as $v):?>
                                    <li>
                                        <p><?=$v->name?></p>
                                    </li>
                                    <? endforeach;?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="bg-white" id="egg-donation-costs">
                <div class="row desktop-version">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$eggdonation['sub'][2]->name?></h3>
                        </div>
                    </div>

					<div class="col-sm-12">
                    	<p><?= $eggdonation['content']->content?></p>
					</div>

                    <? foreach ( $eggdonation['cost'] as $v):?>
                    <div class="col-lg-4">
                        <div class="cost-item">
                            <div class="cost-title">
                                <p><?=$v->name?></p>
                            </div>
                            <? $types = $v->types;?>
                            <? foreach ($types as $type):?>
                                <div class="collapse-text">
                                    <a data-toggle="collapse" href="#collapseExample<?=$type->id?>">
                                        <p><?=$type->name?></p>
                                        <div class="collapse-arrow">
                                            <img src="/images/collapse-arrow.png" alt="">
                                        </div>
                                    </a>
                                </div>
                                <div class="collapse" id="collapseExample<?=$type->id?>">
                                    <div class="collapsed-text">
                                        <?=$type->content?>
                                    </div>
                                </div>
                            <? endforeach;?>
                            <div class="cost-title-left">
                                <?=$v->content?>
                            </div>
                            <? if(Yii::$app->user->isGuest):?>
                            <? if($v->statusPriceGuest):?>
                            <div class="price">
                                <p><?=$v->price?></p>
                            </div>
                            <? endif;?>
                            <? if(!$v->statusPriceGuest):?>
                                <a href="#register-email" class="btn ths-popup-link btn-start-program mb-2"><?=$eggdonation['translation'][1]->name;?></a>
                            <? endif;?>
                            <? endif;?>


                            <? if(!Yii::$app->user->isGuest):?>
                                <? if($v->statusPriceUser):?>
                                    <div class="price">
                                        <p><?=$v->price?></p>
                                    </div>
                                <? endif;?>
                                <? if(!$v->statusPriceUser):?>
                                    <a href="#register-email" class="btn ths-popup-link btn-start-program"><?=$eggdonation['translation'][1]->name;?></a>
                                <? endif;?>
                                <a href="" class="btn btn-start-program"><?=$eggdonation['translation'][0]->name;?></a>
                            <? endif;?>
                            <a href="/site/egg-donation-cost?id=<?=$v->id?>" class="btn btn-start-program"><?=$eggdonation['translation'][2]->name;?></a>
                        </div>
                    </div>
                    <? endforeach;?>
                </div>
                <div class="row mobile-version">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$eggdonation['sub'][2]->name?></h3>
                        </div>
                    </div>
                    <div class="mobile-owl owl-carousel owl-theme">
                        <? foreach ( $eggdonation['cost'] as $v):?>
                        <div class="item">
                            <div class="col-lg-4">
                                <div class="cost-item">
                                    <div class="cost-title">
                                        <p><?=$v->name?></p>
                                    </div>
                                    <? $types = $v->types;?>
                                    <? foreach ($types as $type):?>
                                        <div class="collapse-text">
                                            <a data-toggle="collapse" href="#collapseExample<?=$type->id?>">
                                                <p><?=$type->name?></p>
                                                <div class="collapse-arrow">
                                                    <img src="/images/collapse-arrow.png" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="collapse" id="collapseExample<?=$type->id?>">
                                            <div class="collapsed-text">
                                                <?=$type->content?>
                                            </div>
                                        </div>
                                    <? endforeach;?>
                                    <div class="cost-title-left">
                                        <?=$v->content;?>
                                    </div>

                                    <? if(Yii::$app->user->isGuest):?>
                                        <? if($v->statusPriceGuest):?>
                                            <div class="price">
                                                <p><?=$v->price?></p>
                                            </div>
                                        <? endif;?>
									 
                                        <? if(!$v->statusPriceGuest):?>
                                            <a href="#register-email" class="btn ths-popup-link btn-start-program"><?=$eggdonation['translation'][1]->name;?></a>
                                        <? endif;?>
                                    <? endif;?>

                                    <? if(!Yii::$app->user->isGuest):?>
                                        <? if($v->statusPriceUser):?>
                                            <div class="price">
                                                <p><?=$v->price?></p>
                                            </div>
                                        <? endif;?>
                                        <a href="" class="btn btn-start-program"><?=$eggdonation['translation'][0]->name;?></a>
                                    <? endif;?>
                                    <a href="/site/egg-donation-cost?id=<?=$v->id?>" class="btn btn-start-program"><?=$eggdonation['translation'][2]->name;?></a>
                                </div>
                            </div>
                        </div>
                        <? endforeach;?>
                    </div>
                </div>

            </div>

            <div class="mobile-version bg-grey" >
                <div class="mobile-bg-block">
                    <div class="egg-donation-text">
                        <p><?=$eggdonation['banner']->name?></p>
                    </div>
                    <a href="" class="btn btn-find-sur"><?=$eggdonation['banner']->buttonName?></a>
                </div>
            </div>

            <div class="bg-egg-donation-section">
                <div class="row desktop-version">
                    <div class="offset-lg-7 col-sm-5">
                        <div class="egg-donation-text">
                            <p><?=$eggdonation['banner']->name?></p>
                        </div>
						<?if(!Yii::$app->user->isGuest):?>
							<a href="/database/egg-donor-database" class="btn btn-find-sur"><?=$eggdonation['banner']->buttonName?></a>
						<? endif;?>
						<?if(Yii::$app->user->isGuest):?>
							<a href="#modal-missing" class="btn btn-find-sur ths-popup-link"><?=$eggdonation['banner']->buttonName?></a>
						<? endif;?>

                    </div>
                </div>
            </div>
        </div>

    </div>
</section>

