
<section class="inner-surrogate">
    <div class="container">
        <div class="row">
            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>
            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$surrogate['main']->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$surrogate['main']->bigTitle?></h3>
                </div>
                <div class="inner-title-description">
                    <p><?=$surrogate['main']->slogan?></p>
                </div>
            </div>

            <div class="offset-lg-4 col-lg-2 col-6">
                <div class="connect-link">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-lg-2 col-6">
                <div class="connect-link">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link second-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <? if($surrogate['howAllWorks']->status):?>
                <div class="bg-grey" id="surrogate-how-it-works">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title">
                                <h3><?=$surrogate['sub'][0]->name?></h3>
                            </div>
                            <div class="video">
                                <iframe width="100%" height="500px"
                                        src="<?=$surrogate['howAllWorks']->url;?>">
                                </iframe>
                            </div>
                        </div>
                    </div>
                </div>
            <? endif;?>

            <div class="bg-grey" id="surrogate-about">
                <div class="row desktop-version">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][1]->name?></h3>
                        </div>
                    </div>
                    <div class="col-lg-8">
                        <div class="section-subtitle">
                            <p><?= $surrogate['about']->name?></p>
                        </div>
                        <div class="about-surrogacy-text">
                            <?= $surrogate['about']->content?>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="about-img">
                            <img src="<?= $surrogate['about']->getImage();?>" class="img-fluid" alt="pregnant woman" title="pregnant woman">
                        </div>
                    </div>

                </div>
                <div class="row mobile-version">
                    <div class="section-title">
                        <h3><?=$surrogate['sub'][1]->name?></h3>
                    </div>
                    <div class="section-subtitle">
                        <p><?= $surrogate['about']->name?></p>
                    </div>
                    <div class="about-surrogacy-text">
                        <?= $surrogate['about']->contenta?>
                    </div>
                    <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#mobile1" aria-expanded="false" aria-controls="mobile1">
                        <?=$surrogate['translation'][0]->name?>
                    </button>
                    <div class="collapse" id="mobile1">
                        <div class="about-surrogacy-text">
                            <?= $surrogate['about']->contentb?>
                        </div>
                        <div class="col-lg-4">
                            <div class="about-img">
                                <img src="<?= $surrogate['about']->getImage();?>" class="img-fluid" alt="pregnant woman" title="pregnant woman">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-white" id="surrogate-benefits">
                <div class="row">
                    <div class="col-lg-12 ">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][2]->name?> </h3>
                        </div>
                        <div class="row desktop-version">
                            <? $m=0;?>
                            <? foreach ($surrogate['benefit'] as $v):?>
                            <? $m++;?>
                            
                           
                            <? if($m % 5 == 1):?>
                            <div class="flex-row">
                            <? endif;?>
                                <div class="flex-row-item">
                                    <div class="round-num">
                                        <span><?=$m?></span>
                                    </div>
                                    <div class="benefit-text">
                                        <p><?=$v->name?></p>
                                    </div>
                                </div>
                            <? if($m % 5 == 0):?>
                            </div>
                            
                            <? endif;?>
                            <? endforeach;?>

                            <? if($m % 5 != 0):?>
                            </div>
                            <? endif;?>
                        </div>
                    </div>
                    <div class="mobile-owl owl-carousel owl-theme">
                        <? $m=0;?>
                        <? foreach ($surrogate['benefit'] as $v):?>
                        <? $m++;?>
                        <? if($m % 2 == 1):?>
                        <div class="item">
                        <? endif;?>
                            <div class="round-num">
                                <span><?=$m?></span>
                            </div>
                            <div class="benefit-text">
                                <p><?=$v->name?></p>
                            </div>
                        <? if($m % 2 == 0):?>
                        </div>
                        <? endif;?>
                        <? endforeach;?>

                        <? if($m % 2 != 0):?>
                        </div>
                        <? endif;?>
                    </div>
                </div>
            </div>

            <div class="bg-white border-top" id="surrogate-requirements">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][3]->name?></h3>
                        </div>
                        <div class="flex-row">
                            <? foreach ($surrogate['requirement'] as $v):?>
                            <div class="row-item">
                                <div class="require-title">
                                    <p><?=$v->name?></p>
                                </div>
                                <div class="require-list">
                                    <?=$v->content?>
                                </div>
                            </div>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-grey" id="surrogate-for-foreign">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][4]->name?></h3>
                        </div>
                        <div class="foreign-text">
                          <?=$surrogate['foreigncon']->content?>
                        </div>
                    </div>
                    <? $m=0;?>
                    <? $blockSize = count($surrogate['foreigntype'])/2;?>
                    <? foreach ($surrogate['foreigntype']  as $v):?>
                    <? $m++;?>
                    <? if($m % $blockSize == 1 ):?>
                    <div class="col-sm-6">
                    <? endif;?>
                        <div class="flex-img-item">
                            <div class="icon-box">
                                <img src="<?=$v->getImage();?>" alt="">
                            </div>
                            <div class="flex-text">
                                <p><?=$v->content?></p>
                            </div>
                        </div>
                    <? if($m % $blockSize == 0):?>
                        </div>
                    <? endif;?>
                    <? endforeach;?>
                    <? if($m % $blockSize != 0):?>
                    </div>
                    <? endif;?>
                </div>
            </div>

            <div class="bg-white" id="surrogate-process">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3><?=$surrogate['sub'][5]->name?></h3>
                    </div>
                    <?=$surrogate['stagesContent']->content;?>
                    <div class="accordion" id="accordionExample">
                        <? $active = "show";?>
                        <? foreach ($surrogate['stages'] as $v):?>
                        <div class="card">
                            <div class="card-header" id="heading<?=$v->id?>">
                                <h2 class="mb-0">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne">
                                        <?=$v->name?>
                                    </button>
                                </h2>
                            </div>

                            <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body foreign-text">
                                    <?=$v->content?>
                                </div>
                            </div>
                        </div>
                        <? $active = "";?>
                        <? endforeach;?>
                    </div>
                </div>
            </div>

            <div class="bg-white border-top" id="surrogate-compensation">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][6]->name?></h3>
                        </div>
                        <div class="compensation-title">
                            <p><?=$surrogate['compensation']->name?></p>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <?=$surrogate['compensation']->contenta?>
                    </div>
                    <div class="col-sm-6">
                        <?=$surrogate['compensation']->contentb?>
                    </div>
                    <div class="col-sm-12">
                        <div class="box-compensation">
                            <?=$surrogate['compensation']->contentc?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-grey" id="surrogate-school">
                <div class="row desktop-version">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][7]->name?></h3>
                        </div>
                        <div class="about-surrogacy-text">
                            <?=$surrogate['school']->contenta?>
                            <img src="<?=$surrogate['school']->getImage()?>" class="img-left img-fluid" alt="surragacy-school" title="surragacy-school">
                            <?=$surrogate['school']->contentb?>

                            <?=$surrogate['school']->contentc?>
                        </div>
                    </div>
                </div>
                <div class="row mobile-version">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][7]->name?></h3>
                        </div>
                        <img src="<?=$surrogate['school']->getImage();?>" class="img-left img-fluid" alt="surragacy-school" title="surragacy-school">
                        <div class="about-surrogacy-text">
                            <?=$surrogate['school']->contentd?>
                        </div>
                        <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#mobile2" aria-expanded="false" aria-controls="mobile2">
                            <?=$surrogate['translation'][0]->name?>
                        </button>
                        <div class="collapse" id="mobile2">
                            <div class="about-surrogacy-text">
                                <?=$surrogate['school']->contente?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-white">
                <div class="row desktop-version" id="surrogate-doula">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][8]->name?></h3>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="about-surrogacy-text">
                           <?= $surrogate['service']->content?>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <img src="<?= $surrogate['service']->getImage();?>" class="img-fluid" title="" alt="">
                    </div>
                </div>
                <div class="row mobile-version">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][8]->name?></h3>
                        </div>
                        <img src="<?= $surrogate['service']->getImage();?>" class="img-fluid img-left" title="" alt="">

                        <div class="about-surrogacy-text">
                            <?= $surrogate['service']->contenta?>
                        </div>
                        <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#mobile3" aria-expanded="false" aria-controls="mobile3">
                            <?=$surrogate['translation'][0]->name?>
                        </button>
                        <div class="collapse" id="mobile3">
                            <div class="about-surrogacy-text">
                                <?= $surrogate['service']->contentb?>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="bg-white border-top" id="surrogate-psychological">

                <div class="desktop-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][9]->name?></h3>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <?=$surrogate['support']->content?>
                    </div>
                    <div class="col-sm-5">
                        <img src="<?=$surrogate['support']->getImage();?>" class="img-fluid" title="" alt="">
                    </div>
                </div>

                <div class="mobile-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][9]->name?></h3>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <?=$surrogate['support']->contenta?>
                    </div>
                    <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#mobile4" aria-expanded="false" aria-controls="mobile4">
                        <?=$surrogate['translation'][0]->name?>
                    </button>
                    <div class="collapse" id="mobile4">
                        <div class="about-surrogacy-text">
                            <?=$surrogate['support']->contentb?>
                            <img src="<?=$surrogate['support']->getImage();?>" class="img-fluid" title="" alt="">
                        </div>
                    </div>
                </div>

            </div>

            <div class="bg-grey" id="surrogate-stories">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][10]->name?></h3>
                        </div>
                        <div class="owl-surrogate owl-carousel owl-theme">
                            <? foreach ($surrogate['story'] as $v):?>
                                <div class="item">
                                    <div class="section-subtitle">
                                        <p><?=$v->name?></p>
                                    </div>
                                    <div class="compensation-title">
                                        <p><?=$v->city?></p>
                                    </div>
                                    <div class="about-surrogacy-text">
                                        <p><?=$v->contenta?></p>
                                        <? if($v->contentb != null):?>
                                            <div class="read-more">
                                                <a data-toggle="collapse" data-target="#collapseExample<?=$v->id?>" aria-expanded="false" aria-controls="collapseExample1"><p><?=$surrogate['translation'][1]->name?></p></a>
                                            </div>
                                            <div class="collapse" id="collapseExample<?=$v->id?>">
                                                <?=$v->contentb?>
                                            </div>
                                        <? endif;?>
                                    </div>
                                </div>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-white" id="surrogate-egg-donors-faq">
                <div class="desktop-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][11]->name?></h3>
                        </div>
                        <div class="accordion2" id="accordionExample">
                            <? $m=0;?>
                            <? $active = 'show';?>
                            <? foreach ($surrogate['faq'] as $v):?>
                                <? $m++;?>
                                <? if($m < 9):?>
                                <div class="card">
                                    <div class="card-header" id="headingOne">
                                        <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name?></p>
                                    </div>

                                    <div id="collapse<?=$v->id?>" class="collapse <?=$active;?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body foreign-text">
                                            <p><?=$v->content?></p>
                                        </div>
                                    </div>
                                </div>
                                <? endif;?>
                                <? $active = '';?>
                            <? endforeach;?>
                        </div>
                        <? if(count($surrogate['faq'])>8):?>
                        
                        <div class="collapse" id="collapseExample">
                            <div class="accordion2" id="accordionExample">
                                <? $m=0;?>
                                <? $active = 'show';?>
                                <? foreach ($surrogate['faq'] as $v):?>
                                    <? $m++;?>
                                    <? if($m > 8):?>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name?></p>
                                            </div>

                                            <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body foreign-text">
                                                    <p><?=$v->content?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif;?>
                                    <? $active = '';?>
                                <? endforeach;?>
                            </div>
                        </div>
						<button class="btn btn-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <?=$surrogate['translation'][0]->name?>
                        </button>
                        <? endif;?>
                    </div>
                </div>
                <div class="mobile-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][11]->name?></h3>
                        </div>
                        <div class="accordion2" id="accordionExample">
                            <? $active = 'show';?>
                            <?$m =0;?>
                            <? foreach ($surrogate['faq'] as $v):?>
                                <? $m++;?>
                                <? if($m < 4):?>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name;?></p>
                                        </div>
                                        <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body foreign-text">
                                                <p><?=$v->content;?></p>
                                            </div>
                                        </div>
                                    </div>
                                <? endif;?>
                                <? $active = '';?>
                            <? endforeach;?>
                        </div>
                        <? if(count($surrogate['faq'])>3):?>
                        
                        <div class="collapse" id="collapseExample">
                            <div class="accordion2" id="accordionExample">
                                <?$m =0;?>
                                <? foreach ($surrogate['faq'] as $v):?>
                                    <? $m++;?>
                                    <? if($m > 3):?>
                                        <div class="card">
                                            <div class="card-header" id="headingOne">
                                                <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name;?></p>
                                            </div>
                                            <div id="collapse<?=$v->id?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                <div class="card-body foreign-text">
                                                    <p><?=$v->content;?></p>
                                                </div>
                                            </div>
                                        </div>
                                    <? endif;?>
                                <? endforeach;?>
                            </div>
                        </div>
						<button class="btn btn-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                            <?=$surrogate['translation'][0]->name?>
                        </button>
                        <? endif;?>

                    </div>
                </div>
            </div>

            <div class="bg-grey" id="surrogate-how-to-start">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$surrogate['sub'][12]->name?></h3>
                        </div>
                    </div>
                    <div class="col-lg-1 col-3">
                        <div class="circle">
                            <ul>
                                <? $m=0;?>
                                <? foreach ($surrogate['howStart'] as $v):?>
                                    <? $m++;?>
                                    <li><?=$m?></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-11 col-9">
                        <div class="circle-text">
                            <ul>
                                <? foreach ($surrogate['howStart'] as $v):?>
                                    <li><p><?=$v->name?></p></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <? if (!$check):?>
                        <div class="col-sm-12">
                            <button href="#register-email" class="btn btn-more ths-popup-link"><?=$surrogate['translation'][2]->name?></button>
                        </div>
                    <? endif;?>
                </div>
            </div>
        </div>
    </div>
</section>

