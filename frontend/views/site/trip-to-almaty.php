<section class="trip-bg">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$trip['main']->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$trip['main']->bigTitle?></h3>
                </div>
                <div class="inner-title-description">
                    <p><?=$trip['main']->slogan?></p>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <? $m=0;?>
            <? foreach ($trip['data'] as $v):?>
            <? $m++;?>
            <? $images = $v->tripImages;?>

            <? if($m%2==1):?>
                <? $class = 'bg-white'?>
            <? endif;?>

            <? if($m%2==0):?>
                <? $class = 'bg-grey'?>
            <? endif;?>
            <div class="<?=$class?>">
                <div class="row">
                    <div class="col-lg-6 trip-img__wrap">
                        <div class="section-title">
                            <h3><?=$v->name?></h3>
                        </div>
                        <div class="trip-text">
                            <?=$v->content?>
                        </div>
                    </div>
                    <? if(count($images) != 0):?>
					<div class="col-lg-6">

						<div class="fotorama trip-img-main" data-nav="thumbs" data-hash="true" data-allowfullscreen="true">
                            <? foreach ($images as $v):?>
							<img src="<?=$v->getImage();?>" alt="">
                            <? endforeach;?>
						</div>

					</div>
                    <? endif;?>
                </div>
            </div>
            <? endforeach;?>
        </div>
    </div>
</section>