<section class="ifs" id="ifs">

    <!-- IFS -->
    <div class="ifs-top">
        <div class="container">
            <div class="row">
                <?= $this->render('/partials/buttons',
                    ['class'=>'btn inner-btn ths-popup-link'
                ]);?>
            </div>
            <!-- ifs title -->
            <div class="ifs-title">
                <div class="row">
                    <div class="col-12">
                        <h3><?=$ifs['main']->littleTitle?></h3>
                    </div>

                    <div class="col-12">
                        <h1><?=$ifs['main']->bigTitle?>  </h1>
                    </div>

                    <div class="col-12">
                        <h2><?=$ifs['main']->slogan?></h2>
                    </div>
                </div>

                <div class="ifs-main-btn">
                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="ifs-main-btn-item">
                                <? if(Yii::$app->view->params['buttons'][5]->status):?>
                                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                                <? endif;?>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                            <div class="ifs-main-btn-item">
                                <? if(Yii::$app->view->params['buttons'][6]->status):?>
                                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                                <? endif;?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- end ifs title -->

        </div>
    </div>
    <!-- END IFS -->


    <!-- ABOUT US -->
    <div class="ifs-about inner-container" id="ifs-about-us">

        <div class="container">

            <div class="row">
                <div class="col-lg-6 col-md-6">

                    <div class="about-left">
                        <div class="about-left-title">
                            <h2><?=$ifs['sub'][0]->name?></h2>
                            <h3><?=$ifs['about']->subtitlea?></h3>
                        </div>
                        <?=$ifs['about']->content?>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="about-right">
                        <h3><?=$ifs['about']->subtitleb?> </h3>
                        <iframe src="" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>


            </div>

        </div>

    </div>
    <!-- END ABOUT US -->


    <!-- FACTS -->
    <div class="ifs-facts inner-container" id="ifs-ten-facts">

        <div class="container">

            <div class="facts-title">
                <h2><?=$ifs['sub'][1]->name?></h2>
            </div>

            <div class="facts-content">

                <div class="inner-content inner-content-desktop">

                    <?php foreach ($ifs['fact'] as $v):?>
                        <div class="facts-item">
                            <?=$v->contentsvg?>
                            <p><?=$v->content?></p>
                        </div>
                    <?php endforeach;?>


                </div>
                <!-- END 2 СТРОКА -->


                <!-- INNER CONTENT MOBILE -->

                <div class="inner-content inner-content-mobile owl-carousel owl-theme">
                    <!-- 1 СТРОКА -->
                    <?php $m=0;?>
                    <?php foreach ($ifs['fact'] as $v):?>
                        <?php $m++;?>
                        <?php if($m%2==1):?>
                        <div class="inner-content-mobile-slide">
                        <?php endif;?>
                                <div class="facts-item">
                                    <?=$v->contentsvg?>
                                    <p><?=$v->content?></p>
                                </div>
                        <?php if($m%2==0):?>
                        </div>
                        <?php endif;?>
                    <?php endforeach;?>
                </div>

            </div>
            <!-- END 2 СТРОКА -->

            <!-- END INNER CONTENT MOBILE -->

        </div>

    </div>

    </div>
    <!-- END FACTS -->


    <!-- SERVICES -->
    <div class="ifs-services ifs-desktop-services inner-container" id="ifs-our-services">

        <div class="container">

            <div class="row justify-content-center">

                <div class="col-12">
                    <div class="services-title">
                        <h2><?=$ifs['sub'][2]->name?></h2>
                    </div>
                </div>

                <?php foreach ($ifs['service'] as $v):?>
                    <div class="col-lg-3 col-md-3">
                        <div class="services-item">
                            <img src="<?=$v->getImage();?>" alt="consult services">
                            <h3><?=$v->name?></h3>
                            <ul>
                                <?=$v->content?>
                            </ul>
                        </div>
                    </div>
                <?php endforeach;?>


            </div>

        </div>

    </div>
    <!-- END SERVICES -->


    <!-- MOBILE SERVICES -->
    <div class="ifs-services inner-container ifs-mobile-services" id = 'ifs-our-services'>

        <div class="container">

            <div class="row justify-content-center">

                <div class="col-12">
                    <div class="services-title">
                        <h2><?=$ifs['sub'][2]->name?></h2>
                    </div>
                </div>

            </div>

            <div class="services-mobile-wrap owl-carousel owl-theme">
                <!-- 1 СТРОКА -->
                <?php foreach ($ifs['service'] as $v):?>
                    <div class="services-item">
                        <img src="<?=$v->getImage();?>" alt="consult services">
                        <h3><?=$v->name?></h3>
                        <ul>
                            <?=$v->content?>
                        </ul>
                    </div>
                <?php endforeach;?>
            </div>

        </div>

    </div>
    <!-- END MOBILE SERVICES -->



    <!-- DIFFERENCES -->
    <div class="differences inner-container" id="ifs-our-difference">

        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="differences-title">
                        <h2><?=$ifs['sub'][3]->name?> </h2>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="differences-content">

                        <div class="differences-table">

                            <div class="differences-top">
                                <?php $m=0;?>
                                <?php foreach ($ifs['diff'] as $v):?>
                                    <?php $m++?>
                                    <div class="differences-top-item"><a href="#difference-tab-<?=$m?>"><?=$m?></a></div>
                                <?php endforeach;?>
                            </div>

                            <div class="differences-bottom">
                                <?php $m=0;?>
                                <?php foreach ($ifs['diff'] as $v):?>
                                    <?php $m++?>
                                    <div class="differences-bottom-item" id="difference-tab-<?=$m?>">
                                        <p><?=$v->content?></p>
                                    </div>
                                <?php endforeach;?>
                            </div>

                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>
    <!-- END DIFFERENCES -->


    <!-- MEDICAL TEAM -->
    <div class="ifs-medical-team inner-container" id="ifs-our-medical-team">

        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="ifs-medical-team-title">
                        <h2><?=$ifs['sub'][4]->name?></h2>
                    </div>
                </div>
            </div>

            <div class="medical-team-content">

                <div class="medical-arrows">
                    <div class="medical-arrow-left medical-arrow">
                        <a href="#">
                            <svg width="33" height="55" viewBox="0 0 33 55" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M32.0081 1.33887L0.853516 29.4956M0.908312 28.8126L32.0632 53.721" stroke="#4D4D4D"/>
                            </svg>
                        </a>
                    </div>

                    <div class="medical-arrow-right medical-arrow">
                        <a href="#">
                            <svg width="33" height="55" viewBox="0 0 33 55" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.814793 1.33887L31.9694 29.4956M31.9146 28.8126L0.759766 53.721" stroke="#616161"/>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="medical-team-slide-wrap">
                    <div class="medical-team-slide owl-carousel">
                        <?php foreach ($ifs['medteam'] as $v):?>
                            <div class="medical-team-item">
                                <img src="<?=$v->getImage();?>" alt="medical-team">
                                <h2><?=$v->name;?></h2>
                                <h3><?=$v->position;?></h3>
                                <p><?=$v->experience;?></p>
                            </div>
                        <?php endforeach;?>
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!-- END MEDICAL TEAM -->


    <!-- TEAM -->
    <div class="ifs-team inner-container" id="ifs-our-team">

        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="ifs-team-title">
                        <h2><?=$ifs['sub'][5]->name?></h2>
                    </div>
                </div>
            </div>

            <div class="team-content">

                <div class="team-arrows">
                    <div class="team-arrow-left team-arrow">
                        <a href="#">
                            <svg width="33" height="54" viewBox="0 0 33 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M32.0081 0.98584L0.853516 29.1425M0.908312 28.4596L32.0632 53.368" stroke="white"/>
                            </svg>
                        </a>
                    </div>

                    <div class="team-arrow-right team-arrow">
                        <a href="#">
                            <svg width="33" height="54" viewBox="0 0 33 54" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.814793 0.98584L31.9694 29.1425M31.9146 28.4596L0.759766 53.368" stroke="white"/>
                            </svg>
                        </a>
                    </div>
                </div>

                <div class="team-slide-wrap">

                    <div class="team-slide owl-carousel">
                        <?php foreach ($ifs['team'] as $v):?>
                            <div class="team-item">
                                <img src="<?=$v->getImage();?>" alt="medical-team">
                                <h2><?=$v->name;?></h2>
                                <h3><?=$v->position;?></h3>
                                <p><?=$v->experience;?></p>
                            </div>
                        <?php endforeach;?>
                    </div>

                </div>

            </div>

        </div>

    </div>
    <!-- END TEAM -->


    <div class="ifs-contacts inner-container" id="ifs-contacts">

        <div class="container">

            <div class="row">
                <div class="col-12">
                    <div class="ifs-contacts-title">
                        <h2><?=$ifs['sub'][6]->name?></h2>
                    </div>
                </div>
            </div>

            <div class="row ifs-contacts-flex">
                <div class="col-lg-6 col-md-6">
                    <div class="contacts-left">
                        <iframe src="<?= Yii::$app->view->params['contact']->urlMap?>" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-lg-6 col-md-6">
                    <div class="contacts-right">
                        <div class="adress-title">
                            <img src="/images/location.svg" alt="location"><h2><?=$ifs['contact']->name?></h2>
                        </div>

                        <div class="contacts-items">
                            <span><?= Yii::$app->view->params['contact']->address?></span>
                            <span><?= Yii::$app->view->params['contact']->email?></span>
                            <span><?= Yii::$app->view->params['contact']->telegramTel?></span>
                            <span><?= Yii::$app->view->params['contact']->mobileTel?></span>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>