
<section class="blue-bg">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$siteMap['main']->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$siteMap['main']->bigTitle?></h3>
                </div>
            </div>
            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>


            <div class="bg-white">
                <div class="row">
                    <? foreach (Yii::$app->view->params['menu'] as $v):?>
                        <? $m++;?>
                        <? $submenu = $v->menuItems;?>
                        <? if($m < 9):?>
                            <div class="col-lg-2 col-md-4 col-sm-12">
                                <div class="site-map-title">
                                    <p><?=$v->text?></p>
                                </div>

                                <? if($submenu != null):?>
                                    <div class="site-map-text">
                                        <ul>
                                            <? foreach ($submenu as $sub):?>
                                                <li><a href="<?=$v->url?>#<?=$sub->url?>"><?=$sub->name?></a></li>
                                            <? endforeach;?>
                                        </ul>
                                    </div>
                                <? endif;?>
                            </div>
                        <? endif;?>
                    <? endforeach;?>

                    <div class="col-lg-2 col-md-4 col-sm-12">
                        <div class="site-map-title">
                            <p>More</p>
                        </div>
                        <? $m=0;?>
                        <div class="site-map-text">
                            <ul>
                                <? foreach (Yii::$app->view->params['menu'] as $v):?>
                                    <? $m++;?>
                                    <? if($m > 8):?>
                                        <li><a href="<?=$v->url?>"><?=$v->text?></a></li>
                                    <? endif;?>
                                <? endforeach;?>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

