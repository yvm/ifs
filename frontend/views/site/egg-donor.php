
<section class="egg-donor">
    <div class="container">
        <div class="row">
            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>
            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$donor['main']->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$donor['main']->bigTitle?></h3>
                </div>
                <div class="inner-title-description">
                    <p><?=$donor['main']->slogan?></p>
                </div>
            </div>

            <div class="offset-lg-4 col-lg-2 col-6">
                <div class="connect-link egg-donor-link">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-lg-2 col-6">
                <div class="connect-link egg-donor-link">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link second-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>


            <? if($donor['howAllWorks']->status):?>
            <div class="bg-white" id="egg-donor-how-it-works">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$donor['sub'][0]->name?></h3>
                        </div>
                        <div class="video">
                            <iframe width="100%" height="500px"
                                    src="<?=$donor['howAllWorks']->url;?>">
                            </iframe>
                        </div>
                    </div>
                </div>
            </div>
            <? endif;?>

            <div class="bg-grey" id="egg-donor-about-donation">
                <div class="desktop-version row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$donor['sub'][1]->name?></h3>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="about-surrogacy-text">
                            <?= $donor['about']->content?>
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <img src="<?= $donor['about']->getImage();?>" class="img-fluid" alt="" title="">
                    </div>
                </div>
                <div class="mobile-version row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?=$donor['sub'][1]->name?></h3>
                        </div>
                        <div class="about-surrogacy-text">
                            <?= $donor['about']->contenta?>
                        </div>
                        <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#mobile2" aria-expanded="false" aria-controls="mobile2">
                            <?=$donor['translation'][1]->name;?>
                        </button>
                        <div class="collapse" id="mobile2">
                            <div class="about-surrogacy-text">
                                <?= $donor['about']->contentb?>
                            </div>
                            <img src="<?= $donor['about']->getImage();?>" class="img-fluid" alt="" title="">
                        </div>
                    </div>

                </div>
            </div>

            <div class="bg-white" id="egg-donor-benefits">
                <div class="desktop-version row">
                    <div class="col-lg-12">
                        <div class="section-title">
                            <h3><?= $donor['sub'][2]->name?> </h3>
                        </div>
                        <div class="row desktop-version">
                            <? $m=0;?>
                            <? foreach ($donor['benefit'] as $v):?>
                                <? $m++;?>
                                <div class="col-sm-3">
                                    <div class="round-num">
                                        <span><?=$m?></span>
                                    </div>
                                    <div class="benefit-text">
                                        <p><?=$v->name?></p>
                                    </div>
                                </div>
                            <? endforeach;?>
                        </div>
                    </div>
                </div>
                <div class="mobile-version">
                    <div class="section-title">
                        <h3><?= $donor['sub'][2]->name?> </h3>
                    </div>
                    <div class="mobile-owl owl-carousel owl-theme">
                        <? $m=0;?>
                        <? foreach ($donor['benefit'] as $v):?>
                            <? $m++;?>
                            <? if($m % 2 == 1):?>
                                <div class="item">
                            <? endif;?>
                            <div class="round-num">
                                <span><?=$m?></span>
                            </div>
                            <div class="benefit-text">
                                <p><?=$v->name?></p>
                            </div>
                            <? if($m % 2 == 0):?>
                                </div>
                            <? endif;?>
                        <? endforeach;?>

                        <? if($m % 2 != 0):?>
                        </div>
                        <? endif;?>
                    </div>
                </div>
            </div>

            <div class="bg-white border-top" id="egg-donor-requirements">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?= $donor['sub'][3]->name?></h3>
                        </div>
                        <div class="flex-row">
                            <div class="flex-row">
                                <? foreach ($donor['requirement'] as $v):?>
                                    <div class="row-item">
                                        <div class="require-title">
                                            <p><?=$v->name?></p>
                                        </div>
                                        <div class="require-list">
                                            <?=$v->content?>
                                        </div>
                                    </div>
                                <? endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-grey" id="egg-donor-for-foreign">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?= $donor['sub'][4]->name?></h3>
                        </div>
                        <div class="foreign-text">
                            <?=$donor['foreigncon']->content?>
                        </div>
                    </div>
                    <? $m=0;?>
                    <? $blockSize = count($donor['foreigntype'])/2;?>
                    <? foreach ($donor['foreigntype']  as $v):?>
                        <? $m++;?>
                        <? if($m % $blockSize == 1 ):?>
                            <div class="col-sm-6">
                        <? endif;?>
                        <div class="flex-img-item">
                            <div class="icon-box">
                                <img src="<?=$v->getImage();?>" alt="">
                            </div>
                            <div class="flex-text">
                                <p><?=$v->content?></p>
                            </div>
                        </div>
                        <? if($m % $blockSize == 0):?>
                            </div>
                        <? endif;?>
                    <? endforeach;?>
                    <? if($m % $blockSize != 0):?>
                    </div>
                    <? endif;?>
                </div>
            </div>

            <div class="bg-white" id="egg-donor-process">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3><?= $donor['sub'][5]->name?></h3>
                    </div>
                    <div class="accordion" id="accordionExample">
                        <? $active = "show";?>
                        <? foreach ($donor['stages'] as $v):?>
                            <div class="card">
                                <div class="card-header" id="heading<?=$v->id?>">
                                    <h2 class="mb-0">
                                        <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne">
                                            <?=$v->name?>
                                        </button>
                                    </h2>
                                </div>

                                <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                    <div class="card-body foreign-text">
                                        <?=$v->content?>
                                    </div>
                                </div>
                            </div>
                            <? $active = "";?>
                        <? endforeach;?>
                    </div>
                </div>
            </div>

            <div class="bg-white border-top" id="egg-donor-compensation">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3><?= $donor['sub'][6]->name?></h3>
                    </div>
                    <div class="compensation-title">
                       <?=$donor['compensation']->contenta?>
                    </div>
                    <div class="box-compensation">
                        <div class="about-egg-text">
                            <p><?=$donor['compensation']->contentb?></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-grey" id="egg-donor-stories">
                <div class="col-sm-12">
                    <div class="section-title">
                        <h3><?= $donor['sub'][7]->name?></h3>
                    </div>
                    <div class="owl-surrogate owl-carousel owl-theme">
                        <? foreach ($donor['story'] as $v):?>
                            <div class="item">
                                <div class="section-subtitle">
                                    <p><?=$v->name?></p>
                                </div>
                                <div class="compensation-title">
                                    <p><?=$v->city?></p>
                                </div>
                                <div class="about-surrogacy-text">
                                    <p><?=$v->contenta?></p>
                                    <? if($v->contentb != null):?>
                                        <div class="read-more">
                                            <a data-toggle="collapse" data-target="#collapseExample<?=$v->id?>" aria-expanded="false" aria-controls="collapseExample1"><p><?=$donor['translation'][0]->name;?></p></a>
                                        </div>
                                        <div class="collapse" id="collapseExample<?=$v->id?>">
                                            <?=$v->contentb?>
                                        </div>
                                    <? endif;?>
                                </div>
                            </div>
                        <? endforeach;?>
                    </div>
                </div>
            </div>

            <div class="bg-white" id="egg-donor-faq">
                <div class="desktop-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?= $donor['sub'][8]->name?></h3>
                        </div>
                        <div class="accordion2" id="accordionExample">
                            <? $m=0;?>
                            <? $active = 'show';?>
                            <? foreach ($donor['faq'] as $v):?>
                                <? $m++;?>
                                <? if($m < 9):?>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name?></p>
                                        </div>

                                        <div id="collapse<?=$v->id?>" class="collapse <?=$active;?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body foreign-text">
                                                <p><?=$v->content?></p>
                                            </div>
                                        </div>
                                    </div>
                                <? endif;?>
                                <? $active = '';?>
                            <? endforeach;?>
                        </div>
                        <? if(count($donor['faq'])>8):?>
                           
                            <div class="collapse" id="collapseExample">
                                <div class="accordion2" id="accordionExample">
                                    <? $m=0;?>
                                    <? $active = 'show';?>
                                    <? foreach ($donor['faq'] as $v):?>
                                        <? $m++;?>
                                        <? if($m > 8):?>
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name?></p>
                                                </div>

                                                <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body foreign-text">
                                                        <p><?=$v->content?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif;?>
                                        <? $active = '';?>
                                    <? endforeach;?>
                                </div>
                            </div>
						 <button class="btn btn-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                <?=$donor['translation'][1]->name;?>
                            </button>
                        <? endif;?>
                    </div>
                </div>
                <div class="mobile-version row">
                    <div class="col-sm-12">
                        <div class="section-title">
                            <h3><?=$donor['sub'][11]->name?></h3>
                        </div>
                        <div class="accordion2" id="accordionExample">
                            <? $active = 'show';?>
                            <?$m =0;?>
                            <? foreach ($donor['faq'] as $v):?>
                                <? $m++;?>
                                <? if($m < 4):?>
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name;?></p>
                                        </div>
                                        <div id="collapse<?=$v->id?>" class="collapse <?=$active?>" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body foreign-text">
                                                <p><?=$v->content;?></p>
                                            </div>
                                        </div>
                                    </div>
                                <? endif;?>
                                <? $active = '';?>
                            <? endforeach;?>
                        </div>
                        <? if(count($donor['faq'])>3):?>
                            
                            <div class="collapse" id="collapseExample">
                                <div class="accordion2" id="accordionExample">
                                    <?$m =0;?>
                                    <? foreach ($donor['faq'] as $v):?>
                                        <? $m++;?>
                                        <? if($m > 3):?>
                                            <div class="card">
                                                <div class="card-header" id="headingOne">
                                                    <p data-toggle="collapse" data-target="#collapse<?=$v->id?>" aria-expanded="true" aria-controls="collapseOne"><?=$v->name;?></p>
                                                </div>
                                                <div id="collapse<?=$v->id?>" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                                    <div class="card-body foreign-text">
                                                        <p><?=$v->content;?></p>
                                                    </div>
                                                </div>
                                            </div>
                                        <? endif;?>
                                    <? endforeach;?>
                                </div>
                            </div>
						<button class="btn btn-more" type="button" data-toggle="collapse" data-target="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
                                <?=$donor['translation'][1]->name;?>
                            </button>
                        <? endif;?>
                    </div>
                </div>
            </div>

            <div class="bg-grey" id="egg-donor-how-to-start">
                <div class="row">
                    <div class=" col-sm-12">
                        <div class="section-title">
                            <h3><?= $donor['sub'][9]->name?></h3>
                        </div>
                    </div>
                    <div class="col-sm-1 col-3">
                        <div class="circle">
                            <ul>
                                <? $m=0;?>
                                <? foreach ($donor['howStart'] as $v):?>
                                    <? $m++;?>
                                    <li><?=$m?></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <div class="col-sm-11 col-9">
                        <div class="circle-text">
                            <ul>
                                <? foreach ($donor['howStart'] as $v):?>
                                    <li><p><?=$v->name?></p></li>
                                <? endforeach;?>
                            </ul>
                        </div>
                    </div>
                    <? if (!$check):?>
                        <div class="col-sm-12">
                            <button href="#register-email" class="btn btn-more ths-popup-link"><?=$donor['translation'][2]->name;?></button>
                        </div>
                    <? endif;?>

                </div>
            </div>
        </div>
    </div>
</section>

