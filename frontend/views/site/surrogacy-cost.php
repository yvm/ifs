<main>
    <!-- COSTS -->
    <div class="surrogacy-costs" id="surrogacy-costs">
        <div class="container">
            <div class="title">
                <h3><?=$cost->package_title?></h3>
                <h2><?=$cost->package_subtitle?></h2>
                <?=$cost->package_content?>
                <div class="surrogacy-costs-table">
                    <h4><?=$cost->package_tabletitle?></h4>
                    <? foreach ($stages as $v):?>
                        <p><?=$v->name?></p>
                        <?=$v->content;?>
                    <? endforeach;?>
                </div>
                <a href="#get-consult" class="get-cons-btn ths-popup-link"><?=$translation[5]->name;?></a>
            </div>
        </div>
    </div>
    <!-- END COSTS -->
</main>