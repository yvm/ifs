<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 28.03.2019
 * Time: 16:22
 */
$this->title = '404';
?>

<section class="error-404">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p></p>
                </div>
                <div class="error-title">
                    <h3>404</h3>
                </div>
                <div class="error-subtitle">
                    <p><?=Yii::$app->view->params['404']->name?></p>
                </div>
            </div>


            <div class="bg-white">
                <div class="desktop-version row">
                    <div class="col-lg-12">
                        <div class="text-404">
                            <?=Yii::$app->view->params['404']->content?>
                        </div>
                    </div>
                    <? $m=0;?>
                    <? foreach ( Yii::$app->view->params['menu'] as $v):?>
                        <? $m++;?>
                        <? $submenu = $v->menuItems;?>
                        <? if($m % 3 == 1):?>
                        <div class="col-lg-3">
                            <div class="err-list">
                        <? endif;?>
                                <? if($v->url != null):?>
                                    <a href="<?=$v->url?>"><?=$v->text?></a> <br>
                                <? endif;?>
                                <? if($v->url == null):?>
                                    <? $m--;?>
                                    <? foreach ($submenu as $sub):?>
                                        <? $m++;?>
                                        <? if($m % 3 == 1):?>
                                            <div class="col-lg-3">
                                                <div class="err-list">
                                        <? endif;?>
                                        <a href="<?=$sub->url?>"><?=$sub->name?></a> <br>
                                        <? if($m % 3 == 0):?>
                                            </div>
                                                </div>
                                        <? endif;?>
                                    <? endforeach;?>
                                <? endif;?>
                        <? if($m % 3 == 0):?>
                            </div>
                        </div>
                        <? endif;?>
                    <? endforeach;?>
                    <? if($m % 3 != 0):?>
                        </div>
                    </div>
                    <? endif;?>

                </div>
                <div class="mobile-version">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="text-404">
                                <?=Yii::$app->view->params['404']->content?>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="err-list">
                                <? $m=0?>
                                <? $len = (count(Yii::$app->view->params['menu'])+2)/2;?>
                                <? foreach (Yii::$app->view->params['menu'] as $v):?>
                                    <? $m++;?>
                                    <? $submenu = $v->menuItems;?>

                                    <? if($v->url == null):?>
                                        <? $m--;?>
                                        <? foreach ($submenu as $sub):?>
                                            <? $m++;?>
                                            <? if($len>$m):?>
                                                <a href="<?=$sub->url?>"><?=$sub->name?></a> <br>
                                            <? endif;?>
                                        <? endforeach;?>
                                    <? endif;?>

                                    <? if($v->url != null):?>
                                        <? if($len>$m):?>
                                            <a href="<?=$v->url?>"><?=$v->text?></a> <br>
                                        <? endif;?>
                                    <? endif;?>
                                <? endforeach;?>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="err-list">
                                <? $m=0?>
                                <? $len = (count(Yii::$app->view->params['menu'])+2)/2;?>
                                <? foreach (Yii::$app->view->params['menu'] as $v):?>
                                    <? $m++;?>
                                    <? $submenu = $v->menuItems;?>
                                    <? if($v->url == null):?>
                                        <? $m--;?>
                                        <? foreach ($submenu as $sub):?>
                                            <? $m++;?>
                                            <? if($len<$m):?>
                                                <a href="<?=$sub->url?>"><?=$sub->name?></a> <br>
                                            <? endif;?>
                                        <? endforeach;?>
                                    <? endif;?>
                                    <? if($v->url != null):?>
                                        <? if($len<$m):?>
                                            <a href="<?=$v->url?>"><?=$v->text?></a> <br>
                                        <? endif;?>
                                    <? endif;?>
                                <? endforeach;?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</section>
