<main>
    <!-- BANNER -->
    <div class="main-banner" style="background-image: url('/images/surrogacy.jpg');">
        <div class="container">

            <div class="banner-buttons">
                <?= $this->render('/partials/buttons',
                    ['class'=>'ths-popup-link'
                ]);?>
            </div>
            <div class="banner-info">
                <h4><?=$surrogacy['main']->littleTitle?></h4>
                <h1><?=$surrogacy['main']->bigTitle?></h1>
                <h3><?=$surrogacy['main']->slogan?></h3>
            </div>
            <div class="banner-nav">
                <? if(Yii::$app->view->params['buttons'][5]->status):?>
                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][6]->status):?>
                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                <? endif;?>
            </div>

        </div>
    </div>
    <!-- END BANNER -->

    <!-- PROGRAM -->
    <div class="surrogacy-program" id="surrogacy-programs">
        <div class="container">
            <div class="title">
                <h3><?=$surrogacy['sub'][0]->name?></h3>
            </div>

            <div class="program-content">
               <?=$surrogacy['programcon']->content?>
            </div>

            <div class="program-status">

                <? foreach ($surrogacy['programtype'] as $v):?>

                <div class="status-item">
                    <div class="status-stars">
                        <? for($i=0;$i<$v->countStar;$i++):?>
                            <img src="/images/star.png">
                        <? endfor;?>
                    </div>
                    <span><?=$v->name?></span>
                    <p><?=$v->content?></p>
                </div>
                <? endforeach;?>
            </div>
        </div>
    </div>
    <!-- END PROGRAM -->


    <!-- PROCESS -->
    <div class="surrogacy-process" id="surrogacy-the-process">
        <div class="container">
            <div class="title">
                <h3><?=$surrogacy['sub'][1]->name?></h3>
            </div>
            <div class="process-wrapper">
                <? $m=0;?>
                <? foreach ($surrogacy['process']  as $v):?>
                    <? $m++;?>
                    <? if($m<7):?>
                    <div class="process-item">
                        <h4><?=$v->name?></h4>
                        <span><?=$v->day?></span>
                        <hr>
                        <p><?=$v->content?></p>
                    </div>
                    <?endif;?>

                    <? if($m>6):?>
                        <div class="process-min-item">
                            <h4><?=$v->name?></h4>
                            <span><?=$v->day?></span>
                            <hr>
                            <p><?=$v->content?></p>
                            <? if($m < count($surrogacy['process'])):?>
                                <img src="/images/vector.png">
                            <? endif;?>
                        </div>
                    <?endif;?>
                <? endforeach;?>
            </div>
        </div>
    </div>
    <!-- END PROCESS -->


    <!-- MOB. PROCESS -->
    <div class="mob-process container">
        <div class="process owl-carousel slide-one owl-theme">
            <? foreach ($surrogacy['process']  as $v):?>
                <div class="process-item">
                    <h4><?=$v->name?></h4>
                    <span><?=$v->day?></span>
                    <hr>
                    <p><?=$v->content?></p>
                </div>
            <? endforeach;?>
        </div>
    </div>
    <!-- END MOB. PROCESS -->


    <!-- COSTS -->
    <div class="surrogacy-costs" id="surrogacy-costs">
        <div class="container">
            <div class="title">
                <h3><?=$surrogacy['sub'][2]->name?></h3>
                <p><?=$surrogacy['costContent']->content?></p>
            </div>

            <div class="costs-wrapper">
                <? foreach ($surrogacy['cost'] as $v):?>
                <div class="costs-item">
                    <div class="status-stars">
                        <? for($i=0;$i<$v->countStar;$i++):?>
                        <img src="/images/star.png">
                        <? endfor;?>
                    </div>
                    <h3><?=$v->name?></h3>
                    <? $types = $v->types;?>
                    <div class="panel-group" id="accordion">
                        <? foreach ($types as $type):?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?=$type->id?>">
                                        <?=$type->name?>
                                    </a>
                                    <span class="line"></span>
                                </h4>
                            </div>
                            <div id="collapseOne<?=$type->id?>" class="panel-collapse collapse in">
                                <div class="panel-body">
                                    <?=$type->content?>
                                </div>
                            </div>
                        </div>
                        <? endforeach;?>
                    </div>

                    <div class="cost-title-left">
                        <?=$v->content?>
                    </div>
                    <? if(Yii::$app->user->isGuest):?>
                        <? if($v->statusPriceGuest):?>
                            <div class="price">
                                <p><?=$v->price?></p>
                            </div>
                        <? endif;?>
                        <? if(!$v->statusPriceGuest):?>
                            <a href="#register-email" class="order-price__btn ths-popup-link"><?=$surrogacy['translation'][0]->name;?></a>
                        <? endif;?>
                    <? endif;?>

                    <? if(!Yii::$app->user->isGuest):?>
                        <? if($v->statusPriceUser):?>
                            <div class="price">
                                <p><?=$v->price?></p>
                            </div>
                        <? endif;?>
                        <? if(!$v->statusPriceUser):?>
                            <a href="#register-email" class="order-price__btn ths-popup-link"><?=$surrogacy['translation'][0]->name;?></a>
                        <? endif;?>
                        <a href="" class="btn btn-start-program"><?=$surrogacy['translation'][1]->name;?></a>
                    <? endif;?>
					<a href="/site/surrogacy-cost?id=<?=$v->id?>" class="order-price__btn"><?=$surrogacy['translation'][2]->name;?></a>
                </div>
                <? endforeach;?>

            </div>
        </div>
    </div>
    <!-- END COSTS -->


    <!-- MOB. COSTS -->
    <div class="surrogacy-costs-mob container owl-carousel owl-theme">
        <? foreach ($surrogacy['cost'] as $v):?>
        <div class="costs-item">
            <div class="status-stars">
                <? for($i=0;$i<$v->countStar;$i++):?>
                    <img src="/images/star.png">
                <? endfor;?>
            </div>
            <h3><?=$v->name?></h3>
            <? $types = $v->types;?>
            <div class="panel-group" id="accordion">
                <? foreach ($types as $type):?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne<?=$type->id?>">
                                    <?=$type->name?>
                                </a>
                                <span class="line"></span>
                            </h4>
                        </div>
                        <div id="collapseOne<?=$type->id?>" class="panel-collapse collapse in">
                            <div class="panel-body">
                                <?=$type->content?>
                            </div>
                        </div>
                    </div>
                <? endforeach;?>
            </div>
            <div class="cost-title-left">
                <?=$v->content?>
            </div>
            <? if(Yii::$app->user->isGuest):?>
                <? if($v->statusPriceGuest):?>
                    <div class="price">
                        <p><?=$v->price?></p>
                    </div>
                <? endif;?>
                <? if(!$v->statusPriceGuest):?>
                    <a href="#register-email" class="order-price__btn ths-popup-link"><?=$surrogacy['translation'][0]->name;?></a>
                <? endif;?>
            <? endif;?>

            <? if(!Yii::$app->user->isGuest):?>
                <? if($v->statusPriceUser):?>
                    <div class="price">
                        <p><?=$v->price?></p>
                    </div>
                <? endif;?>
                <? if(!$v->statusPriceUser):?>
                    <a href="#register-email" class="order-price__btn ths-popup-link"><?=$surrogacy['translation'][0]->name;?></a>
                <? endif;?>
                <a href="" class="btn btn-start-program"><?=$surrogacy['translation'][0]->name;?></a>
            <? endif;?>
            <a href="/site/surrogacy-cost?id=<?=$v->id?>" class="order-price__btn"><?=$surrogacy['translation'][2]->name;?></a>
        </div>
        <? endforeach;?>

    </div>
    <!-- END MOB. COSTS -->


    <!-- FIND-SURROGATE -->
    <? if($surrogacy['banner']->status):?>
    <div class="find-surrogate">
        <div class="container">
            <div class="find-surrogate__caption">
                <h4><?=$surrogacy['banner']->content?></h4>
                 <?if(!Yii::$app->user->isGuest):?>
                    <a href="/database/surrogate-database" class="find-btn"><?=$surrogacy['banner']->buttonName?></a>
                <? endif;?>
                <?if(Yii::$app->user->isGuest):?>
                    <a href="#modal-missing" class="find-btn ths-popup-link"><?=$surrogacy['banner']->buttonName?></a>
                <? endif;?>
            </div>
        </div>
    </div>
    <? endif;?>
    <!-- END FIND-SURROGATE -->


    <!-- School For Surrogates -->
    <div class="school-for-surrogates" id="surrogacy-school">
        <div class="container">
            <div class="title">
                <h3><?=$surrogacy['sub'][3]->name?></h3>
            </div>
            <div class="pregnancy">
                <p><?=$surrogacy['school']->contenta?></p>
            </div>
            <div class="school-wrapper">
                <div class="school-left-side">
                    <img src="<?=$surrogacy['school']->getImage()?>">
                </div>

                <div class="school-right-side">
                    <p><?=$surrogacy['school']->contentb?></p>
                </div>
            </div>
            <div class="pregnancy">
                <p><?=$surrogacy['school']->contentc?></p>
            </div>
        </div>
    </div>
    <!-- End School For Surrogates -->


    <!-- Duol Services -->
    <div class="duol-services" id="surrogacy-doula-services">
        <div class="container">
            <div class="title">
                <h3><?=$surrogacy['sub'][4]->name?></h3>
            </div>
            <div class="duol-wrapper">
                <div class="duol-left-side">
                    <?=$surrogacy['service']->content?>
                </div>
                <div class="duol-right-side">
                    <img src="<?=$surrogacy['service']->getImage();?>">
                </div>
            </div>
        </div>
    </div>
    <!-- End Duol Services -->
</main>