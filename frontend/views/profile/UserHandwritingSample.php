<div class="profile-add-photo">
    <div class="title">
        <h3><a href="#modal-guidelines-hand" class="ths-popup-link">Guidelines</a></h3>
        <h4>Select File:</h4>
        <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 1 file)</p>
        <span><?=$error;?></span>
        <? if(!$UserHandwritingSample):?>
            <button type="button" name="addVideo" class="uploadImgHandwritingSample add" <?if($UserHandwritingSample){?>disabled="disabled"<?}?>>Add file</button>
        <? endif;?>
    </div>
</div>
<div class="edit-add-photos">
    <ul>
        <?if($UserHandwritingSample){?>
            <li class="photo-uploaded">
                <div class="my-profile_hs__block">
                    <div class="photo-place" data-id="<?=$photo->id?>">
                        <a href='/backend/web/images/userhandwritingsample/<?=Yii::$app->user->id.'/'.$UserHandwritingSample->video?>' target="_blank">
                            <img src="/frontend/web/images/doc.png" style="width:100px;">
                        </a>
                        <div class="del-ed-photo">
                            <button type="button" name="delImg" class="delete_img_profile_HandwritingSample" data-id="<?=$UserHandwritingSample->id?>" data-type="UserHandwritingSample">
                                <img src="/images/delete-img.png">
                            </button>
                        </div>
                    </div>
                </div>
            </li>
        <?}else{?>

            <li>
                <div class="photo-place">
                    <button type="button" name="uploadImg" class="uploadImgHandwritingSample">
                        <img src="/images/addPlus.png">
                    </button>
                </div>
            </li>
        <?}?>
        <input type="file" style="display: none;" class="uploadImgInputHandwritingSample">
</div>