<div class="card-header" id="headingFive">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
            Favorites
        </button>
    </h5>
</div>
<div id="collapseFive" class="collapse show" aria-labelledby="headingFive" data-parent="#accordion">
    <div class="mob-profile-favorites">
        <? use common\models\UserPhoto;

        if(count($userFavorites) == 0):?>
            Ваше избранное пусто!
        <? endif;?>
        <? if(count($userFavorites) != 0):?>
            <ul>
                <? foreach ($userFavorites as $v):?>
                    <li >
                        <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                            <div class="favorites-img">
                                <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                                $photo = $userImage[0]->photo;?>
                                <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">
                                <button type="button" name="delImg" class="delete_favorite_user_mob" data-user-id="<?=$v->favorite_user_id?>">
                                    <img src="/images/delete-img.png">
                                </button>
                            </div>
                            <div class="surrogate-id">
                                <p>[<?=$v->user->shortRoleName;?>-<?=$v->profile->createdDateWithoutDot;?><?=$v->user->orderID;?>]</p>
                                <p><?=$v->user->roleName;?></p>
                            </div>
                        </a>
                    </li>
                <? endforeach;?>
            </ul>
        <? endif;?>
    </div>
</div>