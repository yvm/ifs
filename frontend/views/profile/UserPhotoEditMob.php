<div class="mob-photo-place">
    <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" style="width: 150px;height: 95px;">
    <div class="del-ed-photo">
        <? if(count($UserPhoto) != 1):?>
            <button type="button" name="delImg" class="delete_img_profile_photo_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
                <img src="/images/delete-img.png">
            </button>
        <? endif;?>
        <button type="button" name="edImg" class="edit_img_profile_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
            <img src="/images/edit-img.png">
        </button>
    </div>
</div>
<div class="file-name-size">
    <p><?=$photo->photo?></p>
    <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
</div>
