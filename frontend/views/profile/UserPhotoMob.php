<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
            Photo
        </button>
    </h5>
</div>
<div id="collapseThree" class="mob-profile_photo collapse show" aria-labelledby="headingThree" data-parent="#accordion">
    <div class="title">
        <span><a href="#modal-guidelines-photo" class="ths-popup-link">Guidelines</a></span>
    </div>
    <div class="mob-profile_photo-info">
        <h4>Select Photo</h4>
        <p>(support only *.png, *.jpg, *.jpeg,and maximum size: 5mb, maximum 10 photos)</p>
        <span><?=$error;?></span>
        <span></span>
    </div>
    <? if(count($UserPhoto) < 9):?>
        <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addPhoto">
            Add photo
        </button>
    <? endif;?>

    <div class="mob-add-photos__wrapper">
        <ul>
            <?foreach($UserPhoto as $k_Photo => $photo){?>
                <li class="mob-uploaded-img">
                    <div class="mob-photo-place">
                        <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" style="width: 150px;height: 95px;">
                        <div class="del-ed-photo">
                            <? if(count($UserPhoto) != 1):?>
                                <button type="button" name="delImg" class="delete_img_profile_photo_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                    <img src="/images/delete-img.png">
                                </button>
                            <? endif;?>
                            <button type="button" name="edImg" class="edit_img_profile_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                <img src="/images/edit-img.png">
                            </button>
                        </div>
                    </div>
                    <div class="file-name-size">
                        <p><?=$photo->photo?></p>
                        <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                    </div>
                </li>
            <? }?>

            <?if($k_Photo < 9){?>
                <li>
                    <div class="mob-photo-place">
                        <button type="button" name="uploadImg" class="uploadImg_mob">
                            <img src="/images/addPlus.png">
                        </button>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
    <input type="file" style="display: none;" class="uploadImgInput_mob">
</div>