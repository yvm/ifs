<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseDoc" aria-expanded="false" aria-controls="collapseDoc">
            Documents
        </button>
    </h5>
</div>
<div id="collapseDoc" class="mob-profile_photo collapse show" aria-labelledby="headingDoc" data-parent="#accordion">
    <div class="title">
        <span><a href="#modal-guidelines-doc" class="ths-popup-link">Guidelines</a></span>
    </div>
    <div class="mob-profile_photo-info">
        <h4>Select File:</h4>
        <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 10 documents)</p>
        <span><?=$error;?></span>
    </div>
    <? if(count($UserDocuments) < 9):?>
        <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addDoc">
            Add File
        </button>
    <? endif;?>

    <div class="mob-add-photos__wrapper">
        <ul>
            <?foreach($UserDocuments as $k_Photo => $photo){?>
                <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                    <div class="mob-photo-place">
                        <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank" style="width: 120px;height: 95px;">
                            <img src="/frontend/web/images/doc.png">
                        </a>
                        <div class="del-ed-photo">
                            <button type="button" name="delImg" class="delete_img_profile_documents_mob" data-id="<?=$photo->id?>" data-type="UserDocuments">
                                <img src="/images/delete-img.png">
                            </button>
                        </div>
                    </div>
                    <div class="file-name-size">
                        <p><?=$photo->img?></p>
                        <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                    </div>
                </li>
            <? }?>

            <?if($k_Photo < 9){?>
                <li>
                    <div class="mob-photo-place">
                        <button type="button" name="uploadImg" class="uploadImgDocuments_doc">
                            <img src="/images/addPlus.png">
                        </button>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
    <input type="file" style="display: none;" class="uploadImgInputDocuments_mob">
</div>