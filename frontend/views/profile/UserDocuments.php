<div class="profile-add-photo">
    <div class="title">
        <h3><a href="#modal-guidelines-doc" class="ths-popup-link">Guidelines</a></h3>
        <h4>Select File:</h4>
        <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 10 documents)</p>
        <span><?=$error;?></span>
        <? if(count($UserDocuments) < 9):?>
            <button type="button" name="btnAddPhoto" id="addPhoto" class="uploadImgDocuments">Add File</button>
        <? endif;?>
    </div>
</div>
<div class="edit-add-photos documents">
    <ul>
        <?foreach($UserDocuments as $k_Photo => $photo){?>
            <li class="photo-uploaded">
                <div class="photo-place" data-id="<?=$photo->id?>">
                    <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank">
                        <img src="/frontend/web/images/doc.png">
                    </a>
                    <div class="del-ed-photo">
                        <button type="button" name="delImg" class="delete_img_profile_documents" data-id="<?=$photo->id?>" data-type="UserDocuments">
                            <img src="/images/delete-img.png">
                        </button>
                    </div>
                </div>
                <div class="file-name-size">
                    <p><?=$photo->img?></p>
                    <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                </div>
                </a>
            </li>
        <?}?>
        <?if($k_Photo < 9){?>
            <li>
                <div class="photo-place">
                    <button type="button" name="uploadImg" class="uploadImgDocuments">
                        <img src="/images/addPlus.png">
                    </button>
                </div>
            </li>
        <?}?>
    </ul>
</div>
<input type="file" style="display: none;" class="uploadImgInputDocuments">