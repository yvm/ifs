<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseVideo" aria-expanded="false" aria-controls="collapseVideo">
            Video
        </button>
    </h5>
</div>
<div id="collapseVideo" class="mob-profile_photo collapse show" aria-labelledby="headingVideo" data-parent="#accordion">
    <div class="title">
        <span><a href="#modal-guidelines-video" class="ths-popup-link">Guidelines</a></span>
    </div>
    <div class="mob-profile_photo-info">
        <h4>Select File:</h4>
        <p>(support only *.mp4, *.3gp, *.ogg and maximum size: 100 Mb, maximum 1 video)</p>
        <span><?=$error;?></span>
    </div>
    <?if(!$UserVideo):?>
        <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addVideo">
            Add Video
        </button>
    <? endif;?>

    <div class="mob-add-photos__wrapper">
        <ul>
            <?if($UserVideo){?>
                <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                    <div class="mob-photo-place">
                        <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                            <img src="/frontend/web/images/video_profile.png">
                        </a>
                        <div class="del-ed-photo">
                            <button type="button" name="deleteVideo" class="delete_img_profile_video_mob" data-id="<?=$UserVideo->id;?>" data-type="UserVideo">
                                <img src="/images/delete-img.png">
                            </button>
                        </div>
                    </div>
                </li>
            <?}else{?>
                <li>
                    <div class="mob-photo-place">
                        <button type="button" name="uploadImg" class="uploadImgVideo_mob">
                            <img src="/images/addPlus.png">
                        </button>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
    <input type="file" style="display: none;" class="uploadImgInputVideo_mob">
</div>