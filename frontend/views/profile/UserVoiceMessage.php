<div class="profile-add-photo">
    <div class="title">
        <h3><a href="#modal-guidelines-voice" class="ths-popup-link">Guidelines</a></h3>
        <h4>Select File:</h4>
        <p>(support only *.mp3  maximum size: 5mb, maximum 1 file)</p>
        <span><?=$error;?></span>
        <?if(!$UserVoiceMessage):?>
            <button type="button" name="addVideo" class="uploadImgVoiceMessage add" <?if($UserVoiceMessage){?>disabled="disabled"<?}?>>Add video</button>
        <? endif;?>
    </div>
</div>
<?if($UserVoiceMessage){?>
    <div class="my-profile_hs__block">
        <div class="profile-iframe-video VoiceMessage">
            <a href='/backend/web/images/uservoicemessage/<?=Yii::$app->user->id.'/'.$UserVoiceMessage->video?>' target="_blank">
                <img src="/frontend/web/images/video_profile.png" style="width:100px;">
            </a>
            <button type="button" name="deleteVideo" class="delete_img_profile_VoiceMessage" data-id="<?=$UserVoiceMessage->id?>" data-type="UserVoiceMessage">
                <img src="/images/delete-img.png">
            </button>
        </div>
    </div>
<?}else{?>
    <div class="photo-place profile-iframe-video VoiceMessage">
        <button type="button" name="uploadImg" class="uploadImgVoiceMessage">
            <img src="/images/addPlus.png">
        </button>
    </div>
<?}?>
<input type="file" style="display: none;" class="uploadImgInputVoiceMessage">