<?php

use common\models\UserPhoto;

?>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="/js/profile.js"></script>
<main class="egg-donor-profile-main" style="background-color: #1371B6;">
    <!-- BANNER -->
	
    <div class="profile-banner">
        <div class="container">
            <h2>My profile</h2>
        </div>
    </div>
    <!-- END BANNER -->

    <!-- Egg donor profile -->
    <div style="padding-bottom: 3rem;" class="container">
        <div class="egg-donor-profile">
            <div class="egg-donor-profile__section">
                <div class="profile-photos">
                    <img class="xzoom" src="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$userPhoto[0]->photo?>" xoriginal="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$userPhoto[0]->photo?>">
                    <div class="xzoom-thumbs owl-carousel owl-theme egg-donor-xzoom-photos">
                        <? foreach ($userPhoto as $v):?>
                            <a href="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>"><img class="xzoom-gallery" xpreview="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>" src="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>"></a>
                        <? endforeach;?>
                    </div>
                    <?if($userVideo){?>
                        <a class="egg-donor-video" href="<?='/'.@backend.'/web/images/uservideo/'.Yii::$app->user->identity->id.'/'.$userVideo->video?>">
                            <img src="/images/video.png">
                            Watch video
                        </a>
                    <?}?>
                </div>

                <div class="egg-donor-avilable">
                    <div class="reg-date">
                        <div class="profi-row">
                            <span><span style="color:#FF5C28;"><?=$profile->fio?></span> [<?=$user->getShortRoleName();?>-<?=$profile->getCreatedDateWithoutDot();?><?=$user->getOrderID();?>]</span>
                            <?if($user->proven){?>
                                <img src="/images/union.svg" alt="union" title="Proven">
                            <?}?>
                        </div>
                        <p>Registration Date: <?=$profile->getCreatedDate();?></p>
                        <span><?=$user->status_profile ? "Active profile":"Suspended Profile"?></span>
                        <? if(!$user->status_profile):?>
                        <p style="color: red;">Your profile has been suspended. If you have any question, please do not hesitate to contact us.
                            <? endif;?>
                    </div>

                    <div class="intended">
                        <p><?=$user->getRoleName();?></p>
                    </div>
                    <div class="intended">
                        <p>Interested in : <?=$user->getTypeName();?></p>
                    </div>
                    <div class="avilable-text">
                        <p>

                            <? if($user->active == 1):?> <?='Available'?><? endif;?>
                            <? if($user->active == 2):?> <?='Not Available'?><?endif;?>
                            <? if($user->active == 3):?> <?='Busy with Surrogate/Egg Donor'?><?endif;?>
                        </p>
                    </div>
                    <form class="avilable-content avilable-edit" action="/profile/update">
                        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                        <button class="save-btn" type="button" name="button-save" style="display: none" id="profile_status_save">
                            Save
                        </button>
                        <? if($user->status_edit_profile && $user->status_profile):?>
                            <button class="edit-btn" type="button" name="button-save" id="profile_status_edit">
                                <img src="/images/edit.png">
                            </button>
                        <?endif;?>

                        <p id="profile_status_text"><?=$profile->status?></p>
                        <textarea id="profile_status_textarea" style="display: none;width: 100%;" name="Profiles[status]"><?=$profile->status?></textarea>

                    </form>
                    <div class="profile-country">
                        <img src="/images/country.png"><?=$personalInfo->getCountryName();?>
                    </div>
                    <div class="profile-country">
                        <?=$personalInfo->getDateOfBirth();?> years
                    </div>
                    <div class="profile-country">
                        <?=$personalInfo->getMaritalStatus();?>
                    </div>
                </div>
                <div class="egg-donor-section">
                    <div class="egg-donor-pers-info">
                        <div class="egg-donor-title">
                            Contacts
                        </div>
                        <div class="profile-contacts">
                            <p>Адрес: <?=$personalInfo->address?></p>
                            <ul>
                                <li>
                    <span>
                      <a href="#"><img src="/images/skype-pr.png"><?=$profile->skype;?></a>
                    </span>
                                    <span>
                      <a href="tel:<?=$profile->telegram;?>"><img src="/images/telegram-logo.png"><?=$profile->telegram;?></a>
                    </span>
                                </li>
                                <li>
                    <span>
                      <a href="tel:<?=$profile->work_contact;?>"><img src="/images/viber.png"><?=$profile->work_contact;?></a>
                    </span>
                                    <span>
                      <a href="tel:<?=$profile->mobile;?>"><img src="/images/tel-pr.png"><?=$profile->mobile;?></a>
                    </span>
                                </li>
                                <li>
                    <span>
                      <a href="tel:<?=$profile->vatzup;?>"><img src="/images/whatsapp.png"><?=$profile->vatzup;?></a>
                    </span>
                                    <span>
                      <a href="mailto:<?=$profile->email;?>"><img src="/images/mail-pr.png"><?=$profile->email;?></a>
                    </span>
                                </li>
                            </ul>
                        </div>
                    </div>

                    <div class="compensation">
                        <div class="egg-donor-title">
                            Program Type
                        </div>
                        <div class="profile-program-type">
                            <p> Surrogacy and Egg Donation</p>
                            <div class="profile-economy-other">
                                <span>Economy</span>
                                <span>Program Start Date 18.02.2019</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="my-profile-tabs">
                <ul class="tabs">
                    <li class="tab-link current" data-tab="tab-1">
                        Program
                        <?if($program_got_it){?>
                            <span class="sum_no_got_it all"><?=$program_got_it?></span>
                        <?}?>
                    </li>
                    <li class="tab-link" data-tab="tab-2">Settings</li>
                    <li class="tab-link" data-tab="tab-3">Photo</li>
                    <li class="tab-link" data-tab="tab-4">Documents</li>
                    <li class="tab-link" data-tab="tab-5">Video</li>
                    <li class="tab-link" data-tab="tab-6">Favorites</li>
                </ul>

                <div id="tab-1" class="tab-content current">
                    <div class="my-profile-tab-programs">
                        <ul class="programs-tab">
                            <?foreach($program as $k => $v){?>
                                <li class="program-link <?if($k==0){?>current-active<?}?>" data-tabs="program<?=$k?>">Program <?=$k+1?></li>
                            <?}?>
                        </ul>
                        <?$inc = 0;?>
                        <?foreach($program as $k => $v){?>
                            <div id="program<?=$k?>" class="program-content <?if($k == 0){?>current-active<?}?>">
                                <div class="general-info">
                                    <ul class="general-info-tabs">
                                        <?$inc++;$div1 = $inc;?>
                                        <li class="general-info-link general-current" data-general-tab="general<?=$inc?>">General information</li>
                                        <?$inc++;$div2 = $inc;?>
                                        <li class="general-info-link" data-general-tab="general<?=$inc?>">
                                            Program Stages
                                            <?if(count($v->program_stages_got_it)){?>
                                                <span class="sum_no_got_it id<?=$v->id?>"><?=count($v->program_stages_got_it)?></span>
                                            <?}?>
                                        </li>
                                    </ul>
                                    <div id="general<?=$div1?>" class="general-info-content general-current">
                                        <ul>
                                            <li>
                                                <p>Program Type</p>
                                                <p><?
                                                    $array_program_type = [2 => 'Surrogacy', 3 => 'Egg Donation',
                                                        5 => 'Biotransportation'];
                                                    $v->program_type = unserialize($v->program_type);
                                                    if($v->program_type)
                                                        foreach($v->program_type as $v_pr)
                                                            echo $array_program_type[$v_pr].'<br />';
                                                    ?>
                                                </p>
                                            </li>
                                            <li>
                                                <p>Program Name</p>
                                                <p><?=$v->program_name?></p>
                                            </li>
                                            <li>
                                                <p>Program Status</p>
                                                <p><?=$v->program_status?></p>
                                            </li>
                                            <li>
                                                <p>Program Start Date</p>
                                                <p><?=$v->beginning_program?></p>
                                            </li>
                                            <li>
                                                <p>Program End Date</p>
                                                <p><?=$v->completion_program?></p>
                                            </li>
                                            <li>
                                                <p>Program Country</p>
                                                <p><?
                                                    $Program_country = \common\models\Countrys::findOne(['id' => $v->program_country]);
                                                    echo $Program_country->name;
                                                    ?></p>
                                            </li>
                                            <li>
                                                <p>Program City</p>
                                                <p><?
                                                    $program_city = \common\models\City::findOne($v->program_city);
                                                    echo $program_city->name;
                                                    ?></p>
                                            </li>
                                            <li>
                                                <p>Program Cost</p>
                                                <p><?=$v->program_cost.' '.$v->program_cost_valuta?></p>
                                            </li>
                                            <li>
                                                <p>Program Manager</p>
                                                <p><?
                                                    $program_manager = \common\models\ProgramManager::findOne($v->program_manager);
                                                    echo $program_manager->name;
                                                    ?></p>
                                            </li>
                                            <!--                                        <li>-->
                                            <!--                                            <p>Заметки:</p>-->
                                            <!--                                            <p>--><?//$v->program_type?><!--</p>-->
                                            <!--                                        </li>-->
                                            <?//if($user->role != 5){?>
                                            <li>
                                                <p>Clinic</p>
                                                <p><?
                                                    $ivf_clinic = \common\models\IvfClinic::findOne($v->ivf_clinic);
                                                    echo $ivf_clinic->name;
                                                    ?></p>
                                            </li>
                                            <li>
                                                <p>Physician</p>
                                                <p><?
                                                    $physician = \common\models\Physician::findOne($v->physician);
                                                    echo $physician->name;
                                                    ?></p>
                                            </li>
                                            <li>
                                                <p>Surrogate</p>
                                                <p><?=$v->surrogate?></p>
                                            </li>
                                            <li>
                                                <p>Egg Donor</p>
                                                <p><?=$v->egg_donor?></p>
                                            </li>
                                            <?//}?>
                                            <li>
                                                <p>Notes</p>
                                                <p><?=$v->program_notes?></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div id="general<?=$div2?>" class="general-info-content">
                                        <table class="table table-bordered table-responsive">
                                            <thead>
                                            <tr>
                                                <th scope="col">Stage Number</th>
                                                <th scope="col">Start and End Date</th>
                                                <th scope="col">Stage Name</th>
                                                <th scope="col">Stage Description</th>
                                                <th scope="col">Works performed</th>
                                                <th scope="col">User’s actions in current stage</th>
                                                <th scope="col">Files</th>
                                                <th scope="col">Notes</th>
                                                <th scope="col">Next scheduled stage</th>
                                                <th scope="col">The scheduled start and end date of the next stage</th>
                                                <th scope="col">Scheduled user actions in the next step</th>
                                                <th scope="col">Got It</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?foreach($v->program_stages as $v_p_s){?>
                                                <tr <?if(!$v_p_s->got_it) echo 'style="border: 2px solid #82EF5B"';?> class="li_program_stage id<?=$v_p_s->id?>">
                                                    <th scope="row"><?=$v_p_s->stage_number?></td>
                                                    <td><?=$v_p_s->start_date.' - '.$v_p_s->start_date?></td>
                                                    <td><?=$v_p_s->stage_name?></td>
                                                    <td><?=$v_p_s->stage_description?></td>
                                                    <td><?=$v_p_s->completed_work?></td>
                                                    <td><?=$v_p_s->user_actions?></td>
                                                    <td>
                                                        <?$images = unserialize($v_p_s->images)?>
                                                        <?if($images)foreach($images as $v_img){?>
                                                            <a href="/backend/web/<?=$v_p_s->path.$v_img?>"><?=$v_img?></a>
                                                        <?}?>
                                                    </td>
                                                    <td><?=$v_p_s->notes?></td>
                                                    <td><?=$v_p_s->next_scheduled_stage?></td>
                                                    <td><?=$v_p_s->scheduled_start.' - '.$v_p_s->scheduled_end?></td>
                                                    <td><?=$v_p_s->scheduled_user?></td>
                                                    <td><?if(!$v_p_s->got_it){?><div class="Got_It" data-id="<?=$v_p_s->id?>" data-program="<?=$v->id?>" style="cursor:pointer;">Got It</div><?}?></td>
                                                </tr>
                                            <?}?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?}?>
                    </div>
                </div>
                <?if($program_got_it && $user->got_it){?>
                <div class="modal fade show" id="popUpWindow" style="padding-right: 0px; display: block;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <!-- header -->
                                        <div class="modal-header">
<!--                                            <img class="modal-title" style="margin: 0 auto;" src="..." alt="ifs">-->
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <!-- body -->
                                        <div class="modal-header">
                                            <form role="form">
                                                <div class="form-group">
                                                    <p class="text-muted">
                                                        We have posted an update on your program.<br />
                                                        Please check it in your profile as soon as possible.
                                                        <a href="/profile/index">
                                                            Go to
                                                        </a>
                                                    </p>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?}?>
                <?
                    if(Yii::$app->user->id && Yii::$app->user->identity->got_it == 1){
                        $user = \common\models\User::findOne(Yii::$app->user->id);
                        $user->scenario = \common\models\User::GotIt;
                        $user->got_it = 0;
                        $user->save(false);
                    }
                ?>
                <? if($user->status_edit_profile && $user->status_profile):?>
                    <div id="tab-2" class="tab-content">
                        <div class="profile-settings">
                            <form action="/profile/edit" >
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                                <ul>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>1.	First Name</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[first_name]" placeholder="Viktoria" value="<?=$profile->first_name?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>12.	Facebook</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[facebook]" placeholder=": http://facebook.com/Adam/" value="<?=$profile->facebook?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>2.	Last Name</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[last_name]" placeholder="Lewin" value="<?=$profile->last_name?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>13.	Instagram :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[instagram]" placeholder="http://instagram.com/Adam/" value="<?=$profile->instagram?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>3.	Country</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[country]">
                                                    <? foreach ($country as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->country==$v->id ? 'selected':''?>><?=$v->name?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>14.	Twitter :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[twitter]" placeholder="http://twiter.com/Adam/" value="<?=$profile->twitter?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>4.	State</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[state]">
                                                    <? foreach ($state as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->state==$v->id ? 'selected':''?>><?=$v->name?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>15.	VK :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[vk]"  placeholder="http://vk.com/Adam/" value="<?=$profile->vk?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>5.	City</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[city]" >
                                                    <? foreach ($city as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->city==$v->id ? 'selected':''?>><?=$v->name;?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>16.	Email :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="email" name="Profiles[email]" placeholder="kolobok@gmail.com" value="<?=$profile->email?>">
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>6.	Address :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="PersonalInfo[address]" placeholder="Lenin str, building  132" value="<?=$personalInfo->address?>" >
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>17.	Marital Status</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[marital_status]">
                                                    <option value="1" <?=$personalInfo->marital_status==1 ? 'selected':''?>>Married</option>
                                                    <option value="2" <?=$personalInfo->marital_status==2 ? 'selected':''?>>Not Married</option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>7.	Zip/Postal Code</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="PersonalInfo[zip_postal_code]" placeholder="123456" value="<?=$personalInfo->zip_postal_code;?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>18.	Статус участия в программе</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="User[active]">
                                                    <option value="1" <?=$user->active==1 ? 'selected':''?>>Available</option>
                                                    <option value="2" <?=$user->active==2 ? 'selected':''?>>Not Available</option>
                                                    <option value="3" <?=$user->active==3 ? 'selected':''?>>Busy with Surrogate/Egg Donor </option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>8.	Phone :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[mobile]" placeholder=" +17775467342" value="<?=$profile->mobile?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
<!--                                            <div class="setting-cell">-->
<!--                                                <p>19.	Previous Surrogate:</p>-->
<!--                                            </div>-->
<!--                                            <div class="setting-cell">-->
<!--                                                <select name="prev-surrogate">-->
<!--                                                    <option value="1">1</option>-->
<!--                                                    <option value="2">2</option>-->
<!--                                                </select>-->
<!--                                            </div>-->
                                            <?if($user->proven){?>
                                                <img src="/images/union.svg" alt="union" title="Proven">
                                            <?}?>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>9.	Skype :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[skype]" placeholder="Kolobok" value="<?=$profile->skype?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>Old password   <a style="cursor: pointer;" class="change_pass">Change password</a></p>
                                            </div>
                                            <div class="setting-cell">
                                                <div class="input-password">
                                                    <input type="password" id="old-password" name="User[old_password]" placeholder="*********">
                                                    <button type="button" name="button" id="old_password_button" onclick="oldPassword()">
                                                        <img src="/images/view.png">
                                                    </button>
                                                    <script>
                                                        $('body').on('click', '#old_password_button', function (e) {
                                                            if($('#old-password').prop('type') == 'text' ){
                                                                $('#old-password').attr('type', 'password');
                                                            }else{
                                                                $('#old-password').attr('type', 'text');
                                                            }
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>10.	WhatsApp</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[vatzup]" placeholder="+17775467342"  value="<?=$profile->vatzup?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>New password</p>
                                            </div>
                                            <div class="setting-cell">
                                                <div class="input-password">
                                                    <input type="password" name="User[password]" id="new-password" placeholder="*********">
                                                    <button type="button" name="button" id="new_password_button" onclick="newPassword()">
                                                        <img src="/images/view.png">
                                                    </button>
                                                    <script>
                                                        $('body').on('click', '#new_password_button', function (e) {
                                                            if($('#new-password').prop('type') == 'text' ){
                                                                $('#new-password').attr('type', 'password');
                                                            }else{
                                                                $('#new-password').attr('type', 'text');
                                                            }
                                                        });
                                                    </script>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>11.	Telegram :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[telegram]" placeholder=""+17775467342 value="<?=$profile->telegram?>">
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>Confirm New Password </p>
                                            </div>
                                            <div class="setting-cell">
                                                <div class="input-password">
                                                    <input type="password" name="User[password_repeat]" id="new-password2" placeholder="*********">
                                                    <button type="button" name="button" id="new_password2_button" onclick="newPassword2()">
                                                        <img src="/images/view.png">
                                                    </button>
                                                </div>
                                                <script>
                                                    $('body').on('click', '#new_password2_button', function (e) {
                                                        if($('#new-password2').prop('type') == 'text' ){
                                                            $('#new-password2').attr('type', 'password');
                                                        }else{
                                                            $('#new-password2').attr('type', 'text');
                                                        }
                                                    });
                                                </script>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <a class="btn-setting-save btn-setting-save-parent">
                                    Save
                                </a>
                            </form>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-content">
                        <div class="my-profile-photos" id="profile_photo">
                            <div class="profile-add-photo">
                                <div class="title">
                                    <h3><a href="#modal-guidelines-photo" class="ths-popup-link">Guidelines</a></h3>
                                    <h4>Select Photo</h4>
                                    <p>(support only *.png, *.jpg, *.jpeg,and maximum size: 5mb, maximum 10 photos)</p>
                                    <? if(count($UserPhoto) < 9):?>
                                        <button type="button" name="btnAddPhoto"  class="uploadImg">Add photo</button>
                                    <? endif;?>
                                </div>
                            </div>
                            <div class="edit-add-photos">
                                <ul>
                                    <?foreach($UserPhoto as $k_Photo => $photo){?>
                                        <li class="photo-uploaded">

                                            <div class="photo-place" data-id="<?=$photo->id?>">
                                                <div class="aniimated-thumbnials">
                                                    <a href="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" target="_blank">
                                                        <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>">
                                                    </a>
                                                </div>
                                                <div class="del-ed-photo">
                                                    <? if(count($UserPhoto) != 1):?>
                                                        <button type="button" name="delImg" class="delete_img_profile_photo" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                                            <img src="/images/delete-img.png">
                                                        </button>
                                                    <? endif;?>
                                                    <button type="button" name="edImg" class="edit_img_profile" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                                        <img src="/images/edit-img.png">
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="file-name-size">
                                                <p><?=$photo->photo?></p>
                                                <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                                            </div>
                                        </li>
                                    <?}?>
                                    <?if($k_Photo < 9){?>
                                        <li>
                                            <div class="photo-place">
                                                <button type="button" name="uploadImg" class="uploadImg">
                                                    <img src="/images/addPlus.png">
                                                </button>
                                            </div>
                                        </li>
                                    <?}?>
                                </ul>
                            </div>
                            <input type="file" style="display: none;" class="uploadImgInput">
                        </div>
                    </div>
                    <div id="tab-5" class="tab-content">
                        <div class="my-profile-photos" id="profile_video">
                            <div class="profile-add-photo">
                                <div class="title">
                                    <h3><a href="#modal-guidelines-video" class="ths-popup-link">Guidelines</a></h3>
                                    <h4>Select Video:</h4>
                                    <p>(support only *.mp4, *.3gp, *.ogg and maximum size: 100 Mb, maximum 1 video)</p>

                                    <?if(!$UserVideo):?>
                                        <button type="button" name="addVideo" class="uploadImgVideo add" <?if($UserVideo){?>disabled="disabled"<?}?>>Add video</button>
                                    <? endif;?>
                                </div>
                            </div>
                            <?if($UserVideo){?>
                                <div class="my-profile-video__block">
                                    <div class="profile-iframe-video">
                                        <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                                            <img src="/frontend/web/images/video_profile.png" style="width:100px;">
                                        </a>
                                        <button type="button" name="deleteVideo" class="delete_img_profile_video" data-id="<?=$UserVideo->id?>" data-type="UserVideo">
                                            <img src="/images/delete-img.png">
                                        </button>
                                    </div>
                                </div>
                            <?}else{?>
                                <div class="photo-place profile-iframe-video">
                                    <button type="button" name="uploadImg" class="uploadImgVideo">
                                        <img src="/images/addPlus.png">
                                    </button>
                                </div>
                            <?}?>
                            <input type="file" style="display: none;" class="uploadImgInputVideo">
                        </div>
                    </div>
                    <div id="tab-4" class="tab-content">
                        <div class="my-profile-photos" id="profile_document">
                            <div class="profile-add-photo">
                                <div class="title">
                                    <h3><a href="#modal-guidelines-photo" class="ths-popup-link">Guidelines</a></h3>
                                    <h4>Select File:</h4>
                                    <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 10 documents)</p>
                                    <? if(count($UserDocuments) < 9):?>
                                        <button type="button" name="btnAddPhoto"  class="uploadImgDocuments">Add File</button>
                                    <? endif;?>
                                </div>
                            </div>
                            <div class="edit-add-photos documents">
                                <ul>
                                    <?foreach($UserDocuments as $k_Photo => $photo){?>
                                        <li class="photo-uploaded">
                                            <div class="photo-place" data-id="<?=$photo->id?>">
                                                <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank">
                                                    <img src="/frontend/web/images/doc.png">
                                                </a>
                                                <div class="del-ed-photo">
                                                    <button type="button" name="delImg" class="delete_img_profile_documents" data-id="<?=$photo->id?>" data-type="UserDocuments">
                                                        <img src="/images/delete-img.png">
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="file-name-size">
                                                <p><?=$photo->img?></p>
                                                <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                                            </div>
                                            </a>
                                        </li>
                                    <?}?>
                                    <?if($k_Photo < 9){?>
                                        <li>
                                            <div class="photo-place">
                                                <button type="button" name="uploadImg" class="uploadImgDocuments">
                                                    <img src="/images/addPlus.png">
                                                </button>
                                            </div>
                                        </li>
                                    <?}?>
                                </ul>
                            </div>
                            <input type="file" style="display: none;" class="uploadImgInputDocuments">
                        </div>
                    </div>
                    <div id="tab-6" class="tab-content">
                        <div class="my-profile-favorites">
                            <? if(count($userFavorites) != 0):?>
                                <ul>
                                    <? foreach ($userFavorites as $v):?>
                                        <li >

                                            <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                                                <div class="favorites-img">
                                                    <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                                                    $photo = $userImage[0]->photo;?>
                                                    <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">
                                                    <button type="button" name="delImg" class="delete_favorite_user" data-user-id="<?=$v->favorite_user_id?>">
                                                        <img src="/images/delete-img.png">
                                                    </button>
                                                </div>
                                                <div class="surrogate-id">
                                                    <p>[<?=$v->user->shortRoleName;?>-<?=$v->profile->createdDateWithoutDot;?><?=$v->user->orderID;?>]</p>
                                                    <p><?=$v->user->roleName;?></p>
                                                </div>
                                            </a>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            <? endif;?>
                            <? if(count($userFavorites) == 0):?>
                                Ваше избранное пусто!
                            <? endif;?>
                        </div>
                    </div>
                <? endif;?>
                <? if(!$user->status_edit_profile || !$user->status_profile):?>
                    <div id="tab-2" class="tab-content">
                        <div class="profile-settings">
                            <form action="/profile/edit" >
                                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                                <ul>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>1.	First Name</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[first_name]" placeholder="Viktoria" value="<?=$profile->first_name?>" disabled>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>12.	Facebook</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[facebook]" placeholder=": http://facebook.com/Adam/" value="<?=$profile->facebook?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>2.	Last Name</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[last_name]" placeholder="Lewin" value="<?=$profile->last_name?>" disabled>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>13.	Instagram :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[instagram]" placeholder="http://instagram.com/Adam/" value="<?=$profile->instagram?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>3.	Country</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[country]" disabled>
                                                    <? foreach ($country as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->country==$v->id ? 'selected':''?>><?=$v->name?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>14.	Twiter :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[twitter]" placeholder="http://twiter.com/Adam/" value="<?=$profile->twitter?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>4.	State</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[state]" disabled>
                                                    <? foreach ($state as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->state==$v->id ? 'selected':''?>><?=$v->name?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>15.	VK :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[vk]"  placeholder="http://vk.com/Adam/" value="<?=$profile->vk?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>5.	City</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[city]" disabled>
                                                    <? foreach ($city as $v):?>
                                                        <option value="<?=$v->id?>" <?=$personalInfo->city==$v->id ? 'selected':''?>><?=$v->name;?></option>
                                                    <? endforeach;?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>16.	Email :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="email" name="Profiles[email]" placeholder="kolobok@gmail.com" value="<?=$profile->email?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>6.	Address :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="PersonalInfo[address]" placeholder="Lenin str, building  132" value="<?=$personalInfo->address?>" disabled>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>17.	Marital Status</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="PersonalInfo[marital_status]" disabled>
                                                    <option value="1" <?=$personalInfo->marital_status==1 ? 'selected':''?>>Married</option>
                                                    <option value="2" <?=$personalInfo->marital_status==2 ? 'selected':''?>>Not Married</option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>7.	Zip/Postal Code</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="PersonalInfo[zip_postal_code]" placeholder="123456" value="<?=$personalInfo->zip_postal_code;?>" disabled>
                                            </div>
                                        </div>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>18.	Статус участия в программе</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="User[active]" disabled>
                                                    <option value="1" <?=$user->active==1 ? 'selected':''?>>Available</option>
                                                    <option value="2" <?=$user->active==2 ? 'selected':''?>>Not Available</option>
                                                    <option value="3" <?=$user->active==3 ? 'selected':''?>>Busy with Surrogate/Egg Donor </option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>8.	Phone :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[mobile]" placeholder=" +17775467342" value="<?=$profile->mobile?>" disabled>
                                            </div>
                                        </div>
                                        <div class="setting-row" style="display: none;">
                                            <div class="setting-cell">
                                                <p>19.	Previous Surrogate:</p>
                                            </div>
                                            <div class="setting-cell">
                                                <select name="prev-surrogate" disabled>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                </select>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>9.	Skype :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[skype]" placeholder="Kolobok" value="<?=$profile->skype?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>10.	WhatsApp</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[vatzup]" placeholder="+17775467342"  value="<?=$profile->vatzup?>" disabled>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="setting-row">
                                            <div class="setting-cell">
                                                <p>11.	Telegram :</p>
                                            </div>
                                            <div class="setting-cell">
                                                <input type="text" name="Profiles[telegram]" placeholder="+17775467342" value="<?=$profile->telegram?>" disabled>
                                            </div>
                                        </div>

                                    </li>
                                </ul>

                            </form>
                        </div>
                    </div>
                    <div id="tab-3" class="tab-content">
                        <div class="my-profile-photos" id="profile_photo">
                            <div class="edit-add-photos">
                                <ul>
                                    <?foreach($UserPhoto as $k_Photo => $photo){?>
                                        <li class="photo-uploaded">

                                            <div class="photo-place" data-id="<?=$photo->id?>">
                                                <div class="aniimated-thumbnials">
                                                    <a href="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" target="_blank">
                                                        <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>">
                                                    </a>
                                                </div>
                                            </div>
                                            <div class="file-name-size">
                                                <p><?=$photo->photo?></p>
                                                <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                                            </div>
                                        </li>
                                    <?}?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="tab-4" class="tab-content">
                        <div class="my-profile-photos" id="profile_document">
                            <div class="edit-add-photos documents">
                                <ul>
                                    <?foreach($UserDocuments as $k_Photo => $photo){?>
                                        <li class="photo-uploaded">
                                            <div class="photo-place" data-id="<?=$photo->id?>">
                                                <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank">
                                                    <img src="/frontend/web/images/doc.png">
                                                </a>
                                            </div>
                                            <div class="file-name-size">
                                                <p><?=$photo->img?></p>
                                                <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                                            </div>
                                            </a>
                                        </li>
                                    <?}?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div id="tab-5" class="tab-content">
                        <div class="my-profile-videos">

                            <?if($UserVideo){?>
                                <div class="my-profile-video__block">
                                    <div class="profile-iframe-video">
                                        <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                                            <img src="/frontend/web/images/video_profile.png" style="width:100px;">
                                        </a>
                                    </div>
                                </div>
                            <?}else{?>

                            <?}?>
                            <input type="file" style="display: none;" class="uploadImgInputVideo">
                        </div>
                    </div>
                    <div id="tab-6" class="tab-content">
                        <div class="my-profile-favorites">
                            <? if(count($userFavorites) != 0):?>
                                <ul>
                                    <? foreach ($userFavorites as $v):?>
                                        <li >

                                            <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                                                <div class="favorites-img">
                                                    <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                                                    $photo = $userImage[0]->photo;?>
                                                    <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">
                                                </div>
                                                <div class="surrogate-id">
                                                    <p>[<?=$v->user->shortRoleName;?>-<?=$v->profile->createdDateWithoutDot;?><?=$v->user->orderID;?>]</p>
                                                    <p><?=$v->user->roleName;?></p>
                                                </div>
                                            </a>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            <? endif;?>
                            <? if(count($userFavorites) == 0):?>
                                Ваше избранное пусто!
                            <? endif;?>
                        </div>
                    </div>
                <? endif;?>

            </div>
        </div>
    </div>
    <!-- Egg donor profile end -->
</main>





<!-- Egg donor MOB profile -->
<script src="/js/profile_mob.js"></script>
<main class="egg-donor-profile-main" style="background-color: #1371B6;" id="egg-donor-profile-main--mob">
    <!-- BANNER -->
    <div class="profile-banner">
        <div class="container">
            <h2>My profile</h2>
        </div>
    </div>
    <!-- END BANNER -->

    <div class="mob-my-profile">
        <div class="mob-profile-photos">

            <div class="main-photo">
                <img class="xzoom" src="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$userPhoto[0]->photo?>" xoriginal="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$userPhoto[0]->photo?>">
            </div>
            <div class="plate-photos">
                <? foreach ($userPhoto as $v):?>
                    <a href="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>" class="plate-photo-item"><img class="xzoom-gallery" xpreview="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>" src="<?='/'.@backend.'/web/images/userphoto/'.Yii::$app->user->identity->id.'/'.$v->photo?>"></a>
                <? endforeach;?>
            </div>
        </div>
        <div class="mob-profile_info">
            <div class="name">
                <span><?=$profile->fio;?></span>
                <?if($user->proven){?>
                    <img src="/images/union.svg" alt="union" title="Proven">
                <?}?>

            </div>
            <a class="mob-profile-id" href="<?='/'.@backend.'/web/images/uservideo/'.Yii::$app->user->identity->id.'/'.$userVideo->video?>">
                <span>[<?=$user->getShortRoleName();?>-<?=$profile->getCreatedDateWithoutDot();?><?=$user->getOrderID();?>]</span>
                <img src="/images/video.png">
            </a>
            <div class="mob-regis-date">
                <span>Registration Date: <?=$profile->getCreatedDate();?></span>
            </div>
            <div class="mob-profile_status">
                <span><?=$user->status_profile ? "Active profile":"Suspended Profile"?></span>
                <? if(!$user->status_profile):?>
                <p style="color: red;">Your profile has been suspended. If you have any question, please do not hesitate to contact us.
                    <? endif;?>
            </div>
        </div>
        <div class="mob-intended">
            <span><?=$user->getRoleName();?></span>
        </div>
        <div class="mob-avilable">
        <span>
             <? if($user->active == 1):?> <?='Available'?><? endif;?>
             <? if($user->active == 2):?> <?='Not Available'?><?endif;?>
             <? if($user->active == 3):?> <?='Busy with Surrogate/Egg Donor'?><?endif;?>
        </span>
        </div>
        <form class="avilable-content avilable-edit mob-avilable-content" action="/profile/update">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <button class="save-btn" type="button" name="button-save" style="display: none" id="profile_status_save_mob">
                Save
            </button>
            <? if($user->status_edit_profile && $user->status_profile):?>
                <button class="edit-btn" type="button" name="button-save" id="profile_status_edit_mob">
                    <img src="/images/edit.png">
                </button>
            <?endif;?>

            <p id="profile_status_text_mob"><?=$profile->status?></p>
            <textarea id="profile_status_textarea_mob" style="display: none;width: 100%;" name="Profiles[status]"><?=$profile->status?></textarea>

        </form>
        <div class="mob-profile-country">
            <img src="/images/country.png">
            <span><?=$personalInfo->getCountryName();?></span>
        </div>
        <div class="mob-profile-year">
            <span><?=$personalInfo->getDateOfBirth();?> years</span>
        </div>
        <div class="mob-profile-marital">
            <span><?=$personalInfo->getMaritalStatus();?></span>
        </div>

        <!--   Contact    -->
        <div class="mob-profile-contacts">
            <span>Contacts</span>
        </div>
        <div class="mob-profile-address">
            <p>Address: <span><?=$personalInfo->address?> </span></p>
        </div>
        <div class="mob-social-networks">
            <ul>
                <li>
            <span>
              <a href="#"><img src="/images/skype-pr.png"><?=$profile->skype;?></a>
            </span>
                    <span>
              <a href="tel:<?=$profile->telegram;?>"><img src="/images/telegram-logo.png"><?=$profile->telegram;?></a>
            </span>
                </li>
                <li>
            <span>
              <a href="tel:<?=$profile->work_contact;?>"><img src="/images/viber.png"><?=$profile->work_contact;?></a>
            </span>
                    <span>
              <a href="tel:<?=$profile->mobile;?>"><img src="/images/tel-pr.png"><?=$profile->mobile;?></a>
            </span>
                </li>
                <li>
            <span>
              <a href="tel:<?=$profile->vatzup;?>"><img src="/images/whatsapp.png"><?=$profile->vatzup;?></a>
            </span>
                    <span>
              <a href="mailto:<?=$profile->email;?>"><img src="/images/mail-pr.png"><?=$profile->email;?></a>
            </span>
                </li>
            </ul>
        </div>

        <!--   Program type   -->
        <div class="mob-program-type">
            <span>Program Type</span>
        </div>
        <div class="mob-sur-egg-don">
            <span>: Surrogacy and Egg Donation</span>
        </div>
        <div class="mob-eco-program">
            <span>Economy</span>
            <span>Program Start Date <br> 18.02.2019 </span>
        </div>

        <!--   Block   -->
        <div class="mobile-program" id="accordion">
            <div class="card mob-program-card">
                <div class="card-header" id="headingOne">
                    <h5 class="mb-0">
                        <?foreach($program as $k => $v){?>
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapse<?=$k?>" aria-expanded="true" aria-controls="collapse<?=$k?>">
                                Program
                                <span class="new-message"><?=$k?></span>
                            </button>
                        <?}?>
                    </h5>
                </div>
                <?foreach($program as $k => $v){?>
                    <div id="collapse<?=$k?>" class="collapse mob-gen-info" aria-labelledby="headingOne" data-parent="#accordion">
                        <ul>
                            <h3>General information</h3>
                            <li>
                                <p>Program Type</p>
                                <p><?
                                    $array_program_type = [2 => 'Surrogacy', 3 => 'Egg Donation',
                                        5 => 'Biotransportation'];
                                    if($v->program_type)
                                        foreach($v->program_type as $v_pr)
                                            echo $array_program_type[$v_pr].'<br />';
                                    ?></p>
                            </li>
                            <li>
                                <p>Program Name</p>
                                <p><?=$v->program_name?></p>
                            </li>
                            <li>
                                <p>Program Status</p>
                                <p><?=$v->program_status?></p>
                            </li>
                            <li>
                                <p>Program Start Date</p>
                                <p><?=$v->beginning_program?></p>
                            </li>
                            <li>
                                <p>Program End Date</p>
                                <p><?=$v->completion_program?></p>
                            </li>
                            <li>
                                <p>Program Country</p>
                                <p><?
                                    $Program_country = \common\models\Countrys::findOne(['id' => $v->program_country]);
                                    echo $Program_country->name;
                                    ?></p>
                            </li>
                            <li>
                                <p>Program City</p>
                                <p><?
                                    $program_city = \common\models\City::findOne($v->program_city);
                                    echo $program_city->name;
                                    ?></p>
                            </li>
                            <li>
                                <p>Program Cost</p>
                                <p><?=$v->program_cost.' '.$v->program_cost_valuta?></p>
                            </li>
                            <li>
                                <p>Program Manager</p>
                                <p><?
                                    $program_manager = \common\models\ProgramManager::findOne($v->program_manager);
                                    echo $program_manager->name;
                                    ?></p>
                            </li>
                            <li>
                                <p>Clinic</p>
                                <p><?
                                    $ivf_clinic = \common\models\IvfClinic::findOne($v->ivf_clinic);
                                    echo $ivf_clinic->name;
                                    ?></p>
                            </li>
                            <li>
                                <p>Physician</p>
                                <p><?
                                    $physician = \common\models\Physician::findOne($v->physician);
                                    echo $physician->name;
                                    ?></p>
                            </li>
                            <li>
                                <p>Surrogate</p>
                                <p><?=$v->surrogate?></p>
                            </li>
                            <li>
                                <p>Egg Donor</p>
                                <p><?=$v->egg_donor?></p>
                            </li>
                            <li>
                                <p>Notes</p>
                                <p><?=$v->program_notes?></p>
                            </li>
                        </ul>

                        <div class="mob-program-stage-table">
                            <h3>Program Stages</h3>
                            <ul>
                                <li>
                                    <p>Stage Number</p>
                                    <p>Start and End Date</p>
                                    <p>Stage Name</p>
                                    <p>Stage Description</p>
                                    <p>Works performed</p>
                                    <p>User’s actions in current stage</p>
                                    <p>Files</p>
                                    <p>Notes</p>
                                    <p>Next scheduled stage</p>
                                    <p>The scheduled start and end date of the next stage</p>
                                    <p>Scheduled user actions in the next step</p>
                                    <p>Got It</p>
                                </li>
                                <?foreach($v->program_stages as $v_p_s){?>
                                    <li <?if(!$v_p_s->got_it) echo 'style="border: 2px solid #82EF5B"';?> class="li_program_stage id<?=$v_p_s->id?>">
                                        <p><?=$v_p_s->stage_number?></p>
                                        <p><?=$v_p_s->start_date.' - '.$v_p_s->start_date?></p>
                                        <p><?=$v_p_s->stage_name?></p>
                                        <p><?=$v_p_s->stage_description?></p>
                                        <p><?=$v_p_s->completed_work?></p>
                                        <p><?=$v_p_s->user_actions?></p>
                                        <p>
                                            <?$images = unserialize($v_p_s->images)?>
                                            <?if($images)foreach($images as $v_img){?>
                                                <a href="/backend/web/<?=$v_p_s->path.$v_img?>"><?=$v_img?></a>
                                            <?}?>
                                        </p>
                                        <p><?=$v_p_s->notes?></p>
                                        <p><?=$v_p_s->next_scheduled_stage?></p>
                                        <p><?=$v_p_s->scheduled_start.' - '.$v_p_s->scheduled_end?></p>
                                        <p><?=$v_p_s->scheduled_user?></p>
                                        <p><?if(!$v_p_s->got_it){?><div class="Got_It" data-id="<?=$v_p_s->id?>" data-program="<?=$v->id?>" style="cursor:pointer;">Got It</div><?}?></p>
                                    </li>
                                <?}?>
                            </ul>
                        </div>
                    </div>
                <?}?>
            </div>
            <? if($user->status_edit_profile && $user->status_profile):?>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Settings
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="mob-profile-settings">
                            <form action="index.html" method="post">
                                <ul>
                                    <li>
                                        <p>1.	First Name</p>
                                        <input type="text" name="Profiles[first_name]" placeholder="Viktoria" value="<?=$profile->first_name?>">
                                    </li>
                                    <li>
                                        <p>2.	Last Name</p>
                                        <input type="text" name="Profiles[last_name]" placeholder="Lewin" value="<?=$profile->last_name?>">
                                    </li>
                                    <li>
                                        <p>3.	Country</p>
                                        <select name="PersonalInfo[country]">
                                            <? foreach ($country as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->country==$v->id ? 'selected':''?>><?=$v->name?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>4.	State</p>
                                        <select name="PersonalInfo[state]">
                                            <? foreach ($state as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->state==$v->id ? 'selected':''?>><?=$v->name?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>5.	City</p>
                                        <select name="PersonalInfo[city]" >
                                            <? foreach ($city as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->city==$v->id ? 'selected':''?>><?=$v->name;?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>6.	Address :</p>
                                        <input type="text" name="PersonalInfo[address]" placeholder="Lenin str, building  132" value="<?=$personalInfo->address?>" >
                                    </li>
                                    <li>
                                        <p>7.	Zip/Postal Code</p>
                                        <input type="text" name="PersonalInfo[zip_postal_code]" placeholder="123456" value="<?=$personalInfo->zip_postal_code;?>">
                                    </li>
                                    <li>
                                        <p>8.	Phone :</p>
                                        <input type="text" name="Profiles[mobile]" placeholder=" +17775467342" value="<?=$profile->mobile?>">
                                    </li>
                                    <li>
                                        <p>9.	Skype :</p>
                                        <input type="text" name="Profiles[skype]" placeholder="Kolobok" value="<?=$profile->skype?>">
                                    </li>
                                    <li>
                                        <p>10.	WhatsApp</p>
                                        <input type="text" name="Profiles[vatzup]" placeholder="+17775467342"  value="<?=$profile->vatzup?>">
                                    </li>
                                    <li>
                                        <p>11.	Telegram :</p>
                                        <input type="text" name="Profiles[telegram]" placeholder="+17775467342" value="<?=$profile->telegram?>">
                                    </li>
                                    <li>
                                        <p>12.	Facebook</p>
                                        <input type="text" name="Profiles[facebook]" placeholder=": http://facebook.com/Adam/" value="<?=$profile->facebook?>">
                                    </li>
                                    <li>
                                        <p>13.	Instagram :</p>
                                        <input type="text" name="Profiles[instagram]" placeholder="http://instagram.com/Adam/" value="<?=$profile->instagram?>">
                                    </li>
                                    <li>
                                        <p>14.	Twitter :</p>
                                        <input type="text" name="Profiles[twitter]" placeholder="http://twiter.com/Adam/" value="<?=$profile->twitter?>">
                                    </li>
                                    <li>
                                        <p>15.	VK :</p>
                                        <input type="text" name="Profiles[vk]"  placeholder="http://vk.com/Adam/" value="<?=$profile->vk?>">
                                    </li>
                                    <li>
                                        <p>16.	Email :</p>
                                        <input type="email" name="Profiles[email]" placeholder="kolobok@gmail.com" value="<?=$profile->email?>">
                                    </li>
                                    <li>
                                        <p>17.	Marital Status</p>
                                        <select name="PersonalInfo[marital_status]">
                                            <option value="1" <?=$personalInfo->marital_status==1 ? 'selected':''?>>Married</option>
                                            <option value="2" <?=$personalInfo->marital_status==2 ? 'selected':''?>>Single</option>
                                        </select>
                                    </li>
                                    <li>
                                        <p>18.	Статус участия в программе</p>
                                        <select name="User[active]">
                                            <option value="1" <?=$user->active==1 ? 'selected':''?>>Available</option>
                                            <option value="2" <?=$user->active==2 ? 'selected':''?>>Not Available</option>
                                            <option value="3" <?=$user->active==3 ? 'selected':''?>>Busy with Surrogate/Egg Donor</option>
                                        </select>
                                    </li>
                                    <li>
                                        <?if($user->proven){?>
                                            <img src="/images/union.svg" alt="union" title="Proven">
                                        <?}?>
                                    </li>
                                    <li>
                                        <p>Old password   <a style="cursor: pointer;" class="change_pass">Change password</a></p>
                                        <div class="input-password">
                                            <input type="password" id="old-password-mob" name="User[old_password]" placeholder="*********">
                                            <button type="button" name="button" id="old_password_button_mob" onclick="oldPassword()">
                                                <img src="/images/view.png">
                                            </button>
                                            <script>
                                                $('body').on('click', '#old_password_button_mob', function (e) {
                                                    if($('#old-password-mob').prop('type') == 'text' ){
                                                        $('#old-password-mob').attr('type', 'password');
                                                    }else{
                                                        $('#old-password-mob').attr('type', 'text');
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </li>
                                    <li>
                                        <p>New password</p>
                                        <div class="input-password">
                                            <input type="password" name="User[password]" id="new-password-mob" placeholder="*********">
                                            <button type="button" name="button" id="new_password_button_mob" onclick="newPassword()">
                                                <img src="/images/view.png">
                                            </button>
                                            <script>
                                                $('body').on('click', '#new_password_button_mob', function (e) {
                                                    if($('#new-password-mob').prop('type') == 'text' ){
                                                        $('#new-password-mob').attr('type', 'password');
                                                    }else{
                                                        $('#new-password-mob').attr('type', 'text');
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </li>
                                    <li>
                                        <p>Confirm New Password </p>
                                        <div class="input-password">
                                            <input type="password" name="User[password_repeat]" id="new-password2-mob" placeholder="*********">
                                            <button type="button" name="button" id="new_password2_button_mob" onclick="newPassword2()">
                                                <img src="/images/view.png">
                                            </button>
                                            <script>
                                                $('body').on('click', '#new_password2_button_mob', function (e) {
                                                    if($('#new-password2-mob').prop('type') == 'text' ){
                                                        $('#new-password2-mob').attr('type', 'password');
                                                    }else{
                                                        $('#new-password2-mob').attr('type', 'text');
                                                    }
                                                });
                                            </script>
                                        </div>
                                    </li>
                                </ul>
                                <a class="btn-setting-save btn-setting-save-parent">
                                    Save
                                </a>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card" id="profile_photo_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Photo
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse mob-profile_photo" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="title">
                            <span><a href="#modal-guidelines-photo" class="ths-popup-link">Guidelines</a></span>
                        </div>
                        <div class="mob-profile_photo-info">
                            <h4>Select Photo</h4>
                            <p>(support only *.png, *.jpg, *.jpeg,and maximum size: 5mb, maximum 10 photos)</p>
                        </div>
                        <? if(count($UserPhoto) < 9):?>
                            <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addPhoto">
                                Add photo
                            </button>
                        <? endif;?>

                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?foreach($UserPhoto as $k_Photo => $photo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <div class="aniimated-thumbnials">
                                                <a href="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" target="_blank">
                                                    <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" style="width: 150px;height: 95px;">
                                                </a>
                                            </div>
                                            <div class="del-ed-photo">
                                                <? if(count($UserPhoto) != 1):?>
                                                    <button type="button" name="delImg" class="delete_img_profile_photo_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                                        <img src="/images/delete-img.png">
                                                    </button>
                                                <? endif;?>
                                                <button type="button" name="edImg" class="edit_img_profile_mob" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                                    <img src="/images/edit-img.png">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="file-name-size">
                                            <p><?=$photo->photo?></p>
                                            <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                                        </div>
                                    </li>
                                <? }?>

                                <?if($k_Photo < 9){?>
                                    <li>
                                        <div class="mob-photo-place">
                                            <button type="button" name="uploadImg" class="uploadImg_mob">
                                                <img src="/images/addPlus.png">
                                            </button>
                                        </div>
                                    </li>
                                <? }?>
                            </ul>
                        </div>
                        <input type="file" style="display: none;" class="uploadImgInput_mob">
                    </div>
                </div>
                <div class="card" id="profile_doc_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseDoc" aria-expanded="false" aria-controls="collapseDoc">
                                Documents
                            </button>
                        </h5>
                    </div>
                    <div id="collapseDoc" class="collapse mob-profile_photo" aria-labelledby="headingDoc" data-parent="#accordion">
                        <div class="title">
                            <span><a href="#modal-guidelines-doc" class="ths-popup-link">Guidelines</a></span>
                        </div>
                        <div class="mob-profile_photo-info">
                            <h4>Select File:</h4>
                            <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 10 documents)</p>
                        </div>
                        <? if(count($UserDocuments) < 9):?>
                            <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addDoc">
                                Add File
                            </button>
                        <? endif;?>

                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?foreach($UserDocuments as $k_Photo => $photo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank" style="width: 120px;height: 95px;">
                                                <img src="/frontend/web/images/doc.png">
                                            </a>
                                            <div class="del-ed-photo">
                                                <button type="button" name="delImg" class="delete_img_profile_documents_mob" data-id="<?=$photo->id?>" data-type="UserDocuments">
                                                    <img src="/images/delete-img.png">
                                                </button>
                                            </div>
                                        </div>
                                        <div class="file-name-size">
                                            <p><?=$photo->img?></p>
                                            <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                                        </div>
                                    </li>
                                <? }?>

                                <?if($k_Photo < 9){?>
                                    <li>
                                        <div class="mob-photo-place">
                                            <button type="button" name="uploadImg" class="uploadImgDocuments_doc">
                                                <img src="/images/addPlus.png">
                                            </button>
                                        </div>
                                    </li>
                                <? }?>
                            </ul>
                        </div>
                        <input type="file" style="display: none;" class="uploadImgInputDocuments_mob">
                    </div>
                </div>
                <div class="card" id="profile_video_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseVideo" aria-expanded="false" aria-controls="collapseVideo">
                                Video
                            </button>
                        </h5>
                    </div>
                    <div id="collapseVideo" class="collapse mob-profile_photo" aria-labelledby="headingVideo" data-parent="#accordion">
                        <div class="title">
                            <span><a href="#modal-guidelines-video" class="ths-popup-link">Guidelines</a></span>
                        </div>
                        <div class="mob-profile_photo-info">
                            <h4>Select File:</h4>
                            <p>(support only *.mp4, *.3gp, *.ogg and maximum size: 100 Mb, maximum 1 video)</p>
                        </div>
                        <?if(!$UserVideo):?>
                            <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addVideo">
                                Add Video
                            </button>
                        <? endif;?>

                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?if($UserVideo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                                                <img src="/frontend/web/images/video_profile.png">
                                            </a>
                                            <div class="del-ed-photo">
                                                <button type="button" name="deleteVideo" class="delete_img_profile_video_mob" data-id="<?=$UserVideo->id;?>" data-type="UserVideo">
                                                    <img src="/images/delete-img.png">
                                                </button>
                                            </div>
                                        </div>
                                    </li>
                                <?}else{?>
                                    <li>
                                        <div class="mob-photo-place">
                                            <button type="button" name="uploadImg" class="uploadImgVideo_mob">
                                                <img src="/images/addPlus.png">
                                            </button>
                                        </div>
                                    </li>
                                <? }?>
                            </ul>
                        </div>
                        <input type="file" style="display: none;" class="uploadImgInputVideo_mob">
                    </div>
                </div>
                <div class="card" id="profile_favorites_mob">
                    <div class="card-header" id="headingFive">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                Favorites
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="mob-profile-favorites">
                            <? if(count($userFavorites) == 0):?>
                                Ваше избранное пусто!
                            <? endif;?>
                            <? if(count($userFavorites) != 0):?>
                                <ul>
                                    <? foreach ($userFavorites as $v):?>
                                        <li >
                                            <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                                                <div class="favorites-img">
                                                    <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                                                    $photo = $userImage[0]->photo;?>
                                                    <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">
                                                    <button type="button" name="delImg" class="delete_favorite_user_mob" data-user-id="<?=$v->favorite_user_id?>">
                                                        <img src="/images/delete-img.png">
                                                    </button>
                                                </div>
                                                <div class="surrogate-id">
                                                    <p>[<?=$v->user->shortRoleName;?>-<?=$v->profile->createdDateWithoutDot;?><?=$v->user->orderID;?>]</p>
                                                    <p><?=$v->user->roleName;?></p>
                                                </div>
                                            </a>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            <? endif;?>
                        </div>
                    </div>
                </div>
            <? endif;?>
            <? if(!$user->status_edit_profile || !$user->status_profile):?>
                <div class="card">
                    <div class="card-header" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                Settings
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="mob-profile-settings">
                            <form action="index.html" method="post">
                                <ul>
                                    <li>
                                        <p>1.	First Name</p>
                                        <input type="text" name="Profiles[first_name]" placeholder="Viktoria" value="<?=$profile->first_name?>" disabled>
                                    </li>
                                    <li>
                                        <p>2.	Last Name</p>
                                        <input type="text" name="Profiles[last_name]" placeholder="Lewin" value="<?=$profile->last_name?>" disabled>
                                    </li>
                                    <li>
                                        <p>3.	Country</p>
                                        <select name="PersonalInfo[country]" disabled>
                                            <? foreach ($country as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->country==$v->id ? 'selected':''?>><?=$v->name?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>4.	State</p>
                                        <select name="PersonalInfo[state]" disabled>
                                            <? foreach ($state as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->state==$v->id ? 'selected':''?>><?=$v->name?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>5.	City</p>
                                        <select name="PersonalInfo[city]" disabled >
                                            <? foreach ($city as $v):?>
                                                <option value="<?=$v->id?>" <?=$personalInfo->city==$v->id ? 'selected':''?>><?=$v->name;?></option>
                                            <? endforeach;?>
                                        </select>
                                    </li>
                                    <li>
                                        <p>6.	Address :</p>
                                        <input type="text" name="PersonalInfo[address]" placeholder="Lenin str, building  132" value="<?=$personalInfo->address?>" disabled>
                                    </li>
                                    <li>
                                        <p>7.	Zip/Postal Code</p>
                                        <input type="text" name="PersonalInfo[zip_postal_code]" placeholder="123456" value="<?=$personalInfo->zip_postal_code;?>" disabled>
                                    </li>
                                    <li>
                                        <p>8.	Phone :</p>
                                        <input type="text" name="Profiles[mobile]" placeholder=" +17775467342" value="<?=$profile->mobile?>" disabled>
                                    </li>
                                    <li>
                                        <p>9.	Skype :</p>
                                        <input type="text" name="Profiles[skype]" placeholder="Kolobok" value="<?=$profile->skype?>" disabled>
                                    </li>
                                    <li>
                                        <p>10.	WhatsApp</p>
                                        <input type="text" name="Profiles[vatzup]" placeholder="+17775467342"  value="<?=$profile->vatzup?>" disabled>
                                    </li>
                                    <li>
                                        <p>11.	Telegram :</p>
                                        <input type="text" name="Profiles[telegram]" placeholder="+17775467342" value="<?=$profile->telegram?>" disabled>
                                    </li>
                                    <li>
                                        <p>12.	Facebook</p>
                                        <input type="text" name="Profiles[facebook]" placeholder=": http://facebook.com/Adam/" value="<?=$profile->facebook?>" disabled>
                                    </li>
                                    <li>
                                        <p>13.	Instagram :</p>
                                        <input type="text" name="Profiles[instagram]" placeholder="http://instagram.com/Adam/" value="<?=$profile->instagram?>" disabled>
                                    </li>
                                    <li>
                                        <p>14.	Twiter :</p>
                                        <input type="text" name="Profiles[twitter]" placeholder="http://twiter.com/Adam/" value="<?=$profile->twitter?>" disabled>
                                    </li>
                                    <li>
                                        <p>15.	VK :</p>
                                        <input type="text" name="Profiles[vk]"  placeholder="http://vk.com/Adam/" value="<?=$profile->vk?>" disabled>
                                    </li>
                                    <li>
                                        <p>16.	Email :</p>
                                        <input type="email" name="Profiles[email]" placeholder="kolobok@gmail.com" value="<?=$profile->email?>" disabled>
                                    </li>
                                    <li>
                                        <p>17.	Marital Status</p>
                                        <select name="PersonalInfo[marital_status]" disabled>
                                            <option value="1" <?=$personalInfo->marital_status==1 ? 'selected':''?>>Married</option>
                                            <option value="2" <?=$personalInfo->marital_status==2 ? 'selected':''?>>Single</option>
                                        </select>
                                    </li>
                                    <li>
                                        <p>18.	Статус участия в программе</p>
                                        <select name="User[active]" disabled>
                                            <option value="1" <?=$user->active==1 ? 'selected':''?>>Available</option>
                                            <option value="2" <?=$user->active==2 ? 'selected':''?>>Not Available</option>
                                            <option value="3" <?=$user->active==3 ? 'selected':''?>>Busy with Surrogate/Egg Donor </option>
                                        </select>
                                    </li>


                                </ul>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card" id="profile_photo_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                Photo
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse mob-profile_photo" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?foreach($UserPhoto as $k_Photo => $photo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <div class="aniimated-thumbnials">
                                                <a href="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" target="_blank">
                                                    <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" style="width: 150px;height: 95px;">
                                                </a>
                                            </div>
                                        </div>
                                        <div class="file-name-size">
                                            <p><?=$photo->photo?></p>
                                            <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                                        </div>
                                    </li>
                                <? }?>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card" id="profile_doc_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseDoc" aria-expanded="false" aria-controls="collapseDoc">
                                Documents
                            </button>
                        </h5>
                    </div>
                    <div id="collapseDoc" class="collapse mob-profile_photo" aria-labelledby="headingDoc" data-parent="#accordion">

                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?foreach($UserDocuments as $k_Photo => $photo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <a href='/backend/web/images/userdocuments/<?=Yii::$app->user->id.'/'.$photo->img?>' target="_blank" style="width: 120px;height: 95px;">
                                                <img src="/frontend/web/images/doc.png">
                                            </a>
                                        </div>
                                        <div class="file-name-size">
                                            <p><?=$photo->img?></p>
                                            <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->user->id.'/'.$photo->img)/1000);?> кб.</span>
                                        </div>
                                    </li>
                                <? }?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card" id="profile_video_mob">
                    <div class="card-header" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseVideo" aria-expanded="false" aria-controls="collapseVideo">
                                Video
                            </button>
                        </h5>
                    </div>
                    <div id="collapseVideo" class="collapse mob-profile_photo" aria-labelledby="headingVideo" data-parent="#accordion">
                        <div class="mob-add-photos__wrapper">
                            <ul>
                                <?if($UserVideo){?>
                                    <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                                        <div class="mob-photo-place">
                                            <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                                                <img src="/frontend/web/images/video_profile.png">
                                            </a>
                                        </div>
                                    </li>
                                <?}?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card" id="profile_favorites_mob">
                    <div class="card-header" id="headingFive">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseFive" aria-expanded="false" aria-controls="collapseThree">
                                Favorites
                            </button>
                        </h5>
                    </div>
                    <div id="collapseFive" class="collapse" aria-labelledby="headingFive" data-parent="#accordion">
                        <div class="mob-profile-favorites">
                            <? if(count($userFavorites) == 0):?>
                                Ваше избранное пусто!
                            <? endif;?>
                            <? if(count($userFavorites) != 0):?>
                                <ul>
                                    <? foreach ($userFavorites as $v):?>
                                        <li >
                                            <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                                                <div class="favorites-img">
                                                    <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                                                    $photo = $userImage[0]->photo;?>
                                                    <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">

                                                </div>
                                                <div class="surrogate-id">
                                                    <p>[<?=$v->user->shortRoleName;?>-<?=$v->profile->createdDateWithoutDot;?><?=$v->user->orderID;?>]</p>
                                                    <p><?=$v->user->roleName;?></p>
                                                </div>
                                            </a>
                                        </li>
                                    <? endforeach;?>
                                </ul>
                            <? endif;?>
                        </div>
                    </div>
                </div>
            <? endif;?>

        </div>
    </div>

</main>



