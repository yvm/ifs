<div class="profile-add-photo">
    <div class="title">
        <h3><a href="#modal-guidelines-photo" class="ths-popup-link">Guidelines</a></h3>
        <h4>Select Photo</h4>
        <p>(support only *.png, *.jpg, *.jpeg,and maximum size: 5mb, maximum 10 photos)</p>
        <span><?=$error;?></span>
        <? if(count($UserPhoto) < 9):?>
            <button type="button" name="btnAddPhoto" id="addPhoto" class="uploadImg">Add photo</button>
        <? endif;?>
    </div>
</div>
<div class="edit-add-photos">
    <ul>
        <?foreach($UserPhoto as $k_Photo => $photo){?>
            <li class="photo-uploaded">

                <div class="photo-place" data-id="<?=$photo->id?>">
                    <div class="aniimated-thumbnials">
                        <a href="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>" target="_blank">
                            <img src="<?='/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo?>">
                        </a>
                    </div>
                    <div class="del-ed-photo">
                        <? if(count($UserPhoto) != 1):?>
                            <button type="button" name="delImg" class="delete_img_profile_photo" data-id="<?=$photo->id?>" data-type="UserPhoto">
                                <img src="/images/delete-img.png">
                            </button>
                        <? endif;?>
                        <button type="button" name="edImg" class="edit_img_profile" data-id="<?=$photo->id?>" data-type="UserPhoto">
                            <img src="/images/edit-img.png">
                        </button>
                    </div>
                </div>
                <div class="file-name-size">
                    <p><?=$photo->photo?></p>
                    <span><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->user->id.'/'.$photo->photo)/1000);?> кб.</span>
                </div>
            </li>
        <?}?>
        <?if($k_Photo < 9){?>
            <li>
                <div class="photo-place">
                    <button type="button" name="uploadImg" class="uploadImg">
                        <img src="/images/addPlus.png">
                    </button>
                </div>
            </li>
        <?}?>
    </ul>
</div>
<input type="file" style="display: none;" class="uploadImgInput">




