<main class="egg-donor-profile-main" style="background-color: #1371B6;">
    <!-- BANNER -->
    <div class="main-banner egg-donor-profile-banner">
        <div class="container">
            <div class="banner-buttons">
                <? if(Yii::$app->view->params['buttons'][0]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][0]->name?></a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][1]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][1]->name?></a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][2]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][2]->name?> </a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][3]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][3]->name?></a>
                <? endif;?>
            </div>
            <div class="banner-info">
                <h4>INTERNATIONAL FERTILITY SOLUTIONS</h4>
                <h1>Intended Parent Profile</h1>
            </div>
            <div class="banner-nav">
                <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
            </div>
        </div>
    </div>
    <!-- END BANNER -->

    <!-- Egg donor profile -->
    <div style="padding-bottom: 3rem;" class="container">
        <div class="egg-donor-profile">
            <div class="egg-donor-profile__section">
                <div class="egg-donor-photos">
                    <img class="xzoom" src="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$userPhoto[0]->photo?>" xoriginal="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$userPhoto[0]->photo?>">
                    <div class="xzoom-thumbs owl-carousel owl-theme egg-donor-xzoom-photos">
                        <? foreach ($userPhoto as $v):?>
                            <a href="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>"><img class="xzoom-gallery" xpreview="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>" src="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>"></a>
                        <? endforeach;?>
                    </div>
                    <a class="egg-donor-video" href="<?='/'.@backend.'/web/images/uservideo/'.Yii::$app->user->identity->id.'/'.$userVideo->video?>" target="_blank">
                        <img src="/images/video.png">
                        Watch video
                    </a>
                </div>
                <div class="egg-donor-avilable">
                    <div class="reg-date">
                        <div class="profi-row">
                            <span><span style="color:#FF5C28;"><?=$profile->fio?></span> [<?=$user->getShortRoleName();?>-<?=$profile->getCreatedDateWithoutDot();?><?=$user->getOrderID();?>]</span>
                            <?if($user->proven){?>
                                <img src="/images/union.svg" alt="union" title="Proven">
                            <?}?>
                            <?if($user->profi){?>
                                <span class="profi" title="Previous <?=$user->getRoleName();?>">profi</span>
                            <?}else{?>
                                <?if($user->miscellaneous->have_been_eggdonor == 1){?>
                                    <span class="profi" title="Previous <?=$user->getRoleName();?>">profi</span>
                                <?}?>
                            <?}?>
                        </div>
                        <p>Registration Date: <?=$profile->getCreatedDate();?></p>
                        <span><?=$user->status_profile ? "Active profile":"Suspended Profile"?></span>
                        <? if(!$user->status_profile):?>
                        <p style="color: red;">Your profile has been suspended. If you have any question, please do not hesitate to contact us.
                            <? endif;?>
                    </div>

                    <div class="intended">
                        <p><?=$user->getRoleName();?></p>
                    </div>
                    <div class="avilable-text">
                        <p>
                            <? if($user->active == 1):?> <?='Available'?><? endif;?>
                            <? if($user->active == 2):?> <?='Not Available'?><?endif;?>
                            <? if($user->active == 3):?> <?='Busy with Surrogate/Egg Donor'?><?endif;?>
                        </p>
                    </div>

                    <div class="avilable-content">
                        <p>
                            <?=$profile->status?>
                        </p>
                    </div>

                </div>
                <div class="egg-donor-section">
                    <div class="egg-donor-pers-info">
                        <div class="egg-donor-title">
                            Personal information
                        </div>
                        <div class="donor-characteristic">
                            <div class="characteristic-item">
                                <p>Height(sm)</p>
                                <p><?=$personal_info->height?></p>
                            </div>
                            <div class="characteristic-item">
                                <p>Weight(kg)*</p>
                                <p><?=$personal_info->weight?></p>
                            </div>
                            <div class="characteristic-item">
                                <p>Hair</p>
                                <p><?=$personal_info->hair_type?></p>
                            </div>
                        </div>
                        <div class="egg-donor-country">
                            <img src="/images/country.png"><?=$personal_info->getCountryName();?>
                        </div>
                        <div class="egg-donor-country">
                            <?
                            $birthDate = $personal_info->date_of_birth;
                            $birthDate = explode("-", $birthDate);
                            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                ? ((date("Y") - $birthDate[0]) - 1)
                                : (date("Y") - $birthDate[0]));
                            ?>
                            <?=$age?> years
                        </div>
                        <div class="egg-donor-country">
                            <?
                            if($personal_info->marital_status == 1)
                                echo 'Married';
                            else
                                echo 'Single';
                            ?>
                        </div>
                    </div>

                </div>
            </div>
            <div class="egg-donor-profile-buttons">
                <a href="<?=Yii::$app->view->params['buttons'][6]->url?>" class="egg-donor__btn ths-popup-link">Contact</a>
                <a style="cursor: pointer"  class="egg-donor__btn" id="add-user-to-fav" data-user-id="<?=$user->id?>" data-user="<?=Yii::$app->user->identity->id;?>"><?=in_array($user->id,$fav)? 'Remove from the favorites':'Add to favorites'; ?></a>
                <a href="/database/intended-parent-database" class="egg-donor__btn">Back to Database</a>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>
                $(document).ready(function () {
                    $("#add-user-to-fav").click(function () {
                        var favorite_user_id = $(this).attr('data-user-id');
                        var user_id = $(this).attr('data-user');
                        if(user_id == ""){
                            alert('Чтобы добавить пользователь в избранное нужно войти!');
                        }else {
                            $.ajax({
                                url: "/profile/favorite",
                                dataType: "json",
                                data: {favorite_user_id: favorite_user_id},
                                method: "post",
                                success: function (data) {
                                    if(data.status){
                                        $("#add-user-to-fav").html('Remove from the favorites');
                                    }else{
                                        $("#add-user-to-fav").html('Add to favorites');
                                    }
                                },
                                error: function () {
                                    alert('Упс, что-то пошло не так!');
                                }
                            });
                        }

                    });
                });
            </script>



            <div class="egg-donor-tabs">
                <ul class="tabs">
                    <li class="tab-link current" data-tab="pi">Personal Information</li>
                    <li class="tab-link" data-tab="ci">Contact Information</li>
                    <li class="tab-link" data-tab="ai">Aditional Information</li>
                    <li class="tab-link" data-tab="cc">Consultation</li>
                    <li class="tab-link" data-tab="d">Documents</li>
                </ul>

                <div class="egg-donor-tabs__content">
                    <div id="pi" class="tab-content current">
                        <ul class="adi-info">
                            <li>
                                <span>Gender</span>
                                <span>
                                    <?if($personal_info->gender == 1) echo 'Male';else echo 'Female';?>
                                </span>
                            </li>
                            <li>
                                <span>Marital Status</span>
                                <span>
                                    <?if($personal_info->marital_status == 1) echo 'Married';else echo 'Single';?>
                                </span>
                            </li>
                            <li>
                                <span>Date of Birth</span>
                                <span><?=$personal_info->date_of_birth?></span>
                            </li>
                            <li>
                                <span>Citizenship</span>
                                <span><?=$personal_info->getCitizenshipName();?></span>
                            </li>
                            <li>
                                <span>Zip/Postal Code</span>
                                <span><?=$personal_info->zip_postal_code?></span>
                            </li>
                            <li>
                                <span>Country</span>
                                <span><?=$personal_info->getCountryName()?></span>
                            </li>
                            <li>
                                <span>State</span>
                                <span><?=$personal_info->getStateName()?></span>
                            </li>
                            <li>
                                <span>City</span>
                                <span><?=$personal_info->getCityName()?></span>
                            </li>

                            <li>
                                <span>Address</span>
                                <span><?=$personal_info->address?></span>
                            </li>
                        </ul>
                    </div>
                    <div id="ci" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Mobile Contact Number</span>
                                <span><?=$profile->mobile?></span>
                            </li>

                            <li>
                                <span>Work Contact Number</span>
                                <span><?=$profile->work_contact?></span>
                            </li>

                            <li>
                                <span>Home Contact Number</span>
                                <span><?=$profile->home_contact?></span>
                            </li>

                            <li>
                                <span>Telegram  </span>
                                <span><?=$profile->telegram?></span>
                            </li>

                            <li>
                                <span>Twitter</span>
                                <span><?=$profile->twitter?></span>
                            </li>

                            <li>
                                <span>Skype</span>
                                <span><?=$profile->skype?></span>
                            </li>
                            <li>
                                <span>Facebook</span>
                                <span><?=$profile->facebook?></span>
                            </li>

                            <li>
                                <span>VK</span>
                                <span><?=$profile->vk?></span>
                            </li>
                            <li>
                                <span>WhatsApp</span>
                                <span><?=$profile->vatzup?></span>
                            </li>

                            <li>
                                <span>lnstagram  </span>
                                <span><?=$profile->instagram?></span>
                            </li>
                        </ul>
                    </div>
                    <div id="ai" class="tab-content">
                        <ul class="adi-info">
                            <? if($user->type == 1):?>

                                <li>
                                    <span>Reason for pursuing surrogacy</span>
                                    <span>
                                        <?if($AditionalInformation->reason_surrogacy == 1){?>
                                            Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 2){?>
                                            Secondary Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 3){?>
                                            Medical Condition
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 4){?>
                                            Age
                                        <?}?>
                                    </span>
                                    <span>Describes:</span>
                                    <span><?=$AditionalInformation->describe?></span>
                                </li>

                                <li>
                                    <span>Surrogate needed?</span>
                                    <span>
                                        <?if($AditionalInformation->surrogate_needed == 1){?>
                                            Yes. I require a surrogate
                                        <?}?>
                                        <?if($AditionalInformation->surrogate_needed == 2){?>
                                            No. I do not require a surrogate
                                        <?}?>
                                    </span>
                                </li>
                                <?if($AditionalInformation->surrogate_needed == 1){?>
                                    <li>
                                        <span>Message to Surrogate/Egg Donor</span>
                                        <span><?=$AditionalInformation->message_to_surrogate_eggDonor?></span>
                                    </li>
                                <?}?>
                                <li>
                                    <span>Timing to Begin</span>
                                    <span>
                                        <?if($AditionalInformation->timing_to_begin == 1){?>
                                            As Soon as Possible
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 2){?>
                                            Within the Next 6 Months
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 3){?>
                                            Within the Next Year
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 4){?>
                                            Not sure – I would like to learn more
                                        <?}?>
                                    </span>
                                </li>

                                <li>
                                    <span>Are you financially prepared for the surrogacy and egg donation process and have reviewed
                                                    the costs involved on our <a href="/site/surrogacy#surrogacy-costs">Cost of Surrogacy</a> page ?</span>
                                    <span>
                                        <?if($AditionalInformation->financially_prepared == 1){?>
                                            Yes
                                        <?}?>
                                        <?if($AditionalInformation->financially_prepared == 2){?>
                                            No
                                        <?}?>
                                    </span>
                                </li>
                                <li>
                                    <?
                                    $know_about_us = [1 => 'Google Search', 2=> 'Google Media', 3 => 'Yahoo Search',
                                        4 => 'Bing Search', 5 => 'Facebook', 6 => 'Youtube', 7 => 'Instagram',
                                        8 => 'Telegram', 9 => 'Twitter', 10 => 'Pinterest', 11 => 'Google+', 12 => 'Tumblr'
                                        , 13 => 'VK', 14 => 'Ok', 15 => 'Flickr', 16 => 'Referral', 17 => 'Other'];
                                    ?>
                                    <span>How did you know about us?</span>
                                    <span>
                                         <?=$know_about_us[$AditionalInformation->know_about_us]?>
                                    </span>
                                    <? if($AditionalInformation->know_about_us != 16 || $AditionalInformation->know_about_us != 17):?>
                                        <span>Describes:</span>
                                        <span>

                                            <?if($AditionalInformation->know_about_us == 16){?>
                                                <?=$AditionalInformation->referal?>
                                            <?}elseif($AditionalInformation->know_about_us == 17){?>
                                                <?=$AditionalInformation->who_recommended?>
                                            <?}?>
                                        </span>
                                    <? endif;?>
                                </li>
                            <? endif;?>

                            <? if($user->type == 2):?>

                                <li>
                                    <span>Reason for pursuing surrogacy</span>
                                    <span>
                                        <?if($AditionalInformation->reason_surrogacy == 1){?>
                                            Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 2){?>
                                            Secondary Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 3){?>
                                            Medical Condition
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 4){?>
                                            Age
                                        <?}?>
                                    </span>
                                    <span>Describes:</span>
                                    <span><?=$AditionalInformation->describe?></span>
                                </li>

                                <li>
                                    <span>Egg donor needed?</span>
                                    <span>
                                        <?if($AditionalInformation->egg_donor_needed == 1){?>
                                            Yes. I require an egg donor
                                        <?}?>
                                        <?if($AditionalInformation->egg_donor_needed == 2){?>
                                            No. I will be using our own or a friend's eggs.
                                        <?}?>
                                    </span>
                                </li>
                                <?if($AditionalInformation->egg_donor_needed == 1){?>
                                    <li>
                                        <span>Message to Surrogate/Egg Donor</span>
                                        <span><?=$AditionalInformation->message_to_surrogate_eggDonor?></span>
                                    </li>
                                <?}?>
                                <?if($AditionalInformation->surrogate_needed == 1){?>
                                    <li>
                                        <span>Message to Surrogate/Egg Donor</span>
                                        <span><?=$AditionalInformation->message_to_surrogate_eggDonor?></span>
                                    </li>
                                <?}?>
                                <li>
                                    <span>Timing to Begin</span>
                                    <span>
                                        <?if($AditionalInformation->timing_to_begin == 1){?>
                                            As Soon as Possible
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 2){?>
                                            Within the Next 6 Months
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 3){?>
                                            Within the Next Year
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 4){?>
                                            Not sure – I would like to learn more
                                        <?}?>
                                    </span>
                                </li>

                                <li>
                                    <span>Are you financially prepared for the surrogacy and egg donation process and have reviewed
                                                    the costs involved on our  <a href="/site/egg-donation#egg-donation-costs">Cost of Egg Donation</a> page?</span>
                                    <span>
                                        <?if($AditionalInformation->financially_prepared == 1){?>
                                            Yes
                                        <?}?>
                                        <?if($AditionalInformation->financially_prepared == 2){?>
                                            No
                                        <?}?>
                                    </span>
                                </li>
                                <li>
                                    <?
                                    $know_about_us = [1 => 'Google Search', 2=> 'Google Media', 3 => 'Yahoo Search',
                                        4 => 'Bing Search', 5 => 'Facebook', 6 => 'Youtube', 7 => 'Instagram',
                                        8 => 'Telegram', 9 => 'Twitter', 10 => 'Pinterest', 11 => 'Google+', 12 => 'Tumblr'
                                        , 13 => 'VK', 14 => 'Ok', 15 => 'Flickr', 16 => 'Referral', 17 => 'Other'];
                                    ?>
                                    <span>How did you know about us?</span>
                                    <span>
                                         <?=$know_about_us[$AditionalInformation->know_about_us]?>
                                    </span>
                                    <? if($AditionalInformation->know_about_us != 16 || $AditionalInformation->know_about_us != 17):?>
                                        <span>Describes:</span>
                                        <span>

                                            <?if($AditionalInformation->know_about_us == 16){?>
                                                <?=$AditionalInformation->referal?>
                                            <?}elseif($AditionalInformation->know_about_us == 17){?>
                                                <?=$AditionalInformation->who_recommended?>
                                            <?}?>
                                        </span>
                                    <? endif;?>
                                </li>
                            <? endif;?>


                            <? if($user->type == 3):?>

                                <li>
                                    <span>Reason for pursuing surrogacy</span>
                                    <span>
                                        <?if($AditionalInformation->reason_surrogacy == 1){?>
                                            Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 2){?>
                                            Secondary Infertility
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 3){?>
                                            Medical Condition
                                        <?}?>
                                        <?if($AditionalInformation->reason_surrogacy == 4){?>
                                            Age
                                        <?}?>
                                    </span>
                                    <span>Describes:</span>
                                    <span><?=$AditionalInformation->describe?></span>
                                </li>

                                <li>
                                    <span>Surrogate needed?</span>
                                    <span>
                                        <?if($AditionalInformation->surrogate_needed == 1){?>
                                            Yes. I require a surrogate
                                        <?}?>
                                        <?if($AditionalInformation->surrogate_needed == 2){?>
                                            No. I do not require a surrogate
                                        <?}?>
                                    </span>
                                </li>

                                <li>
                                    <span>Egg donor needed?</span>
                                    <span>
                                        <?if($AditionalInformation->egg_donor_needed == 1){?>
                                            Yes. I require an egg donor
                                        <?}?>
                                        <?if($AditionalInformation->egg_donor_needed == 2){?>
                                            No. I will be using our own or a friend's eggs.
                                        <?}?>
                                    </span>
                                </li>
                                <?if($AditionalInformation->surrogate_needed == 1 || $AditionalInformation->egg_donor_needed == 1){?>
                                    <li>
                                        <span>Message to Surrogate/Egg Donor</span>
                                        <span><?=$AditionalInformation->message_to_surrogate_eggDonor?></span>
                                    </li>
                                <?}?>
                                <li>
                                    <span>Timing to Begin</span>
                                    <span>
                                        <?if($AditionalInformation->timing_to_begin == 1){?>
                                            As Soon as Possible
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 2){?>
                                            Within the Next 6 Months
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 3){?>
                                            Within the Next Year
                                        <?}?>
                                        <?if($AditionalInformation->timing_to_begin == 4){?>
                                            Not sure – I would like to learn more
                                        <?}?>
                                    </span>
                                </li>

                                <li>
                                    <span>Are you financially prepared for the surrogacy and egg donation process and have reviewed
                                                        the costs involved on our <a href="/site/surrogacy#surrogacy-costs">Cost of Surrogacy</a> page and <a href="/site/egg-donation#egg-donation-costs">Cost of Egg Donation</a> page?</span>
                                    <span>
                                        <?if($AditionalInformation->financially_prepared == 1){?>
                                            Yes
                                        <?}?>
                                        <?if($AditionalInformation->financially_prepared == 2){?>
                                            No
                                        <?}?>
                                    </span>
                                </li>
                                <li>
                                    <?
                                    $know_about_us = [1 => 'Google Search', 2=> 'Google Media', 3 => 'Yahoo Search',
                                        4 => 'Bing Search', 5 => 'Facebook', 6 => 'Youtube', 7 => 'Instagram',
                                        8 => 'Telegram', 9 => 'Twitter', 10 => 'Pinterest', 11 => 'Google+', 12 => 'Tumblr'
                                        , 13 => 'VK', 14 => 'Ok', 15 => 'Flickr', 16 => 'Referral', 17 => 'Other'];
                                    ?>
                                    <span>How did you know about us?</span>
                                    <span>
                                         <?=$know_about_us[$AditionalInformation->know_about_us]?>
                                    </span>
                                    <? if($AditionalInformation->know_about_us != 16 || $AditionalInformation->know_about_us != 17):?>
                                        <span>Describes:</span>
                                        <span>

                                            <?if($AditionalInformation->know_about_us == 16){?>
                                                <?=$AditionalInformation->referal?>
                                            <?}elseif($AditionalInformation->know_about_us == 17){?>
                                                <?=$AditionalInformation->who_recommended?>
                                            <?}?>
                                        </span>
                                    <? endif;?>
                                </li>
                            <? endif;?>
                        </ul>
                    </div>
                    <div id="cc" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Are you ready to schedule a consultation?</span>
                                <span>
                                    <?if($Consultation->ready_consultation == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Consultation->ready_consultation == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <?if($Consultation->ready_consultation == 1){?>
                                <li>
                                    <span>Questions & Comments</span>
                                    <span><?=$Consultation->questions_comments?></span>
                                </li>
                                <li>
                                    <span>Desired Consult Location</span>
                                    <span>
                                        <?
                                        echo $Consultation->consultationLocation->name;
                                        ?>
                                    </span>
                                </li>
                                <li>
                                    <span>Desired Consult Time</span>
                                    <span>
                                        <?=$Consultation->time_date?>
                                        <?=$Consultation->time_time?>.00
                                        <?if($Consultation->time_from == 1){?>
                                            GM+2
                                        <?}?>
                                        <?if($Consultation->time_from == 2){?>
                                            GM+3
                                        <?}?>
                                        <?if($Consultation->time_from == 2){?>
                                            GM+4
                                        <?}?>
                                    </span>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                    <div id="d" class="tab-content">
                        <ul class="adi-info">

                            <?foreach($UserDocuments as $k_Photo => $photo){?>
                                <li class="photo-uploaded">
                                    <div class="photo-place">
                                        <a href='/backend/web/images/userdocuments/<?=$user->id.'/'.$photo->img?>' target="_blank">
                                            <img src="/frontend/web/images/doc.png" style="width:100px;">
                                        </a>
                                    </div>
                                </li>
                            <?}?>

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Egg donor profile end -->
</main>