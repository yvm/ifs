<main class="egg-donor-profile-main" style="background-color: #1371B6;">
    <!-- BANNER -->
    <div class="main-banner egg-donor-profile-banner">
        <div class="container">
            <div class="banner-buttons">
                <? if(Yii::$app->view->params['buttons'][0]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][0]->name?></a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][1]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][1]->name?></a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][2]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][2]->name?> </a>
                <? endif;?>
                <? if(Yii::$app->view->params['buttons'][3]->status):?>
                    <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['buttons'][3]->name?></a>
                <? endif;?>
            </div>
            <div class="banner-info">
                <h4>INTERNATIONAL FERTILITY SOLUTIONS</h4>
                <h1>Surrogate Profile</h1>
            </div>
            <div class="banner-nav">
                <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
            </div>
        </div>
    </div>
    <!-- END BANNER -->

    <!-- Egg donor profile -->
    <div style="padding-bottom: 3rem;" class="container">
        <div class="egg-donor-profile">
            <div class="egg-donor-profile__section">
                <div class="egg-donor-photos">
                    <img class="xzoom" src="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$userPhoto[0]->photo?>" xoriginal="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$userPhoto[0]->photo?>">
                    <div class="xzoom-thumbs owl-carousel owl-theme egg-donor-xzoom-photos ">
                        <? foreach ($userPhoto as $v):?>
                            <a href="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>"><img class="xzoom-gallery" xpreview="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>" src="<?='/'.@backend.'/web/images/userphoto/'.$user->id.'/'.$v->photo?>"></a>
                        <? endforeach;?>
                    </div>
                    <a class="egg-donor-video" href="<?='/'.@backend.'/web/images/uservideo/'.Yii::$app->user->identity->id.'/'.$userVideo->video?>" target="_blank">
                        <img src="/images/video.png">
                        Watch video
                    </a>
                </div>
                <div class="egg-donor-avilable">
                    <div class="reg-date">
                        <div class="profi-row">
                            <span><span style="color:#FF5C28;"><?=$profile->fio?></span> [<?=$user->getShortRoleName();?>-<?=$profile->getCreatedDateWithoutDot();?><?=$user->getOrderID();?>]</span>
                            <?if($user->proven){?>
                                <img src="/images/union.svg" alt="union" title="Proven">
                            <?}?>
                            <?if($user->profi){?>
                                <span class="profi" title="Previous <?=$user->getRoleName();?>">profi</span>
                            <?}else{?>
                                <?if($user->miscellaneous->have_been_eggdonor == 1){?>
                                    <span class="profi" title="Previous <?=$user->getRoleName();?>">profi</span>
                                <?}?>
                            <?}?>

                        </div>

                        <p>Registration Date: <?=$profile->getCreatedDate();?></p>
                        <span><?=$user->status_profile ? "Active profile":"Suspended Profile"?></span>
                        <? if(!$user->status_profile):?>
                        <p style="color: red;">Your profile has been suspended. If you have any question, please do not hesitate to contact us.
                            <? endif;?>
                    </div>

                    <div class="intended">
                        <p><?=$user->getRoleName();?></p>
                    </div>
                    <div class="avilable-text">
                        <p>
                            <? if($user->active == 1):?> <?='Available'?><? endif;?>
                            <? if($user->active == 2):?> <?='Not Available'?><?endif;?>
                            <? if($user->active == 3):?> <?='Currently family helping'?><?endif;?>
                        </p>
                    </div>

                    <div class="avilable-content">
                        <p>
                            <?=$profile->status?>
                        </p>
                    </div>

                </div>
                <div class="egg-donor-section">
                    <div class="egg-donor-pers-info">
                        <div class="egg-donor-title">
                            Personal information
                        </div>
                        <div class="donor-characteristic">
                            <div class="characteristic-item">
                                <p>Height(sm)</p>
                                <p><?=$personal_info->height?></p>
                            </div>
                            <div class="characteristic-item">
                                <p>Weight(kg)*</p>
                                <p><?=$personal_info->weight?></p>
                            </div>
                        </div>
                        <div class="egg-donor-country">
                            <img src="/images/country.png"><?=$personal_info->getCountryName();?>
                        </div>
                        <div class="egg-donor-country">
                            <?
                            $birthDate = $personal_info->date_of_birth;
                            $birthDate = explode("-", $birthDate);
                            $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                ? ((date("Y") - $birthDate[0]) - 1)
                                : (date("Y") - $birthDate[0]));
                            ?>
                            <?=$age?> years
                        </div>
                        <div class="egg-donor-country">
                            <?
                            if($personal_info->marital_status == 1)
                                echo 'Married';
                            else
                                echo 'Single';
                            ?>
                        </div>
                    </div>
                    <? if($admission->compensation):?>
                        <? if($user->status_comp):?>
                            <div class="compensation">
                                <div class="egg-donor-title">
                                    <img src="/images/money.png">Компенсация
                                </div>
                                <div class="price">
                                    <?=$Miscellaneous->desired_compensation?> <?=$Miscellaneous->other_compensation_currency==null ? $Miscellaneous->compensation_currency:$Miscellaneous->other_compensation_currency;?>
                                </div>
                            </div>
                        <? endif;?>
                    <? endif;?>
                </div>
            </div>
            <div class="egg-donor-profile-buttons">
                <a href="<?=Yii::$app->view->params['buttons'][6]->url?>" class="egg-donor__btn ths-popup-link">Contact</a>
                <a style="cursor: pointer"  class="egg-donor__btn" id="add-user-to-fav" data-user-id="<?=$user->id?>" data-user="<?=Yii::$app->user->identity->id;?>"><?=in_array($user->id,$fav)? 'Remove from the favorites':'Add to favorites'; ?></a>
                <a href="/database/surrogate-database" class="egg-donor__btn">Back to Database</a>
            </div>

            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script>
                $(document).ready(function () {
                    $("#add-user-to-fav").click(function () {
                        var favorite_user_id = $(this).attr('data-user-id');
                        var user_id = $(this).attr('data-user');
                        if(user_id == ""){
                            alert('Чтобы добавить пользователь в избранное нужно войти!');
                        }else {
                            $.ajax({
                                url: "/profile/favorite",
                                dataType: "json",
                                data: {favorite_user_id: favorite_user_id},
                                method: "post",
                                success: function (data) {
                                    if(data.status){
                                        $("#add-user-to-fav").html('Remove from the favorites');
                                    }else{
                                        $("#add-user-to-fav").html('Add to favorites');
                                    }

                                },
                                error: function () {
                                    alert('Упс, что-то пошло не так!');
                                }
                            });
                        }

                    });
                });
            </script>


            <div class="egg-donor-tabs">
                <ul class="tabs">
                    <li class="tab-link current" data-tab="pi">Personal Information</li>
                    <li class="tab-link" data-tab="ci">Contact Information</li>
                    <li class="tab-link" data-tab="sa">Surrogate Availability</li>
                    <li class="tab-link" data-tab="pas">Pregnancies and Surrogacies</li>
                    <li class="tab-link" data-tab="phh">Personal Health History</li>
                    <li class="tab-link" data-tab="mh">Mental Health</li>
                    <li class="tab-link" data-tab="sh">Sexual History</li>
                    <li class="tab-link" data-tab="pc">Personal Characteristics</li>
                    <li class="tab-link" data-tab="eao">Education and Occupation</li>
                    <li class="tab-link" data-tab="m">Miscellaneous</li>
                    <li class="tab-link" data-tab="fhh">Family Health History</li>
                    <li class="tab-link" data-tab="hd">Health Details</li>
                    <li class="tab-link" data-tab="d">Documents</li>
                    <li class="tab-link" data-tab="vm">Voice Messages</li>

                </ul>

                <div class="egg-donor-tabs__content">
                    <div id="pi" class="tab-content current">
                        <ul class="adi-info">
                            <li>
                                <span>Gender</span>
                                <span>
                                    <?if($personal_info->gender == 1) echo 'Male';else echo 'Female';?>
                                </span>
                            </li>
                            <li>
                                <span>Marital Status</span>
                                <span>
                                    <?if($personal_info->marital_status == 1) echo 'Married';else echo 'Single';?>
                                </span>
                            </li>
                            <li>
                                <span>Date of Birth</span>
                                <span><?=$personal_info->date_of_birth?></span>
                            </li>
                            <li>
                                <span>Citizenship</span>
                                <span><?=$personal_info->getCitizenshipName();?></span>
                            </li>
                            <li>
                                <span>Zip/Postal Code</span>
                                <span><?=$personal_info->zip_postal_code?></span>
                            </li>
                            <li>
                                <span>Country</span>
                                <span><?=$personal_info->getCountryName()?></span>
                            </li>
                            <li>
                                <span>State</span>
                                <span><?=$personal_info->getStateName()?></span>
                            </li>
                            <li>
                                <span>City</span>
                                <span><?=$personal_info->getCityName()?></span>
                            </li>

                            <li>
                                <span>Address</span>
                                <span><?=$personal_info->address?></span>
                            </li>
                        </ul>
                    </div>
                    <div id="ci" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Mobile Contact Number</span>
                                <span><?=$profile->mobile?></span>
                            </li>

                            <li>
                                <span>Work Contact Number</span>
                                <span><?=$profile->work_contact?></span>
                            </li>

                            <li>
                                <span>Home Contact Number</span>
                                <span><?=$profile->home_contact?></span>
                            </li>

                            <li>
                                <span>Telegram  </span>
                                <span><?=$profile->telegram?></span>
                            </li>

                            <li>
                                <span>Twitter</span>
                                <span><?=$profile->twitter?></span>
                            </li>

                            <li>
                                <span>Skype</span>
                                <span><?=$profile->skype?></span>
                            </li>
                            <li>
                                <span>Facebook</span>
                                <span><?=$profile->facebook?></span>
                            </li>

                            <li>
                                <span>VK</span>
                                <span><?=$profile->vk?></span>
                            </li>
                            <li>
                                <span>WhatsApp</span>
                                <span><?=$profile->vatzup?></span>
                            </li>

                            <li>
                                <span>lnstagram  </span>
                                <span><?=$profile->instagram?></span>
                            </li>
                        </ul>
                    </div>
                    <div id="sa" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Willing to travel (select distance)</span>
                                <span>
                                <? if($SurrogateAvailability->willing_to_travel==1) echo 'Any';
                                else echo $SurrogateAvailability->willing_to_travel;?>
                                </span>
                            </li>
                            <li>
                                <span>Would you allow the intended parents to attend doctor's appointments and be present during the birth? </span>
                                <span>
                                     <?if($SurrogateAvailability->would_you_allow == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($SurrogateAvailability->would_you_allow == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>I am willing as a Surrogate to travel to an intended parent(s) clinic that would require an overnight stay of 51 days or more </span>
                                <span>
                                     <?if($SurrogateAvailability->i_am_willing_as == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($SurrogateAvailability->i_am_willing_as == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Can you travel abroad? </span>
                                <span>
                                     <?if($SurrogateAvailability->can_you_travel == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($SurrogateAvailability->can_you_travel == 2){?>
                                        No
                                    <?}?>
                                    <?if($SurrogateAvailability->can_you_travel == 3){?>
                                        Могу на 2-3 дня
                                    <?}?>
                                    <?if($SurrogateAvailability->can_you_travel == 4){?>
                                        Могу на 2 недели
                                    <?}?>
                                    <?if($SurrogateAvailability->can_you_travel == 5){?>
                                        Могу на месяц и более
                                    <?}?>
                                    <?if($SurrogateAvailability->can_you_travel == 6){?>
                                        Необходимо обсудить отдельно
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Do you have passport to travel abroad </span>
                                <span>
                                     <?if($SurrogateAvailability->have_passport == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($SurrogateAvailability->have_passport == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Are you willing to self inject medications with proper instruction for a period of 1-4 weeks for my participation in a cycle sync? </span>
                                <span>
                                     <?if($SurrogateAvailability->willing_to_self_in_cycle_sync == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($SurrogateAvailability->willing_to_self_in_cycle_sync == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div id="pas" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Are you pregnant now? </span>
                                <span>
                                     <?if($PregnanciesAndSurrogacies->are_you_pregnant == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PregnanciesAndSurrogacies->are_you_pregnant == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Are you currently breastfeeding? </span>
                                <span>
                                     <?if($PregnanciesAndSurrogacies->are_you_currently == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PregnanciesAndSurrogacies->are_you_currently == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Have you ever been a surrogate before? </span>
                                <span>
                                     <?if($PregnanciesAndSurrogacies->have_you_ever == 0) echo 'Never';
                                     elseif($PregnanciesAndSurrogacies->have_you_ever == 100) echo '10+';
                                     else echo $PregnanciesAndSurrogacies->have_you_ever;
                                     ?>
                                </span>
                            </li>
                            <?if($PregnanciesAndSurrogacies->have_you_ever != 0):?>
                                <li>
                                    <span>Name of IVF Clinic </span>
                                    <span><?=$PregnanciesAndSurrogacies->name_of_IVF;?></span>
                                </li>
                            <? endif;?>

                            <li>
                                <span> Number of Pregnancies </span>
                                <span>
                                      <?if($PregnanciesAndSurrogacies->number_pregnancies == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->number_pregnancies;
                                      ?>
                                </span>
                            </li>
                            <?if($PregnanciesAndSurrogacies->number_pregnancies != 0):?>
                                <li>
                                    <span> How many children have you given birth to? </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->how_many_children == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->how_many_children;
                                      ?>
                                    </span>
                                </li>
                                <li>
                                    <span> How many children do you have now </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->children_now == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->children_now;
                                      ?>
                                    </span>
                                </li>
                                <li>
                                    <span>  Number of Miscarriage  </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->number_miscarriages == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->number_miscarriages;
                                      ?>
                                    </span>
                                </li>
                                <li>
                                    <span>  Number of Abortions  </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->number_abortions == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->number_abortions;
                                      ?>
                                    </span>
                                </li>
                                <li>
                                    <span> Number of Stillbirths  </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->number_stillbirths == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->number_stillbirths;
                                      ?>
                                    </span>
                                </li>
                                <li>
                                    <span>  Number of Live Births </span>
                                    <span>
                                      <?if($PregnanciesAndSurrogacies->number_live_births == 100) echo '10+';
                                      else echo $PregnanciesAndSurrogacies->number_live_births;
                                      ?>
                                    </span>
                                </li>

                                <li>
                                    <span> List each of your delivery dates, whether they were vaginal or caesarean, how many weeks you delivered at, and the weight of each baby </span>
                                    <span><?=$PregnanciesAndSurrogacies->list_each?></span>
                                </li>
                                <li>
                                    <span>If any of your children are deceased, please state cause of death and the age of child at death </span>
                                    <span><?=$PregnanciesAndSurrogacies->if_any_of_your?></span>
                                </li>
                                <li>
                                    <span> Please list any additional reproductive history (Birth control issues, regular cysts, endometriosis, issues getting pregnant, complications during pregnancy etc. </span>
                                    <span><?=$PregnanciesAndSurrogacies->please_list?></span>
                                </li>

                            <? endif;?>

                        </ul>
                    </div>
                    <div id="phh" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Are you in generally good health? </span>
                                <span>
                                     <?if($PersonalHistory->good_health == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->good_health == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->good_health == 0):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->good_health_describe?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Have you ever been told you are infertile or do you have any fertility diagnoses such as Polycystic ovarian syndrome (PCOS), endometriosis, anovulation or irregular periods? </span>
                                <span>
                                     <?if($PersonalHistory->have_you_ever == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->have_you_ever == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->have_you_ever == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->have_you_ever_describe?></span>
                                <? endif;?>
                            </li>


                            <li>
                                <span>Blood Group </span>
                                <span><?=$PersonalHistory->blood_group?></span>
                            </li>
                            <li>
                                <span>Please indicate with a check mark whether you have had any of the following  </span>
                                <span><?=$PersonalHistory->please_indicate?></span>
                            </li>
                            <li>
                                <span>Are you currently taking any medications or herbal supplements (examples include ADD/ADHD medication, allergy medicine, diet pills, etc.)? </span>
                                <span>
                                     <?if($PersonalHistory->you_currently == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->you_currently == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->you_currently == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->you_currently_describe?></span>
                                <? endif;?>
                            </li>
                            <li>
                                <span>Have you taken any medications in the last 5 years that you are no longer taking? </span>
                                <span>
                                     <?if($PersonalHistory->have_you_taken == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->have_you_taken == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Are you currently taking birth control? </span>
                                <span>
                                     <?if($PersonalHistory->birth_control == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->birth_control == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <?if($PersonalHistory->birth_control == 1){?>
                                <li>
                                    <span>What is the method of birth control?</span>
                                    <span><?=$PersonalHistory->method_birth_control?></span>
                                </li>
                            <?}?>

                            <li>
                                <span>If you are currently taking birth control, were you menstrual cycles regular before starting hormonal birth control?  </span>
                                <span>
                                     <?if($PersonalHistory->currently_taking == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->currently_taking == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Do you ever skip menstrual periods? For example, has there ever been a time where you have had no period during a given month? </span>
                                <span>
                                     <?if($PersonalHistory->menstrual_periods == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->menstrual_periods == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever taken Depo-Provera?  </span>
                                <span>
                                     <?if($PersonalHistory->depo_provera == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->depo_provera == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->depo_provera == 1):?>
                                    <span>  Date of last shot below: </span>
                                    <span><?=$PersonalHistory->depo_provera_date?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Have you ever had an endometrial ablation procedure? </span>
                                <span>
                                    <?if($PersonalHistory->ablation_procedure == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->ablation_procedure == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->ablation_procedure == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->ablation_procedure_describe?></span>
                                <? endif;?>
                            </li>


                            <li>
                                <span>Have you ever used recreational drugs (i.e. marijuana, cocaine, etc)? </span>
                                <span>
                                     <?if($PersonalHistory->recreational_drugs == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($PersonalHistory->recreational_drugs == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->recreational_drugs == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->recreational_drugs_describe?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Do you currently use nicotine including smoking cigarettes/use e-cigarettes? </span>
                                <span>
                                     <?if($PersonalHistory->smoking_cigarettes == 1){?>
                                         Casual
                                     <?}?>
                                    <?if($PersonalHistory->smoking_cigarettes == 0){?>
                                        No
                                    <?}?>
                                    <?if($PersonalHistory->smoking_cigarettes == 2){?>
                                        Regular
                                    <?}?>
                                </span>

                            </li>

                            <li>
                                <span>Do you drink alcohol? </span>
                                <span>
                                     <?if($PersonalHistory->drink_alcohol == 1){?>
                                         1-2 drinks per week
                                     <?}?>
                                    <?if($PersonalHistory->drink_alcohol == 0){?>
                                        No
                                    <?}?>
                                    <?if($PersonalHistory->drink_alcohol == 2){?>
                                        2-7 drinks per week
                                    <?}?>
                                    <?if($PersonalHistory->drink_alcohol == 3){?>
                                        NMore than 7 drinks per week
                                    <?}?>
                                </span>
                            </li>



                            <li>
                                <span>Have you ever received treatment for drug/alcohol abuse?</span>
                                <span>
                                    <?if($PersonalHistory->alcohol_abuse == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->alcohol_abuse == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->alcohol_abuse == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->alcohol_abuse_d?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Have any members of your immediate family ever had any issues with drug/alcohol addiction?</span>
                                <span>
                                    <?if($PersonalHistory->alcohol_addiction == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->alcohol_addiction == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->alcohol_addiction == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->alcohol_addiction_d?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>	Have you been the victim of rape and/or physical/sexual abuse?</span>
                                <span>
                                    <?if($PersonalHistory->sexual_abuse == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->sexual_abuse == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->sexual_abuse == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->sexual_abuse_d?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>	Have you had any cancer?</span>
                                <span>
                                    <?if($PersonalHistory->had_any_cancer == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->had_any_cancer == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->had_any_cancer == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->had_any_cancer_d?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Have you ever had a blood transfusion?</span>
                                <span>
                                    <?if($PersonalHistory->blood_transfusion == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->blood_transfusion == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever been refused as a blood donor?</span>
                                <span>
                                    <?if($PersonalHistory->blood_donor == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->blood_donor == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->blood_donor == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->blood_donor_d?></span>
                                <? endif;?>
                            </li>


                            <li>
                                <span>Have you had any hospitalizations and/or surgeries?</span>
                                <span>
                                    <?if($PersonalHistory->hospitalizations == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($PersonalHistory->hospitalizations == 0){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($PersonalHistory->hospitalizations == 1):?>
                                    <span> Describes: </span>
                                    <span><?=$PersonalHistory->hospitalizations_d?></span>
                                <? endif;?>
                            </li>
                        </ul>
                    </div>
                    <div id="mh" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>Have you ever experienced postpartum depression? </span>
                                <span>
                                     <?if($MentalHealth->postpartum_depression == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($MentalHealth->postpartum_depression == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever been diagnosed with an emotional condition or illness? </span>
                                <span>
                                     <?if($MentalHealth->condition_or_illness == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($MentalHealth->condition_or_illness == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever attempted suicide? </span>
                                <span>
                                     <?if($MentalHealth->attempted_suicide == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($MentalHealth->attempted_suicide == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever been treated by a mental health professional? </span>
                                <span>
                                     <?if($MentalHealth->health_professional == 1){?>
                                         Yes
                                     <?}?>
                                    <?if($MentalHealth->health_professional == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                            <?if($MentalHealth->health_professional == 1){?>
                                <li>
                                    <span>Describes </span>
                                    <span><?=$MentalHealth->describe?></span>
                                </li>
                            <?}?>
                        </ul>
                    </div>
                    <div id="sh" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>How many sexual partners have you had in the last 3 years? </span>
                                <span><?=$SexualHistory->many_sexual_partners;?></span>
                            </li>

                            <li>
                                <span>Are you currently sexually active? </span>
                                <span>
                                    <?if($SexualHistory->sexually_active == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($SexualHistory->sexually_active == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div id="pc" class="tab-content">
                        <ul class="adi-info">
                            <li>
                                <span>What do you like most about yourself? </span>
                                <span><?=$userCharacteristic->what_do_you_like_about_yourself;?></span>
                            </li>
                            <li>
                                <span>If you could change one thing about yourself, what would it be? </span>
                                <span><?=$userCharacteristic->if_you_could_change;?></span>
                            </li>
                            <li>
                                <span>What does family mean to you? </span>
                                <span><?=$userCharacteristic->what_does_family_mean;?></span>
                            </li>
                            <li>
                                <span>Message to intended parents (опишите ваши пожелания, вопросы, ожидания от потенциальных родителей) </span>
                                <span><?=$userCharacteristic->message_to_intended_parents;?></span>
                            </li>
                            <li>
                                <span>Why are you interested in being a surrogate and what do you hope to achieve through this process?  </span>
                                <span><?=$userCharacteristic->why_are_you_interested;?></span>
                            </li>

                        </ul>
                    </div>
                    <div id="eao" class="tab-content">
                        <ul class="adi-info">

                            <li>
                                <span>Educational Level </span>
                                <span>
                                    <?if($userEducation->educational_level == 1){?>
                                        Currently in school
                                    <?}?>
                                    <?if($userEducation->educational_level == 2){?>
                                        High School
                                    <?}?>
                                    <?if($userEducation->educational_level == 3){?>
                                        College
                                    <?}?>
                                    <?if($userEducation->educational_level == 4){?>
                                        Bachelor
                                    <?}?>
                                    <?if($userEducation->educational_level == 5){?>
                                        Doctorate
                                    <?}?>
                                </span>
                            </li>
                            <li>
                                <span>Occupation </span>
                                <span><?=$userEducation->occupation;?></span>
                            </li>

                            <li>
                                <span> What is your native language? </span>
                                <span><?=$userEducation->what_is_your_native_language;?></span>
                            </li>

                            <li>
                                <span>Are you currently employed? </span>
                                <span>
                                    <?if($userEducation->are_you_currently_employed == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($userEducation->are_you_currently_employed == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Do you know any language(other than native language)? </span>
                                <span>
                                    <?if($userEducation->do_you_know_any_language == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($userEducation->do_you_know_any_language == 2){?>
                                        No
                                    <?}?>
                                </span>
                                <? if($userEducation->do_you_know_any_language == 1):?>
                                    <span> Foreign Language </span>
                                    <span>
                                        <?if($userEducation->foreign_language == 1) echo 'English';
                                        elseif($userEducation->foreign_language == 2) echo 'Spanish';
                                        elseif($userEducation->foreign_language == 3) echo 'German';
                                        elseif($userEducation->foreign_language == 4) echo 'Italian';
                                        elseif($userEducation->foreign_language == 5) echo 'French';
                                        elseif($userEducation->foreign_language == 6) echo 'Russian';
                                        elseif($userEducation->foreign_language == 7) echo 'Other';
                                        ?>
                                </span>
                                <? endif;?>
                            </li>
                        </ul>
                    </div>
                    <div id="m" class="tab-content">
                        <ul class="adi-info">

                            <li>
                                <span>How did you know about us? </span>
                                <span><?=$Miscellaneous->how_know_about_us;?></span>
                                <? if($Miscellaneous->how_know_about_us == 'Other'):?>
                                    <span>Describes: </span>
                                    <span><?=$Miscellaneous->please_describe?></span>
                                <? endif;?>
                                <? if($Miscellaneous->how_know_about_us == 'Referral'):?>
                                    <span>Describes: </span>
                                    <span><?=$Miscellaneous->if_you_select_referral?></span>
                                <? endif;?>
                            </li>

                            <li>
                                <span>Have you ever been involved in a lawsuit (including personal bankruptcy)? If yes, please fully describe, including the dates, the reason(s), and outcome(s) </span>
                                <span><?=$Miscellaneous->have_ever_been;?></span>
                            </li>

                            <li>
                                <span>Have you or anyone in your household been convicted of a felony? (Please note: screening will involve a criminal background check on all adults in your household.) </span>
                                <span>
                                    <?if($Miscellaneous->have_you_or_anyone == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->have_you_or_anyone == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you been an egg donor before? </span>
                                <span>
                                    <?if($Miscellaneous->have_been_eggdonor == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->have_been_eggdonor == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you ever applied to be an egg donor? </span>
                                <span>
                                    <?if($Miscellaneous->have_ever_applied == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->have_ever_applied == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Are you interested in becoming an egg donor? Or learning more about egg donation?  </span>
                                <span>
                                    <?if($Miscellaneous->are_you_interested == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->are_you_interested == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Would you be willing to selectively reduce in the event of high order multiples (twins, triplets, quads)?  </span>
                                <span>
                                    <?if($Miscellaneous->willing_selectively_reduce == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->willing_selectively_reduce == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Would you be willing to have an amniocentesis and other diagnostic testing to check for birth defects or other problems with the fetus?  </span>
                                <span>
                                    <?if($Miscellaneous->willing_have_amniocentesis == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->willing_have_amniocentesis == 2){?>
                                        No
                                    <?}?>
                                    <?if($Miscellaneous->willing_have_amniocentesis == 3){?>
                                        Yes, but only under the recommendation of the obstetrician
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>If there was a severe medical problem with the fetus and the intended parents wanted you to abort the pregnancy, would you be willing to honour their wishes?  </span>
                                <span>
                                    <?if($Miscellaneous->was_severe_medical == 1){?>
                                        Yes, the decision is up to the intended parents
                                    <?}?>
                                    <?if($Miscellaneous->was_severe_medical == 2){?>
                                        No
                                    <?}?>
                                    <?if($Miscellaneous->was_severe_medical == 3){?>
                                        Yes, but only under the recommendation of the obstetrician
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>If the parents requested, would you be willing to pump, freeze and ship your breast milk for the baby?  </span>
                                <span>
                                    <?if($Miscellaneous->if_parents_requested == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->if_parents_requested == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <li>
                                <span>Have you discussed with your family/friends your desire to become a surrogate? Are they supportive of your decision?  </span>
                                <span><?=$Miscellaneous->have_you_discussed_become_surrogate?></span>
                            </li>

                            <li>
                                <span>If approved as an IFS surrogate, would you be willing to accept a lower fee to help our less well-off Intended Parents with costs associated with this journey?  </span>
                                <span>
                                    <?if($Miscellaneous->if_approved_as_ifs_surrogate == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($Miscellaneous->if_approved_as_ifs_surrogate == 2){?>
                                        No
                                    <?}?>
                                </span>
                            </li>
                        </ul>
                    </div>
                    <div id="fhh" class="tab-content">
                        <ul class="adi-info">

                            <li>
                                <span>How many biologicaly siblings do/did you have?</span>
                                <span><?=$FamilyHealthHistory->did_you_have;?></span>
                            </li>

                            <li>
                                <span>Are you adopted?</span>
                                <span>
                                    <?if($FamilyHealthHistory->you_adopted == 1){?>
                                        Yes
                                    <?}?>
                                    <?if($FamilyHealthHistory->you_adopted == 0){?>
                                        No
                                    <?}?>
                                </span>
                            </li>

                            <? if($FamilyHealthHistory->you_adopted == 0):?>
                                <? foreach ($FamilyMemberInformation as $v):?>
                                    <li>
                                        <span>Relation </span>
                                        <span><?=$v->relation?></span>
                                        <span>Date of Birth </span>
                                        <span><?=$v->date_of_birth?></span>
                                        <span>Living </span>
                                        <span><?=$v->living?></span>
                                        <span>Body Type </span>
                                        <span><?=$v->body_type?></span>
                                        <span>Other Details </span>
                                        <span><?=$v->other_details?></span>
                                    </li>
                                <? endforeach;?>
                            <? endif;?>
                        </ul>
                    </div>
                    <div id="hd" class="tab-content">
                        <ul class="adi-info">
                            <? foreach ($HealthDetails as $v):?>
                                <li>
                                    <span><?=$v->type?> </span>
                                    <span>Relation: <?=$v->relation?></span>
                                    <span>Details: <?=$v->details?></span>
                                </li>
                            <? endforeach;?>
                        </ul>
                    </div>
                    <div id="d" class="tab-content">
                        <ul class="adi-info">

                            <?foreach($UserDocuments as $k_Photo => $photo){?>
                                <li class="photo-uploaded">
                                    <div class="photo-place">
                                        <a href='/backend/web/images/userdocuments/<?=$user->id.'/'.$photo->img?>' target="_blank">
                                            <img src="/frontend/web/images/doc.png" style="width:100px;">
                                        </a>
                                    </div>
                                </li>
                            <?}?>

                        </ul>
                    </div>
                    <div id="vm" class="tab-content">
                        <?if($UserVoiceMessage){?>
                            <ul class="adi-info">
                                <li>
                                    <a href='/backend/web/images/uservoicemessage/<?=$user->id.'/'.$UserVoiceMessage->video?>' target="_blank">
                                        <img src="/frontend/web/images/video_profile.png" style="width:100px;">
                                    </a>
                                </li>
                            </ul>
                        <? }?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Egg donor profile end -->
</main>