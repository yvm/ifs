<div class="profile-add-photo">
    <div class="title">
        <h3><a href="#modal-guidelines-video" class="ths-popup-link">Guidelines</a></h3>
        <h4>Select Video:</h4>
        <p>(support only *.mp4, *.3gp, *.ogg and maximum size: 100 Mb, maximum 1 video)</p>
        <span><?=$error;?></span>
        <?if(!$UserVideo):?>
            <button type="button" name="addVideo" class="uploadImgVideo add" <?if($UserVideo){?>disabled="disabled"<?}?>>Add video</button>
        <? endif;?>
    </div>
</div>
<?if($UserVideo){?>
    <div class="my-profile-video__block">
        <div class="profile-iframe-video">
            <a href='/backend/web/images/uservideo/<?=Yii::$app->user->id.'/'.$UserVideo->video?>' target="_blank">
                <img src="/frontend/web/images/video_profile.png" style="width:100px;">
            </a>
            <button type="button" name="deleteVideo" class="delete_img_profile_video" data-id="<?=$UserVideo->id?>" data-type="UserVideo">
                <img src="/images/delete-img.png">
            </button>
        </div>
    </div>
<?}else{?>
    <div class="photo-place profile-iframe-video">
        <button type="button" name="uploadImg" class="uploadImgVideo">
            <img src="/images/addPlus.png">
        </button>
    </div>
<?}?>
<input type="file" style="display: none;" class="uploadImgInputVideo">