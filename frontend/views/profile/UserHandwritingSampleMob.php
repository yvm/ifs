<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseHandSample" aria-expanded="false" aria-controls="collapseHandSample">
            Handwriting Sample
        </button>
    </h5>
</div>
<div id="collapseHandSample" class="mob-profile_photo collapse show" aria-labelledby="headingHandSample" data-parent="#accordion">
    <div class="title">
        <span><a href="#modal-guidelines-hand" class="ths-popup-link">Guidelines</a></span>
    </div>
    <div class="mob-profile_photo-info">
        <h4>Select File:</h4>
        <p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 1 file)</p>
        <span><?=$error;?></span>
    </div>
    <? if(!$UserHandwritingSample):?>
        <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addHandSample">
            Add File
        </button>
    <? endif;?>

    <div class="mob-add-photos__wrapper">
        <ul>
            <?if($UserHandwritingSample){?>
                <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                    <div class="mob-photo-place">
                        <a href='/backend/web/images/userhandwritingsample/<?=Yii::$app->user->id.'/'.$UserHandwritingSample->video;?>' target="_blank" style="width: 120px;height: 95px;">
                            <img src="/frontend/web/images/doc.png">
                        </a>
                        <div class="del-ed-photo">
                            <button type="button" name="delImg" class="delete_img_profile_HandwritingSample_mob" data-id="<?=$photo->id?>" data-type="UserHandwritingSample">
                                <img src="/images/delete-img.png">
                            </button>
                        </div>
                    </div>
                </li>
            <?}else{?>
                <li>
                    <div class="mob-photo-place">
                        <button type="button" name="uploadImg" class="uploadImgHandwritingSample_mob">
                            <img src="/images/addPlus.png">
                        </button>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
    <input type="file" style="display: none;" class="uploadImgInputHandwritingSample_mob">
</div>