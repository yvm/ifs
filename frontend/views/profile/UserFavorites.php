<?php

use common\models\UserPhoto;

?>
<?=$error?>
<? if(count($userFavorites) != 0):?>
    <ul>
        <? foreach ($userFavorites as $v):?>
            <li >
                <a href="/profile/<?=$v->getRoleName();?>?id=<?=$v->favorite_user_id?>">
                    <div class="favorites-img">
                        <? $userImage = UserPhoto::find()->where('user_id='.$v->favorite_user_id)->all();
                        $photo = $userImage[0]->photo;?>
                        <img src="<?='/'.@backend.'/web/images/userphoto/'.$v->favorite_user_id.'/'.$photo?>">
                        <button type="button" name="delImg" class="delete_favorite_user" data-user-id="<?=$v->favorite_user_id?>">
                            <img src="/images/delete-img.png">
                        </button>
                    </div>
                    <div class="surrogate-id">
                        <p>[<?=$v->user->getShortRoleName();?>-<?=$v->profile->getCreatedDateWithoutDot();?><?=$v->user->getOrderID();?>]</p>
                        <p><?=$v->user->getRoleName();?></p>
                    </div>
                </a>
            </li>
        <? endforeach;?>
    </ul>
<? endif;?>
<? if(count($userFavorites) == 0):?>
    Ваше избранное пусто!
<? endif;?>