<div class="card-header" id="headingThree">
    <h5 class="mb-0">
        <button class="btn btn-link" data-toggle="collapse" data-target="#collapseVoiceMessage" aria-expanded="false" aria-controls="collapseVoiceMessage">
            Voice Message
        </button>
    </h5>
</div>
<div id="collapseVoiceMessage" class="mob-profile_photo collapse show" aria-labelledby="headingDoc" data-parent="#accordion">
    <div class="title">
        <span><a href="#modal-guidelines-voice" class="ths-popup-link">Guidelines</a></span>
    </div>
    <div class="mob-profile_photo-info">
        <h4>Select File:</h4>
        <p>(support only *.mp3  maximum size: 5mb, maximum 1 file)</p>
        <span><?=$error;?></span>
    </div>
    <? if(count($UserDocuments) < 9):?>
        <button type="button" name="button" class="btn-addPhoto" data-toggle="modal"  id="addVoiceMessage">
            Add File
        </button>
    <? endif;?>

    <div class="mob-add-photos__wrapper">
        <ul>
            <?if($UserVoiceMessage){?>
                <li class="mob-uploaded-img" data-id="<?=$photo->id?>">
                    <div class="mob-photo-place">
                        <a href='/backend/web/images/uservoicemessage/<?=Yii::$app->user->id.'/'.$UserVoiceMessage->video?>' target="_blank">
                            <img src="/frontend/web/images/video_profile.png" >
                        </a>
                        <div class="del-ed-photo">
                            <button type="button" name="delImg" class="delete_img_profile_VoiceMessage_mob" data-id="<?=$UserVoiceMessage->id?>" data-type="UserVoiceMessage">
                                <img src="/images/delete-img.png">
                            </button>
                        </div>
                    </div>
                </li>
            <?}else{?>
                <li>
                    <div class="mob-photo-place">
                        <button type="button" name="uploadImg" class="uploadImgVoiceMessage_mob">
                            <img src="/images/addPlus.png">
                        </button>
                    </div>
                </li>
            <? }?>
        </ul>
    </div>
    <input type="file" style="display: none;" class="uploadImgInputVoiceMessage_mob">
</div>