<section class="biotransportation-registr ths-modal-popup" id="biotransportation-registr" style="display: none;">

    <!-- BIOTRANSPARTATION REGISTRATION -->

    <div class="registr-main inner-container">

        <div class="container">
            <?
                use common\models\Countrys;
                use common\models\City;
                use common\models\Region;

                $countries = Countrys::find()->all();
                $region = Region::findAll(['country_id' => 152]);
                $city = City::findAll(['region_id' => 0]);
            ?>
            <form action="">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
				<input type="hidden" value="" name="BiotransportationForm[price]" class="price-biotransportation">
                <div class="modal-close-btn">
                    <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
                </div>
                <div class="row">
                    <div class="col-12">
                        <div class="registr-logo">
                            <img src="/images/logo.svg" alt="logo">
                        </div>
                    </div>
                </div>

                <!-- pickup biomaterial -->
                <div class="pickup-biomaterial biomaterial-content">

                    <div class="row">

                        <div class="col-12">
                            <div class="registr-title">
                                <h1>Where to pick up the biomaterial</h1>
                            </div>
                        </div>

                    </div>

                    <div class="row mt-3 mb-5">

                        <div class="col-lg-6 col-md-6 col-sm-12 col-md-6">
                            <div class="registr-input">
                                <h3>Name of the Organization <span>*</span></h3>
                                <input type="text" name="BiotransportationForm[name_pick_up]">
                            </div>
                        </div>

                        <div class="col-lg-6 col-sm-12 col-md-6">
                            <div class="registr-input registr-calendar">
                                <h3>Biomaterial Transportation Date* </h3>
                                <input type="date" name="BiotransportationForm[date]">
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-12">
                            <div class="registr-min-title">
                                <h2>The address where the biomaterial is stored</h2>
                            </div>
                        </div>

                    </div>


                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="registr-input">
                                <h3>Contact Person <span>*</span></h3>
                                <input type="text" name="BiotransportationForm[contact_person]">
                            </div>

                            <div class="registr-input">
                                <h3>Contact Phone Number <span>*</span></h3>
                                <input type="text" name="BiotransportationForm[phone]">
                            </div>

                            <div class="registr-input">
                                <h3>Adress <span>*</span></h3>
                                <input type="text" name="BiotransportationForm[adress]">
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div class="registr-input">
                                <h3>Country <span>*</span></h3>
                                <?= yii\helpers\Html::dropDownList('BiotransportationForm[country]', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                            </div>

                            <div class="registr-input state" style="display:none;">
                                <h3>State <span>*</span></h3>
                                <?= yii\helpers\Html::dropDownList('BiotransportationForm[state]', '', \yii\helpers\ArrayHelper::map($region, 'id', 'name'), ['prompt' => '']) ?>
                            </div>

                            <div class="registr-input">
                                <h3>City <span>*</span></h3>
                                <?= yii\helpers\Html::dropDownList('BiotransportationForm[city]', '', \yii\helpers\ArrayHelper::map($city, 'id', 'name'), ['prompt' => '']) ?>
                            </div>
                        </div>

                    </div>

                </div>
                <!-- pickup biomaterial end -->


                <!-- deliver biomaterial -->
                <div class="deliver-biomaterial biomaterial-content">
                    <div class="row">

                        <div class="col-12">
                            <div class="registr-min-title">
                                <h2>Where to deliver the biomaterial</h2>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12">

                            <div class="registr-input">
                                <h3>Name of the Organization <span>*</span></h3>
                                <input type="text" name="BiotransportationForm[name_deliver]">
                            </div>

                        </div>

                    </div>

                    <div class="row">

                        <div class="col-12">
                            <div class="registr-min-title">
                                <h2>Where to deliver the biomaterial</h2>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div>
                                <div class="registr-input">
                                    <h3>Contact Person <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[contact_deliver]">
                                </div>

                                <div class="registr-input">
                                    <h3>Contact Phone Number <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[phone_deliver]">
                                </div>

                                <div class="registr-input">
                                    <h3>Adress <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[adress_deliver]">
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div>
                                <div class="registr-input">
                                    <h3>Country <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[country_deliver]', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                                </div>

                                <div class="registr-input state_deliver" style="display:none;">
                                    <h3>State <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[state_deliver]', '', \yii\helpers\ArrayHelper::map($region, 'id', 'name'), ['prompt' => '']) ?>
                                </div>

                                <div class="registr-input">
                                    <h3>City <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[city_deliver]', '', \yii\helpers\ArrayHelper::map($city, 'id', 'name'), ['prompt' => '']) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- deliver biomaterial end -->



                <!-- information about -->
                <div class="information-about biomaterial-content">

                    <div class="row">

                        <div class="col-12">
                            <div class="registr-min-title">
                                <h2>Information about the transportation client</h2>
                            </div>
                        </div>

                    </div>

                    <div class="row">

                        <div class="col-lg-6 col-md-6 col-sm-12">
                            <div>
                                <div class="registr-input">
                                    <h3>First name <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[first_name]">
                                </div>

                                <div class="registr-input">
                                    <h3>Last name <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[last_name]">
                                </div>

                                <div class="registr-input">
                                    <h3>Mobile phone number <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[mobile_phone]">
                                </div>

                                <div class="registr-input">
                                    <h3>Email <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[email]">
                                </div>
                            </div>
                        </div>





                        <div class="col-lg-6 col-md-6 col-sm-12">

                            <div>
                                <div class="registr-input">
                                    <h3>Country <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[country_transportation]', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                                </div>

                                <div class="registr-input state_transportation" style="display:none;">
                                    <h3>State <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[state_transportation]', '', \yii\helpers\ArrayHelper::map($region, 'id', 'name'), ['prompt' => '']) ?>
                                </div>

                                <div class="registr-input">
                                    <h3>City <span>*</span></h3>
                                    <?= yii\helpers\Html::dropDownList('BiotransportationForm[city_transportation]', '', \yii\helpers\ArrayHelper::map($city, 'id', 'name'), ['prompt' => '']) ?>
                                </div>

                                <div class="registr-input">
                                    <h3>Adress <span>*</span></h3>
                                    <input type="text" name="BiotransportationForm[adress_transportation]">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <!-- information about end -->


                <!-- biomaterial information -->
                <div class="biomaterial-information biomaterial-content">

                    <div class="row">

                        <div class="col-12">
                            <div class="registr-min-title">
                                <h2>Biomaterial Information</h2>
                            </div>
                        </div>

                    </div>

                    <div class="information-content">

                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-12 col-md-6">

                                <div class="biomaterial-type">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="information-title">
                                                <h3>Biomaterial Type <span>*</span></h3>
                                            </div>
                                        </div>

                                        <div class="col-8">

                                            <div class="information-item">

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-1" name="BiotransportationForm[biomaterial_type]" value="Sperm">
                                                    <label for="information-checkbox-1">Sperm</label>
                                                </div>

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-2" name="BiotransportationForm[biomaterial_type]" value="Oocytes">
                                                    <label for="information-checkbox-2">Oocytes</label>
                                                </div>

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-3" name="BiotransportationForm[biomaterial_type]" value="Embryos">
                                                    <label for="information-checkbox-3">Embryos</label>
                                                </div>

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-4" name="BiotransportationForm[biomaterial_type]" value="Other">
                                                    <label for="information-checkbox-4">Other</label>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-12 col-md-6">

                                <div class="tube-type">
                                    <div class="row">
                                        <div class="col-4">
                                            <div class="information-title">
                                                <h3>Test tube Type <span>*</span></h3>
                                            </div>
                                        </div>

                                        <div class="col-8">

                                            <div class="information-item">

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-5" name="BiotransportationForm[tube_type]" value="Соломина">
                                                    <label for="information-checkbox-5">Соломина</label>
                                                </div>

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-6" name="BiotransportationForm[tube_type]" value="Cryotop">
                                                    <label for="information-checkbox-6">Cryotop</label>
                                                </div>

                                                <div class="information-input">
                                                    <input type="radio" id="information-checkbox-7" name="BiotransportationForm[tube_type]" value="Cryotube">
                                                    <label for="information-checkbox-7">Cryotube</label>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        <div class="tube-number">
                            <div class="row">

                                <div class="col-lg-6 col-md-6 col-sm-12 col-md-6">

                                    <div class="the-number">
                                        <div class="information-title">
                                            <h3>Tube Number <span>*</span></h3>
                                        </div>

                                        <div class="registr-input">
                                            <select id="" name="BiotransportationForm[tube_number]">
                                                <option value="1">1</option>
                                                <option value="2">2</option>
                                                <option value="3">3</option>
                                                <option value="4">4</option>
                                                <option value="5">5</option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="additional-information">
                                        <div class="information-title">
                                            <h3>Additional Information</h3>
                                        </div>

                                        <div class="information-textarea">
                                            <textarea id="" cols="60" rows="4" name="BiotransportationForm[additional_information]"></textarea>
                                        </div>
                                    </div>

                                </div>


                                <div class="col-lg-6 col-md-6 col-sm-12 col-md-6">

                                    <div class="is-biomaterial-infected">
                                        <div class="row">
                                            <div class="col-5">
                                                <div class="information-title">
                                                    <h3>Is biomaterial infected? <span>*</span></h3>
                                                </div>
                                            </div>

                                            <div class="col-7">

                                                <div class="information-item">
													<div class="information-input">
														<input id="information-checkbox-8" type="radio" name="BiotransportationForm[biomaterial_infected]" value="Yes">
														<label for="information-checkbox-8">Yes</label>
													</div>
													<div class="information-input">
														<input id="information-checkbox-9" type="radio" name="BiotransportationForm[biomaterial_infected]" value="No">
														<label for="information-checkbox-9">No</label>
													</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="errors-biotransportation-registr-form" style="color:red;margin-left:8.333333%;padding-left:12px;"></div>
                    <div class="success-biotransportation-registr-form" style="color:green;margin-left:8.333333%;padding-left:12px;display: none;">Спасибо! Ваша заявка принята.</div>
                    <div class="biomaterial-button">
                        <button class="send_biotransportation_registr">Apply</button>
                    </div>

                    <div class="biomaterial-paragraph">
                        <p>We do not share your personal information with third parties. We do not share your  contact information without your consent.</p>
                    </div>

                </div>
                <!-- end biomaterial information -->
            </form>
        </div>

        <!-- END BIOTRANSPARTATION REGISTRATION -->

    </div></section>