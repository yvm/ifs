<div class="modal-blue-bg ths-modal-popup modal-guidelines" id="modal-guidelines-video">

    <div class="modal-announce">
        <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
        </div>

        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>

        <?=Yii::$app->view->params['guideline_video']->content;?>

        <button>Understand</button>

    </div>

</div>
<script>
    $('body').on('click', '#modal-guidelines-video button', function (e) {
        $('#modal-guidelines-video').hide();
    });
</script>


