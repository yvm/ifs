<div class="modal-blue-bg ths-modal-popup" id="modal-guidelines">

  <form action="">

    <div class="modal-announce">
      <div class="modal-close-btn">
        <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
      </div>

      <div class="modal-logo">
        <img src="/images/logo.png" alt="" title="">
      </div>

      <div class="guidlines-text">
        <h3 class="guidlines-title">Image Guidelines</h3>
        <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body length shot. Additional photos can be from past years. You may also send in photos of you with your family, friends, significant others and etc. </p>
      </div>

      <ul class="guidlines-text guidlines-list">
        <h3 class="guidlines-title">Here are some guidelines and tips to taking and sharing photos for your profile.</h3>
        <li>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the brightest light is behind you causing your face to be in darkness, photos where there are shadows on your face and photos that are blurry.</li>
        <li>Sending photos of yourself with other people is fine as long as you have other photos where you are the focus and the other people in the photo are presentable. If your photo includes other people, please label who is in the photo such as “Me with Husband”.</li>
        <li>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples include photos of you hiking, biking, painting or cooking.</li>
        <li>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high resolution setting are on.</li>
        <li>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting structures behind you, and avoid taking selfies in the car.</li>
      </ul>

      <button type="button" class=" btn-modal modal-close" data-dismiss="modal-announce">Understand</button>

    </div>

  </form>


</div>
