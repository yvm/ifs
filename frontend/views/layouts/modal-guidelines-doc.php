<div class="modal-blue-bg ths-modal-popup modal-guidelines" id="modal-guidelines-doc">

    <div class="modal-announce">
        <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
        </div>

        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>

       <?=Yii::$app->view->params['guideline_doc']->content;?>

        <button>Understand</button>

    </div>

</div>
<script>
    $('body').on('click', '#modal-guidelines-doc button', function (e) {
        $('#modal-guidelines-doc').hide();
    });
</script>


