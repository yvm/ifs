<footer class="footer" id="footer">

    <div class="container">

        <div class="footer-content d-flex">

            <div class="footer-left-mobile">
                <div class="footer-mobile-location footer-mobile-item">
                    <img src="/images/location.svg" alt="location">
                    <span><?=Yii::$app->view->params['contact']->address?></span>
                </div>

                <div class="footer-mobile-message footer-mobile-item">
                    <img src="/images/message.svg" alt="message">
                    <span><?=Yii::$app->view->params['contact']->email?></span>
                </div>

                <div class="footer-mobile-phone footer-mobile-item">
                    <img src="/images/phone.svg" alt="phone">
                    <span><?=Yii::$app->view->params['contact']->mobileTel?></span>
                </div>
            </div>

            <div class="footer-left">
                <span><?=Yii::$app->view->params['contact']->address?></span>
                <span><?=Yii::$app->view->params['contact']->email?></span>
                <span><?=Yii::$app->view->params['contact']->telegramTel?></span>
                <span><?=Yii::$app->view->params['contact']->mobileTel?></span>
            </div>

            <div class="footer-right">
                <div class="footer-right-title">
                    <h3><?=Yii::$app->view->params['translation'][1]->name?></h3>
                </div>

                <div class="footer-right-social">
					<? if(Yii::$app->view->params['contact']->youtube != null):?><a href="<?=Yii::$app->view->params['contact']->youtube?>" target="_blank"><img src="/images/youtube.svg" alt="youtube"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->vk != null):?> <a href="<?=Yii::$app->view->params['contact']->vk?>" target="_blank"><img src="/images/vk.svg" alt="vk"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->ok != null):?> <a href="<?=Yii::$app->view->params['contact']->ok?>" target="_blank"><img src="/images/ok.svg" alt="ok"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->google != null):?><a href="<?=Yii::$app->view->params['contact']->google?>" target="_blank"><img src="/images/google-plus.svg" alt="google-plus"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->telegram != null):?><a href="<?=Yii::$app->view->params['contact']->telegram?>" target="_blank"><img src="/images/telegram.svg" alt="telegram"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->facebook != null):?>  <a href="<?=Yii::$app->view->params['contact']->facebook?>" target="_blank"><img src="/images/facebook.svg" alt="facebook"></a><?endif;?>
					<? if(Yii::$app->view->params['contact']->twitter != null):?>   <a href="<?=Yii::$app->view->params['contact']->twitter?>" target="_blank"><img src="/images/twitter.svg" alt="twitter"></a><?endif;?>
                </div>

                <div class="footer-right-copyright">
                    <p><?=Yii::$app->view->params['translation'][2]->name?></p>
                </div>

                <div class="footer-mobile-copyright">
                    <p><?=Yii::$app->view->params['translation'][3]->name?></p>
                </div>

            </div>
        </div>

    </div>
</footer>

<script>
    function getVK(purl,ptitle,pimg) {

        var url  = 'http://vk.com/share.php?';
        if (purl) {
            url += 'url=' + encodeURIComponent(purl);
        }
        if (ptitle) {
            url += '&title=' + encodeURIComponent(ptitle);
        }
        if (pimg) {
            url += '&image=' + encodeURIComponent(pimg);
        }
        url += '&noparse=true';

        return popup(url)
    }


    function getFacebook(ptitle,text,purl,pimg) {
        var url  = 'http://www.fb.com/sharer.php?s=100';
        if (ptitle) {
            url += '&p[title]='     + encodeURIComponent(ptitle);
        }
        if (text) {
            url += '&p[summary]='   + encodeURIComponent(text);
        }
        if (purl) {
            url += '&p[url]='       + encodeURIComponent(purl);
        }
        if (pimg) {
            url += '&p[images][0]=' + encodeURIComponent(pimg);
        }
        return popup(url)
    }


    function getTwitter(ptitle,purl) {
        var url = "https://twitter.com/intent/tweet?",
            MAX_LEN_TW = 140,
            content = ptitle,
            site_url = purl,
            index = MAX_LEN_TW;
        while ((content + site_url).length > MAX_LEN_TW) {
            index = content.lastIndexOf(' ', index - 1);
            if (index !== -1 && index - 4 - site_url.length <= MAX_LEN_TW) {
                content = content.slice(0, index);
                content += '... ';
            } else if (index === -1) {
                content = '';
            }
        }
        if (purl) {
            url += "original_referer=" + encodeURIComponent(purl);
        }
        if (ptitle) {
            url += "&text=" + encodeURIComponent(content);
        }
        url += "&tw_p=tweetbutton";
        if (purl) {
            url += "&url=" + encodeURIComponent(site_url);
        }
        return popup(url)
    }



    function popup(url) {
        window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
    }

</script>