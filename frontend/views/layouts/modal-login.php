<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
<div class="modal-blue-bg ths-modal-popup login-popup" id="login-popup">
    <form class="modal-announce" action="/site/login">
        <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
          </div>
          <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
          </div>
          <form action="">
            <label for=""><?=Yii::$app->view->params['translationModal'][11]->name;?></label>
            <div class="email-input">
              <input type="text"  name="LoginForm[email]">
            </div>
            <div class="warning-text">
              <p class="error" style="display: none"><img src="/images/warning.png" alt="">Email or password is incorrect. <br>
                Please try again or reset your password </p>
            </div>
            <label for=""><?=Yii::$app->view->params['translationModal'][12]->name;?></label>
            <div class="password-input">
                <input type="password" name="LoginForm[password]" id="password">
                <button type="button" id="toggle"><svg width="22" height="14" viewBox="0 0 22 14" fill="none" xmlns="http://www.w3.org/2000/svg">
<path class="eye-login" d="M21.1878 6.32546C21.0039 6.0739 16.6224 0.166016 11.0264 0.166016C5.4304 0.166016 1.04869 6.0739 0.864997 6.32522C0.690834 6.56386 0.690834 6.88754 0.864997 7.12619C1.04869 7.37775 5.4304 13.2856 11.0264 13.2856C16.6224 13.2856 21.0039 7.37771 21.1878 7.12639C21.3622 6.88779 21.3622 6.56386 21.1878 6.32546ZM11.0264 11.9284C6.90435 11.9284 3.33423 8.00726 2.27739 6.72536C3.33286 5.44234 6.89551 1.52321 11.0264 1.52321C15.1482 1.52321 18.7181 5.44371 19.7754 6.72629C18.7199 8.00927 15.1573 11.9284 11.0264 11.9284Z" fill="#C2C2C2"/>
<path class="eye-login"  d="M11.0287 2.6543C8.7836 2.6543 6.95703 4.48087 6.95703 6.72592C6.95703 8.97097 8.7836 10.7975 11.0287 10.7975C13.2737 10.7975 15.1003 8.97097 15.1003 6.72592C15.1003 4.48087 13.2737 2.6543 11.0287 2.6543ZM11.0287 9.44031C9.53187 9.44031 8.31427 8.22266 8.31427 6.72592C8.31427 5.22918 9.53191 4.01153 11.0287 4.01153C12.5254 4.01153 13.743 5.22918 13.743 6.72592C13.743 8.22266 12.5254 9.44031 11.0287 9.44031Z" fill="#C2C2C2"/>
</svg>
</button>
            </div>
            <div class="forgot-pass">
              <p><a href="#modal-pass-reset" class="ths-popup-link"><?=Yii::$app->view->params['translationModal'][13]->name;?></a></p>
            </div>
            <div class="log-reg-btns">
              <a href="#" class="btn-login"><?=Yii::$app->view->params['translationModal'][14]->name;?></a>
              <a href="#register-email" class="btn-registration ths-popup-link"><?=Yii::$app->view->params['translationModal'][15]->name;?></a>
            </div>
          </form>
    </form>
</div>
<script>
    $('body').on('click', '.btn-login', function (e) {
        $.ajax({
            type: 'POST',
            url: '/site/login',
            data: $(this).closest('form').serialize(),
            success: function (response) {
                if(response == 1){
                    window.location.href = "/profile/index";
                }else{
                    $('.error').show();
                    $('.error').html(response);
                }
            },
            error: function () {
            }
        });
    });

    $('body').on('click', '#toggle', function (e) {
        if($('#password').prop('type') == 'text' ){
            $('#password').attr('type', 'password');
        }else{
            $('#password').attr('type', 'text');
        }
    });
    
</script>