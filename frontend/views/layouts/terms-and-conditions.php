<div class="terms-and-conditions ths-modal-popup" id="terms-and-conditions" style="display: none;">
    <div class="terms-and-conditions-container">
        <div class="row">
            <div class="col-12">
                <div class="terms_header">
                    <div class="terms_logo">
                        <div class="terms_logo_center">
                            <img src="images/logo.png" alt="">
                        </div>
                        <div class="terms_logo_close modal-close">
                            <img src="images/close.png" alt="">
                        </div>
                    </div>
                    <div class="terms_h1">
                        <h1><?=Yii::$app->view->params['translationModal'][60]->name;?></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="terms_scroll">
                    <?$text_site = \common\models\RegistrationText::findOne(['type' => 1])?>
                    <?=$text_site->text?>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="terms_butons">
                    <a href="#registration-main" class="ths-popup-link registration-main-a"><?=Yii::$app->view->params['translationModal'][58]->name;?></a>
                    <a href="#" class="modal-close"><?=Yii::$app->view->params['translationModal'][59]->name;?></a>
                </div>
            </div>
        </div>
    </div>
</div>