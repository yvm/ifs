<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="modal-blue-bg ths-modal-popup" id="modal-pass-reset2">
    <div class="modal-announce">
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>
        <div class="modal-close-btn">
            <a style="cursor: pointer" id="close-modal"><img src="/images/modal-close.png" alt=""></a>
        </div>
        <div class="modal-text  text-center">
            <p><?=Yii::$app->view->params['translationModal'][21]->name;?></p>
        </div>
    </div>
</div>
<script>
    $('body').on('click', '#close-modal', function (e) {
        $('#modal-pass-reset2').hide();
    });
</script>