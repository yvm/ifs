<div class="modal-missing ths-modal-popup" id="modal-missing">

    <form action="">
        <div class="modal-missing__item">
            <div class="modal-close-btn">
                <a href="#" class="modal-close">
                    <img src="/images/modal-close.png" alt="">
                </a>
            </div>

            <div class="modal-logo">
                <img src="/images/logo.png" alt="" title="">
            </div>

            <div class="modal-text  text-center">
                <p><?=Yii::$app->view->params['translationModal'][44]->name;?> </p>
            </div>

            <div class="modal-missing__btn">
                <a href="#login-popup" class="ths-popup-link"><?=Yii::$app->view->params['translationModal'][14]->name;?></a>
                <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['translationModal'][45]->name;?></a>
            </div>

        </div>
    </form>

</div>
