  <div class="registration-form sign-up-popup ths-modal-popup" id="sign-up-popup" <?if($_GET['success'] && $_GET['success']==true){?>style="display:block;"<?}?>>
       <div class="container reg_white_background">
           <form action="/site/signup2">
            <?
                if(Yii::$app->session['success_registration'])
                    $profile = \common\models\Profiles::findOne(Yii::$app->session['success_registration']);
                else
                    $profile = new \common\models\Profiles;
            ?>
            <div class="white_box">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                <div class="row">
                <div class="col-12">
                        <div class="reg_head">
                            <div class="logo_reg_form">
                                <img src="/images/logo.png" alt="">
                            </div>
                            <a href="#" class="reg_cross_close modal-close"><img src="/images/close.png" alt=""></a>
                        </div>
                    </div>
                </div>
                    <div class="row">
                        <div class="col-12">  
                            <div class="Name_email">
                                <div class="reg_mobile_text">
                                  <p><?=Yii::$app->view->params['translationModal'][50]->name;?></p>
                                </div>
                                <div >
                                    <p><?=Yii::$app->view->params['translationModal'][25]->name;?> *</p>
                                    <input type="text" name="Profiles[first_name]" value="<?=$profile->first_name?>">
                                </div>
                                <div >
                                    <p><?=Yii::$app->view->params['translationModal'][26]->name;?> *</p>
                                    <input type="text" name="Profiles[last_name]" value="<?=$profile->last_name?>">
                                </div>
                                <div class="reg_mobile_hide">
                                    <p><?=Yii::$app->view->params['translationModal'][11]->name;?> *</p>
                                    <input type="text" name="Profiles[email]" value="<?=$profile->email?>">
                                    <span class="email_error"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="socials">  
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][30]->name;?></p>
                                    <input type="text" name="Profiles[vatzup]" value="<?=$profile->vatzup?>">
                                </div>
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][31]->name;?></p>
                                    <input type="text" name="Profiles[skype]" value="<?=$profile->skype?>">
                                </div>
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][28]->name;?></p>
                                    <input type="text" name="Profiles[telegram]" value="<?=$profile->telegram?>">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="tel_and_numb">  
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][27]->name;?> *</p>
                                    <input type="text" name="Profiles[mobile]" value="<?=$profile->mobile?>">
                                </div>
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][12]->name;?> *</p>
                                    <input type="password" name="User[password]">
                                    
                                </div>
                                <div>
                                    <p><?=Yii::$app->view->params['translationModal'][51]->name;?> *</p>
                                    <input type="password" name="User[password_repeat]">
                                    <span class="password_error"></span>
                                </div>
                            </div>
                            <div class="pass_note">
                                <p><?=Yii::$app->view->params['translationModal'][52]->name;?></p>
                            </div>
                            <div class="submit">
                                <a href="#" class="registration_submit_button"><?=Yii::$app->view->params['translationModal'][18]->name;?></a>
                            </div>
                        </div>
                    </div>
                <div class="row">
                    <div class="col-12">
                    <p class="offer_sign_up"><?=Yii::$app->view->params['translationModal'][16]->name;?> <a href="#login-popup" class="offer_sign_up_link ths-popup-link"><?=Yii::$app->view->params['translationModal'][17]->name;?></a></p>
                    </div>
                </div>
                <div class="errors2" style="color:red;"></div>
            </form>
            </div>
       </div>
  </div>