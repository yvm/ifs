<?php
use yii\helpers\Html;
use frontend\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	
    <?php $this->head() ?>
</head>
	<script src="../js/jquery-3.2.1.min.js"></script>
<body>   
<?php $this->beginBody() ?>

<!-- preloader -->
<script type="text/javascript">
	$(document).ready(function() {
		$('.mobile-menu').hide();
        $("#transition-loader").fadeOut("slow");
	});
</script>
	

<div class="transition-loader" id="transition-loader">
    <div class="transition-loader-inner">
      <label></label>
      <label></label>
      <label></label>
      <label></label>
      <label></label>
      <label></label>
    </div>
</div>
<!-- end preloader -->

<?=$this->render('_header');?>

<?=$content?>

<?=$this->render('_footer');?>
<?php $this->endBody(); ?>
</body>
	<script type="text/javascript">
$(".slide-one").owlCarousel(
{
  items: 2,
	responsive: {
	0: {
		items:1
	}, 
		768: {
			items:2
		}
	}
}
);
</script>

</html>
<?php $this->endPage() ?>

