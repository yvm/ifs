
<script>
    $('body').on('click', 'input[name="IfsForm[role]"]', function (e) {
        if( $(this).val() != 1){
            $('.interested').hide();
        }else{
            $('.interested').show();
        }
    });
</script>
<div class="modal-blue-bg-lg ths-modal-popup" id="ifs-popup">
    <div class="big-modal">
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>
        <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
        </div>
        <?
            use common\models\Countrys;
            use common\models\City;

            $countries = Countrys::find()->all();
            $city = City::findAll(['region_id' => 0]);
        ?>
        <form action="">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <div class="row">
                <div class="offset-lg-3 col-sm-3">
                    <div class="name-input">
                        <label for=""><?=Yii::$app->view->params['translationModal'][25]->name;?> *</label>
                        <input type="text" name="IfsForm[firstname]">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="name-input">
                        <label for=""><?=Yii::$app->view->params['translationModal'][26]->name;?> *</label>
                        <input type="text" name="IfsForm[lastname]">
                    </div>
                </div>
            </div>
            <div class="radio-box">
                <div class="row">
                    <div class="offset-lg-1 col-sm-1">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][2]->name;?></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][3]->name;?>
                            <input type="radio" name="IfsForm[role]" value="1" checked>
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][4]->name;?>
                            <input type="radio" name="IfsForm[role]" value="2">
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][5]->name;?>
                            <input type="radio" name="IfsForm[role]" value="3">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][6]->name;?>
                            <input type="radio" name="IfsForm[role]" value="4">
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][7]->name;?>
                            <input type="radio" name="IfsForm[role]" value="5">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <div class="name-input interested">
                            <label for=""><?=Yii::$app->view->params['translationModal'][8]->name;?></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="radio-input interested"><?=Yii::$app->view->params['translationModal'][9]->name;?>
                            <input type="checkbox" name="IfsForm[type][]" value="1" checked>
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input interested"><?=Yii::$app->view->params['translationModal'][10]->name;?>
                            <input type="checkbox" name="IfsForm[type][]" value="2">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][27]->name;?> *</label>
                            <input type="text" name="IfsForm[mobile_phone_number]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][11]->name;?> *</label>
                            <input type="text" name="IfsForm[email]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][28]->name;?></label>
                            <input type="text" name="IfsForm[telegram]">
                        </div>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input country_modal">
                            <label for=""><?=Yii::$app->view->params['translationModal'][29]->name;?></label>
                            <?= yii\helpers\Html::dropDownList('IfsForm[country]', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][30]->name;?></label>
                            <input type="text" name="IfsForm[WatsApp]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][31]->name;?></label>
                            <input type="text" name="IfsForm[Skype]">
                        </div>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input country_modal">
                            <label for=""><?=Yii::$app->view->params['translationModal'][32]->name;?></label>
                            <?= yii\helpers\Html::dropDownList('IfsForm[city]', '', \yii\helpers\ArrayHelper::map($city, 'id', 'name'), ['prompt' => '']) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-lg-1 col-sm-10">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][33]->name;?></label>
                            <input type="text" name="IfsForm[subject]">

                            <textarea id="" placeholder="<?=Yii::$app->view->params['translationModal'][34]->name;?>" name="IfsForm[message]"></textarea>
                        </div>
                        <div class="program-items">
                            <div class="attach-file ifs-form">
                                <a href=""><img src="/images/attach-icon.png" alt=""><?=Yii::$app->view->params['translationModal'][35]->name;?></a> <br>
                                <p> <?=Yii::$app->view->params['translationModal'][36]->name;?> </p>
                            </div>
                            <div class="modal-files ifs-form">

                            </div>
                            <input type="file" class="uploadImgInputIfsForm" style="display: none;">
                        </div>
                    </div>
                    <div class="errors-ifs-form" style="color:red;margin-left:8.333333%;padding-left:12px;"></div>
                    <div class="success-ifs-form" style="color:green;margin-left:8.333333%;padding-left:12px;display: none;">
                        <?=Yii::$app->view->params['translationModal'][37]->name;?>
                    </div>
                    <div class="col-sm-12">
                        <a href="" class="btn-modal-lg send_ifs_form"><?=Yii::$app->view->params['translationModal'][38]->name;?></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
