<section class="registration-main ths-modal-popup" id="registration-main" style="display: none;" >
    <div class="registration-main-container">
        <form action="/site/signup4">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <?
                if(Yii::$app->session['success_registration'])
                    $UserAgree = \common\models\UserAgree::findOne(Yii::$app->session['success_registration']);
                else if(Yii::$app->session['success_registration_step3'])
                    $UserAgree = \common\models\UserAgree::findOne(Yii::$app->session['success_registration_step3']);
                else if(Yii::$app->session['success_registration_step2'])
                    $UserAgree = \common\models\UserAgree::findOne(Yii::$app->session['success_registration_step2']);
                else
                    $UserAgree = new \common\models\UserAgree;
            ?>
            <div class="row">
                <div class="col-12">
                    <div class="form_main_logo">
                        <div class="form_main_logo_cl">
                            <div>
                                <img src="images/logo.png" alt="">
                            </div>
                            <div class="modal-close">
                                <img src="images/close.png" alt="">
                            </div>
                        </div>
                        <h2><?=Yii::$app->view->params['translationModal'][53]->name;?></h2>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <?
                        if(Yii::$app->session['success_registration'])
                            $user = \common\models\User::findOne(Yii::$app->session['success_registration']);
                        else if(Yii::$app->session['success_registration_step3'])
                            $user = \common\models\User::findOne(Yii::$app->session['success_registration_step3']);
                        else if(Yii::$app->session['success_registration_step2'])
                            $user = \common\models\User::findOne(Yii::$app->session['success_registration_step2']);
                        $role = '';
                        if($user->role == 1)
                            $role = "Intended Parent";
                        if($user->role == 2)
                            $role = "Surrogate";
                        if($user->role == 3)
                            $role = "Egg Donor";
                        if($user->role == 4)
                            $role = "Surrogate & Egg Donor";
                        if($user->role == 5)
                            $role = "Biotransportation Client";
                    ?>
                    <div class="form_main_header">
                        <h1><?=Yii::$app->view->params['translationModal'][54]->name;?> <?=$role?></h1>
                        <p>
                            <?=Yii::$app->view->params['translationModal'][55]->name;?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="form_main_questions">
                        <?$text_site = \common\models\RegistrationText::findAll(['role' => $user->role, 'type' => 3])?>
                        <?$inc = 1;foreach($text_site as $k => $v){?>
                            <div class="checkbox">
                                <?$agree = 'agree'.$inc;?>
                                <input type="checkbox" name="UserAgree[agree<?=$inc?>]" value="1" <?if($UserAgree->$agree == 1){?>checked<?}?>>
                                <?=$v->text?>
                                <br>
                            </div>
                            <?$inc++;}?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="registration_main_signature">
                        <?
                            if(Yii::$app->session['success_registration'])
                                $profile = \common\models\Profiles::findOne(Yii::$app->session['success_registration']);
                            else if(Yii::$app->session['success_registration_step3'])
                                $profile = \common\models\Profiles::findOne(Yii::$app->session['success_registration_step3']);
                            else if(Yii::$app->session['success_registration_step2'])
                                $profile = \common\models\Profiles::findOne(Yii::$app->session['success_registration_step2']);
                            else
                                $profile = new \common\models\Profiles;
                        ?>
                        <div class="main_signature_inp">
                            <p><?=Yii::$app->view->params['translationModal'][56]->name;?></p>
                            <input type="text" placeholder="Arthur Smith" value="<?=$profile->fio?>" name="Profiles[fio]">
                        </div>
                        <div class="main_signature_inp">
                            <p>Email</p>
                            <input type="text" placeholder="a_smith@gmail" value="<?=$profile->email?>" name="Profiles[email]">
                        </div>
                        <div class="main_signature_inp">
                            <p><?=Yii::$app->view->params['translationModal'][57]->name;?></p>
                            <input type="text" placeholder="24" value="<?=$profile->created_at?>" readonly="readonly" class="created_at">
                        </div>
                    </div>
                    <div class="errors4" style="color:red;"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="registration_main_confirm">
                        <a href="#" class="registration-main-a2"><?=Yii::$app->view->params['translationModal'][58]->name;?></a>
                        <a href="#" class="modal-close"><?=Yii::$app->view->params['translationModal'][59]->name;?></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>