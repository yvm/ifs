<div class="modal-blue-bg">

    <div class="modal-announce">
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>
        <div class="modal-close-btn">
            <a href=""><img src="/images/modal-close.png" alt=""></a>
        </div>
        <div class="modal-text  text-center">
            <p><span> <?=Yii::$app->view->params['translationModal'][46]->name;?></span></p>
            <p> <?=Yii::$app->view->params['translationModal'][47]->name;?></p>
            <p> <?=Yii::$app->view->params['translationModal'][48]->name;?></p>
        </div>
        <a href="" class="btn btn-modal-light"> <?=Yii::$app->view->params['translationModal'][49]->name;?></a>
    </div>

</div>
