<script>
    $('body').on('click', 'input[name="GetAConsultation[role]"]', function (e) {
        if( $(this).val() != 1){
            $('.interested').hide();
        }else{
            $('.interested').show();
        }
    });
</script>
<div class="modal-blue-bg-lg ths-modal-popup" id="get-consult">
    <div class="big-modal">
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>
        <div class="modal-title">
            <p><?=Yii::$app->view->params['translationModal'][39]->name;?></p>
        </div>
        <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
        </div>
        <?
            use common\models\DesiredConsultLocation;
            use common\models\Countrys;
            use common\models\City;

            $countries = Countrys::find()->all();
            $city = City::findAll(['region_id' => 0]);
            $DesiredConsultLocation = DesiredConsultLocation::find()->all();
        ?>
        <form action="/site/modal">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <div class="row">
                <div class="offset-lg-3 col-sm-3">
                    <div class="name-input">
                        <label for=""><?=Yii::$app->view->params['translationModal'][25]->name;?> *</label>
                        <input type="text" name="GetAConsultation[firstname]">
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="name-input">
                        <label for=""><?=Yii::$app->view->params['translationModal'][26]->name;?> *</label>
                        <input type="text" name="GetAConsultation[lastname]">
                    </div>
                </div>
            </div>
            <div class="radio-box">
                <div class="row">
                    <div class="offset-lg-1 col-sm-1">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][2]->name;?></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][3]->name;?>
                            <input type="radio" name="GetAConsultation[role]" value="1" checked>
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][4]->name;?>
                            <input type="radio" name="GetAConsultation[role]" value="2">
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][5]->name;?>
                            <input type="radio" name="GetAConsultation[role]" value="3">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-sm-3">
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][6]->name;?>
                            <input type="radio" name="GetAConsultation[role]" value="4">
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input"><?=Yii::$app->view->params['translationModal'][7]->name;?>
                            <input type="radio" name="GetAConsultation[role]" value="5">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="col-sm-2">
                        <div class="name-input interested">
                            <label for=""><?=Yii::$app->view->params['translationModal'][8]->name;?></label>
                        </div>
                    </div>
                    <div class="col-sm-2">
                        <label class="radio-input interested"><?=Yii::$app->view->params['translationModal'][9]->name;?>
                            <input type="checkbox" name="GetAConsultation[type][]" value="1" checked>
                            <span class="checkmark"></span>
                        </label>
                        <label class="radio-input interested"> <?=Yii::$app->view->params['translationModal'][10]->name;?>
                            <input type="checkbox" name="GetAConsultation[type][]" value="2">
                            <span class="checkmark"></span>
                        </label>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input">
                            <label for="mobile_phone"><?=Yii::$app->view->params['translationModal'][27]->name;?> *</label>
                            <input type="text" name="GetAConsultation[mobile_phone_number]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for="email"><?=Yii::$app->view->params['translationModal'][11]->name;?> *</label>
                            <input type="text" name="GetAConsultation[email]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for="telegram"><?=Yii::$app->view->params['translationModal'][28]->name;?></label>
                            <input type="text" name="GetAConsultation[telegram]">
                        </div>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input country_modal">
                            <label for=""><?=Yii::$app->view->params['translationModal'][29]->name;?></label>
                            <?= yii\helpers\Html::dropDownList('GetAConsultation[country]', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][30]->name;?></label>
                            <input type="text" name="GetAConsultation[WatsApp]">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][31]->name;?></label>
                            <input type="text" name="GetAConsultation[Skype]">
                        </div>
                    </div>
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input country_modal">
                            <label for=""><?=Yii::$app->view->params['translationModal'][32]->name;?></label>
                            <?= yii\helpers\Html::dropDownList('GetAConsultation[city]', '', \yii\helpers\ArrayHelper::map($city, 'id', 'name'), ['prompt' => '']) ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="offset-lg-1 col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][40]->name;?></label>
                            <?= yii\helpers\Html::dropDownList('GetAConsultation[DesiredConsultLocation]', '', \yii\helpers\ArrayHelper::map($DesiredConsultLocation, 'id', 'name'), ['prompt' => '']) ?>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][41]->name;?></label>
                            <input type="date" name="GetAConsultation[data]">
                            <input type="text" name="GetAConsultation[time]" placeholder="<?=Yii::$app->view->params['translationModal'][43]->name;?>">
                            <select name="GetAConsultation[GMT]" id="GMT">
                                <?for($i = 1; $i <= 12; $i++){?>
                                    <option value="GMT +<?=$i?>">GMT +<?=$i?></option>
                                <?}?>
                                <?for($i = -1; $i >= -12; $i--){?>
                                    <option value="GMT <?=$i?>">GMT <?=$i?></option>
                                <?}?>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="name-input">
                            <label for=""><?=Yii::$app->view->params['translationModal'][42]->name;?> </label>
                            <textarea name="GetAConsultation[QuestionsComments]" id="" cols="30" rows="5"></textarea>
                        </div>

                    </div>
                    <div class="errors-get-a-consultation" style="color:red;margin-left:8.333333%;padding-left:12px;"></div>
                    <div class="success-get-a-consultation" style="color:green;margin-left:8.333333%;padding-left:12px;display: none;">
                        <?=Yii::$app->view->params['translationModal'][37]->name;?>
                    </div>
                    <div class="col-sm-12">
                        <a href="" class="btn-modal-lg send_GET_A_CONSULTATION"><?=Yii::$app->view->params['translationModal'][38]->name;?></a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>