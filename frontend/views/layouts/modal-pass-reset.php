<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<div class="modal-blue-bg ths-modal-popup" id="modal-pass-reset">
    <div class="modal-announce">
        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>
		 <div class="modal-close-btn">
            <a style="cursor: pointer" id="close-modal"><img src="/images/modal-close.png" alt=""></a>
        </div>
        <div class="modal-text  text-center">
            <p><?=Yii::$app->view->params['translationModal'][19]->name;?></p>
        </div>
        <form action="/site/updatePasswordByEmail">
            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
            <label for=""><?=Yii::$app->view->params['translationModal'][11]->name;?></label>
            <div class="email-input">
                <input type="email" name="User[email]">
            </div>
            <div class="warning-text">
                <p class="can_not_be_empty" style="display: none"><img src="/images/warning.png" alt=""> Email can not be empty
                </p>
                <p class="email_not_found" style="display: none"><img src="/images/warning.png" alt="">This email address was not found in our records. <br>
                    Please enter another email or <a href="#register-email" class="ths-popup-link"><?=Yii::$app->view->params['translationModal'][24]->name;?></a>
                </p>
                <p class="wrong_email_addrress" style="display: none"><img src="/images/warning.png" alt="">Email is not a valid address
                </p>
                <p class="error" style="display: none"><img src="/images/warning.png" alt=""></p>
            </div>
            <a style="cursor: pointer" class="space-top-sm btn-modal"><?=Yii::$app->view->params['translationModal'][20]->name;?></a>
        </form>
    </div>
</div>

<script>
    $('body').on('click', '.btn-modal', function (e) {

        $.ajax({
            url: '/site/update-password-by-email',
            data: $(this).closest('form').serialize(),
            type: "POST",
            success: function (response) {
                if(response == 1){
                    $('#modal-pass-reset').hide();
                    $('#modal-pass-reset2').show();
                }else if(response == 2){
                    $('.email_not_found').show();
                    $('.wrong_email_addrress').hide();
                    $('.can_not_be_empty').hide();
                    $('.error').hide();
                }else if(response == 3){
                    $('.email_not_found').hide();
                    $('.wrong_email_addrress').show();
                    $('.can_not_be_empty').hide();
                    $('.error').hide();
                }
                else if(response == 4){
                    $('.email_not_found').hide();
                    $('.wrong_email_addrress').hide();
                    $('.can_not_be_empty').show();
                    $('.error').hide();
                }else{
                    $('.error').html(response);
                    $('.error').show();
                    $('.email_not_found').hide();
                    $('.wrong_email_addrress').hide();
                    $('.can_not_be_empty').hide();
                }
            },
            error: function () {
            }
        });
    });
	
	
	 $('body').on('click', '#close-modal', function (e) {
        $('#modal-pass-reset').hide();
    });


</script>