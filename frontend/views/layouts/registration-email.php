<div class="registration-main-main ths-modal-popup" id="register-email">
  <div class="registration-main-main-container">
    <form action="/site/signup">
    <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
      <div class="row">
        <div class="col-12">
          <div class="main_reg">
            <div class="form_main_main_logo_cl">
              <div>
                <img src="/images/logo.png" alt="">
              </div>
              <a href="#" class="modal-close">
                <img src="/images/close.png" alt="">
              </a>
            </div>
            <h2><?=Yii::$app->view->params['translationModal'][0]->name;?></h2>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="i_am_interested_field">
            <div class="your_email_section">
              <div class="y_email_section_1">
                <h3><?=Yii::$app->view->params['translationModal'][1]->name;?> </h3><input type="text" name="SignupForm[email]">
              </div>
              <div class="y_email_section_2" style="color:red;">

              </div>

            </div>
            <div class="i_am_field_section">
              <div class="i_am_field_section_left">
                <p><?=Yii::$app->view->params['translationModal'][2]->name;?></p>
              </div>
              <div class="i_am_field_section_right">
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][3]->name;?></p>
                  <input type="radio" checked="checked" name="SignupForm[type]" value="1">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][4]->name;?></p>
                  <input type="radio" name="SignupForm[type]" value="2">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][5]->name;?></p>
                  <input type="radio" name="SignupForm[type]" value="3">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][6]->name;?></p>
                  <input type="radio" name="SignupForm[type]" value="4">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][7]->name;?></p>
                  <input type="radio" name="SignupForm[type]" value="5">
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
            <div class="interested_in_section_field">
              <div class="interested_in_section_field_left">
                <p><?=Yii::$app->view->params['translationModal'][8]->name;?></p>
              </div>
              <div class="interested_in_section_field_right">
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][9]->name;?></p>
                  <input type="checkbox" checked="checked" name="SignupForm[type1][]" value="1">
                  <span class="checkmark"></span>
                </label>
                <label class="radio-input">
                  <p><?=Yii::$app->view->params['translationModal'][10]->name;?></p>
                  <input type="checkbox" name="SignupForm[type1][]" value="2">
                  <span class="checkmark"></span>
                </label>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-12">
          <div class="registration_button_main_main">
            <a href="#" class="registration_button_main_main_a">
              <p><?=Yii::$app->view->params['translationModal'][18]->name;?></p>
            </a>
          </div>
        </div>
        <div class="col-12">
          <div class="registration_captcha_main_main" style="display:none;">
             <input type="hidden" name="reg_user" value="0">
              <input type="text" name="SignupForm[captcha_repeat]" style="margin-top: -10px;">
              <div class="captcha_div" style="background: #4a5e68;color: #fff;padding:10px;display:block;width:135px;text-align: center">
                  <img src="/images/captcha/<?=$_SESSION['name_capcha']?>.png">
              </div>
              <div class="btn btn-ticked add_sl_per"><?=Yii::t('app', 'Обновить')?></div>
              <div class="y_email_section_4" style="display:none;color:red;">
              </div>
          </div>
        </div>
        <div class="col-12">
          <div class="registration_footer_main_main">
            <p><?=Yii::$app->view->params['translationModal'][16]->name;?><a href="#login-popup" class="ths-popup-link"> Click here to Login</a></p>
          </div>
        </div>
        <div class="errors1" style="color:red;"></div>
      </div>
    </form>
  </div>
</div>