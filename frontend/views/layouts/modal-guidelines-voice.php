<div class="modal-blue-bg ths-modal-popup modal-guidelines" id="modal-guidelines-voice">

    <div class="modal-announce">
        <div class="modal-close-btn">
            <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
        </div>

        <div class="modal-logo">
            <img src="/images/logo.png" alt="" title="">
        </div>

        <?=Yii::$app->view->params['guideline_voice']->content;?>

        <button>Understand</button>

    </div>

</div>
<script>
    $('body').on('click', '#modal-guidelines-voice button', function (e) {
        $('#modal-guidelines-voice').hide();
    });
</script>


