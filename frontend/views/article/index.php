
<section class="blog">
    <div class="container">
        <div class="row">
            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
                ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$main->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$main->bigTitle?></h3>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="bg-white">
                <div class="row">
                    <div class="col-lg-12">
                        <? foreach ($article['data'] as $v):?>
                            <div class="news-item">
                                <a href="/article/view?id=<?=$v->id?>">
                                    <div class="news-img">
                                        <img src="<?=$v->getImage();?>"  class="img-fluid" title="" alt="">
                                    </div>
                                    <div class="news-block">
                                        <div class="news-date">
                                            <div class="news-icon">
                                                <img src="/images/clock-icon.png" alt="">
                                            </div>
                                            <p><?=$v->getDate();?></p>
                                        </div>
                                        <div class="news-title">
                                            <p><?=$v->name;?></p>
                                        </div>
                                        <div class="news-text">
                                            <p><?=\app\controllers\ContentController::cutStr($v->content,700)?></p>
                                        </div>
                                        <div class="tags news-text">
                                            <?=$v->tag;?>
                                        </div>
                                        <div class="share-box">
                                            <div class="news-text space-top-sm">
                                                <p><?=$translation[0]->name?>:</p>
                                            </div>
                                            <div class="share-icons">
                                                <? $url = Yii::$app->request->hostInfo . '/blog/view?id='.$v->id;?>
                                                <a href="#" class="share-link" onclick="getVK('<?=$url?>','<?=$v->name?>','<?=$v->getImage();?>')"><img src="/images/share1.png" alt=""></a>
                                                <a href="#" class="share-link" onclick="getFacebook('<?=$url?>');"><img src="/images/share3.png" alt=""></a>
                                                <a href="#" class="share-link" onclick="getTwitter('<?=$v->name?>','<?=$url?>','<?=$v->getImage();?>')"><img src="/images/share4.png" alt=""></a>
                                              
                                            </div>
                                        </div>
                                    </div>

                                </a>
                            </div>
                        <? endforeach;?>

                        <?= $this->render('/partials/pagination', [
                            'pages'=>$article['pagination'],
                        ]);?>
                    </div>
                </div>
            </div>

        </div>


    </div>



    </div>
</section>

