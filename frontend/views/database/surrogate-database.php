<!-- DONOR DATABASE -->
<section class="egg-donor-database" id="egg-donor-database">

    <div class="container">

        <div class="donor-database-main">
            <div class="container">

                <!-- DONOR DATABASE BUTTON -->
                <div class="row">
                    <?= $this->render('/partials/buttons',
                        ['class'=>'btn inner-btn ths-popup-link'
                        ]);?>
                </div>
                <!-- END DONOR DATABASE BUTTON -->


                <!-- DONOR DATABASE TITLE -->
                <div class="ifs-title">
                    <div class="row">
                        <div class="col-12">
                            <h3><?=$surDatabase['main']->littleTitle?></h3>
                        </div>

                        <div class="col-12">
                            <h1><?=$surDatabase['main']->bigTitle?> </h1>
                        </div>

                        <div class="col-12">
                            <h2><?=$surDatabase['main']->slogan?></h2>
                        </div>
                    </div>

                    <div class="ifs-main-btn">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                <div class="ifs-main-btn-item">
                                    <a class="ths-popup-link"
                                       href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                <div class="ifs-main-btn-item">
                                    <a class="ths-popup-link"
                                       href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END DONOR DATABASE TITLE -->
            </div>
        </div>
    </div>

    <?php
    use yii\widgets\ActiveForm;
    ?>
    <div class="donor-database-content inner-container">
        <div class="donor-database-title">
            <h2>Please enter the Surrogate ID Number or key words</h2>

            <div class="donor-database-search">
                <form action="/database/surrogate-database-search">
                    <input type="text" name="search">
                    <button><img src="/images/search.svg" alt="search"></button>
                </form>
            </div>

            <h4 class="donor-database-search__title">Search Criteria</h4>

        </div>
        <?php $form = ActiveForm::begin(['action' => '/database/surrogate-database']); ?>
        <div class="personal-wrap d-flex">
            <!-- PERSONAL LEFT -->
            <div class="personal-left">
                <div class="personal-title">
                    <h2>Personal Characteristics</h2>
                </div>

                <div class="personal-left-accordion-wrap">
                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Age: </h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="Ot" id="">
                                    <? for($i=$age->from;$i<=$age->to;$i++):?>
                                        <option value="<?=$i?>">from <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>

                            <div class="select-item">
                                <? $selected = '';?>
                                <select name="Do" id="">
                                    <? for($i=$age->from;$i<=$age->to;$i++):?>
                                        <? if($i == $age->to):?>
                                            <? $selected = 'selected';?>
                                        <? endif;?>
                                        <option value="<?=$i?>" <?=$selected;?>>to <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="personal-checkbox personal-characteristics d-flex">
                        <div class="checkbox-title personal-all-title">
                            <h3>Marital Status</h3>
                        </div>

                        <div class="checkbox-wrap d-flex">
                            <div class="checkbox-item">
                                <input type="radio" id="personal-checkbox-1" name="marital" value="1">
                                <label for="personal-checkbox-1">Married</label>
                            </div>

                            <div class="checkbox-item">
                                <input type="radio" id="personal-checkbox-2" name="marital" value="2">
                                <label for="personal-checkbox-2">Single</label>
                            </div>
                        </div>
                    </div>


                    <div class="personal-select personal-characteristics d-flex">
                        <div class="checkbox-title personal-all-title">
                            <h3>Location</h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <?= yii\helpers\Html::dropDownList('country', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                            </div>
                        </div>

                    </div>


                </div>


            </div>

            <!-- END PERSONAL LEFT -->

            <!-- PERSONAL RIGHT -->
            <div class="personal-right">

                <div class="personal-right-accordion">
                    <div class="personal-title">
                        <h2>Advanced criteria</h2>
                    </div>

                    <div class="personal-right-accordion-wrap">

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Available to Begin</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-3" value="1">
                                    <label for="personal-checkbox-3">Available</label>
                                </div>

                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-4" value="2">
                                    <label for="personal-checkbox-4">Not Available</label>
                                </div>

                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-5" value="3">
                                    <label for="personal-checkbox-5">Currently Family Helping</label>
                                </div>
                            </div>
                        </div>


                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Previous Surrogate</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" name="previous_surrogate" value="1">
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>My favourites</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" name="my_favourites">
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>New and updated surrogates from the last 15 days</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" id="personal-checkbox-1" name="new">
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Video</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" id="personal-checkbox-1" name="video">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="personal-button">
                <a href="#" class="search_surrogates">Search</a>
            </div>

            <div class="personal-link">
                <a href="#" class="ClearSearches">Clear Searches</a>
            </div>
            <?php ActiveForm::end(); ?>
            <!-- END PERSONAL RIGHT -->
        </div>

        <div class="personal-buttons d-flex">
            <div class="personal-buttons-item">
                <a href="#" class="My_Favorites">My Favorites</a>
            </div>

            <div class="personal-buttons-item">
                <a href="#" class="New_and_Updated">New and Updated</a>
            </div>

            <div class="personal-buttons-item">
                <a href="#" class="View_All_Surrogates">View All Surrogates</a>
            </div>
        </div>

        <div class="search_result">
            <div class="database-finded">
                <?if($model['data']){?>
                    <span>Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов</span>
                <?}else{?>
                    <span>Sorry, no results were found.<br />Try refining your search.</span>
                <?}?>
            </div>

            <div class="database-pag-status">
                <div class="database-status">
                    <span class="database-circle"></span>
                    <span> - </span>
                    <span>Свободна</span>
                </div>

                <?if($model['data']):?>
                    <?= $this->render('/partials/database_pagination', [
                        'pages'=>$model['pagination'],
                    ]);?>
                <? endif;?>

                <div class="empty-block"></div>
            </div>


            <div class="database-profile-wrap">
                <script>
                    <?if ($model['data']) {?>
                    $('.database-finded').html('Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов');
                    <?} else {?>
                    $('.database-finded').html('Sorry, no results were found.\n' +
                        '<br/>Try refining your search.\n');
                    <?}?>
                </script>
                <?if($model['data'])foreach($model['data'] as $v){?>
                    <div class="database-profile">
                        <div class="profile-circle">
                            <span></span>
                        </div>

                        <?if(Yii::$app->view->params['admission_surrogate']->photo):?>
                            <div class="profile-image">
                                <img class="offed-image" src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                                     style="width: 100%;">
                            </div>
                        <? endif;?>
                        <?if(!Yii::$app->view->params['admission_surrogate']->photo):?>
                            <div class="profile-image">
                                <img class="offed-image passive-profile" src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                                     style="width: 100%;">
                            </div>
                        <? endif;?>


                        <div class="database-profi">
                            <?if($v->profi){?>
                                <h3 title="Previous Surrogate">Profi</h3>
                            <?}else{?>
                                <?if($v->miscellaneous->have_been_eggdonor == 1){?>
                                    <h3 title="Previous Surrogate">Profi</h3>
                                <?}?>
                            <?}?>
                            <?if($v->proven){?>
                                <img src="/images/union.svg" alt="union" title="Proven">
                            <?}?>
                        </div>

                        <div class="database-surrogate">
                            <h3><?=$v->getShortRoleName();?>-<?=$v->profile->getCreatedDateWithoutDot();?><?=$v->getOrderID();?>
                            </h3>
                            <span><?=$v->getRoleName();?></span>
                        </div>

                        <div class="database-avilible">
                        <span style="font-family: roboto; color: #82EF5B;">
                            <? if($v->active == 1):?> <?='Available'?>
                            <? endif;?>
                            <? if($v->active == 2):?> <?='Not Available'?>
                            <?endif;?>
                            <? if($v->active == 3):?> <?='Currently Family Helping'?>
                            <?endif;?>
                        </span>

                        </div>



                        <div class="database-location">
                            <div class="location-image">
                                <img src="/images/location.svg" alt="location">
                                <h3><?=$v->personal_info->getCountryName();?></h3>
                            </div>

                            <div class="location-item">
                                <?
                                $birthDate = $v->personal_info->date_of_birth;
                                $birthDate = explode("-", $birthDate);
                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                    ? ((date("Y") - $birthDate[0]) - 1)
                                    : (date("Y") - $birthDate[0]));
                                ?>
                                <p><?=$age?> года</p>
                                <p>
                                    <?
                                    if($v->personal_info->marital_status == 1)
                                        echo 'Married';
                                    else
                                        echo 'Single';
                                    ?>
                                </p>
                            </div>
                        </div>

                        <? if(Yii::$app->view->params['admission_surrogate']->compensation):?>
                            <? if($v->status_comp):?>
                                <div class="database-compensation">
                                    <img src="/images/database-compensation.svg" alt="compensation">
                                    <p>Компенсация</p>
                                    <h3><?=$v->miscellaneous->desired_compensation;?> <?=$v->miscellaneous->other_compensation_currency==null ? $v->miscellaneous->compensation_currency:$v->miscellaneous->other_compensation_currency;?>
                                    </h3>
                                </div>
                            <? endif;?>
                        <? endif;?>

                        <div class="database-photo-video">
                            <div class="empty-item" title="Add to Favorites" style="cursor:pointer;" id="add-user-to-fav"
                                 data-user-id="<?=$v->id?>" data-user="<?=Yii::$app->user->identity->id?>">
                                <div class="measurement-item<?=$v->id;?>">
                                    <? if(!Yii::$app->user->isGuest):?>
                                        <? if(in_array($v->id,$fav)):?><img src="/images/heart-fill.svg" alt="heart">
                                        <? endif;?>
                                        <? if(!in_array($v->id,$fav)):?><img src="/images/heart.svg" alt="heart">
                                        <? endif;?>
                                    <? endif;?>

                                    <? if(Yii::$app->user->isGuest):?>
                                        <img src="/images/heart.svg" alt="heart">
                                    <? endif;?>
                                </div>
                            </div>

                            <?if(Yii::$app->view->params['admission_surrogate']->photo):?>
                                <div class="photo-item" title="View more Photos">
                                    <a href="#">
                                        <img src="/images/database-img.svg" alt="img">
                                    </a>
                                    <div class="aniimated-thumbnials">
                                        <? foreach ($v->photos as $photo):?>
                                            <a href="/backend/web/images/userphoto/<?=$v->id?>/<?=$photo->photo?>"></a>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            <? endif?>

                            <?if(!Yii::$app->view->params['admission_surrogate']->photo):?>
                                <div class="photo-item grey-area" title="View more Photos">
                                    <a >
                                        <img src="/images/database-img.svg" alt="img">
                                    </a>
                                </div>
                            <? endif?>

                            <?if(Yii::$app->view->params['admission_surrogate']->video):?>
                                <?if($v->video->video != null){ $class = '';}?>
                                <?if($v->video->video == null){ $class = 'grey-area';}?>
                            <? endif?>
                            <?if(!Yii::$app->view->params['admission_surrogate']->video){ $class = 'grey-area';}?>

                            <div class="video-item <?=$class;?>" title="View Video">
                                <? if($class=='grey-area'):?>
                                    <button type="button" class="btn btn-primary button-movie">
                                        <img src="/images/database-video.svg" alt="img">
                                    </button>
                                <? endif;?>

                                <? if($class==''):?>
                                <button type="button" class="btn btn-primary button-movie" data-toggle="modal"
                                        data-target="#exampleModal<?=$v->id;?>">
                                    <img src="/images/database-video.svg" alt="img">
                                </button>
                                <? endif;?>
                                <!-- Modal -->
                                  <div class="modal fade" id="exampleModal<?=$v->id;?>" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
									

									
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
												
                                               
												<video id="video-player"controls name="media" autostart="false"  height="100%" width="100%" >
												<source src="<?='/backend/web/images/uservideo/'.$v->id.'/'.$v->video->video?>" type="video/mp4">
												</video>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
<script type="text/javascript">
$(document).ready(function(){
$('#exampleModal<?=$v->id;?>').on('hidden.bs.modal', function () {


 var $video = jQuery('video', jQuery(this));
        $video[0].pause();


});
	$('#exampleModal<?=$v->id;?>').on('shown.bs.modal', function () {


 var $video = jQuery('video', jQuery(this));
        $video[0].play();


});
});
</script>
                                </div>
                            </div>
                        </div>

                        <?if($v->role == 2){ $url = 'surrogate';}?>
                        <?if($v->role == 4){ $url = 'surrogate-and-egg-donor';}?>
                        <div class="database-button">
                            <a href="/profile/<?=$url;?>?id=<?=$v->id?>">Полный профиль</a>
                        </div>
                    </div>
                <?}?>

            </div>

            <?if($model['data']):?>
                <?= $this->render('/partials/database_pagination', [
                    'pages'=>$model['pagination'],
                ]);?>
            <? endif;?>

        </div>
</section>
<!-- END DONOR DATABASE -->