<div class="database-finded">
    <?if($model['data']){?>
        <span>Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов</span>
    <?}else{?>
        <span>Sorry, no results were found.<br />Try refining your search.</span>
    <?}?>
</div>

<div class="database-pag-status">
    <div class="database-status">
        <span class="database-circle"></span>
        <span> - </span>
        <span>Свободна</span>
    </div>

    <?if($model['data']):?>
        <?= $this->render('/partials/database_pagination', [
            'pages'=>$model['pagination'],
        ]);?>
    <? endif;?>

    <div class="empty-block"></div>
</div>


<div class="database-profile-wrap">
    <script>
        <?if($model['data']){?>
        $('.database-finded').html('Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов');
        <?}else{?>
        $('.database-finded').html('Sorry, no results were found.\n' +
            '<br/>Try refining your search.\n');
        <?}?>
    </script>
    <?if($model['data'])foreach($model['data'] as $v){?>
        <div class="database-profile">
            <div class="profile-circle">
                <span></span>
            </div>

            <?if(Yii::$app->view->params['admission_donor']->photo):?>
                <div class="profile-image">
                    <img src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                         style="width: 100%;">
                </div>
            <? endif;?>
            <?if(!Yii::$app->view->params['admission_donor']->photo):?>
                <div class="profile-image">
                    <img class="passive-profile" src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                         style="width: 100%;">
                </div>
            <? endif;?>


            <div class="database-profi">
                <?if($v->profi){?>
                    <h3 title="Previous Surrogate">Profi</h3>
                <?}else{?>
                    <?if($v->miscellaneous->have_been_eggdonor == 1){?>
                        <h3 title="Previous Surrogate">Profi</h3>
                    <?}?>
                <?}?>
                <?if($v->proven){?>
                    <img src="/images/union.svg" alt="union" title="Proven">
                <?}?>
            </div>

            <div class="database-surrogate">
                <h3><?=$v->getShortRoleName();?>-<?=$v->profile->getCreatedDateWithoutDot();?><?=$v->getOrderID();?></h3>
                <span><?=$v->getRoleName();?></span>
            </div>

            <div class="database-avilible">
                        <span style="font-family: roboto; color: #82EF5B;">
                            <? if($v->active == 1):?> <?='Available'?><? endif;?>
                            <? if($v->active == 2):?> <?='Not Available'?><?endif;?>
                            <? if($v->active == 3):?> <?='Currently Family Helping'?><?endif;?>
                        </span>
            </div>

            <div class="database-location">
                <div class="location-image">
                    <img src="/images/location.svg" alt="location">
                    <h3><?=$v->personal_info->getCountryName();?></h3>
                </div>

                <div class="location-item">
                    <?
                    $birthDate = $v->personal_info->date_of_birth;
                    $birthDate = explode("-", $birthDate);
                    $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                        ? ((date("Y") - $birthDate[0]) - 1)
                        : (date("Y") - $birthDate[0]));
                    ?>
                    <p><?=$age?> года</p>
                    <p>
                        <?
                        if($v->personal_info->marital_status == 1)
                            echo 'Married';
                        else
                            echo 'Single';
                        ?>
                    </p>

                </div>


            </div>

            <? if(Yii::$app->view->params['admission_donor']->compensation):?>
                <? if($v->status_comp):?>
                    <div class="database-compensation">
                        <img src="/images/database-compensation.svg" alt="compensation">
                        <p>Компенсация</p>
                        <h3><?=$v->miscellaneous->desired_compensation;?> <?=$v->miscellaneous->other_compensation_currency==null ? $v->miscellaneous->compensation_currency:$v->miscellaneous->other_compensation_currency;?>
                        </h3>
                    </div>
                <? endif;?>
            <? endif;?>

            <div class="database-measurement">

                <div class="measurement-item">
                    <p><?=$v->personal_info->height;?>sm</p>
                    <p><?=$v->personal_info->weight;?>kg</p>
                    <p>
                        <?if($v->personal_info->eye_color == 1){ echo "Blue";}?>
                        <?if($v->personal_info->eye_color == 2){ echo "Gray";}?>
                        <?if($v->personal_info->eye_color == 3){ echo "Green";}?>
                        <?if($v->personal_info->eye_color == 4){ echo "Amber";}?>
                        <?if($v->personal_info->eye_color == 5){ echo "Olive";}?>
                        <?if($v->personal_info->eye_color == 6){ echo "Brown";}?>
                        <?if($v->personal_info->eye_color == 7){ echo "Black";}?>
                        <?if($v->personal_info->eye_color == 8){ echo "Yellow";}?>
                    </p>
                    <p>
                        <?if($v->personal_info->natural_hair_color_now == 1){ echo "Black";}?>
                        <?if($v->personal_info->natural_hair_color_now == 2){ echo "Brown";}?>
                        <?if($v->personal_info->natural_hair_color_now == 3){ echo "Redhead";}?>
                        <?if($v->personal_info->natural_hair_color_now == 4){ echo "Blond";}?>
                        <?if($v->personal_info->natural_hair_color_now == 5){ echo "Light Blond";}?>
                    </p>
                </div>

            </div>

            <div class="database-photo-video">
                <div class="empty-item" title="Add to Favorites" style="cursor:pointer;"  id="add-user-to-fav" data-user-id="<?=$v->id?>" data-user="<?=Yii::$app->user->identity->id?>">
                    <div class="measurement-item<?=$v->id;?>">
                        <? if(!Yii::$app->user->isGuest):?>
                            <? if(in_array($v->id,$fav)):?><img src="/images/heart-fill.svg" alt="heart" ><? endif;?>
                            <? if(!in_array($v->id,$fav)):?><img src="/images/heart.svg" alt="heart"> <? endif;?>
                        <? endif;?>

                        <? if(Yii::$app->user->isGuest):?>
                            <img src="/images/heart.svg" alt="heart" >
                        <? endif;?>
                    </div>
                </div>

                <?if(Yii::$app->view->params['admission_donor']->photo):?>
                    <div class="photo-item" title="View more Photos">
                        <a href="#">
                            <img src="/images/database-img.svg" alt="img">
                        </a>
                        <div class="aniimated-thumbnials">
                            <? foreach ($v->photos as $photo):?>
                                <a href="/backend/web/images/userphoto/<?=$v->id?>/<?=$photo->photo?>"></a>
                            <?endforeach;?>
                        </div>
                    </div>
                <? endif?>

                <?if(!Yii::$app->view->params['admission_donor']->photo):?>
                    <div class="photo-item grey-area" title="View more Photos">
                        <a >
                            <img src="/images/database-img.svg" alt="img">
                        </a>
                    </div>
                <? endif?>

                <?if(Yii::$app->view->params['admission_donor']->video):?>
                    <?if($v->video->video != null){ $class = '';}?>
                    <?if($v->video->video == null){ $class = 'grey-area';}?>
                <? endif?>
                <?if(!Yii::$app->view->params['admission_donor']->video){ $class = 'grey-area';}?>

                <div class="video-item <?=$class;?>" title="View Video">
                    <? if($class=='grey-area'):?>
                        <button type="button" class="btn btn-primary button-movie">
                            <img src="/images/database-video.svg" alt="img">
                        </button>
                    <? endif;?>

                    <? if($class==''):?>
                        <button type="button" class="btn btn-primary button-movie" data-toggle="modal"
                                data-target="#exampleModal<?=$v->id;?>">
                            <img src="/images/database-video.svg" alt="img">
                        </button>
                    <? endif;?>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModal<?=$v->id;?>" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">



                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">


                                    <video id="video-player"controls name="media" autostart="false"  height="100%" width="100%" >
                                        <source src="<?='/backend/web/images/uservideo/'.$v->id.'/'.$v->video->video?>" type="video/mp4">
                                    </video>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                        <script type="text/javascript">
                            $(document).ready(function(){
                                $('#exampleModal<?=$v->id;?>').on('hidden.bs.modal', function () {


                                    var $video = jQuery('video', jQuery(this));
                                    $video[0].pause();


                                });
                                $('#exampleModal<?=$v->id;?>').on('shown.bs.modal', function () {


                                    var $video = jQuery('video', jQuery(this));
                                    $video[0].play();


                                });
                            });
                        </script>
                    </div>
                </div>
            </div>

            <?if($v->role == 3){ $url = 'egg-donor';}?>
            <?if($v->role == 4){ $url = 'surrogate-and-egg-donor';}?>
            <div class="database-button">
                <a href="/profile/<?=$url;?>?id=<?=$v->id?>">Полный профиль</a>
            </div>
        </div>
    <?}?>

</div>

<?if($model['data']):?>
    <?= $this->render('/partials/database_pagination', [
        'pages'=>$model['pagination'],
    ]);?>
<? endif;?>
