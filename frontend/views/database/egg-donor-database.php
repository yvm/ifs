
<!-- DONOR DATABASE -->
<section class="egg-donor-database" id="egg-donor-database">

    <div class="container">

        <div class="donor-database-main">
            <div class="container">

                <!-- DONOR DATABASE BUTTON -->
                <div class="row">
                    <?= $this->render('/partials/buttons',
                        ['class'=>'btn inner-btn ths-popup-link'
                        ]);?>
                </div>
                <!-- END DONOR DATABASE BUTTON -->


                <!-- DONOR DATABASE TITLE -->
                <div class="ifs-title">
                    <div class="row">
                        <div class="col-12">
                            <h3><?=$surDatabase['main']->littleTitle?></h3>
                        </div>

                        <div class="col-12">
                            <h1><?=$surDatabase['main']->bigTitle?>  </h1>
                        </div>

                        <div class="col-12">
                            <h2><?=$surDatabase['main']->slogan?></h2>
                        </div>
                    </div>

                    <div class="ifs-main-btn">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                <div class="ifs-main-btn-item">
                                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6 col-6">
                                <div class="ifs-main-btn-item">
                                    <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- END DONOR DATABASE TITLE -->
            </div>
        </div>
    </div>

    <?php
    use yii\widgets\ActiveForm;
    ?>
    <div class="donor-database-content inner-container">
        <div class="donor-database-title">
            <h2>Please enter the Surrogate ID Number or key words</h2>

            <div class="donor-database-search">
                <form action="/database/egg-donor-database-search">
                    <input type="text" name="search">
                    <button><img src="/images/search.svg" alt="search"></button>
                </form>
            </div>

            <h4 class="donor-database-search__title">Search Criteria</h4>

        </div>
        <?php $form = ActiveForm::begin(['action' => '/database/surrogate-database']); ?>
        <div class="personal-wrap d-flex">
            <!-- PERSONAL LEFT -->
            <div class="personal-left">

                <div class="personal-title">
                    <h2>Physical Characteristics  </h2>
                </div>

                <div class="personal-left-accordion-wrap">
                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Age</h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <div class="select-item">
                                    <select name="Ot" id="">
                                        <? for($i=$age->from;$i<=$age->to;$i++):?>
                                            <option value="<?=$i?>">От <?=$i?></option>
                                        <? endfor;?>
                                    </select>
                                </div>

                                <div class="select-item">
                                    <? $selected = '';?>
                                    <select name="Do" id="">
                                        <? for($i=$age->from;$i<=$age->to;$i++):?>
                                            <? if($i == $age->to):?>  <? $selected = 'selected';?><? endif;?>
                                            <option value="<?=$i?>" <?=$selected;?>>До <?=$i?></option>
                                        <? endfor;?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Height (sm)</h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="height_Ot" id="">
                                    <? for($i=$height->from;$i<=$height->to;$i++):?>
                                        <option value="<?=$i?>">От <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>

                            <div class="select-item">
                                <? $selected = '';?>
                                <select name="height_Do" id="">
                                    <? for($i=$height->from;$i<=$height->to;$i++):?>
                                        <? if($i == $height->to):?>  <? $selected = 'selected';?><? endif;?>
                                        <option value="<?=$i?>" <?=$selected;?>>До <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Weight (kg)</h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="weight_Ot" id="">
                                    <? for($i=$weight->from;$i<=$weight->to;$i++):?>
                                        <option value="<?=$i?>">От <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>

                            <div class="select-item">
                                <? $selected = '';?>
                                <select name="weight_Do" id="">
                                    <? for($i=$weight->from;$i<=$weight->to;$i++):?>
                                        <? if($i == $weight->to):?>  <? $selected = 'selected';?><? endif;?>
                                        <option value="<?=$i?>" <?=$selected;?>>До <?=$i?></option>
                                    <? endfor;?>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Eye Color </h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="eye_color" id="">
                                    <option></option>
                                    <option value="1">Blue</option>
                                    <option value="2">Gray</option>
                                    <option value="3">Green</option>
                                    <option value="4">Amber</option>
                                    <option value="5">Olive</option>
                                    <option value="6">Brown</option>
                                    <option value="7">Black</option>
                                    <option value="8">Yellow</option>
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Hair Color </h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="hair_color" id="">
                                    <option></option>
                                    <option value="1">Black</option>
                                    <option value="2">Brown</option>
                                    <option value="3">Redhead</option>
                                    <option value="4">Blond</option>
                                    <option value="5">Light blond </option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>


                <div class="personal-title">
                    <h2>Personal Criteria </h2>
                </div>

                <div class="personal-left-accordion-wrap">
                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Ethnic Origin </h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="ethnic" id="">
                                    <option></option>
                                    <option value="1" <?if($EthnicOrigin->ethnicA == '1') echo 'selected';?>>Albanian</option>
                                    <option value="2" <?if($EthnicOrigin->ethnicA == '2') echo 'selected';?>>American Indian</option>
                                    <option value="3" <?if($EthnicOrigin->ethnicA == '3') echo 'selected';?>>Argentinean</option>
                                    <option value="4" <?if($EthnicOrigin->ethnicA == '4') echo 'selected';?>>Armenian</option>
                                    <option value="5" <?if($EthnicOrigin->ethnicA == '5') echo 'selected';?>>Austrian</option>
                                    <option value="6" <?if($EthnicOrigin->ethnicA == '6') echo 'selected';?>>Azerbaijani</option>
                                    <option value="7" <?if($EthnicOrigin->ethnicA == '7') echo 'selected';?>>Bahamian</option>
                                    <option value="8" <?if($EthnicOrigin->ethnicA == '8') echo 'selected';?>>Bajan</option>
                                    <option value="9" <?if($EthnicOrigin->ethnicA == '9') echo 'selected';?>>Bangladeshi</option>
                                    <option value="10" <?if($EthnicOrigin->ethnicA == '10') echo 'selected';?>>Bashkir</option>
                                    <option value="11" <?if($EthnicOrigin->ethnicA == '11') echo 'selected';?>>Belgian</option>
                                    <option value="12" <?if($EthnicOrigin->ethnicA == '12') echo 'selected';?>>Belarusian</option>
                                    <option value="13" <?if($EthnicOrigin->ethnicA == '13') echo 'selected';?>>Bolivian</option>
                                    <option value="14" <?if($EthnicOrigin->ethnicA == '14') echo 'selected';?>>Bulgarian</option>
                                    <option value="15" <?if($EthnicOrigin->ethnicA == '15') echo 'selected';?>>Cajun</option>
                                    <option value="16" <?if($EthnicOrigin->ethnicA == '16') echo 'selected';?>>Cambodian</option>
                                    <option value="17" <?if($EthnicOrigin->ethnicA == '17') echo 'selected';?>>Caribbean</option>
                                    <option value="18" <?if($EthnicOrigin->ethnicA == '18') echo 'selected';?>>Chilean</option>
                                    <option value="19" <?if($EthnicOrigin->ethnicA == '19') echo 'selected';?>>Chinese</option>
                                    <option value="20" <?if($EthnicOrigin->ethnicA == '20') echo 'selected';?>>Croatian</option>
                                    <option value="21" <?if($EthnicOrigin->ethnicA == '21') echo 'selected';?>>Cuban</option>
                                    <option value="22" <?if($EthnicOrigin->ethnicA == '22') echo 'selected';?>>Czech</option>
                                    <option value="23" <?if($EthnicOrigin->ethnicA == '23') echo 'selected';?>>Danish</option>
                                    <option value="24" <?if($EthnicOrigin->ethnicA == '24') echo 'selected';?>>Dominican</option>
                                    <option value="25" <?if($EthnicOrigin->ethnicA == '25') echo 'selected';?>>Dutch</option>
                                    <option value="26" <?if($EthnicOrigin->ethnicA == '26') echo 'selected';?>>Ecuadorian</option>
                                    <option value="27" <?if($EthnicOrigin->ethnicA == '27') echo 'selected';?>>Dutch</option>
                                    <option value="28" <?if($EthnicOrigin->ethnicA == '28') echo 'selected';?>>Egyptian</option>
                                    <option value="29" <?if($EthnicOrigin->ethnicA == '29') echo 'selected';?>>Eritrean</option>
                                    <option value="30" <?if($EthnicOrigin->ethnicA == '30') echo 'selected';?>>English</option>
                                    <option value="31" <?if($EthnicOrigin->ethnicA == '31') echo 'selected';?>>Estonian</option>
                                    <option value="32" <?if($EthnicOrigin->ethnicA == '32') echo 'selected';?>>Ethiopian</option>
                                    <option value="33" <?if($EthnicOrigin->ethnicA == '33') echo 'selected';?>>Filipino</option>
                                    <option value="34" <?if($EthnicOrigin->ethnicA == '34') echo 'selected';?>>Finnish</option>
                                    <option value="35" <?if($EthnicOrigin->ethnicA == '35') echo 'selected';?>>Flemish</option>
                                    <option value="36" <?if($EthnicOrigin->ethnicA == '36') echo 'selected';?>>French</option>
                                    <option value="37" <?if($EthnicOrigin->ethnicA == '37') echo 'selected';?>>French-Canadian</option>
                                    <option value="38" <?if($EthnicOrigin->ethnicA == '38') echo 'selected';?>>Georgian</option>
                                    <option value="39" <?if($EthnicOrigin->ethnicA == '39') echo 'selected';?>>German</option>
                                    <option value="40" <?if($EthnicOrigin->ethnicA == '40') echo 'selected';?>>Ghanaian</option>
                                    <option value="41" <?if($EthnicOrigin->ethnicA == '41') echo 'selected';?>>Greek</option>
                                    <option value="42" <?if($EthnicOrigin->ethnicA == '42') echo 'selected';?>>Guatemalan</option>
                                    <option value="43" <?if($EthnicOrigin->ethnicA == '43') echo 'selected';?>>Haitian</option>
                                    <option value="44" <?if($EthnicOrigin->ethnicA == '44') echo 'selected';?>>Hmong</option>
                                    <option value="45" <?if($EthnicOrigin->ethnicA == '45') echo 'selected';?>>Honduran</option>
                                    <option value="46" <?if($EthnicOrigin->ethnicA == '46') echo 'selected';?>>Hungarian</option>
                                    <option value="47" <?if($EthnicOrigin->ethnicA == '47') echo 'selected';?>>Icelandic</option>
                                    <option value="48" <?if($EthnicOrigin->ethnicA == '48') echo 'selected';?>>Indian</option>
                                    <option value="49" <?if($EthnicOrigin->ethnicA == '49') echo 'selected';?>>Indonesian</option>
                                    <option value="50" <?if($EthnicOrigin->ethnicA == '50') echo 'selected';?>>Iranian</option>
                                    <option value="51" <?if($EthnicOrigin->ethnicA == '51') echo 'selected';?>>Iraqi</option>
                                    <option value="52" <?if($EthnicOrigin->ethnicA == '52') echo 'selected';?>>Irish</option>
                                    <option value="53" <?if($EthnicOrigin->ethnicA == '53') echo 'selected';?>>Israeli</option>
                                    <option value="54" <?if($EthnicOrigin->ethnicA == '54') echo 'selected';?>>Italian</option>
                                    <option value="55" <?if($EthnicOrigin->ethnicA == '55') echo 'selected';?>>Jamaican</option>
                                    <option value="56" <?if($EthnicOrigin->ethnicA == '56') echo 'selected';?>>Japanese</option>
                                    <option value="57" <?if($EthnicOrigin->ethnicA == '57') echo 'selected';?>>Jewish</option>
                                    <option value="58" <?if($EthnicOrigin->ethnicA == '58') echo 'selected';?>>Kazakh</option>
                                    <option value="59" <?if($EthnicOrigin->ethnicA == '59') echo 'selected';?>>Kirgyz</option>
                                    <option value="60" <?if($EthnicOrigin->ethnicA == '60') echo 'selected';?>>Khmer</option>
                                    <option value="61" <?if($EthnicOrigin->ethnicA == '61') echo 'selected';?>>Korean</option>
                                    <option value="62" <?if($EthnicOrigin->ethnicA == '62') echo 'selected';?>>Lao</option>
                                    <option value="63" <?if($EthnicOrigin->ethnicA == '63') echo 'selected';?>>Latvian</option>
                                    <option value="64" <?if($EthnicOrigin->ethnicA == '64') echo 'selected';?>>Lebanese</option>
                                    <option value="65" <?if($EthnicOrigin->ethnicA == '65') echo 'selected';?>>Liberian</option>
                                    <option value="66" <?if($EthnicOrigin->ethnicA == '66') echo 'selected';?>>Lithuanian</option>
                                    <option value="67" <?if($EthnicOrigin->ethnicA == '67') echo 'selected';?>>Liberian</option>
                                    <option value="68" <?if($EthnicOrigin->ethnicA == '68') echo 'selected';?>>Malaysian</option>
                                    <option value="69" <?if($EthnicOrigin->ethnicA == '69') echo 'selected';?>>Mauritian</option>
                                    <option value="70" <?if($EthnicOrigin->ethnicA == '70') echo 'selected';?>>Mexican</option>
                                    <option value="71" <?if($EthnicOrigin->ethnicA == '71') echo 'selected';?>>Moldavian</option>
                                    <option value="72" <?if($EthnicOrigin->ethnicA == '72') echo 'selected';?>>Morocco</option>
                                    <option value="73" <?if($EthnicOrigin->ethnicA == '73') echo 'selected';?>>Nigerian</option>
                                    <option value="74" <?if($EthnicOrigin->ethnicA == '74') echo 'selected';?>>Norwegian</option>
                                    <option value="75" <?if($EthnicOrigin->ethnicA == '75') echo 'selected';?>>Pacific Islander</option>
                                    <option value="76" <?if($EthnicOrigin->ethnicA == '76') echo 'selected';?>>Pakistani</option>
                                    <option value="77" <?if($EthnicOrigin->ethnicA == '77') echo 'selected';?>>Palestinian</option>
                                    <option value="78" <?if($EthnicOrigin->ethnicA == '78') echo 'selected';?>>Panamanian</option>
                                    <option value="79" <?if($EthnicOrigin->ethnicA == '79') echo 'selected';?>>Persian</option>
                                    <option value="80" <?if($EthnicOrigin->ethnicA == '80') echo 'selected';?>>Peruvian</option>
                                    <option value="81" <?if($EthnicOrigin->ethnicA == '82') echo 'selected';?>>Polish</option>
                                    <option value="82" <?if($EthnicOrigin->ethnicA == '81') echo 'selected';?>>Portuguese</option>
                                    <option value="83" <?if($EthnicOrigin->ethnicA == '82') echo 'selected';?>>Puerto Rican</option>
                                    <option value="84" <?if($EthnicOrigin->ethnicA == '84') echo 'selected';?>>Romanian</option>
                                    <option value="85" <?if($EthnicOrigin->ethnicA == '85') echo 'selected';?>>Russian</option>
                                    <option value="86" <?if($EthnicOrigin->ethnicA == '86') echo 'selected';?>>Rwandan</option>
                                    <option value="87" <?if($EthnicOrigin->ethnicA == '87') echo 'selected';?>>Salvadorian</option>
                                    <option value="88" <?if($EthnicOrigin->ethnicA == '88') echo 'selected';?>>Saudi</option>
                                    <option value="89" <?if($EthnicOrigin->ethnicA == '89') echo 'selected';?>>Scottish</option>
                                    <option value="90" <?if($EthnicOrigin->ethnicA == '90') echo 'selected';?>>Sicilian</option>
                                    <option value="91" <?if($EthnicOrigin->ethnicA == '91') echo 'selected';?>>Slovakian</option>
                                    <option value="92" <?if($EthnicOrigin->ethnicA == '92') echo 'selected';?>>Slovakian</option>
                                    <option value="93" <?if($EthnicOrigin->ethnicA == '93') echo 'selected';?>>Spanish</option>
                                    <option value="94" <?if($EthnicOrigin->ethnicA == '94') echo 'selected';?>>Sudanese</option>
                                    <option value="95" <?if($EthnicOrigin->ethnicA == '95') echo 'selected';?>>Swedish</option>
                                    <option value="96" <?if($EthnicOrigin->ethnicA == '96') echo 'selected';?>>Swiss</option>
                                    <option value="97" <?if($EthnicOrigin->ethnicA == '97') echo 'selected';?>>Tatar</option>
                                    <option value="98" <?if($EthnicOrigin->ethnicA == '98') echo 'selected';?>>Tajik</option>
                                    <option value="99" <?if($EthnicOrigin->ethnicA == '99') echo 'selected';?>>Thai</option>
                                    <option value="100" <?if($EthnicOrigin->ethnicA == '100') echo 'selected';?>>Trinidadian</option>
                                    <option value="101" <?if($EthnicOrigin->ethnicA == '101') echo 'selected';?>>Tripoli</option>
                                    <option value="102" <?if($EthnicOrigin->ethnicA == '102') echo 'selected';?>>Turkish</option>
                                    <option value="103" <?if($EthnicOrigin->ethnicA == '103') echo 'selected';?>>Ukrainian</option>
                                    <option value="104" <?if($EthnicOrigin->ethnicA == '104') echo 'selected';?>>Uigur</option>
                                    <option value="105" <?if($EthnicOrigin->ethnicA == '105') echo 'selected';?>>Uzbek</option>
                                    <option value="106" <?if($EthnicOrigin->ethnicA == '106') echo 'selected';?>>Venezuelan</option>
                                    <option value="107" <?if($EthnicOrigin->ethnicA == '107') echo 'selected';?>>Vietnamese</option>
                                    <option value="108" <?if($EthnicOrigin->ethnicA == '108') echo 'selected';?>>Welsh</option>
                                    <option value="109" <?if($EthnicOrigin->ethnicA == '109') echo 'selected';?>> West Indian</option>
                                    <option value="110" <?if($EthnicOrigin->ethnicA == '110') echo 'selected';?>>Yemenite</option>
                                    <option value="111" <?if($EthnicOrigin->ethnicA == '111') echo 'selected';?>>Yugoslavian</option>
                                    <option value="112" <?if($EthnicOrigin->ethnicA == '112') echo 'selected';?>>Other</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="personal-select personal-characteristics d-flex">
                        <div class="select-title personal-all-title">
                            <h3>Education </h3>
                        </div>

                        <div class="select-wrap">
                            <div class="select-item">
                                <select name="education" id="">
                                    <option></option>
                                    <option value="1">Currently in School</option>
                                    <option value="2">High School</option>
                                    <option value="3">College</option>
                                    <option value="4">Bachelor</option>
                                    <option value="5">Master</option>
                                    <option value="6">Doctorate</option>
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="personal-checkbox personal-characteristics d-flex">
                        <div class="checkbox-title personal-all-title">
                            <h3>Marital Status</h3>
                        </div>

                        <div class="checkbox-wrap d-flex">
                            <div class="checkbox-item">
                                <input type="radio" id="personal-checkbox-1" name="marital" value="1">
                                <label for="personal-checkbox-1">Married</label>
                            </div>

                            <div class="checkbox-item">
                                <input type="radio" id="personal-checkbox-2" name="marital" value="2">
                                <label for="personal-checkbox-2">Single</label>
                            </div>
                        </div>
                    </div>


                </div>

            </div>

            <!-- END PERSONAL LEFT -->

            <!-- PERSONAL RIGHT -->
            <div class="personal-right">

                <div class="personal-right-accordion">


                    <div class="personal-title">
                        <h2>Location</h2>
                    </div>

                    <div class="personal-right-accordion-wrap">
                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Country </h3>
                            </div>
                            <div class="select-wrap">
                                <div class="select-item">
                                    <?= yii\helpers\Html::dropDownList('country', '', \yii\helpers\ArrayHelper::map($countries, 'id', 'name'), ['prompt' => '']) ?>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="personal-title">
                        <h2>Advanced criteria</h2>
                    </div>

                    <div class="personal-right-accordion-wrap">

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Available to Begin</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-3" value="1">
                                    <label for="personal-checkbox-3">Available</label>
                                </div>

                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-4" value="2">
                                    <label for="personal-checkbox-4">Not Available</label>
                                </div>

                                <div class="checkbox-item">
                                    <input type="radio" name="available_to_begin" id="personal-checkbox-5" value="3">
                                    <label for="personal-checkbox-5">Currently Family Helping</label>
                                </div>
                            </div>
                        </div>


                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Previous Egg Donor</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" name="previous_surrogate" value="1">
                                </div>
                            </div>
                        </div>


                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Blood Types  </h3>
                            </div>

                            <div class="select-wrap">
                                <div class="select-item">
                                    <select name="blood_group" id="">
                                        <option></option>
                                        <option value="A+">A+</option>
                                        <option value="A-">A-</option>
                                        <option value="B+">B+</option>
                                        <option value="B-">B-</option>
                                        <option value="AB+">AB+</option>
                                        <option value="AB-">AB-</option>
                                        <option value="O+">O+</option>
                                        <option value="O-">O-</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>My favourites</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" name="my_favourites">
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>New and updated egg donors from the last 15 days </h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" id="personal-checkbox-1" name="new">
                                </div>
                            </div>
                        </div>

                        <div class="personal-right-content d-flex">
                            <div class="right-content-title personal-all-title">
                                <h3>Video</h3>
                            </div>

                            <div class="checkbox-wrap">
                                <div class="checkbox-item">
                                    <input type="checkbox" id="personal-checkbox-1" name="video">
                                </div>
                            </div>
                        </div>

                    </div>

                </div>

            </div>

            <div class="personal-button">
                <a href="#" class="search_egg_donor">Search</a>
            </div>

            <div class="personal-link">
                <a href="#" class="ClearSearches">Clear Searches</a>
            </div>
            <?php ActiveForm::end(); ?>
            <!-- END PERSONAL RIGHT -->
        </div>

        <div class="personal-buttons d-flex">
            <div class="personal-buttons-item">
                <a href="#" class="My_Favorites_Egg_Donor">My Favorites</a>
            </div>

            <div class="personal-buttons-item">
                <a href="#" class="New_and_Updated_Egg_Donor">New and Updated</a>
            </div>

            <div class="personal-buttons-item">
                <a href="#" class="View_All_Egg_Donor">View All Egg Donor</a>
            </div>
        </div>

        <div class="search_result">
            <div class="database-finded">
                <?if($model['data']){?>
                    <span>Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов</span>
                <?}else{?>
                    <span>Sorry, no results were found.<br />Try refining your search.</span>
                <?}?>
            </div>

            <div class="database-pag-status">
                <div class="database-status">
                    <span class="database-circle"></span>
                    <span> - </span>
                    <span>Свободна</span>
                </div>

                <?if($model['data']):?>
                    <?= $this->render('/partials/database_pagination', [
                        'pages'=>$model['pagination'],
                    ]);?>
                <? endif;?>

                <div class="empty-block"></div>
            </div>


            <div class="database-profile-wrap">
                <script>
                    <?if($model['data']){?>
                    $('.database-finded').html('Найдено <?=count($model['data'])?> из <?=$model['pagination']->totalCount?> результатов');
                    <?}else{?>
                    $('.database-finded').html('Sorry, no results were found.\n' +
                        '<br/>Try refining your search.\n');
                    <?}?>
                </script>
                <?if($model['data'])foreach($model['data'] as $v){?>
                    <div class="database-profile">
                        <div class="profile-circle">
                            <span></span>
                        </div>

                        <?if(Yii::$app->view->params['admission_donor']->photo):?>
                            <div class="profile-image">
                                <img src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                                     style="width: 100%;">
                            </div>
                        <? endif;?>
                        <?if(!Yii::$app->view->params['admission_donor']->photo):?>
                            <div class="profile-image">
                                <img class="passive-profile" src="/backend/web/images/userphoto/<?=$v->id?>/<?=$v->photo->photo?>" alt="image"
                                     style="width: 100%;">
                            </div>
                        <? endif;?>


                        <div class="database-profi">
                            <?if($v->profi){?>
                                <h3 title="Previous Surrogate">Profi</h3>
                            <?}else{?>
                                <?if($v->miscellaneous->have_been_eggdonor == 1){?>
                                    <h3 title="Previous Surrogate">Profi</h3>
                                <?}?>
                            <?}?>
                            <?if($v->proven){?>
                                <img src="/images/union.svg" alt="union" title="Proven">
                            <?}?>
                        </div>

                        <div class="database-surrogate">
                            <h3><?=$v->getShortRoleName();?>-<?=$v->profile->getCreatedDateWithoutDot();?><?=$v->getOrderID();?></h3>
                            <span><?=$v->getRoleName();?></span>
                        </div>

                        <div class="database-avilible">
                        <span style="font-family: roboto; color: #82EF5B;">
                            <? if($v->active == 1):?> <?='Available'?><? endif;?>
                            <? if($v->active == 2):?> <?='Not Available'?><?endif;?>
                            <? if($v->active == 3):?> <?='Currently Family Helping'?><?endif;?>
                        </span>
                        </div>

                        <div class="database-location">
                            <div class="location-image">
                                <img src="/images/location.svg" alt="location">
                                <h3><?=$v->personal_info->getCountryName();?></h3>
                            </div>

                            <div class="location-item">
                                <?
                                $birthDate = $v->personal_info->date_of_birth;
                                $birthDate = explode("-", $birthDate);
                                $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[2], $birthDate[1], $birthDate[0]))) > date("md")
                                    ? ((date("Y") - $birthDate[0]) - 1)
                                    : (date("Y") - $birthDate[0]));
                                ?>
                                <p><?=$age?> года</p>
                                <p>
                                    <?
                                    if($v->personal_info->marital_status == 1)
                                        echo 'Married';
                                    else
                                        echo 'Single';
                                    ?>
                                </p>

                            </div>


                        </div>

                        <? if(Yii::$app->view->params['admission_donor']->compensation):?>
                            <? if($v->status_comp):?>
                                <div class="database-compensation">
                                    <img src="/images/database-compensation.svg" alt="compensation">
                                    <p>Компенсация</p>
                                    <h3><?=$v->miscellaneous->desired_compensation;?> <?=$v->miscellaneous->other_compensation_currency==null ? $v->miscellaneous->compensation_currency:$v->miscellaneous->other_compensation_currency;?>
                                    </h3>
                                </div>
                            <? endif;?>
                        <? endif;?>

                        <div class="database-measurement">

                            <div class="measurement-item">
                                <p><?=$v->personal_info->height;?>sm</p>
                                <p><?=$v->personal_info->weight;?>kg</p>
                                <p>
                                    <?if($v->personal_info->eye_color == 1){ echo "Blue";}?>
                                    <?if($v->personal_info->eye_color == 2){ echo "Gray";}?>
                                    <?if($v->personal_info->eye_color == 3){ echo "Green";}?>
                                    <?if($v->personal_info->eye_color == 4){ echo "Amber";}?>
                                    <?if($v->personal_info->eye_color == 5){ echo "Olive";}?>
                                    <?if($v->personal_info->eye_color == 6){ echo "Brown";}?>
                                    <?if($v->personal_info->eye_color == 7){ echo "Black";}?>
                                    <?if($v->personal_info->eye_color == 8){ echo "Yellow";}?>
                                </p>
                                <p>
                                    <?if($v->personal_info->natural_hair_color_now == 1){ echo "Black";}?>
                                    <?if($v->personal_info->natural_hair_color_now == 2){ echo "Brown";}?>
                                    <?if($v->personal_info->natural_hair_color_now == 3){ echo "Redhead";}?>
                                    <?if($v->personal_info->natural_hair_color_now == 4){ echo "Blond";}?>
                                    <?if($v->personal_info->natural_hair_color_now == 5){ echo "Light Blond";}?>
                                </p>
                            </div>

                        </div>

                        <div class="database-photo-video">
                            <div class="empty-item" title="Add to Favorites" style="cursor:pointer;"  id="add-user-to-fav" data-user-id="<?=$v->id?>" data-user="<?=Yii::$app->user->identity->id?>">
                                <div class="measurement-item<?=$v->id;?>">
                                    <? if(!Yii::$app->user->isGuest):?>
                                        <? if(in_array($v->id,$fav)):?><img src="/images/heart-fill.svg" alt="heart" ><? endif;?>
                                        <? if(!in_array($v->id,$fav)):?><img src="/images/heart.svg" alt="heart"> <? endif;?>
                                    <? endif;?>

                                    <? if(Yii::$app->user->isGuest):?>
                                        <img src="/images/heart.svg" alt="heart" >
                                    <? endif;?>
                                </div>
                            </div>

                            <?if(Yii::$app->view->params['admission_donor']->photo):?>
                                <div class="photo-item" title="View more Photos">
                                    <a href="#">
                                        <img src="/images/database-img.svg" alt="img">
                                    </a>
                                    <div class="aniimated-thumbnials">
                                        <? foreach ($v->photos as $photo):?>
                                            <a href="/backend/web/images/userphoto/<?=$v->id?>/<?=$photo->photo?>"></a>
                                        <?endforeach;?>
                                    </div>
                                </div>
                            <? endif?>

                            <?if(!Yii::$app->view->params['admission_donor']->photo):?>
                                <div class="photo-item grey-area" title="View more Photos">
                                    <a >
                                        <img src="/images/database-img.svg" alt="img">
                                    </a>
                                </div>
                            <? endif?>

                            <?if(Yii::$app->view->params['admission_donor']->video):?>
                                <?if($v->video->video != null){ $class = '';}?>
                                <?if($v->video->video == null){ $class = 'grey-area';}?>
                            <? endif?>
                            <?if(!Yii::$app->view->params['admission_donor']->video){ $class = 'grey-area';}?>

                            <div class="video-item <?=$class;?>" title="View Video">
                                <? if($class=='grey-area'):?>
                                    <button type="button" class="btn btn-primary button-movie">
                                        <img src="/images/database-video.svg" alt="img">
                                    </button>
                                <? endif;?>

                                <? if($class==''):?>
                                    <button type="button" class="btn btn-primary button-movie" data-toggle="modal"
                                            data-target="#exampleModal<?=$v->id;?>">
                                        <img src="/images/database-video.svg" alt="img">
                                    </button>
                                <? endif;?>
                                <!-- Modal -->
                                <div class="modal fade" id="exampleModal<?=$v->id;?>" tabindex="-1" role="dialog"
                                     aria-labelledby="exampleModalLabel" aria-hidden="true">
									

									
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
												
                                               
												<video id="video-player"controls name="media" autostart="false"  height="100%" width="100%" >
												<source src="<?='/backend/web/images/uservideo/'.$v->id.'/'.$v->video->video?>" type="video/mp4">
												</video>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary"
                                                        data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
<script type="text/javascript">
$(document).ready(function(){
$('#exampleModal<?=$v->id;?>').on('hidden.bs.modal', function () {


 var $video = jQuery('video', jQuery(this));
        $video[0].pause();


});
	$('#exampleModal<?=$v->id;?>').on('shown.bs.modal', function () {


 var $video = jQuery('video', jQuery(this));
        $video[0].play();


});
});
</script>
                                </div>
                            </div>
                        </div>

                        <?if($v->role == 3){ $url = 'egg-donor';}?>
                        <?if($v->role == 4){ $url = 'surrogate-and-egg-donor';}?>
                        <div class="database-button">
                            <a href="/profile/<?=$url;?>?id=<?=$v->id?>">Полный профиль</a>
                        </div>
                    </div>
                <?}?>

            </div>

            <?if($model['data']):?>
                <?= $this->render('/partials/database_pagination', [
                    'pages'=>$model['pagination'],
                ]);?>
            <? endif;?>

        </div>
</section>
	
<!-- END DONOR DATABASE -->
