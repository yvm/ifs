<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.05.2019
 * Time: 13:18
 */

use yii\widgets\ActiveForm; ?>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="col-12">
            <div class="register-bg">
                <div class="row">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="/surrogate/step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step4" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step5" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="/surrogate/step6" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step7" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step8" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step9" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step10" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>

                    <div class="tab-pane fade active show" id="pills-step6" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="register-tab-title">
                                    <p>Education and Occupation</p>
                                </div>
                            </div>
                            <div class="errors" style="color:red;">
<!--                                --><?//=$form->errorSummary($userEducation);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="education_step6">
                                    <div class="col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>Educational Level <span>*</span></p>
                                            <?= $form->field($userEducation, 'educational_level')->dropDownList(
                                                [
                                                    1 => "Currently in school", 2 => "High School", 3 => "College", 4 => "Bachelor", 5 => "Master",6 => "Doctorate"
                                                ],
                                                ['class'=>'col-12 col-sm-12 col-4 edu_level_select'])->label(false);
                                            ?>

                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>Occupation <span>*</span></p>
                                            <?= $form->field($userEducation, 'occupation')->textInput(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>What is your native language <span>*</span></p>
                                            <?= $form->field($userEducation, 'what_is_your_native_language')->textInput(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="education_step6">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18 mobile_employed">
                                            <p>Are you currently employed? <span>*</span></p>
                                            <div>
                                                <?= $form->field($userEducation, 'are_you_currently_employed')->radioList(
                                                    [1 => 'Yes', 2 => 'No'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';
                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18 questions_13-18_nth5">
                                            <div>
                                                <div>
                                                    <p>Do you know any language <br> (other than native language)? <span>*</span>
                                                    </p>
                                                </div>
                                                <div>
                                                    <?= $form->field($userEducation, 'do_you_know_any_language')->radioList(
                                                        [1 => 'Yes', 2 => 'No'],
                                                        [
                                                            'item' => function($index, $label, $name, $checked, $value) {
                                                                if($checked == 1)
                                                                    $checked = 'checked';
                                                                $return = '<label class="radio-input">'.$label;
                                                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                                $return .= '<span class="checkmark"></span>';
                                                                $return .= '</label>';
                                                                return $return;
                                                            }
                                                        ]
                                                    )->label(false) ?>
                                                </div>
                                            </div>
                                            <div id="surrogate_step6_lang" style="display: <?=$userEducation->do_you_know_any_language == 2 || $userEducation->do_you_know_any_language === null ? 'none':''?>">
                                                <p>
                                                    Foreign Language
                                                </p>
                                                <select name="UserEducation[foreign_language]">
                                                    <option value="1" <?if($userEducation->foreign_language == '1') echo 'selected';?>>English</option>
                                                    <option value="2" <?if($userEducation->foreign_language == '2') echo 'selected';?>>Spanish</option>
                                                    <option value="3" <?if($userEducation->foreign_language == '3') echo 'selected';?>>German</option>
                                                    <option value="4" <?if($userEducation->foreign_language == '4') echo 'selected';?>>Italian</option>
                                                    <option value="5" <?if($userEducation->foreign_language == '5') echo 'selected';?>>French</option>
                                                    <option value="6" <?if($userEducation->foreign_language == '6') echo 'selected';?>>Russian</option>
                                                    <option value="7" <?if($userEducation->foreign_language == '7') echo 'selected';?>>Other</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18 questions_13-18_last">
                                            <p>What type of educational goals did/do you have? <span>*</span></p>
                                            <?= $form->field($userEducation, 'what_type_of_educational_goals')->textInput(['maxlength' => true])->label(false) ?>

                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="btn_step6">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/egg-donor/step5'">Back</button>
                                    <button class="btn-next-step">Next Step</button>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
