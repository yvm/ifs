<div class="file-list-item UserHandwritingSample_img_block_<?=$UserHandwritingSample->id?>">
    <div class="file-name">
        <p><?=$UserHandwritingSample->video?></p>
    </div>
    <div class="delete-icon">
        <a href="" class="delete_img" data-id="<?=$UserHandwritingSample->id?>" data-type="UserHandwritingSample"><img src="/images/delete-icon.png" alt=""></a>
    </div>
    <div class="size">
        <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userhandwritingsample/'.Yii::$app->session['reg_user'].'/'.$UserHandwritingSample->video)/1000);?> кб.</p>
    </div>
</div>
<div class="error-oversize"><?=$error?></div>