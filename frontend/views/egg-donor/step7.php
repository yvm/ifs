<?php
use yii\widgets\ActiveForm;
?>


<div class="overlay">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">
            <img src="images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body length shot. Additional photos can be from past years. You may also send in photos of you with your family, friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li><p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the brightest light is behind you causing your face to be in darkness, photos where there are shadows on your face and photos that are blurry.</p></li>
                    <li><p>Sending photos of yourself with other people is fine as long as you have other photos where you are the focus and the other people in the photo are presentable. If your photo includes other people, please label who is in the photo such as “Me with Husband”.</p></li>
                    <li><p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples include photos of you hiking, biking, painting or cooking.</p></li>
                    <li><p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high resolution setting are on.</p></li>
                    <li><p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting structures behind you, and avoid taking selfies in the car.</p></li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="/surrogate/step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step4" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step5" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="/surrogate/step6" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step7" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step8" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step9" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step10" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step4" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>
                            <div class="errors" style="color:red;">
<!--                                --><?//=$form->errorSummary($Miscellaneous);?>
                            </div>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                            <div class="register-tab-title">
                                <h3>Miscellaneous</h3>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="register-input">
                                        <label for="">How did you know about us? <span>*</span></label>
                                        <select  id="surrogate_select_13" name="Miscellaneous[how_know_about_us]">
                                            <option value="Google Search" <?if($Miscellaneous->how_know_about_us == 'Google Search') echo 'selected';?>>Google Search</option>
                                            <option value="Google Media" <?if($Miscellaneous->how_know_about_us == 'Google Media') echo 'selected';?>>  Google Media</option>
                                            <option value="Yahoo Search" <?if($Miscellaneous->how_know_about_us == 'Yahoo Search') echo 'selected';?>>  Yahoo Search</option>
                                            <option value="Bing Search" <?if($Miscellaneous->how_know_about_us == 'Bing Search') echo 'selected';?>>  Bing Search</option>
                                            <option value="Facebook" <?if($Miscellaneous->how_know_about_us == 'Facebook') echo 'selected';?>>  Facebook</option>
                                            <option value="Youtube" <?if($Miscellaneous->how_know_about_us == 'Youtube') echo 'selected';?>>  Youtube</option>
                                            <option value="Instagram" <?if($Miscellaneous->how_know_about_us == 'Instagram') echo 'selected';?>>  Instagram </option>
                                            <option value="Telegram" <?if($Miscellaneous->how_know_about_us == 'Telegram') echo 'selected';?>>  Telegram </option>
                                            <option value="Twitter" <?if($Miscellaneous->how_know_about_us == 'Twitter') echo 'selected';?>>  Twitter </option>
                                            <option value="Google+" <?if($Miscellaneous->how_know_about_us == 'Google+') echo 'selected';?>>  Google+ </option>
                                            <option value="Tumblr" <?if($Miscellaneous->how_know_about_us == 'Tumblr') echo 'selected';?>>  Tumblr </option>
                                            <option value="VK" <?if($Miscellaneous->how_know_about_us == 'VK') echo 'selected';?>>  VK </option>
                                            <option value="Ok" <?if($Miscellaneous->how_know_about_us == 'Ok') echo 'selected';?>>  Ok </option>
                                            <option value="Flickr" <?if($Miscellaneous->how_know_about_us == 'Flickr') echo 'selected';?>>  Flickr </option>
                                            <option value="Referral" <?if($Miscellaneous->how_know_about_us == 'Referral') echo 'selected';?>>  Referral </option>
                                            <option value="Other" <?if($Miscellaneous->how_know_about_us == 'Other') echo 'selected';?>>  Other </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-sm-6" >
                                    <div class="simple-input" id="surrogate_tab_13_1" style="display: <?=$Miscellaneous->how_know_about_us == 'Other'? '':'none'?>">
                                        <label for="">Please Describe</label>
                                        <?= $form->field($Miscellaneous, 'please_describe')->textarea(['maxlength' => true])->label(false) ?>
                                    </div>
                                    <div class="simple-input" id="surrogate_tab_13_2"  style="display: <?=$Miscellaneous->how_know_about_us == 'Referral'? '':'none'?>">
                                        <label for="">If you selected “Referral” above, please tell us the name of the person who referred you
                                        </label>
                                        <?= $form->field($Miscellaneous, 'if_you_select_referral')->textarea(['maxlength' => true])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="simple-input">
                                        <label for="">Have you ever been involved in a lawsuit (including personal bankruptcy)? If yes, please fully describe, including the dates, the reason(s), and outcome(s)? <span>*</span></label>
                                        <?= $form->field($Miscellaneous, 'have_ever_been')->textarea(['maxlength' => true])->label(false) ?>
                                    </div>
                                </div>


                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <label for="">Have you or anyone in your household been convicted of a felony? (Please note: screening will involve a criminal background check on all adults in your household.)? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <?= $form->field($Miscellaneous, 'have_you_or_anyone')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <label for="">Have you been a surrogate before? <span>*</span> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <?= $form->field($Miscellaneous, 'have_been_eggdonor')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <label for="">Have you ever applied to be a surrogate? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <?= $form->field($Miscellaneous, 'have_ever_applied')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <label for="">Are you interested in becoming a surrogate? Or learning more about surrogacy? <span>*</span> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <?= $form->field($Miscellaneous, 'are_you_interested')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>


                                <div class="col-sm-4">
                                    <div class="small-textarea">
                                        <label for="">Have you discussed with your family/friends your desire to donate? Are they supportive of your decision? <span>*</span></label>
                                        <?= $form->field($Miscellaneous, 'have_you_discussed_desire_donate')->textarea(['maxlength' => true])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="row">
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <label for=""> If approved as an IFS donor, would you be willing to accept a lower fee to help our less well-off Intended Parents with costs associated with this journey?  <span>*</span> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-5">
                                            <?= $form->field($Miscellaneous, 'if_approved_as_ifs_donor')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="register-input">
                                        <label for="">Desired compensation amount <span>*</span></label>
                                        <div class="flex-one">
											<select  name="Miscellaneous[desired_compensation]"  class="small-w">
                                                <? for($i=$compensation->from;$i<=$compensation->to;$i+=$compensation->move):?>
                                                    <option value="<?=$i?>" <?if($Miscellaneous->desired_compensation == $i) echo 'selected';?>><?=$i?></option>
                                                <? endfor;?>
                                            </select>

                                            <? if($valuta != null):?>
                                                <div class="money">
                                                    <p style="display: <?=$Miscellaneous->other_compensation_currency == null ? '':'none'; ?>" id="compensation_currency"><?=$valuta->text;?></p>
                                                </div>
                                            <? endif;?>

                                            <? if($valuta != null):?>
                                                <input name="Miscellaneous[compensation_currency]"  value="<?=$valuta->text;?>" style="display: none;" >
                                            <? endif;?>

                                            <input class="other_compensation_currency"  name="Miscellaneous[other_compensation_currency]" value="<?=$Miscellaneous->other_compensation_currency;?>" style="display: <?=$Miscellaneous->other_compensation_currency == null ? 'none':''; ?>">
                                            <p style="cursor: pointer;display: <?=$Miscellaneous->other_compensation_currency == null ? '':'none'; ?>" class="write_other_currency">Write other currency</p>
                                            <p style="cursor: pointer;display: <?=$Miscellaneous->other_compensation_currency == null ? 'none':''; ?>" class="select_current_currency">Select current currency</p>

                                        </div>
                                        <div class="input-info-text">
                                            <p>The greater the amount of compensation, the longer you have to wait for the intended parents to participate in the program</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6 col-6">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/egg-donor/step6'">Back</button>
                                </div>
                                <div class="col-sm-6 col-6">
                                    <button class="btn-next-step">Next Step</button>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
