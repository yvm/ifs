<?php

use yii\widgets\LinkPager;
?>
<div class="database-pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pages,
        'options' => ['class' => ''],
        'activePageCssClass' => 'pag-active',
        'hideOnSinglePage' => true,
        'nextPageLabel' =>false,
        'prevPageLabel' => false,
    ]);
    ?>
</div>
