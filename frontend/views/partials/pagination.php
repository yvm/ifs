<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 01.03.2019
 * Time: 17:54
 */

use yii\widgets\LinkPager;

?>

<div class="pagination">
    <?php
    echo LinkPager::widget([
        'pagination' => $pages,
        'activePageCssClass' => 'active',
        'hideOnSinglePage' => true,
        'nextPageLabel' =>false,
        'prevPageLabel' => false,
    ]);
    ?>
</div>
