
<div class="overlay" id="modal-guidelines">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">

            <div class="modal-close-btn">
                <a href="#" class="modal-close"><img src="/images/modal-close.png" alt=""></a>
            </div>
            <img src="images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body length shot. Additional photos can be from past years. You may also send in photos of you with your family, friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li><p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the brightest light is behind you causing your face to be in darkness, photos where there are shadows on your face and photos that are blurry.</p></li>
                    <li><p>Sending photos of yourself with other people is fine as long as you have other photos where you are the focus and the other people in the photo are presentable. If your photo includes other people, please label who is in the photo such as “Me with Husband”.</p></li>
                    <li><p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples include photos of you hiking, biking, painting or cooking.</p></li>
                    <li><p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high resolution setting are on.</p></li>
                    <li><p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting structures behind you, and avoid taking selfies in the car.</p></li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap2">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-home-tab" href="/biotransportation-client/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" href="/biotransportation-client/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" href="/biotransportation-client/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-contact-tab" data-toggle="pill" href="#pills-step4" role="tab" aria-controls="pills-contact" aria-selected="false">Step 4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step4" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <div class="register-tab-title">
                                <?if($user->role == 1){?>
                                    <h3>Documents, Photos, Videos</h3>
                                    <p>Upload Documents/Photos/Videos  </p>
                                <?}elseif($user->role == 2){?>
                                    <h3>Documents, Photos, Videos & Voice Messages</h3>
                                    <p>Upload Documents/Photos/Videos/Voice Messages</p>
                                <?}elseif($user->role == 3){?>
                                    <h3>Documents, Photos, Videos, Voice Messages & Handwriting Sample</h3>
                                    <p>Upload Documents/Photos/Videos/Voice Messages/Handwriting Sample</p>
                                <?}elseif($user->role == 5){?>
                                    <h3>Documents</h3>
                                    <p>Upload Documents </p>
                                <?}?>
                            </div>
                            <div class="register-input">
                                <label for="">Please go through the guidelines before uploading files.</label>
                            </div>
                            <div class="guideline">
                                <a href="#modal-guidelines" class="ths-popup-link">Guidelines</a>
                            </div>
                            <div class="register-sep"></div>
                            <div class="row blurrr">
                                    <div class="col-sm-4">
                                        <div class="img-upload-text">
                                            <p>Select Document: <span>*</span></p>
                                            <div class="upload-text">
                                                <p> (support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 10 documents)</p>
                                            </div>
                                        </div>
                                        <div class="file_upload3">
                                            <div class="upload-text-input">Drag your file here or click in this area </div>
                                            <input type="file" multiple="multiple" id="imgInp" name="UserDocuments[files]" class="UserDocuments">
                                        </div>
                                        <div class="file-list UserDocuments">
                                            <?foreach($UserDocuments as $v){?>
                                                <div class="file-list-item UserDocuments_img_block_<?=$v->id?>">
                                                    <div class="file-icon">
                                                        <img src="<?='/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v->img?>" alt="" style="width:30px;">
                                                    </div>
                                                    <div class="file-name">
                                                        <p><?=$v->img?></p>
                                                    </div>
                                                    <div class="delete-icon">
                                                        <a href="" class="delete_img" data-id="<?=$v->id?>" data-type="UserDocuments"><img src="/images/delete-icon.png" alt=""></a>
                                                    </div>
                                                    <div class="size">
                                                        <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v->img)/1000);?> кб.</p>
                                                    </div>
                                                </div>
                                            <?}?>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="row">
                                            <div class="col-sm-4"></div>
                                            <div class="col-sm-4" style="text-align: center;">
                                                <div class="error_reg" style="color:red;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn-back-step" onclick="window.location.href = '/biotransportation-client/step3'">Back</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <button class="btn-next-step register-my" data-url="biotransportation-client">Register Me</button>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
