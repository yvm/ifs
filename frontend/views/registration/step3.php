<?php
    use yii\widgets\ActiveForm;
?>
<script>
    // Indentend parent egg-donor and surrogate
    $('body').on('click', 'input[name="AditionalInformation[egg_donor_needed]"]', function (e) {

        if($(this).val() == 2){
            if($('input[name="AditionalInformation[surrogate_needed]"]:checked').val() == 2){
                $('#indented_parent_message_surrogate_donor').hide();
            }else {
                $('#indented_parent_message_surrogate_donor').show();
            }
        }else{
            $('#indented_parent_message_surrogate_donor').show();
        }
    });

    $('body').on('click', 'input[name="AditionalInformation[surrogate_needed]"]', function (e) {

        if($(this).val() == 2){
            if($('input[name="AditionalInformation[egg_donor_needed]"]:checked').val() == 2){
                $('#indented_parent_message_surrogate_donor').hide();

            }else {
                $('#indented_parent_message_surrogate_donor').show();
            }
        }else{
            $('#indented_parent_message_surrogate_donor').show();
        }
    });
</script>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap2">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-home-tab" href="/registration/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-profile-tab" href="/registration/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" id="pills-contact-tab" href="#">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="pills-contact-tab" href="/registration/step4">Step 4</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step3" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>
                                <div class="errors" style="color:red;">
<!--                                    --><?//=$form->errorSummary($AditionalInformation);?>
<!--                                    --><?//=$form->errorSummary($Consultation);?>
                                </div>
                                <div class="register-tab-title">
                                    <h3>Aditional Information</h3>
                                </div>
                                <div class="row">
                                    <div class="col-sm-5">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Reason for pursuing surrogacy and egg donation <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <?= $form->field($AditionalInformation, 'reason_surrogacy', [
                                                        'inputOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        [1 => 'Infertility', 2 => 'Secondary Infertility',3 => 'Medical Condition', 4 => 'Age', 5 => 'Other'], ['prompt' => '']
                                                    )->label(false) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-7">
                                        <div class="register-input describe" style="display: <?=$AditionalInformation->reason_surrogacy != null ? '':'none'  ?>;">
                                            <label for="">Please Describe  <span>*</span> </label> <br>
                                            <?= $form->field($AditionalInformation, 'describe')->textarea(['maxlength' => true])->label(false) ?>
                                        </div>

                                    </div>
                                    <div class="register-sep"></div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Egg donor needed? <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($AditionalInformation, 'egg_donor_needed')->radioList(
                                                    [1 => 'Yes. I require an egg donor.', 2 => 'No. I will be using our own or a friend\'s eggs.'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';

                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Surrogate needed? <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($AditionalInformation, 'surrogate_needed')->radioList(
                                                    [1 => 'Yes. I require a surrogate', 2 => 'No. I do not require a surrogate'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';

                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Timing to Begin <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <?= $form->field($AditionalInformation, 'timing_to_begin', [
                                                        'inputOptions' => ['class' => ''],
                                                    ])->dropDownList(
                                                        [1 => 'As Soon as Possible', 2 => ' Within the Next 6 Months', 3 => 'Within the Next Year', 4 => ' Not sure – I would like to learn more']
                                                    )->label(false) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6" style="display: <? if($AditionalInformation->egg_donor_needed == 2 && $AditionalInformation->surrogate_needed == 2) echo 'none';else echo '';?>" id="indented_parent_message_surrogate_donor">
                                        <div class="row">
                                            <div class="col-sm-5 ready_consultation">
                                                <div class="register-input">
                                                    <label for=""> Message to Surrogate/Egg Donor <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 ready_consultation">
                                                <div class="register-input">
                                                    <?= $form->field($AditionalInformation, 'message_to_surrogate_eggDonor')->textarea(['maxlength' => true])->label(false) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Are you financially prepared for the surrogacy and egg donation process and have reviewed
                                                        the costs involved on our <a href="/site/surrogacy#surrogacy-costs">Cost of Surrogacy</a> page and <a href="/site/egg-donation#egg-donation-costs">Cost of Egg Donation</a> page?
                                                        <span>*</span>
                                                    </label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($AditionalInformation, 'financially_prepared')->radioList(
                                                    [1 => 'Yes', 2 => 'No'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';

                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 space-top-sm">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">How did you know about us? <span>*</span></label>
                                                </div>
                                                <div class="register-input who_recommended" style="display: <?=$AditionalInformation->know_about_us == 17 ? '':'none'?>">
                                                    <label for=""> Please Describe <span>*</span></label>
                                                </div>
                                                <div class="register-input referal" style="display: <?=$AditionalInformation->know_about_us == 16 ? '':'none'?>;">
                                                    <label for="">If you selected “Referral” above, please tell us the name of the person who referred you <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <?= $form->field($AditionalInformation, 'know_about_us', [
                                                        'inputOptions' => ['class' => 'small-w'],
                                                    ])->dropDownList(
                                                        [1 => 'Google Search', 2 => 'Google Media', 3 => 'Yahoo Search', 4 => 'Bing Search', 5 => 'Facebook', 6 => 'Youtube',
                                                            7 => 'Instagram', 8 => 'Telegram', 9 => 'Twitter', 10 => 'Pinterest', 11 => 'Google+', 12 => 'Tumblr', 13 => 'VK'
                                                            , 14 => 'Ok', 15 => 'Flickr', 16 => 'Referral', 17 => 'Other']
                                                    )->label(false) ?>
                                                </div>
                                                <div class="simple-input who_recommended" style="display: <?=$AditionalInformation->know_about_us == 17 ? '':'none'?>;">
                                                    <?= $form->field($AditionalInformation, 'who_recommended')->textInput(['maxlength' => true])->label(false) ?>
                                                </div>
                                                <div class="simple-input referal" style="display: <?=$AditionalInformation->know_about_us == 16 ? '':'none'?>;">
                                                    <?= $form->field($AditionalInformation, 'referal')->textInput(['maxlength' => true])->label(false) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="register-sep"></div>
                                    <div class="col-sm-12">
                                        <div class="register-tab-title">
                                            <h3>Consultation</h3>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Are you ready to schedule a consultation? <span>*</span></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <?= $form->field($Consultation, 'ready_consultation')->radioList(
                                                    [1 => 'Yes', 2 => 'No'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';

                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                            <div class="col-sm-5 ready_consultation" <?if($Consultation->ready_consultation === null || $Consultation->ready_consultation == 2){echo 'style="display:none;"';}?>>
                                                <div class="register-input">
                                                    <label for=""> Questions & Comments</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-7 ready_consultation" <?if($Consultation->ready_consultation === null || $Consultation->ready_consultation == 2){echo 'style="display:none;"';}?>>
                                                <div class="register-input">
                                                    <?= $form->field($Consultation, 'questions_comments')->textarea(['maxlength' => true])->label(false) ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="row ready_consultation" <?if($Consultation->ready_consultation === null || $Consultation->ready_consultation == 2){echo 'style="display:none;"';}?>>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Desired Consult Location</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <select name="Consultation[location]">
                                                        <option value=""></option>
                                                        <? foreach ($desiredConsultation as $v):?>
                                                            <option value="<?=$v->id;?>" <?if($Consultation->location == $v->id) echo 'selected';?>><?=$v->name;?></option>
                                                        <? endforeach;?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <div class="register-text">
                                                    <p>We regularly schedule consults in our office in Almaty and other countries. We schedule a number of trips each year to Europe and Asia so that in-person consultations are a possibility there, too. Consultations can also be provided through video conference.</p>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="register-input">
                                                    <label for="">Desired Consult Time</label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="birth-input">
                                                    <?= $form->field($Consultation, 'time_date')->input('date')->label(false) ?>
                                                </div>
                                                <div class="register-input space-top-sm">
                                                    <select name="Consultation[time_time]">
                                                        <option value="" <?if($Consultation->time_time == null) echo 'selected';?>></option>
                                                        <? for($i=1;$i<24;$i++):?>
                                                            <option value="<?=$i?>" <?if($Consultation->time_time == $i) echo 'selected';?>><?=$i?>.00</option>
                                                        <? endfor;?>
                                                    </select>
                                                </div>
                                                <div class="register-input">
                                                    <select name="Consultation[time_from]">
                                                        <option value="" <?if($Consultation->time_from == null) echo 'selected';?>></option>
                                                        <option value="1" <?if($Consultation->time_from == 1) echo 'selected';?>>GM+2</option>
                                                        <option value="2" <?if($Consultation->time_from == 2) echo 'selected';?>>GM+3</option>
                                                        <option value="3" <?if($Consultation->time_from == 3) echo 'selected';?>>GM+4</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="col-sm-6 col-6">
                                        <button class="btn-back-step" type="button" onclick="window.location.href = '/registration/step2'">Back</button>
                                    </div>
                                    <div class="col-sm-6 col-6">
                                        <button class="btn-next-step">Next Step</button>
                                    </div>
                                </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>