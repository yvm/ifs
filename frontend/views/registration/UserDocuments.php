<?foreach($UserDocuments as $v){?>
    <div class="file-list-item UserDocuments_img_block_<?=$v->id?>">
        <div class="file-icon">
            <img src="<?='/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v->img?>" alt="" style="width:30px;">
        </div>
        <div class="file-name">
            <p><?=$v->img?></p>
        </div>
        <div class="delete-icon">
            <a href="" class="delete_img" data-id="<?=$v->id?>" data-type="UserDocuments"><img src="/images/delete-icon.png" alt=""></a>
        </div>
        <div class="size">
            <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v->img)/1000);?> кб.</p>
        </div>
    </div>
<?}?>
<div class="error-oversize"><?=$error?></div>
