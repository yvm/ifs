<?php


use bigpaulie\social\share\Share;
use yii\helpers\Url;

?>

<section class="blog">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$main->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><h3><?=$main->bigTitle?></h3></h3>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="bg-white">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-8">
                                <img src="<?=$blog->getImage();?>" class="img-fluid" alt="">
                                <div class="news-date">
                                    <div class="news-icon">
                                        <img src="/images/clock-icon.png" alt="">
                                    </div>
                                    <p><?=$blog->getDate();?></p>
                                </div>
                                <div class="news-title">
                                    <p><?=$blog->name?></p>
                                </div>
                                <div class="news-text">
                                    <?=$blog->content;?>
                                </div>
                                <div class="news-flex">
                                    <div class="tags news-text fix-width">
                                        <?=$blog->tag;?>
                                    </div>
                                    <div class="share-box">
                                        <div class="news-text space-top-sm">
                                            <p><?=$translation[0]->name?>:</p>
                                        </div>
                                        <div class="share-icons">


                                            <? $url = Yii::$app->request->hostInfo . Yii::$app->request->url;?>
                                            <a href="#" class="share-link" onclick="getVK('<?=$url?>','<?=$blog->name?>','<?=$blog->getImage();?>')"><img src="/images/share1.png" alt=""></a>
                                            <a href="#" class="share-link" onclick="getFacebook('<?=$url?>');"><img src="/images/share3.png" alt=""></a>
                                            <a href="#" class="share-link" onclick="getTwitter('<?=$blog->name?>','<?=$url?>','<?=$blog->getImage();?>')"><img src="/images/share4.png" alt=""></a>
                                           


                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <?if($blog->getDateAsTime() < $blog->getFirstDateAsTime()):?>
                                        <a href="/blog/view?id=<?=$blog->getPreviuosBlogId();?>" class="btn btn-news"><?=$translation[2]->name?></a>
                                        <? endif;?>
                                    </div>
                                    <div class="col-6">
                                        <?if($blog->getDateAsTime() > $blog->getLastDateAsTime()):?>
                                            <a href="/blog/view?id=<?=$blog->getNextBlogId();?>" class="btn btn-news"><?=$translation[3]->name?></a>
                                        <? endif;?>
                                    </div>
                                </div>
                                <a href="/blog/index" class="btn btn-back"><?=$translation[4]->name?></a>
                            </div>
                            <div class="col-lg-4">
                                <div class="archive-title">
                                    <p><?=$translation[5]->name?></p>
                                </div>
                                <div class="archive-separator">
                                </div>
                                <div class="archives-link">
                                    <? foreach ($blog->getLastBlogs() as $v):?>
                                    <a href="/blog/view?id=<?=$v->id?>"><p><?=$v->name?></p></a>
                                    <? endforeach;?>
                                </div>
                                <div class="archive-title space-top">
                                    <p><?=$translation[1]->name?></p>
                                </div>
                                <div class="archive-separator">
                                </div>
                                <div class="archives-link">
                                    <? $m=0;?>
                                    <? foreach ($archives as $v):?>
                                    <? $count = (int)substr($v,strlen($v)-2,1);?>
                                    <? $m++;?>
                                        <?if($count == 0):?>
                                        <a ><p><?=$v?></p></a>
                                        <? endif;?>

                                        <?if($count != 0):?>
                                            <a href="/blog/index?month=<?=$m?>"><p><?=$v?></p></a>
                                        <? endif;?>
                                    <? endforeach;?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>

