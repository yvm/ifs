<?php
use yii\widgets\ActiveForm;
?>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="col-12">
            <div class="register-bg">
                <div class="row">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade active show" id="pills-step6" role="tabpanel" aria-labelledby="pills-home-tab">
                        <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form_step', 'method' => 'post']]); ?>
                        <div class="errors" style="color:red;">
                            <?=$form->errorSummary($FamilyHealthHistory);?>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="register-tab-title">
                                    <p class="step8_title">Family Health History</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="education_step8">
                                    <div class="questions_13-18_step8-wrap col-sm-12 col-md-12 col-lg-6">
                                        <div class="questions_13-18_step8">
                                            <p>How many biologicaly siblings do/did you have? <span>*</span></p>
                                            <?
                                            $select = [];
                                            for($i=0;$i<11;$i++){
                                                $select[] = $i;
                                            }
                                            $select['10+'] = '10+';
                                            ?>
                                            <?= $form->field($FamilyHealthHistory, 'did_you_have', [
                                                'inputOptions' => ['class' => 'col-12 col-sm-12 col-4 family_health_select', 'id' => 'surrogate_step8_13'],
                                            ])->dropDownList(
                                                $select
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row grey_border_step8">
                            <div class="col-12">
                                <div class="education_step8">
                                    <div class="questions_13-18_step8-wrap col-sm-12 col-md-12 col-lg-6">
                                        <div class="questions_13-18_step8 ">
                                            <p>Are you adopted? <span>*</span></p>
                                            <div class="mobile_input_step8">
                                                <?= $form->field($FamilyHealthHistory, 'you_adopted')->radioList(
                                                    [1 => 'Yes', 0 => 'No'],
                                                    [
                                                        'item' => function($index, $label, $name, $checked, $value) {
                                                            if($checked == 1)
                                                                $checked = 'checked';
                                                            $return = '<label class="radio-input">'.$label;
                                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                            $return .= '<span class="checkmark"></span>';
                                                            $return .= '</label>';

                                                            return $return;
                                                        }
                                                    ]
                                                )->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="surrogate_step8_title" style="display: none">
                            <div class="col-12">
                                <div class="register-tab-title">
                                    <p class="step8_title">Family member's Information</p>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                        <div class="row desktop_step8" id="surrogate_step8_content" <?if($FamilyHealthHistory->you_adopted === null || $FamilyHealthHistory->you_adopted == 1){?>style="display: none;"<?}?>>
                            <?if(!$FamilyMemberInformation->isNewRecord){foreach($FamilyMemberInformation as $k => $v){?>
                                <div class="education_step8_2" id="FamilyMemberInformation">
                                    <form style="display: flex;" action="">
                                        <input type="hidden" value="<?=$v->id?>" class="idFamilyMemberInformation"  name="id">
                                        <div class="questions_13-18_step8">
                                            <p>Relation <span>*</span></p>
                                            <select class="col-md-12 col-sm-12 col-4 family_health_select" name="FamilyMemberInformation[relation]">
                                                <option value="Father" <?if($v->relation=='Father')echo 'selected';?>>Father</option>
                                                <option value="Mother" <?if($v->relation=='Mother')echo 'selected';?>>Mother</option>
                                                <option value="Paternal Grandfather" <?if($v->relation=='Paternal Grandfather')echo 'selected';?>> Paternal Grandfather</option>
                                                <option value="Paternal Grandmother" <?if($v->relation=='Paternal Grandmother')echo 'selected';?>> Paternal Grandmother</option>
                                                <option value="Maternal Grandfather" <?if($v->relation=='Maternal Grandfather')echo 'selected';?>>Maternal Grandfather</option>
                                                <option value="Maternal Grandmother" <?if($v->relation=='Maternal Grandmother')echo 'selected';?>>Maternal Grandmother</option>
                                                <? for ($i=1;$i<11;$i++):?>
                                                    <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" <?if($FamilyHealthHistory->did_you_have < $i){?>style="display: none;"<?}?> <?if($v->relation=='Sibling '.$i)echo 'selected';?>>Sibling <?=$i?></option>
                                                <? endfor;?>
                                            </select>
                                        </div>

                                        <div class="questions_13-18_step8_2">
                                            <p>Date of Birth <span>*</span></p>
                                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                            <link rel="stylesheet" href="/resources/demos/style.css">
                                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                            <input type="date" name="FamilyMemberInformation[date_of_birth]" value="<?=$v->date_of_birth?>">
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p>Living</p>
                                            <select class="col-12 col-sm-12 col-md-12 family_health_select" name="FamilyMemberInformation[living]">
                                                <option value="Father" <?if($v->relation==1)echo 'selected';?>>Yes</option>
                                                <option value="Mother" <?if($v->relation==0)echo 'selected';?>>No</option>
                                            </select>
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p> Body Type<span>*</span>
                                            </p>
                                            <input type="text" name="FamilyMemberInformation[body_type]" value="<?=$v->body_type?>">
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p>Other Details<span>*</span>
                                            </p>
                                            <input type="text" name="FamilyMemberInformation[other_details]" value="<?=$v->other_details?>">
                                        </div>

                                        <div class="health-details-right">
                                            <div class="save-edit-wrap">
                                                <span>Save/edit</span>
                                                <div class="save-edit-btn">
                                                    <a href="#" class="save_FamilyMemberInformation" data-id="<?=$v->id?>">
                                                        <img src="/images/save_edit.jpg" alt="save-edit">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="remove-wrap">
                                                <span>Remove</span>
                                                <div class="remove-btn">
                                                    <a href="#" class="delete_FamilyMemberInformation" data-id="<?=$v->id?>">
                                                        <img src="/images/remove.jpg" alt="delete-close">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="health-details-btn">
                                            <a href="#" class="add_relation" data-id="<?=$v->id?>">+Add relation</a>
                                        </div>
                                    </form>
                                </div>
                            <?}}else{?>
							<div class="row">
								<div class="col-12">
									<div class="register-tab-title">
										<p class="step8_title">Family member's Information</p>
									</div>
								</div>
							</div>
                                <div class="education_step8_2" id="FamilyMemberInformation0">
                                    <form style="display: flex; align-items: center;">
                                        <input type="hidden" value="" class="idFamilyMemberInformation" name="id">
                                        <div class="questions_13-18_step8">
                                            <p>Relation <span>*</span></p>
                                            <select class="col-md-12 col-sm-12 col-4 family_health_select" name="FamilyMemberInformation[relation]">
                                                <option value="Father">Father</option>
                                                <option value="Mother">Mother</option>
                                                <option value="Paternal Grandfather"> Paternal Grandfather</option>
                                                <option value="Paternal Grandmother"> Paternal Grandmother</option>
                                                <option value="Maternal Grandfather">Maternal Grandfather</option>
                                                <option value="Maternal Grandmother">Maternal Grandmother</option>
                                                <? for ($i=1;$i<11;$i++):?>
                                                    <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" style="display: none">Sibling <?=$i?></option>
                                                <? endfor;?>
                                            </select>
                                        </div>

                                        <div class="questions_13-18_step8_2">
                                            <p>Date of Birth <span>*</span></p>
                                            <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                            <link rel="stylesheet" href="/resources/demos/style.css">
                                            <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                            <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                            <input type="date" name="FamilyMemberInformation[date_of_birth]" value="">
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p>Living <span>*</span></p>
                                            <select class="col-12 col-sm-12 col-md-12 family_health_select" name="FamilyMemberInformation[living]">
                                                <option value="Father">Yes</option>
                                                <option value="Mother">No</option>
                                            </select>
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p> Body Type<span>*</span>
                                            </p>
                                            <input type="text" name="FamilyMemberInformation[body_type]" value="">
                                        </div>


                                        <div class="questions_13-18_step8_2">
                                            <p>Other Details
                                            </p>
                                            <input type="text" name="FamilyMemberInformation[other_details]" value="">
                                        </div>

                                        <div class="health-details-right">
                                            <div class="save-edit-wrap">
                                                <span>Save/edit</span>
                                                <div class="save-edit-btn">
                                                    <a href="#" class="save_FamilyMemberInformation" data-id="0">
                                                        <img src="/images/save_edit.jpg" alt="save-edit">
                                                    </a>
                                                </div>
                                            </div>

                                            <div class="remove-wrap">
                                                <span>Remove</span>
                                                <div class="remove-btn">
                                                    <a href="#" class="delete_FamilyMemberInformation" data-id="0">
                                                        <img src="/images/remove.jpg" alt="delete-close">
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="health-details-btn">
                                            <a href="#" class="add_relation" data-id="0">+Add relation</a>
                                        </div>
                                    </form>
                                </div>
                            <?}?>
                            <div class="education_step8_2 copyMy" id="" style="display: none;">
                                <form style="display: flex;">
                                    <input type="hidden" value="" class="idFamilyMemberInformation" name="id">
                                    <div class="questions_13-18_step8">
                                        <p>Relation</p>
                                        <select class="col-md-12 col-sm-12 col-4 family_health_select" name="FamilyMemberInformation[relation]">
                                            <option value="Father">Father</option>
                                            <option value="Mother">Mother</option>
                                            <option value="Paternal Grandfather"> Paternal Grandfather</option>
                                            <option value="Paternal Grandmother"> Paternal Grandmother</option>
                                            <option value="Maternal Grandfather">Maternal Grandfather</option>
                                            <option value="Maternal Grandmother">Maternal Grandmother</option>
                                            <? for ($i=1;$i<11;$i++):?>
                                                <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" style="display: none">Sibling <?=$i?></option>
                                            <? endfor;?>
                                        </select>
                                    </div>

                                    <div class="questions_13-18_step8_2">
                                        <p>Date of Birth</p>
                                        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                                        <link rel="stylesheet" href="/resources/demos/style.css">
                                        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                                        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                                        <input type="date" name="FamilyMemberInformation[date_of_birth]" value="">
                                    </div>


                                    <div class="questions_13-18_step8_2">
                                        <p>Living</p>
                                        <select class="col-12 col-sm-12 col-md-12 family_health_select" name="FamilyMemberInformation[living]">
                                            <option value="Father">Yes</option>
                                            <option value="Mother">No</option>
                                        </select>
                                    </div>


                                    <div class="questions_13-18_step8_2">
                                        <p> Body Type
                                        </p>
                                        <input type="text" name="FamilyMemberInformation[body_type]" value="">
                                    </div>


                                    <div class="questions_13-18_step8_2">
                                        <p>Other Details
                                        </p>
                                        <input type="text" name="FamilyMemberInformation[other_details]" value="">
                                    </div>

                                    <div class="health-details-right">
                                        <div class="save-edit-wrap">
                                            <span>Save/edit</span>
                                            <div class="save-edit-btn">
                                                <a href="#" class="save_FamilyMemberInformation" data-id="0">
                                                    <img src="/images/save_edit.jpg" alt="save-edit">
                                                </a>
                                            </div>
                                        </div>

                                        <div class="remove-wrap">
                                            <span>Remove</span>
                                            <div class="remove-btn">
                                                <a href="#" class="delete_FamilyMemberInformation" data-id="0">
                                                    <img src="/images/remove.jpg" alt="delete-close">
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="health-details-btn">
                                        <a href="#" class="add_relation" data-id="0">+Add relation</a>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <div class="row" id="mobile_step8">
                            <div class="col-6">
                                <div class="education_step8_2">
                                    <div class="questions_13-18_step8_2" id="step8_mobile_select">
                                        <p>Relation</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="questions_13-18_step8_2" id="step8_mobile_select">
                                    <p>Date of Birth</p>
                                    <select class="col-12 col-sm-12 col-2 family_health_select" >
                                        <option value="school">22.12.2018</option>
                                        <option value="graduited">21.12.2018</option>
                                        <option value="bachelor degree">20.12.2018</option>
                                        <option value="master degree">19.12.2018</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="questions_13-18_step8_2"  id="step8_mobile_select">
                                    <p>Living</p>
                                    <select class="col-12 col-sm-12 col-md-12 family_health_select">
                                        <option value="school">Yes</option>
                                        <option value="graduited">No</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="questions_13-18_step8_2" id="step8_mobile_body_type">
                                    <p>21. Body Type
                                    </p>
                                    <input type="text">
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="questions_13-18_step8_2" id="step8_mobile_other_details">
                                    <p>15. Other Details
                                    </p>
                                    <input type="text">
                                </div>
                            </div>
                        </div>
                        <div class="row" id="mobile_step8">
                            <div class="col-12">
                                <div class="health-details-right">
                                    <div class="save-edit-wrap">
                                        <span>Save/edit</span>
                                        <div class="save-edit-btn">
                                            <a href="#">
                                                <img src="images/save_edit.jpg" alt="save-edit">
                                            </a>
                                        </div>
                                    </div>

                                    <div class="remove-wrap">
                                        <span>Remove</span>
                                        <div class="remove-btn">
                                            <a href="#">
                                                <img src="images/remove.jpg" alt="delete-close">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <a href="#" class="mobile_add_relation">+Add relation</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-12">
                        <div class="btn_step6">
                            <button class="btn-back-step" onclick="window.location.href = '/surrogate/step7'">Back</button>
                            <button class="btn-next-step form_step">Next Step</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
