<?php
use yii\widgets\ActiveForm;
?>
<style>
    .checkbox input[type="checkbox"]{
        opacity: 0.00000001;
        width: 100%;
        height: 100%;
        z-index: 111;
        cursor: pointer;
    }
</style>
<div class="overlay">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">
            <img src="images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body length shot. Additional photos can be from past years. You may also send in photos of you with your family, friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li><p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the brightest light is behind you causing your face to be in darkness, photos where there are shadows on your face and photos that are blurry.</p></li>
                    <li><p>Sending photos of yourself with other people is fine as long as you have other photos where you are the focus and the other people in the photo are presentable. If your photo includes other people, please label who is in the photo such as “Me with Husband”.</p></li>
                    <li><p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples include photos of you hiking, biking, painting or cooking.</p></li>
                    <li><p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high resolution setting are on.</p></li>
                    <li><p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting structures behind you, and avoid taking selfies in the car.</p></li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step4" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <?php $form = ActiveForm::begin(['options' => ['class' => 'form', 'method' => 'post']]); ?>
                            <div class="errors" style="color:red;">
                                <!--                                --><?//=$form->errorSummary($PersonalHistory);?>
                                <!--                                --><?//=$form->errorSummary($MentalHealth);?>
                                <!--                                --><?//=$form->errorSummary($SexualHistory);?>
                            </div>
                            <div class="register-tab-title">
                                <p>Personal Health History</p>
                            </div>
                            <div class="row">
                                <div class="col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Are you in generally good health? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($PersonalHistory, 'good_health')->radioList(
                                                [1 => 'Yes', 0 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="row good_health" <?if($PersonalHistory->good_health == 1 || $PersonalHistory->good_health === null){?>style="display: none;"<?}?>>
                                        <div class="col-sm-5">
                                            <div class="register-input">
                                                <label for="">Please Describe <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <?= $form->field($PersonalHistory, 'good_health_describe')->textarea(['maxlength' => true])->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Have you ever been told you are infertile or do you have any fertility diagnoses such as Polycystic ovarian syndrome (PCOS), endometriosis, anovulation or irregular periods?  <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($PersonalHistory, 'have_you_ever')->radioList(
                                                [1 => 'Yes', 0 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-5">
                                    <div class="row have_you_ever" <?if($PersonalHistory->have_you_ever == 0){?>style="display: none;"<?}?>>
                                        <div class="col-sm-5">
                                            <div class="register-input">
                                                <label for="">Please Describe <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-7">
                                            <div class="register-input">
                                                <?= $form->field($PersonalHistory, 'have_you_ever_describe')->textarea(['maxlength' => true])->label(false) ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="register-input">
                                        <label for="">Blood Group  <span>*</span></label>
                                        <?= $form->field($PersonalHistory, 'blood_group', [
                                            'inputOptions' => ['class' => 'small-w'],
                                        ])->dropDownList(
                                            ['A+' => 'A+', 'A-' => 'A-', 'B+' => 'B+', 'B-' => 'B-', 'AB+' => 'AB+', 'AB-' => 'AB-', 'O+' => 'O+', 'O-' => 'O-']
                                        )->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-5 space-top-sm">
                                    <div class="register-input">
                                        <label for="">Please indicate with a check mark whether you have had any of the following <span>*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-7">
                                    <?= $form->field($PersonalHistory, 'please_indicate')->checkboxList(
                                        [
                                            'HIV (AIDS)' => 'HIV (AIDS)', 'Chlamydia' => 'Chlamydia', 'Herpes/HSV II' => 'Herpes/HSV II', 'Liver Disease' => 'Liver Disease',
                                            'Syphilis' => 'Syphilis', 'Trichomonas' => 'Trichomonas', 'Hepatitis B or C' => 'Hepatitis B or C', 'Tuberculosis' => 'Tuberculosis',
                                            'Gonorrhea' => 'Gonorrhea', 'HPV' => 'HPV', 'Other STD' => 'Other STD', 'Нет заболеваний' => 'Нет заболеваний'
                                        ],
                                        [
                                            'item' => function($index, $label, $name, $checked, $value) {
                                                if($index == 0)
                                                    $return = '<div class="row"><div class="col-sm-4">
                                                                                <div class="checkbox-input">';
                                                else if($index % 4 == 0)
                                                    $return = '</div></div><div class="col-sm-4">
                                                                                <div class="checkbox-input">';

                                                if($checked == 1)
                                                    $checked = 'checked';
                                                $return .= '<div class="checkbox">';
                                                $return .= '<input type="checkbox" name="'.$name.'" value="'.$value.'" '.$checked.'>';
                                                $return .= '<label for="checkbox"><span>'.$label.'</span></label>';
                                                $return .= '</div>';

                                                return $return;
                                            }
                                        ]
                                    )->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7 space-top">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="register-input">
                                    <label for="">Are you currently taking any medications or herbal supplements (examples include ADD/ADHD medication, allergy medicine, diet pills, etc.)? <span>*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <?= $form->field($PersonalHistory, 'you_currently')->radioList(
                                    [1 => 'Yes', 0 => 'No'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            if($checked == 1)
                                                $checked = 'checked';
                                            $return = '<label class="radio-input">'.$label;
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                            $return .= '<span class="checkmark"></span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                )->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5 space-top">
                        <div class="row you_currently" <?if($PersonalHistory->you_currently == 0){?>style="display: none;"<?}?>>
                            <div class="col-sm-5">
                                <div class="register-input">
									<label for="">Please Describe <span>*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="register-input">
                                    <?= $form->field($PersonalHistory, 'you_currently_describe')->textarea(['maxlength' => true])->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="register-input">
                                    <label for="">Have you taken any medications in the last 5 years that you are no longer taking?  <span>*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <?= $form->field($PersonalHistory, 'have_you_taken')->radioList(
                                    [1 => 'Yes', 0 => 'No'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            if($checked == 1)
                                                $checked = 'checked';
                                            $return = '<label class="radio-input">'.$label;
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                            $return .= '<span class="checkmark"></span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                )->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-5">
                        <div class="row have_you_taken" <?if($PersonalHistory->have_you_taken == 0){?>style="display: none;"<?}?>>
                            <div class="col-sm-5">
                                <div class="register-input">
                                    <label for="">Please list name of medications <span>*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-7">
                                <div class="register-input">
                                    <?= $form->field($PersonalHistory, 'please_list')->textarea(['maxlength' => true])->label(false) ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-7">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="register-input">
                                    <label for="">Are you currently taking birth control? <span>*</span></label>
                                </div>
                            </div>
                            <div class="col-sm-2">
                                <?= $form->field($PersonalHistory, 'birth_control')->radioList(
                                    [1 => 'Yes', 0 => 'No'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            if($checked == 1)
                                                $checked = 'checked';
                                            $return = '<label class="radio-input">'.$label;
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                            $return .= '<span class="checkmark"></span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                )->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 birth_control" <?if($PersonalHistory->birth_control == 0){?>style="display: none;"<?}?>>
                        <div class="register-input">
                            <label for="">What is the method of birth control? <span>*</span> </label>
                        </div>
                    </div>
                    <div class="col-sm-3 birth_control" <?if($PersonalHistory->birth_control == 0){?>style="display: none;"<?}?>>
                        <div class="register-input">
                            <label for="">Please select at least one birth control type</label>
                        </div>
                    </div>
                    <div class="col-sm-9 birth_control" <?if($PersonalHistory->birth_control == 0){?>style="display: none;"<?}?>>
                        <?= $form->field($PersonalHistory, 'method_birth_control')->checkboxList(
                            [
                                'Birth Control Pill' => 'Birth Control Pill', 'Diaphragm' => 'Diaphragm', 'Implanon' => 'Implanon',
                                'Non-Hormonal IUD' => 'Non-Hormonal IUD', 'Patch' => 'Patch', 'Spermicide' => 'Spermicide', 'Vasectomy' => 'Vasectomy',
                                'Cervical Cap' => 'Cervical Cap', 'Female Condom' => 'Female Condom', 'Male Condom' => 'Male Condom',
                                'Norplant' => 'Norplant', 'Rhythm/ovulation calendar' => 'Rhythm/ovulation calendar', 'Sponge' => 'Sponge', 'Other' => 'Other',
                                'DepoProvera Shot' => 'DepoProvera Shot', 'Hormonal IUD' => 'Hormonal IUD', 'Nexplanon' => 'Nexplanon',
                                'Nuva Ring' => 'Nuva Ring', 'Tubal Ligation' => 'Tubal Ligation', 'None' => 'None'
                            ],
                            [
                                'item' => function($index, $label, $name, $checked, $value) {
                                    if($index == 0)
                                        $return = '<div class="row"><div class="col-sm-3"></div><div class="col-sm-3">
                                                                                <div class="checkbox-input">';
                                    else if($index % 7 == 0)
                                        $return = '</div></div><div class="col-sm-3">
                                                                                <div class="checkbox-input">';

                                    if($checked == 1)
                                        $checked = 'checked';
                                    $return .= '<div class="checkbox">';
                                    $return .= '<input type="checkbox" id="checkbox'.$index.'"  name="'.$name.'" value="'.$value.'" '.$checked.'>';
                                    $return .= '<label for="checkbox"><span>'.$label.'</span></label>';
                                    $return .= '</div>';

                                    return $return;
                                }
                            ]
                        )->label(false) ?>
                    </div></div></div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-11">
                            <div class="register-input">
                                <label for="">If you are currently taking birth control, were you menstrual cycles regular before starting hormonal birth control? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <?= $form->field($PersonalHistory, 'currently_taking')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                        <div class="col-sm-11">
                            <div class="register-input">
                                <label for="">Do you ever skip menstrual periods? For example, has there ever been a time where you have had no period during a given month? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <?= $form->field($PersonalHistory, 'menstrual_periods')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                        <div class="col-sm-11">
                            <div class="register-input">
                                <label for="">Have you ever taken Depo-Provera? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <?= $form->field($PersonalHistory, 'depo_provera')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                        <div class="col-sm-12">
                            <div class="row depo_provera" <?if($PersonalHistory->depo_provera == 0){?>style="display: none;"<?}?>>
                                <div class="col-sm-6">
                                    <div class="register-input">
										<label for="">Enter date of last shot below<span>*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input">
                                        <?= $form->field($PersonalHistory, 'depo_provera_date')->input('date')->label(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-11">
                            <div class="register-input">
                                <label for="">Have you ever had an endometrial ablation procedure?<span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-1">
                            <?= $form->field($PersonalHistory, 'ablation_procedure')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                        <div class="col-sm-12 space-top-sm">
                            <div class="row ablation_procedure" <?if($PersonalHistory->ablation_procedure == 0){?>style="display: none;"<?}?>>
                                <div class="col-sm-5">
                                    <div class="register-input">
                                        <label for="">Please Describe<span>*</span></label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <?= $form->field($PersonalHistory, 'ablation_procedure_describe')->textarea(['maxlength' => true])->label(false) ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have you ever used recreational drugs (i.e. marijuana, cocaine, etc)?<span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'recreational_drugs')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row recreational_drugs"<?if($PersonalHistory->recreational_drugs == 0){?>style="display: none;"<?}?>>
                        <div class="col-sm-4">
                            <div class="register-input">
                                <label for="">Please Describe <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="register-input danger-textaread">
                                <?= $form->field($PersonalHistory, 'recreational_drugs_describe')->textarea(['maxlength' => true])->label(false) ?>
                            </div>
                            <!--                                            <div class="danger-text">-->
                            <!--                                                <p>Пожалуйста, заполните данное поле</p>-->
                            <!--                                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-5">
                    <div class="register-input">
                        <label for="">Do you currently use nicotine including smoking cigarettes/use e-cigarettes? <span>*</span></label>
                    </div>
                </div>
                <div class="col-sm-3">
                    <?= $form->field($PersonalHistory, 'smoking_cigarettes')->radioList(
                        [0 => 'No', 1 => 'Casual', 2 => 'Regular'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                if($checked == 1)
                                    $checked = 'checked';
                                $return = '<label class="radio-input">'.$label;
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                $return .= '<span class="checkmark"></span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )->label(false) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-7">
                            <div class="register-input">
                                <label for="">Do you drink alcohol? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-5">
                            <?= $form->field($PersonalHistory, 'drink_alcohol')->radioList(
                                [0 => 'No', 1 => '1-2 drinks per week', 2 => '3-7 drinks per week', 3 => 'More than 7 drinks per week'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
<!--                <div class="col-sm-4">-->
<!--                    <div class="register-input">-->
<!--                        <label for="">Please Describe <span>*</span></label>-->
<!--                    </div>-->
<!--                    <div class="register-input">-->
<!--                        --><?//= $form->field($PersonalHistory, 'smoking_cigarettes_d')->textarea(['maxlength' => true])->label(false) ?>
<!--                    </div>-->
<!--                </div>-->
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have you ever received treatment for drug/alcohol abuse? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'alcohol_abuse')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row alcohol_abuse" <?if($PersonalHistory->alcohol_abuse == 0){?>style="display: none;"<?}?>>
                        <div class="col-sm-4">
                            <div class="register-input">
                                <label for="">Please Describe <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="register-input ">
                                <?= $form->field($PersonalHistory, 'alcohol_abuse_d')->textarea(['maxlength' => true])->label(false) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have any members of your immediate family ever had any issues with drug/alcohol addiction? <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'alcohol_addiction')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row alcohol_addiction" <?if($PersonalHistory->alcohol_addiction == 0){?>style="display: none;"<?}?>>
                        <div class="col-sm-4">
                            <div class="register-input">
                                <label for="">Please Describe <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="register-input ">
                                <?= $form->field($PersonalHistory, 'alcohol_addiction_d')->textarea(['maxlength' => true])->label(false) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have you been the victim of rape and/or physical/sexual abuse <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'sexual_abuse')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row sexual_abuse" <?if($PersonalHistory->sexual_abuse == 0){?>style="display: none;"<?}?>>
                        <div class="col-sm-4">
                            <div class="register-input">
                                <label for="">Please Describe <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="register-input ">
                                <?= $form->field($PersonalHistory, 'sexual_abuse_d')->textarea(['maxlength' => true])->label(false) ?>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have you had any cancer? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'had_any_cancer')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="register-input had_any_cancer" <?if($PersonalHistory->had_any_cancer == 0){?>style="display: none;"<?}?>>
                        <label for="">Please describe below including the relative, type of cancer and age of onset <span>*</span></label>
                    </div>
                    <div class="register-input had_any_cancer" <?if($PersonalHistory->had_any_cancer == 0){?>style="display: none;"<?}?>>
                        <?= $form->field($PersonalHistory, 'had_any_cancer_d')->textarea(['maxlength' => true])->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="row">
                        <div class="col-sm-10">
                            <div class="register-input">
                                <label for="">Have you ever had a blood transfusion? <span>*</span> </label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'blood_transfusion')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="register-input">
                                <label for="">Have you ever been refused as a blood donor? <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <?= $form->field($PersonalHistory, 'blood_donor')->radioList(
                                [1 => 'Yes', 0 => 'No'],
                                [
                                    'item' => function($index, $label, $name, $checked, $value) {
                                        if($checked == 1)
                                            $checked = 'checked';
                                        $return = '<label class="radio-input">'.$label;
                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                        $return .= '<span class="checkmark"></span>';
                                        $return .= '</label>';

                                        return $return;
                                    }
                                ]
                            )->label(false) ?>
                        </div>
                        <div class="col-sm-6">
                            <div class="register-input blood_donor" <?if($PersonalHistory->blood_donor == 0){?>style="display: none;"<?}?>>
                                <label for="">Please list date(s) and reason(s): <span>*</span></label>
                            </div>
                            <div class="register-input blood_donor" <?if($PersonalHistory->blood_donor == 0){?>style="display: none;"<?}?>>
                                <?= $form->field($PersonalHistory, 'blood_donor_d')->textarea(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 area-depo-provera">
                        <div class="row">
                            <div class="col-sm-10">
                                <div class="register-input depo_area">
                                    <label for="">Have you had any hospitalizations and/or surgeries? <span>*</span> </label>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <?= $form->field($PersonalHistory, 'hospitalizations')->radioList(
                                    [1 => 'Yes', 0 => 'No'],
                                    [
                                        'item' => function($index, $label, $name, $checked, $value) {
                                            if($checked == 1)
                                                $checked = 'checked';
                                            $return = '<label class="radio-input">'.$label;
                                            $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                            $return .= '<span class="checkmark"></span>';
                                            $return .= '</label>';

                                            return $return;
                                        }
                                    ]
                                )->label(false) ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="register-input hospitalizations" <?if($PersonalHistory->hospitalizations == 0){?>style="display: none;"<?}?>>
                            <label for="">Describe below with the year each occurred: <span>*</span></label>
                        </div>
                        <div class="register-input hospitalizations" <?if($PersonalHistory->hospitalizations == 0){?>style="display: none;"<?}?>>
                            <?= $form->field($PersonalHistory, 'hospitalizations_d')->textarea(['maxlength' => true])->label(false) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 space-top">
                    <div class="register-tab-title">
                        <p>Mental Health</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <label for="">Have you ever experienced postpartum depression? <span>*</span></label>
                    </div>
                    <?= $form->field($MentalHealth, 'postpartum_depression')->radioList(
                        [1 => 'Yes', 0 => 'No'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                if($checked == 1)
                                    $checked = 'checked';
                                $return = '<label class="radio-input">'.$label;
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                $return .= '<span class="checkmark"></span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <label for="">Have you ever been diagnosed with an emotional condition or illness? <span>*</span></label>
                    </div>
                    <?= $form->field($MentalHealth, 'condition_or_illness')->radioList(
                        [1 => 'Yes', 0 => 'No'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                if($checked == 1)
                                    $checked = 'checked';
                                $return = '<label class="radio-input">'.$label;
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                $return .= '<span class="checkmark"></span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <label for="">Have you ever attempted suicide? <span>*</span> </label>
                    </div>
                    <?= $form->field($MentalHealth, 'attempted_suicide')->radioList(
                        [1 => 'Yes', 0 => 'No'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                if($checked == 1)
                                    $checked = 'checked';
                                $return = '<label class="radio-input">'.$label;
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                $return .= '<span class="checkmark"></span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <label for="">Have you ever been treated by a mental health professional? <span>*</span> </label>
                    </div>
                    <?= $form->field($MentalHealth, 'health_professional')->radioList(
                        [1 => 'Yes', 0 => 'No'],
                        [
                            'item' => function($index, $label, $name, $checked, $value) {
                                if($checked == 1)
                                    $checked = 'checked';
                                $return = '<label class="radio-input">'.$label;
                                $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                $return .= '<span class="checkmark"></span>';
                                $return .= '</label>';

                                return $return;
                            }
                        ]
                    )->label(false) ?>
                </div>
                <div class="offset-lg-6 col-sm-6 space-top-sm health_professional" <?if($MentalHealth->health_professional == 0){?>style="display: none;"<?}?>>
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="register-input">
                                <label for="">Please Describe <span>*</span></label>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="register-input">
                                <?= $form->field($MentalHealth, 'describe')->textarea(['maxlength' => true])->label(false) ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 space-top">
                    <div class="register-tab-title">
                        <p>Sexual History</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <?
                        $array = [];
                        for($i = 0;$i <=50;$i++){
                            $array[$i] = $i;
                        }
                        $array['100'] = '50 +';
                        ?>
                        <label for="">How many sexual partners have you had in the last 3 years? <span>*</span> </label>
                        <?= $form->field($SexualHistory, 'many_sexual_partners', [
                            'inputOptions' => ['class' => 'small-w'],
                        ])->dropDownList(
                            $array
                        )->label(false) ?>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="register-input">
                        <label for="">Are you currently sexually active? <span>*</span> </label>
                        <?= $form->field($SexualHistory, 'sexually_active')->radioList(
                            [1 => 'Yes', 0 => 'No'],
                            [
                                'item' => function($index, $label, $name, $checked, $value) {
                                    if($checked == 1)
                                        $checked = 'checked';
                                    $return = '<label class="radio-input">'.$label;
                                    $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                    $return .= '<span class="checkmark"></span>';
                                    $return .= '</label>';

                                    return $return;
                                }
                            ]
                        )->label(false) ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-6">
                    <button class="btn-back-step" onclick="window.location.href = '/surrogate/step3'">Back</button>
                </div>
                <div class="col-sm-6 col-6">
                    <button class="btn-next-step">Next Step</button>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
</div>
</div>
</div>