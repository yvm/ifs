<?php
use yii\widgets\ActiveForm;
?>
<div class="overlay">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">
            <img src="/images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you
                    facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body
                    length shot. Additional photos can be from past years. You may also send in photos of you with your family,
                    friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li>
                        <p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the
                            brightest light is behind you causing your face to be in darkness, photos where there are shadows on your
                            face and photos that are blurry.</p>
                    </li>
                    <li>
                        <p>Sending photos of yourself with other people is fine as long as you have other photos where you are the
                            focus and the other people in the photo are presentable. If your photo includes other people, please label
                            who is in the photo such as “Me with Husband”.</p>
                    </li>
                    <li>
                        <p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting
                            your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests
                            and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples
                            include photos of you hiking, biking, painting or cooking.</p>
                    </li>
                    <li>
                        <p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high
                            resolution setting are on.</p>
                    </li>
                    <li>
                        <p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting
                            structures behind you, and avoid taking selfies in the car.</p>
                    </li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab"
                                   aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab"
                                   aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>

                        <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form_step', 'method' => 'post']]); ?>
                             <input type="hidden" name="end_step">
                        <?php ActiveForm::end(); ?>

                        <div class="health-details-wrap">

                            <div class="row">

                                <div class="col-12">
                                    <div class="health-details-title">
                                        <h1>Health Details</h1>
                                        <h2>Disease Details</h2>
                                    </div>
                                </div>

                            </div>

                            <?
                                $array = ['Leukemia', 'Immune Deficiency+', 'Thalassemia', 'Sickle Cell Anemia', 'Heart Attack', 'High Blood Pressure', 'High Cholesterol', 'Lung Cancer',
                                    'Cystic Fibrosis', 'Hepatitis A (infection)', 'Hepatitis B (serum)', 'Hepatitis C', 'Ulcer Active Colitis', 'Colon Cancer', 'Crohns Disease',
                                    'Intestinal Cancer', 'Other Gastrointestinal Disorders', 'Tay Sach’s Disease', 'Diabetes Type 1 (juvenile onset)', 'Diabetes Type 2 (Adult Onset)',
                                    'Thyroid Disease', 'Thyroid Cancer', 'Adrenal Dysfunction', 'Other Endocrine Disorder', 'Bladder Cancer', 'Kidney Disease',
                                    'Prostate Cancer (Skene\'s Gland)', 'Uterine Fibroids', 'Cancer of Cervix / Ovaries / Uterus', 'Endometriosis', 'Breast Cancer', 'Hypospadias',
                                    'Other Reproductive Disorders', 'Cleft Lip / Palate', 'Chromosome Problems', 'Down Syndrome', 'Trisomy 13 or 18', 'Fragile X Syndrome',
                                    'Turners Syndrome', 'Huntington Disease', 'Dwarfism', 'Other Congenital Anomalies', 'Rheumatoid Arthritis', 'Scoliosis', 'Osteoporosis',
                                    'Muscular Dystrophy', 'Lupus', 'Gout', 'Other Muscle / Bone / Joint Disorders', 'Mental Retardation', 'Multiple Sclerosis', 'Epilepsy / Seizures',
                                    'Spinal Cord Disorders', 'Other Neurological Disorders', 'Schizophrenia', 'Depression', 'Bipolar', 'ADHD / ADD', 'Nervous Breakdown',
                                    'Suicide Attempt', 'Mental disorders requiring hospitalizations', 'Use of Psych Medications', 'Anxiety Disorder / Panic Attacks',
                                    'Psychotherapy / counseling', 'Other Mental Health Disorders', 'Eczema', 'Melanoma', 'Non melanoma Skin Cancer', 'Pigmentation Disorders',
                                    'Other Skin Disorders', 'Significant Hearing Loss', 'Deafness before 60', 'Blindness', 'Glaucoma', 'Cataracts before 50', 'Alcoholism',
                                    'Drug Abuse or Addiction', 'Eating disorders', 'Phobia (s)', 'Any Cancer Not Listed', 'Other Disorders Not Listed'];
                            ?>

                            <?foreach($array as $v){?>
                                <!-- immune deficiency 1 -->
                                <div class="immune-deficiency-wrap">
                                    <div class="row">
                                            <div class="col-lg-4 col md-4">
                                                <div class="health-details-left">
                                                    <div class="immune-deficiency-title">
                                                        <h3><?=$v?></h3>
                                                    </div>

                                                    <div class="health-details-btn">
                                                        <a href="#" class="add_relation_step9" data-id="<?=$v?>">+Add relation</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <?$type_isset = 0; foreach($HealthDetails as $val){?>
                                                <?if($val->type == $v){ $type_isset++;?>
                                                    <?if($type_isset != 1){?>
                                                        <div class="offset-lg-4 offset-md-4"></div>
                                                    <?}?>
                                                    <form style="display: flex;" <?if($type_isset == 1){?>class="parent"<?}?>>
                                                    <input type="hidden" value="<?=$v?>" name="HealthDetails[type]">
                                                    <input type="hidden" value="<?=$val->id?>" class="HealthDetails" name="id">
                                                    <!-- right 1 -->
                                                    <div class="row">
                                                        <div class="col-lg-5 col-md-5">
                                                            <div class="health-details-center">
                                                                <div class="relation">
                                                                    <!-- 1 -->
                                                                    <div class="health-details-select">
                                                                        <h3>Relation:</h3>
                                                                        <select name="HealthDetails[relation]">
                                                                            <option value="Father" <?if($val->relation=='Father')echo 'selected';?>>Father</option>
                                                                            <option value="Mother" <?if($val->relation=='Mother')echo 'selected';?>>Mother</option>
                                                                            <option value="Paternal Grandfather" <?if($val->relation=='Paternal Grandfather')echo 'selected';?>> Paternal Grandfather</option>
                                                                            <option value="Paternal Grandmother" <?if($val->relation=='Paternal Grandmother')echo 'selected';?>> Paternal Grandmother</option>
                                                                            <option value="Maternal Grandfather" <?if($val->relation=='Maternal Grandfather')echo 'selected';?>>Maternal Grandfather</option>
                                                                            <option value="Maternal Grandmother" <?if($val->relation=='Maternal Grandmother')echo 'selected';?>>Maternal Grandmother</option>
                                                                            <? for ($i=1;$i<11;$i++):?>
                                                                                <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" <?if($FamilyHealthHistory->did_you_have < $i){?>style="display: none;"<?}?> <?if($val->relation=='Sibling '.$i)echo 'selected';?>>Sibling <?=$i?></option>
                                                                            <? endfor;?>
                                                                        </select>
                                                                    </div>
                                                                    <!-- end 1 -->
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-5 col-md-5">
                                                            <div class="health-details-center">
                                                                <div class="details">
                                                                    <!-- 1 -->
                                                                    <div class="health-details-select">
                                                                        <h3>details</h3>
                                                                        <input type="text" name="HealthDetails[details]" value="<?=$val->details?>">
                                                                    </div>
                                                                    <!-- end 1 -->
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-lg-2 col-md-2">
                                                            <div class="health-details-right">
                                                                <div class="save-edit-wrap">
                                                                    <span>Save/edit</span>
                                                                    <div class="save-edit-btn">
                                                                        <a href="#" class="save_edit_step9">
                                                                            <img src="/images/save-edit.svg" alt="save-edit">
                                                                        </a>
                                                                    </div>
                                                                </div>

                                                                <div class="remove-wrap">
                                                                    <span>Remove</span>
                                                                    <div class="remove-btn">
                                                                        <a href="#" class="delete_edit_step9" data-id="<?=$val->id?>">
                                                                            <img src="/images/delete-close.svg" alt="delete-close">
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                                <?}?>
                                            <?}?>
                                        <?if($type_isset == 0){?>
                                            <form style="display: flex;">
                                                <input type="hidden" value="<?=$v?>" name="HealthDetails[type]">
                                                <input type="hidden" value="" class="HealthDetails" name="id">
                                                <!-- right 1 -->
                                                <div class="row">
                                                    <div class="col-lg-5 col-md-5">
                                                        <div class="health-details-center">
                                                            <div class="relation">
                                                                <!-- 1 -->
                                                                <div class="health-details-select">
                                                                    <h3>Relation:</h3>
                                                                    <select name="HealthDetails[relation]">
                                                                        <option value="Father">Father</option>
                                                                        <option value="Mother">Mother</option>
                                                                        <option value="Paternal Grandfather"> Paternal Grandfather</option>
                                                                        <option value="Paternal Grandmother"> Paternal Grandmother</option>
                                                                        <option value="Maternal Grandfather">Maternal Grandfather</option>
                                                                        <option value="Maternal Grandmother">Maternal Grandmother</option>
                                                                        <? for ($i=1;$i<11;$i++):?>
                                                                            <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" <?if($FamilyHealthHistory->did_you_have < $i){?>style="display: none;"<?}?>>Sibling <?=$i?></option>
                                                                        <? endfor;?>
                                                                    </select>
                                                                </div>
                                                                <!-- end 1 -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-5 col-md-5">
                                                        <div class="health-details-center">
                                                            <div class="details">
                                                                <!-- 1 -->
                                                                <div class="health-details-select">
                                                                    <h3>details</h3>
                                                                    <input type="text" name="HealthDetails[details]">
                                                                </div>
                                                                <!-- end 1 -->
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="col-lg-2 col-md-2">
                                                        <div class="health-details-right">
                                                            <div class="save-edit-wrap">
                                                                <span>Save/edit</span>
                                                                <div class="save-edit-btn">
                                                                    <a href="#" class="save_edit_step9">
                                                                        <img src="/images/save-edit.svg" alt="save-edit">
                                                                    </a>
                                                                </div>
                                                            </div>

                                                            <div class="remove-wrap">
                                                                <span>Remove</span>
                                                                <div class="remove-btn">
                                                                    <a href="#" class="delete_edit_step9">
                                                                        <img src="/images/delete-close.svg" alt="delete-close">
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        <?}?>
                                        <!-- end right 1 -->
                                    </div>
                                </div>
                                <!-- end immune deficiency 1 -->
                            <?}?>
                            <!-- button -->
                            <div class="row">

                                <div class="col-12">
                                    <div class="character-register-button">
                                        <button class="btn-back-step" onclick="window.location.href = '/surrogate/step8'">Back</button>
                                        <button class="btn-next-step form_step">Next Step</button>
                                    </div>
                                </div>

                            </div>
                            <!-- end button -->

                        </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- right 2 -->
<div class="copyMy" style="display: none;">
    <div class="offset-lg-4 offset-md-4"></div>
    <form style="display: flex;">
        <input type="hidden" value="<?=$v?>" name="HealthDetails[type]">
        <input type="hidden" value="" class="HealthDetails" name="id">
        <div class="row">
            <div class="col-lg-5 col-md-5">
                <div class="health-details-center">
                    <div class="relation">
                        <!-- 1 -->
                        <div class="health-details-select">
                            <h3>16. Relation:</h3>
                            <select id="" name="HealthDetails[relation]">
                                <option value="Father">Father</option>
                                <option value="Mother">Mother</option>
                                <option value="Paternal Grandfather"> Paternal Grandfather</option>
                                <option value="Paternal Grandmother"> Paternal Grandmother</option>
                                <option value="Maternal Grandfather">Maternal Grandfather</option>
                                <option value="Maternal Grandmother">Maternal Grandmother</option>
                                <? for ($i=1;$i<11;$i++):?>
                                    <option value="Sibling <?=$i?>" class="surrogate_step8_select_<?=$i?>" <?if($FamilyHealthHistory->did_you_have < $i){?>style="display: none;"<?}?>>Sibling <?=$i?></option>
                                <? endfor;?>
                            </select>
                        </div>
                        <!-- end 1 -->
                    </div>
                </div>
            </div>

            <div class="col-lg-5 col-md-5">
                <div class="health-details-center">
                    <div class="details">
                        <!-- 1 -->
                        <div class="health-details-select">
                            <h3>details</h3>
                            <input type="text" name="HealthDetails[details]">
                        </div>
                        <!-- end 1 -->
                    </div>
                </div>
            </div>

            <div class="col-lg-2 col-md-2">
                <div class="health-details-right">
                    <div class="save-edit-wrap">
                        <span>Save/edit</span>
                        <div class="save-edit-btn">
                            <a href="#" class="save_edit_step9">
                                <img src="/images/save-edit.svg" alt="save-edit">
                            </a>
                        </div>
                    </div>

                    <div class="remove-wrap">
                        <span>Remove</span>
                        <div class="remove-btn">
                            <a href="#" class="delete_edit_step9">
                                <img src="/images/delete-close.svg" alt="delete-close">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<!-- end right 2 -->
