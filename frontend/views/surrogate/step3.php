<?php

use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
?>
<div class="overlay">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">
            <img src="images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body length shot. Additional photos can be from past years. You may also send in photos of you with your family, friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li><p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the brightest light is behind you causing your face to be in darkness, photos where there are shadows on your face and photos that are blurry.</p></li>
                    <li><p>Sending photos of yourself with other people is fine as long as you have other photos where you are the focus and the other people in the photo are presentable. If your photo includes other people, please label who is in the photo such as “Me with Husband”.</p></li>
                    <li><p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples include photos of you hiking, biking, painting or cooking.</p></li>
                    <li><p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high resolution setting are on.</p></li>
                    <li><p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting structures behind you, and avoid taking selfies in the car.</p></li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step4" role="tabpanel" aria-labelledby="pills-contact-tab">
                            <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>
                            <div class="errors" style="color:red;">
<!--                                --><?//=$form->errorSummary($SurrogateAvailability);?>
<!--                                --><?//=$form->errorSummary($PregnanciesAndSurrogacies);?>
                            </div>
                            <div class="register-tab-title">
                                <h3>General Information</h3>
                                <p>Personal Information</p>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="label-flexa">
                                        <div class="register-input space-top-sm">
                                            <label for="">Height (sm) <span>*</span></label>
                                        </div>
                                        <div class="input-range">
                                            <div class="top-one">
                                                <input type="text" id="textInput" value="<?if($PersonalInfo->height) echo $PersonalInfo->height;else echo 185?>" name="PersonalInfo[height]">
                                            </div>
                                            <div class="flex-one">
                                                <label for="range-input">150</label><input type="range" value="<?if($PersonalInfo->height) echo $PersonalInfo->height;else echo 185?>" class="range-input textInput" name="rangeInput" min="150" max="220" onchange="updateTextInput(this.value);"><label
                                                        for="range-input">220</label>
                                            </div>
                                        </div>
                                        <div class="measurement space-top-sm">
                                            <a class="height-ft"><?=round((185/30.48), 1)?> ft</a> <a class="height-inch"><?=round((185/2.54), 1)?> inch</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="label-flexa">
                                        <div class="register-input space-top-sm">
                                            <label for="">Weight (kg) <span>*</span></label>
                                        </div>
                                        <div class="input-range">
                                            <div class="top-one">
                                                <input type="text" id="textInput2" value="<?if($PersonalInfo->weight) echo $PersonalInfo->weight;else echo 92?>" name="PersonalInfo[weight]">
                                            </div>
                                            <div class="flex-one">
                                                <label for="range-input">33</label><input type="range" value="<?if($PersonalInfo->weight) echo $PersonalInfo->weight;else echo 92?>" class="range-input textInput2" name="rangeInput" min="33" max="150" onchange="updateTextInput2(this.value);"><label
                                                        for="range-input">150</label>
                                            </div>
                                        </div>
                                        <div class="measurement space-top-sm">
                                            <a class="lb-weight"><?=round((92 / 0.453592), 1)?> lb.</a> <a class="stone-weight"><?=round((92 / 6.35029), 1)?> stone</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="register-sep"></div>
                                <div class="col-sm-12">
                                    <div class="register-tab-title">
                                        <p>Surrogate Availability</p>
                                    </div>
                                </div>
                                <div class="col-sm-6 border-right">
                                    <div class="row">

                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Would you allow the intended parents to attend doctor's appointments and be present during the birth? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($SurrogateAvailability, 'would_you_allow')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
<!--                                            <label class="radio-input">Yes-->
<!--                                                <input type="radio" name="SurrogateAvailability[would_you_allow]" --><?//if($SurrogateAvailability->would_you_allow==1)echo 'checked="checked"';?><!-- value="1">-->
<!--                                                <span class="checkmark"></span>-->
<!--                                            </label>-->
<!--                                            <label class="radio-input"> No-->
<!--                                                <input type="radio" name="SurrogateAvailability[would_you_allow]" --><?//if($SurrogateAvailability->would_you_allow==2)echo 'checked="checked"';?><!-- value="2">-->
<!--                                                <span class="checkmark"></span>-->
<!--                                            </label>-->
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">I am willing as a Surrogate to travel to an intended parent(s) clinic that would require an overnight stay of 51 days or more <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($SurrogateAvailability, 'i_am_willing_as')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Can you travel abroad? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <?= $form->field($SurrogateAvailability, 'can_you_travel')->radioList(
                                                [1 => 'Yes', 2 => 'No', 3 => 'I can for 2-3 days', 4 => 'I can for 2 weeks', 5 => 'I can for a month or more', 6 => 'Needs to be discussed separately'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Do you have passport to travel abroad <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($SurrogateAvailability, 'have_passport')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>

                                        </div>
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Are you willing to self inject medications with proper instruction for a period of 1-4 weeks for my participation in a cycle sync? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($SurrogateAvailability, 'willing_to_self_in_cycle_sync')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="register-sep"></div>
                                <div class="col-sm-12">
                                    <div class="register-tab-title">
                                        <p> Previous Pregnancies and Surrogacies</p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Are you pregnant now? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($PregnanciesAndSurrogacies, 'are_you_pregnant')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                        <div class="col-sm-10">
                                            <div class="register-input">
                                                <label for="">Are you currently breastfeeding? <span>*</span></label>
                                            </div>
                                        </div>
                                        <div class="col-sm-2">
                                            <?= $form->field($PregnanciesAndSurrogacies, 'are_you_currently')->radioList(
                                                [1 => 'Yes', 2 => 'No'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';
                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="register-input">
                                                <label for="">
                                                    Have you ever been a surrogate before?  <span>*</span>
                                                    <span class="hover-img">
                                                            <img src="/images/info-img.png" alt="">
                                                            <span>
                                                                0 means Never
                                                            </span>
                                                       </span>
                                                </label> <br>
                                                <?= $form->field($PregnanciesAndSurrogacies, 'have_you_ever')->dropDownList(
                                                    [
                                                        0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                        8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                                    ],
                                                    ['class'=>'small-w have_you_ever'])->label(false);
                                                ?>

                                            </div>
                                            <div class="simple-input name_of_IVF" <?if($PregnanciesAndSurrogacies->have_you_ever==0)echo 'style="display:none;"';?>>
                                                <label for="">Name of IVF Clinic <span>*</span></label> <br>
                                                <?= $form->field($PregnanciesAndSurrogacies, 'name_of_IVF')->textInput(['maxlength' => true])->label(false) ?>
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="col-sm-7">
                                    <div class="register-input">
                                        <label for="">
                                            Number of Pregnancies <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'number_pregnancies')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            ['class'=>'small-w number_pregnancies'])->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6 ">
                                    <?
                                        if($PregnanciesAndSurrogacies->number_pregnancies == 0 || $PregnanciesAndSurrogacies->number_pregnancies === null)
                                            $disabled = ['class'=>'small-w', 'disabled' => 'disabled'];
                                        else
                                            $disabled = ['class'=>'small-w'];
                                    ?>
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?> space-top no-space" >
                                        <label for="">
                                            How many children have you given birth to?  <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'how_many_children')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>  space-top no-space">
                                        <label for="">
                                            How many children do you have now<span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'children_now')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?> ">
                                        <label for="">
                                            Number of Miscarriage  <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'number_miscarriages')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?> ">
                                        <label for="">
                                            Number of Abortions  <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'number_abortions')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?> ">
                                        <label for="">
                                            Number of Stillbirths  <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'number_stillbirths')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="register-input <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?> ">
                                        <label for="">
                                            Number of Live Births  <span>*</span>
                                        </label> <br>
                                        <?= $form->field($PregnanciesAndSurrogacies, 'number_live_births')->dropDownList(
                                            [
                                                0 => 0, 1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7,
                                                8 => 8, 9 => 9, 10 => 10, 100 => "10+"
                                            ],
                                            $disabled)->label(false);
                                        ?>
                                    </div>
                                </div>
                                <?
                                    if($PregnanciesAndSurrogacies->number_pregnancies == 0 || $PregnanciesAndSurrogacies->number_pregnancies === null)
                                        $disabled = ['maxlength' => true, 'disabled' => 'disabled'];
                                    else
                                        $disabled = ['maxlength' => true];
                                ?>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input space-top no-space">
                                        <label for="">List each of your delivery dates, whether they were vaginal or caesarean, how many weeks you delivered at, and the weight of each baby </label>
                                    </div>
                                </div>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input space-top no-space">
                                        <?= $form->field($PregnanciesAndSurrogacies, 'list_each')->textarea($disabled)->label(false) ?>

                                    </div>
                                </div>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input">
                                        <label for="">If any of your children are deceased, please state cause of death and the age of child at death</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input">
                                        <textarea <?if($PregnanciesAndSurrogacies->number_pregnancies == 0 || $PregnanciesAndSurrogacies->number_pregnancies === null)echo 'disabled="disabled;"'?> name="PregnanciesAndSurrogacies[if_any_of_your]" id="" cols="30" rows="10"><?=$PregnanciesAndSurrogacies->if_any_of_your?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input">
                                        <label for="">Please list any additional reproductive history (Birth control issues, regular cysts, endometriosis, issues getting pregnant, complications during pregnancy etc.</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 <?if($PregnanciesAndSurrogacies->number_pregnancies==0) echo 'blur';else echo 'blurr'?>">
                                    <div class="register-input">
                                        <textarea <?if($PregnanciesAndSurrogacies->number_pregnancies == 0 || $PregnanciesAndSurrogacies->number_pregnancies === null)echo 'disabled="disabled;"'?> name="PregnanciesAndSurrogacies[please_list]" id="" cols="30" rows="10"><?=$PregnanciesAndSurrogacies->please_list?></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6 col-6">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/surrogate/step2'">Back</button>
                                </div>
                                <div class="col-sm-6 col-6">
                                    <button class="btn-next-step">Next Step</button>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
