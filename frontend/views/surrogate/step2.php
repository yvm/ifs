<?php
use yii\widgets\ActiveForm;
?>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step5">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/egg-donor/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/egg-donor/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-step2" role="tabpanel" aria-labelledby="pills-profile-tab">
                            <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>
                            <div class="errors" style="color:red;">
                                <!--                                    --><?//=$form->errorSummary($profile);?>
                                <!--                                    --><?//=$form->errorSummary($PersonalInfo);?>
                            </div>
                            <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>"/>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="register-tab-title">
                                        <h3>  General Information</h3>
                                    </div>
                                    <div class="tab-text">
                                        <?
                                            $role = '';
                                            if($user->role == 1)
                                                $role = "Intended Parent";
                                            if($user->role == 2)
                                                $role = "Surrogate";
                                            if($user->role == 3)
                                                $role = "Egg Donor";
                                            if($user->role == 4)
                                                $role = "Surrogate & Egg Donor";
                                            if($user->role == 5)
                                                $role = "Biotransportation Client";
                                        ?>
                                        <p>Please complete the form below to be considered for our <?=$role?> program. We thank you for your time.</p>
                                        <p>Fields with * are mandatory</p>
                                    </div>
                                    <div class="register-tab-title">
                                        <p>Basic Information </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="firstname">First Name <span>*</span></label> <br>
                                        <?= $form->field($profile, 'first_name')->textInput(['placeholder' => 'Arthur'])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="firstname">Last Name <span>*</span></label> <br>
                                        <?= $form->field($profile, 'last_name')->textInput(['placeholder' => 'Smith'])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="firstname">E-mail <span>*</span> </label> <br>
                                        <?= $form->field($profile, 'email')->textInput(['placeholder' => 'a_smith@gmail'])->label(false) ?>
                                    </div>
                                </div>
                                <div class="register-sep"></div>
                                <div class="col-sm-12">
                                    <div class="register-tab-title">
                                        <p>Personal  Information </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="register-input">
                                                <label for="">Gender <span>*</span> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($PersonalInfo, 'gender')->radioList(
                                                [1 => 'Male', 2 => 'Female'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                        <div class="col-sm-6 space-top-sm">
                                            <div class="register-input">
                                                <label for="">Marital Status <span>*</span> </label>
                                            </div>
                                        </div>
                                        <div class="col-sm-6 space-top-sm">
                                            <?= $form->field($PersonalInfo, 'marital_status')->radioList(
                                                [1 => 'Married', 2 => 'Single'],
                                                [
                                                    'item' => function($index, $label, $name, $checked, $value) {
                                                        if($checked == 1)
                                                            $checked = 'checked';
                                                        $return = '<label class="radio-input">'.$label;
                                                        $return .= '<input type="radio" name="' . $name . '" value="' . $value . '" '.$checked.'>';
                                                        $return .= '<span class="checkmark"></span>';
                                                        $return .= '</label>';

                                                        return $return;
                                                    }
                                                ]
                                            )->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="">Date of Birth <span>*</span></label>
                                        <div class="birth-input">
                                            <?= $form->field($PersonalInfo, 'date_of_birth')->input('date', ['placeholder' => '08.01.1991'])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="register-input">
                                        <label for="">Citizenship <span>*</span></label> <br>
                                        <?= $form->field($PersonalInfo, 'citizenship')->dropDownList(
                                            $countries_arr
                                        )->label(false) ?>
                                    </div>
                                    <div class="delete-input">
                                        <label for="">Zip/Postal Code <span>*</span></label> <br>
                                        <?= $form->field($PersonalInfo, 'zip_postal_code')->textInput(['placeholder' => '13532532'])->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="">Country <span>*</span></label> <br>
                                        <?= $form->field($PersonalInfo, 'country')->dropDownList(
                                            $countries_arr
                                        )->label(false) ?>
                                    </div>
                                    <div class="simple-input country_state" <?if($PersonalInfo->country != 152){?>style="display: none;"<?}?>>
                                        <label for="">State</label> <br>
                                        <?= $form->field($PersonalInfo, 'state')->dropDownList(
                                            $region_arr
                                        )->label(false) ?>
                                    </div>
                                    <div class="register-input">
                                        <label for="">City <span>*</span></label> <br>
                                        <?= $form->field($PersonalInfo, 'city')->dropDownList(
                                            $city_arr
                                        )->label(false) ?>
                                    </div>
                                    <div class="register-input">
                                        <label for="">Address <span>*</span></label> <br>
                                        <?= $form->field($PersonalInfo, 'address')->textInput()->label(false) ?>
                                    </div>
                                    <!--                                        <div class="simple-input">-->
                                    <!--                                            <label for="">State</label> <br>-->
                                    <!--                                            <input type="text">-->
                                    <!--                                        </div>-->
                                </div>
                                <div class="register-sep"></div>
                                <div class="col-sm-12">
                                    <div class="register-tab-title">
                                        <p>Contact Information </p>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="register-input">
                                        <label for="mobile-number">Mobile Contact Number <span>*</span></label> <br>
                                        <?= $form->field($profile, 'mobile')->textInput(['placeholder' => '8 (702) 352 34 32'])->label(false) ?>
                                    </div>
                                    <div class="desktop-version">
                                        <div class="simple-input">
                                            <label for="">Telegram  </label> <br>
                                            <?= $form->field($profile, 'telegram')->textInput()->label(false) ?>
                                        </div>
                                        <div class="simple-input">
                                            <label for="">Facebook   </label> <br>
                                            <?= $form->field($profile, 'facebook')->textInput()->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="mobile-version">
                                        <div class="mobile-social-input">
                                            <div class="soc-icon">
                                                <img src="images/soc1.png" alt="">
                                            </div>
                                            <input type="text">
                                        </div>
                                        <div class="mobile-social-input">
                                            <div class="soc-icon">
                                                <img src="images/soc2.png" alt="">
                                            </div>
                                            <input type="text">
                                        </div>
                                        <div class="mobile-social-input">
                                            <div class="soc-icon">
                                                <img src="images/soc3.png" alt="">
                                            </div>
                                            <input type="text">
                                        </div>
                                        <div class="mobile-social-input">
                                            <div class="soc-icon">
                                                <img src="images/soc4.png" alt="">
                                            </div>
                                            <input type="text">
                                        </div>
                                        <div class="mobile-social-input">
                                            <div class="soc-icon">
                                                <img src="images/soc5.png" alt="">
                                            </div>
                                            <input type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4 desktop-version">
                                    <div class="simple-input">
                                        <label for="">Work Contact Number</label> <br>
                                        <?= $form->field($profile, 'work_contact')->textInput()->label(false) ?>
                                    </div>
                                    <div class="simple-input">
                                        <label for="">Twitter</label> <br>
                                        <?= $form->field($profile, 'twitter')->textInput()->label(false) ?>
                                    </div>
                                    <div class="simple-input">
                                        <label for="">VK  </label> <br>
                                        <?= $form->field($profile, 'vk')->textInput()->label(false) ?>
                                    </div>
                                </div>
                                <div class="col-sm-4 desktop-version">
                                    <div class="simple-input">
                                        <label for="">Home Contact Number </label> <br>
                                        <?= $form->field($profile, 'home_contact')->textInput()->label(false) ?>
                                    </div>
                                    <div class="simple-input">
                                        <label for="">Skype</label> <br>
                                        <?= $form->field($profile, 'skype')->textInput()->label(false) ?>
                                    </div>
                                    <div class="simple-input">
                                        <label for="">WhatsApp  </label> <br>
                                        <?= $form->field($profile, 'vatzup')->textInput()->label(false) ?>
                                    </div>
                                    <div class="simple-input">
                                        <label for="">lnstagram  </label> <br>
                                        <?= $form->field($profile, 'instagram')->textInput()->label(false) ?>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6 col-6">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/egg-donor/step1'">Back</button>
                                </div>
                                <div class="col-sm-6 col-6">
                                    <button class="btn-next-step">Next Step</button>
                                </div>
                            </div>
                            <?php ActiveForm::end(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>