<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.05.2019
 * Time: 15:35
 */
?>
<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.05.2019
 * Time: 13:18
 */

use yii\widgets\ActiveForm; ?>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="col-12">
            <div class="register-bg">
                <div class="row">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="/surrogate/step1" role="tab" aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step2" role="tab" aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step3" role="tab" aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step4" role="tab" aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step5" role="tab" aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="/surrogate/step6" role="tab" aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step7" role="tab" aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step8" role="tab" aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="/surrogate/step9" role="tab" aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="/surrogate/step10" role="tab" aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="tab-content" id="pills-tabContent">
                    <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>

                    <div class="tab-pane fade active show" id="pills-step6" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="register-tab-title">
                                    <h1 style="font-family: roboto-light;font-size: 36px;">Personal Characteristics</h1>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                <div class="register-tab-title">
                                    <h2 style="font-family: roboto-light;font-size: 24px;">Traits and Characteristics</h2>
                                </div>
                            </div>

                            <div class="errors" style="color:red;">
<!--                                --><?//=$form->errorSummary($userCharacteristic);?>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="education_step6">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>What do you like most about yourself? <span>*</span></p>
                                            <?= $form->field($userCharacteristic, 'what_do_you_like_about_yourself')->textarea(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>If you could change one thing about yourself, what would it be? <span>*</span></p>
                                            <?= $form->field($userCharacteristic, 'if_you_could_change')->textarea(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18">
                                            <p>What does family mean to you? <span>*</span></p>
                                            <?= $form->field($userCharacteristic, 'what_does_family_mean')->textarea(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12">
                                <div class="education_step6">
                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4">
                                        <div class="questions_13-18" style="display: block">
                                            <p >Message to intended parents (опишите ваши пожелания, вопросы, ожидания от потенциальных родителей)? <span>*</span> </p>
                                            <?= $form->field($userCharacteristic, 'message_to_intended_parents')->textarea(['maxlength' => true])->label(false) ?>
                                        </div>
                                    </div>

                                    <div class="col-12 col-sm-12 col-md-12 col-lg-4" >
                                        <div class="questions_13-18" style="display: block">
                                            <p >Why are you interested in being a surrogate and what do you hope to achieve through this process? <span>*</span> </p>
                                            <?= $form->field($userCharacteristic, 'why_are_you_interested')->textarea(['maxlength' => true])->label(false);?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <div class="btn_step6">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/surrogate/step4'">Back</button>
                                    <button class="btn-next-step">Next Step</button>
                                </div>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>

