
<section class="blue-bg">
    <div class="container">
        <div class="row">

            <?= $this->render('/partials/buttons',
                ['class'=>'btn inner-btn ths-popup-link'
            ]);?>

            <div class="col-lg-12">
                <div class="inner-title-text">
                    <p><?=$main->littleTitle?></p>
                </div>
                <div class="inner-title">
                    <h3><?=$main->bigTitle?></h3>
                </div>
            </div>

            <div class="offset-lg-4 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][5]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][5]->url?>"><?=Yii::$app->view->params['buttons'][5]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="offset-lg-1 col-6 col-lg-2">
                <div class="connect-link dash-bot">
                    <? if(Yii::$app->view->params['buttons'][6]->status):?>
                        <a class="ths-popup-link" href="<?=Yii::$app->view->params['buttons'][6]->url?>"><?=Yii::$app->view->params['buttons'][6]->name?></a>
                    <? endif;?>
                </div>
            </div>

            <div class="bg-white ">
                <div class="row">
                    <div class="aniimated-thumbnials">
                    <? foreach ($photo['data'] as $v):?>
                        <a href="<?=$v->getImage();?>">
                            <div class="inner-gallery-item">
                                <img src="<?=$v->getImage();?>" class="img-fluid">
                                <span class="inner-caption-title">
                                        <p><?=$v->name;?></p>
                                    </span>
                                <div class="inner-caption-text">
                                    <p> <?=$v->content;?></p>
                                </div>
                            </div>
                        </a>
                    <? endforeach;?>
                    </div>

                    <div class="col-6">
                        <? if($model->getPreviuosId() != null):?>
                        <a href="/photo/view?id=<?=$model->getPreviuosId();?>" class="btn btn-news right-btn"><?=$translation[0]->name?></a>
                        <? endif;?>
                    </div>

                    <div class="col-6">
                        <? if($model->getNextId() != null):?>
                        <a href="/photo/view?id=<?=$model->getNextId();?>" class="btn btn-news"><?=$translation[1]->name?></a>
                        <? endif;?>
                    </div>

                </div>
                <a href="/photo/index" class="btn btn-back"><?=$translation[2]->name?></a>

                <?= $this->render('/partials/pagination', [
                    'pages'=>$photo['pagination'],
                ]);?>

            </div>

        </div>


    </div>



    </div>
</section>

