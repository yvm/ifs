<div class="file-list-item UserVideo_img_block_<?=$UserVideo->id?>">
    <div class="file-icon">
        <img src="<?='/backend/web/images/uservideo/'.Yii::$app->session['reg_user'].'/'.$UserVideo->video?>" alt="">
    </div>
    <div class="file-name">
        <p><?=$UserVideo->video?></p>
    </div>
    <div class="delete-icon">
        <a href="" class="delete_img" data-id="<?=$UserVideo->id?>" data-type="UserVideo"><img src="/images/delete-icon.png" alt=""></a>
    </div>
    <div class="size">
        <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/uservideo/'.Yii::$app->session['reg_user'].'/'.$UserVideo->video)/1000);?> кб.</p>
    </div>
</div>
<div class="error-oversize"><?=$error?></div>