<?foreach($UserPhoto as $v){?>
    <div class="file-list-item UserPhoto_img_block_<?=$v->id?>">
        <div class="file-icon">
            <img src="<?='/backend/web/images/userphoto/'.Yii::$app->session['reg_user'].'/'.$v->photo?>" alt="" style="width:30px;">
        </div>
        <div class="file-name">
            <p><?=$v->photo?></p>
        </div>
        <div class="delete-icon">
            <a href="" class="delete_img" data-id="<?=$v->id?>" data-type="UserPhoto"><img src="/images/delete-icon.png" alt=""></a>
        </div>
        <div class="size">
            <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->session['reg_user'].'/'.$v->photo)/1000);?> кб.</p>
        </div>
    </div>
<?}?>
<div class="error-oversize"><?=$error?></div>
