<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 03.05.2019
 * Time: 13:39
 */

use yii\widgets\ActiveForm; ?>

<div class="overlay">
    <div class="guideline-modal">
        <div class="logo-ifs-modal">
            <img src="/images/logo.png" alt="">
            <div class="guide-modal-title">
                <p>Image Guidelines</p>
            </div>
            <div class="guide-modal-text">
                <p>Surrogates must submit a minimum of 7 photographs taken within the last year and the photos should have you
                    facing the camera (not a selfie!). One photo must be a “head shot” of you. One photo must be a full body
                    length shot. Additional photos can be from past years. You may also send in photos of you with your family,
                    friends, significant others and etc.</p>
            </div>

            <div class="guide-modal-title">
                <p> Here are some guidelines and tips to taking and sharing photos for your profile.</p>
            </div>
            <div class="guide-modal-text">
                <ul>
                    <li>
                        <p>Photos should be bright, in-focus, and show you as the focal point. Try to avoid photos where the
                            brightest light is behind you causing your face to be in darkness, photos where there are shadows on your
                            face and photos that are blurry.</p>
                    </li>
                    <li>
                        <p>Sending photos of yourself with other people is fine as long as you have other photos where you are the
                            focus and the other people in the photo are presentable. If your photo includes other people, please label
                            who is in the photo such as “Me with Husband”.</p>
                    </li>
                    <li>
                        <p>Try photos with different facial expressions, such as a smile with teeth, a smile without teeth, tilting
                            your head, looking straight at the camera, and angling your torso. Sharing photos showing your interests
                            and personality are encouraged. Activity photos can help IPs get to know you in an instant. Examples
                            include photos of you hiking, biking, painting or cooking.</p>
                    </li>
                    <li>
                        <p>Ensure your photos are as high resolution as possible. If you’re using your phone, be sure your high
                            resolution setting are on.</p>
                    </li>
                    <li>
                        <p>Keep backgrounds simple. You should be the focal point of the photo. Avoid people and other distracting
                            structures behind you, and avoid taking selfies in the car.</p>
                    </li>
                </ul>
            </div>
            <button class="btn-modal-light">Understand</button>
        </div>
    </div>
</div>
<div class="modal-blue-bg-lg">
    <div class="container">
        <div class="register-main-title">
            <h2>Registration</h2>
        </div>
        <div class="register-bg">
            <div class="row">
                <div class="col-sm-12">
                    <div class="desktop-version">
                        <div class="tab-wrap3">
                            <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step1">Step 1</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate-and-egg-donor/step2">Step 2</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step3">Step 3</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step4">Step 4</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link active" href="#">Step 5</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step6">Step 6</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate-and-egg-donor/step7">Step 7</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="/surrogate-and-egg-donor/step8">Step 8</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step9">Step 9</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link " href="/surrogate-and-egg-donor/step10">Step 10</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="mobile-version">
                        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab"
                                   aria-controls="pills-home" aria-selected="true">1</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">2</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">3</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">4</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">5</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-step1" role="tab"
                                   aria-controls="pills-home" aria-selected="true">6</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">7</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">8</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-step2" role="tab"
                                   aria-controls="pills-profile" aria-selected="false">9</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-step3" role="tab"
                                   aria-controls="pills-contact" aria-selected="false">10</a>
                            </li>
                        </ul>
                    </div>

                    <!-- step 5 -->
                    <?php $form = ActiveForm::begin([ 'options' => ['class' => 'form', 'method' => 'post']]); ?>
                    <div class="errors" style="color:red;">
<!--                        --><?//=$form->errorSummary($userCharacteristic);?>
<!--                        --><?//=$form->errorSummary($EthnicOrigin);?>
                    </div>
                    <div class="tab-content personal-character personal-step" id="personal-character">

                        <div class="character-title">
                            <div class="row">
                                <div class="col-12">
                                    <h1>Personal Characteristics</h1>
                                </div>
                            </div>
                        </div>

                        <div class="character-min-title">
                            <div class="row">
                                <div class="col-12">
                                    <h2>Traits and Characteristics</h2>
                                </div>
                            </div>
                        </div>

                        <div class="character-input">

                            <div class="row">
                                <!-- 1 строка -->
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Food <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_food')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Color <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_color')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Sport <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_sport')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Book <span>*</span></h3>
                                    </div>

                                    <?= $form->field($userCharacteristic, 'favorite_book')->textarea(['maxlength' => true])->label(false) ?>
                                </div>
                                <!-- end 1 строка -->

                                <!-- 2 строка -->
                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Singer/Group <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_singer_or_group')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Movie <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_movie')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Favorite Hobbies <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'favorite_hobby')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-3 col-md-3 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Artistic Talent <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'artistic_talent')->textarea(['maxlength' => true])->label(false) ?>
                                </div>
                                <!-- end 2 строка -->
                            </div>

                        </div>



                    </div>


                    <div class="personal-character personal-step personal-step-second" id="personal-character">

                        <div class="character-min-title">
                            <div class="row">
                                <div class="col-12">
                                    <h2>Personality and Character</h2>
                                </div>
                            </div>
                        </div>

                        <div class="character-input">

                            <div class="row">

                                <!-- 1 строка -->
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Personality/Character <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'personality_character')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Achievements <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'achievements')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3> Goals <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'goals')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                            </div>
                            <!-- end 1 строка -->

                            <!-- 2 строка -->
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3> Talents <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'talents')->textarea(['maxlength' => true])->label(false) ?>
                                </div>


                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>In 5 years, where do you see yourself in regards to your personal life, goals, career, or
                                            family life? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'in_five_years_where_do_you_see_yourself')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>In 10 years, where do you see yourself in regards to your personal life, goals, career, or
                                            family life <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'in_ten_years_where_do_you_see_yourself')->textarea(['maxlength' => true])->label(false) ?>
                                </div>
                            </div>
                            <!-- end 2 строка -->

                            <!-- 3 строка -->
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>In school, what subjects did you enjoy most? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'in_school_what_subject_did_you_enjoy')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>What were your academic strengths (i.e. Math, English, Science)? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'what_were_your_academic_strengths')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3> What do you like most about yourself? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'what_do_you_like_about_yourself')->textarea(['maxlength' => true])->label(false) ?>

                                </div>
                            </div>
                            <!-- end 3 строка -->


                            <!-- 4 строка -->
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>If you could change one thing about yourself, what would it be? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'if_you_could_change')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>What does family mean to you? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'what_does_family_mean')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3> Who is one person, alive or dead that you admire and why? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'who_is_one_person')->textarea(['maxlength' => true])->label(false) ?>

                                </div>
                            </div>
                            <!-- end 4 строка -->

                            <!-- 5 строка -->
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Memorable Moments <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'memorable_moments')->textarea(['maxlength' => true])->label(false) ?>

                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3> Message to intended parents <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'message_to_intended_parents')->textarea(['maxlength' => true])->label(false) ?>
                                </div>

                                <div class="col-lg-4 col-md-4 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Why are you interested in being an egg donor and what do you hope to achieve through this
                                            process? <span>*</span></h3>
                                    </div>
                                    <?= $form->field($userCharacteristic, 'why_are_you_interested')->textarea(['maxlength' => true])->label(false) ?>

                                </div>
                            </div>
                            <!-- end 5 строка -->


                            <!-- 6 строка -->
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-12">
                                    <div class="character-small-title">
                                        <h3>Religion</h3>
                                    </div>

                                    <div class="input-select">
                                        <select name="UserCharacteristic[religion]" id="">
                                            <option value="1" <?if($userCharacteristic->religion == '1') echo 'selected';?>>Atheist</option>
                                            <option value="2" <?if($userCharacteristic->religion == '2') echo 'selected';?>>Agnostic</option>
                                            <option value="3" <?if($userCharacteristic->religion == '3') echo 'selected';?>>Buddhist</option>
                                            <option value="4" <?if($userCharacteristic->religion == '4') echo 'selected';?>>Christian</option>
                                            <option value="5" <?if($userCharacteristic->religion == '5') echo 'selected';?>>Muslim</option>
                                            <option value="6" <?if($userCharacteristic->religion == '6') echo 'selected';?>>Hindu</option>
                                            <option value="7" <?if($userCharacteristic->religion == '7') echo 'selected';?>>Jewish</option>
                                            <option value="8" <?if($userCharacteristic->religion == '8') echo 'selected';?>>Other</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <!-- end 6 строка -->



                            <!-- 8 строка -->
                            <div class="row">
                                <div class="col-12">
                                    <div class="character-race ethic-origin-title">
                                        <div class="character-small-title">
                                            <h3> Ethnic Origin</h3>
                                        </div>

                                        <div class="character-race-text">
                                            <p>Note: You can select maximum of 4 Ethnicities and each ethnicity should contain its
                                                respective percentage</p>
                                            <span class="red-text">Total Race % must be 100</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- select 1 -->
                            <div class="ethic-origin">
                                <div class="row">
                                    <!-- 1 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                        <div class="select-ethic">
                                            <h3>Select Ethnic</h3>
                                        </div>

                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'ethnicA')->dropDownList(\yii\helpers\ArrayHelper::map($ethnicA, 'id', 'name'))->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 1 -->

                                    <!-- 2 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentB == null && $EthnicOrigin->ethnicB == null ? 'blur':''?>"  id="egg_donor_step5_select_2_country">
                                        <div class="select-ethic">
                                            <h3>Select Ethnic <span class="plus" id="egg_donor_step5_plus_2" style="cursor: pointer">+</span></h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentB == null && $EthnicOrigin->ethnicB == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item ">
                                            <?= $form->field($EthnicOrigin, 'ethnicB')->dropDownList(\yii\helpers\ArrayHelper::map($ethnicA, 'id', 'name'), $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 2 -->

                                    <!-- 3 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentC == null && $EthnicOrigin->ethnicC == null ? 'blur':''?>" id="egg_donor_step5_select_3_country">
                                        <div class="select-ethic">
                                            <h3>Select Ethnic <span class="plus" id="egg_donor_step5_plus_3" style="cursor: pointer">+</span></h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentC == null && $EthnicOrigin->ethnicC == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'ethnicC')->dropDownList(\yii\helpers\ArrayHelper::map($ethnicA, 'id', 'name'), $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 3 -->

                                    <!-- 4 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentD == null && $EthnicOrigin->ethnicD == null ? 'blur':''?>" id="egg_donor_step5_select_4_country">
                                        <div class="select-ethic">
                                            <h3>Select Ethnic <span class="plus" id="egg_donor_step5_plus_4" style="cursor: pointer">+</span></h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentD == null && $EthnicOrigin->ethnicD == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'ethnicD')->dropDownList(\yii\helpers\ArrayHelper::map($ethnicA, 'id', 'name'), $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 4 -->
                                </div>

                            </div>
                            <!-- end select 1 -->


                            <!-- select 2 -->
                            <div class="ethic-origin">
                                <div class="row">
                                    <!-- 1 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3">
                                        <div class="select-ethic">
                                            <h3>Select %</h3>
                                        </div>

                                        <div class="select-ethic-item">
                                            <?
                                            $array_percent = [
                                                100 => 100,
                                                75 => 75,
                                                50 => 50,
                                                25 => 25,
                                                15 => 15,
                                                10 => 10,
                                            ];
                                            ?>
                                            <?= $form->field($EthnicOrigin, 'percentA')->dropDownList($array_percent)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 1 -->

                                    <!-- 2 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentB == null && $EthnicOrigin->ethnicB == null ? 'blur':''?>" id="egg_donor_step5_select_2_percent">
                                        <div class="select-ethic">
                                            <h3>Select % </h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentB == null && $EthnicOrigin->ethnicB == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'percentB')->dropDownList($array_percent, $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 2 -->

                                    <!-- 3 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentC == null && $EthnicOrigin->ethnicC == null ? 'blur':''?>"  id="egg_donor_step5_select_3_percent">
                                        <div class="select-ethic">
                                            <h3>Select %</h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentC == null && $EthnicOrigin->ethnicC == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'percentC')->dropDownList($array_percent, $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 3 -->

                                    <!-- 4 -->
                                    <div class="col-lg-3 col-md-3 col-sm-3 <?=$EthnicOrigin->percentD == null && $EthnicOrigin->ethnicD == null ? 'blur':''?>" id="egg_donor_step5_select_4_percent">
                                        <div class="select-ethic">
                                            <h3>Select % </h3>
                                        </div>
                                        <?
                                        $EthnicOrigin->percentD == null && $EthnicOrigin->ethnicD == null ? $options = ['prompt' => '', 'disabled' => 'disabled']: $options = ['prompt' => ''];
                                        ?>
                                        <div class="select-ethic-item">
                                            <?= $form->field($EthnicOrigin, 'percentD')->dropDownList($array_percent, $options)->label(false) ?>
                                        </div>
                                    </div>
                                    <!-- end 4 -->
                                </div>
                            </div>
                            <!-- end select 2 -->

                        </div>
                        <!-- end 8 строка -->

                        <div class="row">

                            <div class="col-12">
                                <div class="character-register-button">
                                    <button class="btn-back-step" type="button" onclick="window.location.href = '/surrogate-and-egg-donor/step4'">Back</button>
                                    <button class="btn-next-step">Next step</button>
                                </div>
                            </div>

                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>

            </div>

        </div>

        </form>
        <!-- end step 5 -->

    </div>
</div>
