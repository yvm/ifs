<div class="file-list-item UserVoiceMessage_img_block_<?=$UserVoiceMessage->id?>">
    <div class="file-name">
        <p><?=$UserVoiceMessage->video?></p>
    </div>
    <div class="delete-icon">
        <a href="" class="delete_img" data-id="<?=$UserVoiceMessage->id?>" data-type="UserVoiceMessage"><img src="/images/delete-icon.png" alt=""></a>
    </div>
    <div class="size">
        <p><?=ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/uservoicemessage/'.Yii::$app->session['reg_user'].'/'.$UserVoiceMessage->video)/1000);?> кб.</p>
    </div>
</div>
<div class="error-oversize"><?=$error?></div>