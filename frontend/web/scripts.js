$('body').on('change', 'input[name="SignupForm[type]"]', function () {
    if($(this).val() != 1)
        $('.interested_in_section_field').hide();
    else
        $('.interested_in_section_field').show();
});

$('body').on('click', '.registration_button_main_main_a', function (e) {
    // e.preventDefault();
    // $(this).closest('form').submit();
    $.ajax({
        type: 'POST',
        url: '/registration-modal/signup',
        // dataType: 'json',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $('#register-email').hide();
                $('#modal-go-to-email').show();
            }else{
                $('.y_email_section_2').show();
            }
        },
        error: function () {

        }
    });
});

$('body').on('click', '.registration_submit_button', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/registration-modal/signup2',
        data: $(this).closest('form').serialize(),
        dataType: 'JSON',
        success: function (response) {
            if(response.error == 1){
                history.pushState({}, '', '/');
                $('#sign-up-popup').hide();
                $('#terms-and-conditions').show();
            }else if(response.errors = 1){
                $('.errors2').html(response.error);
                if(response.email)
                    $('.email_error').html(response.email);
                else
                    $('.email_error').html('');

                if(response.password_repeat)
                    $('.password_error').html(response.password_repeat);
                else
                    $('.password_repeat').html('');
            }else{
                $('.errors2').html(response.error);
            }
        },
        error: function () {

        }
    });
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

$('body').on('click', '.ths-popup-link.registration-main-a', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/registration-modal/signup3',
        dataType: 'json',
        success: function (response) {
            $('input[name="Profiles[fio]"]').val(response.fio);
            $('input[name="Profiles[email]"]').val(response.email);
            $('.created_at').val(response.created_at);
            $('#terms-and-conditions').hide();
            $('#registration-main').show();
        },
        error: function () {

        }
    });
});

$('body').on('click', '.registration-main-a2', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'POST',
        url: '/registration-modal/signup4',
        dataType: 'json',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response.role == 1) {
                // if(response.type == 3)
                    window.location.href = '/registration/step1';
            }else if(response.role == 5){
                    window.location.href = '/biotransportation-client/step1';
            }else if(response.role == 2){
                window.location.href = '/surrogate/step1';
            }else if(response.role == 3){
                window.location.href = '/egg-donor/step1';
            }else if(response.role == 4){
                window.location.href = '/surrogate-and-egg-donor/step1';
            }else{
                $('.errors4').html(response.error);
            }
        },
        error: function () {

        }
    });
});

$('body').on('change', 'input[name="AditionalInformation[reason_surrogacy]"]', function (e) {
    $('.describe').show();
});

var egg_donor_message1 = 0;
var egg_donor_message2 = 0;

$('body').on('change', 'input[name="AditionalInformation[egg_donor_needed]"]', function (e) {
    egg_donor_message1 = $(this).val();
    if(egg_donor_message1 == 2 && egg_donor_message2 == 2)
        $('.egg-donor-message').hide();
    else
        $('.egg-donor-message').show();
});

$('body').on('change', 'input[name="AditionalInformation[surrogate_needed]"]', function (e) {
    egg_donor_message2 = $(this).val();
    if(egg_donor_message1 == 2 && egg_donor_message2 == 2)
        $('.egg-donor-message').hide();
    else
        $('.egg-donor-message').show();
});

$('body').on('change', 'select[name="AditionalInformation[know_about_us]"]', function (e) {
    if($(this).val() == 17) {
        $('.who_recommended').show();
        $('.referal').hide();
    }
    else if($(this).val() == 16) {
        $('.who_recommended').hide();
        $('.referal').show();
    }
    else{
        $('.who_recommended').hide();
        $('.referal').hide();
    }
});

$('body').on('change', 'input[name="Consultation[ready_consultation]"]', function (e) {
    if($(this).val() == 1)
        $('.ready_consultation').show();
    else
        $('.ready_consultation').hide();
});

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var files;
var data_files = new FormData();

$('input.UserDocuments[type=file]').change(function(){
    files = this.files;

    $.each( files, function( key, value ){
        data_files.append( 'UserDocuments['+key+']', value );
    });
});

$('body').on('change', '.UserDocuments',function(e){
    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/registration/step4',
        data: data_files,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('.file-list.UserDocuments').html(response);
            data_files = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img',function(e){
    e.preventDefault();
    $.ajax({
        url: '/registration/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        dataType: 'json',
        success: function (response) {
            $('.'+response.type+'_img_block_'+response.id).remove();
        },
        error: function () {

        }
    });
});

var video;
var data_video = new FormData();

$('input.UserVideo[type=file]').change(function(){
    video = this.files;

    $.each( video, function( key, value ){
        data_video.append( 'UserVideo['+key+']', value );
    });
});

$('body').on('change', '.UserVideo',function(e){
    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/registration/step4',
        data: data_video,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('.file-list.UserVideo').html(response);
            data_video = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

var photo;
var data_photo = new FormData();

$('input.UserPhoto[type=file]').change(function(){
    photo = this.files;

    $.each( photo, function( key, value ){
        data_photo.append( 'UserPhoto['+key+']', value );
    });
});

$('body').on('change', '.UserPhoto',function(e){
    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/registration/step4',
        data: data_photo,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('.file-list.UserPhoto').html(response);
            data_photo = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

var voice_message;
var data_voice_message = new FormData();

$('input.UserVoiceMessage[type=file]').change(function(){
    voice_message = this.files;

    $.each( voice_message, function( key, value ){
        data_voice_message.append( 'UserVoiceMessage['+key+']', value );
    });
});

$('body').on('change', '.UserVoiceMessage',function(e){
    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/surrogate/step10',
        data: data_voice_message,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('.file-list.UserVoiceMessage').html(response);
            data_voice_message = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

var handwriting_sample;
var data_handwriting_sample = new FormData();

$('input.UserHandwritingSample[type=file]').change(function(){
    handwriting_sample = this.files;

    $.each( handwriting_sample, function( key, value ){
        data_handwriting_sample.append( 'UserHandwritingSample['+key+']', value );
    });
});

$('body').on('change', '.UserHandwritingSample',function(e){
    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/egg-donor/step10',
        data: data_handwriting_sample,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('.file-list.UserHandwritingSample').html(response);
            data_handwriting_sample = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.btn-next-step.register-my',function(e){
    e.preventDefault();
    var url = $(this).attr('data-url');
    $.ajax({
        url: '/'+url+'/register-end',
        method: 'GET',
        success: function (response) {
            if(response == 1)
                window.location.href = '/profile/index';
            else
                $('.error_reg').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('change', '.range-input.textInput',function(e){
    $('#textInput').val($(this).val());
    $('.height-ft').html(Math.round($(this).val()/30.48*10)/10+" ft");
    $('.height-inch').html(Math.round($(this).val() / 2.54*10)/10+" inch");
});

$('body').on('change', '.range-input.textInput2',function(e){
    $('#textInput2').val($(this).val());
    $('.lb-weight').html(Math.round($(this).val() / 0.453592*10)/10+" lb");
    $('.stone-weight').html(Math.round($(this).val() / 6.35029*10)/10+" stone");
});

$('body').on('change', '.have_you_ever', function (e) {
    if($(this).val() != 0)
        $('.name_of_IVF').show();
    else
        $('.name_of_IVF').hide();
});


$('body').on('change', '.have_you_ever_egg_donor', function (e) {
    if($(this).val() != 0)
        $('.name_of_IVF_egg_donor').show();
    else
        $('.name_of_IVF_egg_donor').hide();
});

$('body').on('change', '.number_pregnancies', function (e) {
    if($(this).val() == 0)
        $('.blurr').removeClass('blurr').addClass('blur');
    else
        $('.blur').removeClass('blur').addClass('blurr');
});

//Surrogate Step-6
$('body').on('click', '.surrogate_step6_16_1', function () {
    $('#surrogate_step6_lang').show();

});
$('body').on('click', '.surrogate_step6_16_2', function () {
    $('#surrogate_step6_lang').hide();
});

//Surrogate Step-7
$('body').on('change', '#surrogate_select_13', function () {
    if($(this).val() == 'Other') {
        $('#surrogate_tab_13_1').show();
        $('#surrogate_tab_13_2').hide();
    }else if($(this).val() == 'Referral'){
        $('#surrogate_tab_13_2').show();
        $('#surrogate_tab_13_1').hide();
    }else {
        $('#surrogate_tab_13_1').hide();
        $('#surrogate_tab_13_2').hide();
    }
});

//Surrogate Step-8
$('body').on('click', '#surrogate_checked_8_1', function () {
    $('#surrogate_step8_content').hide();
    $('#surrogate_step8_title').hide();
});

$('body').on('click', '#surrogate_checked_8_2', function () {
    $('#surrogate_step8_content').show();
    $('#surrogate_step8_title').show();
});

$('body').on('change', '#surrogate_step8_13', function () {
    // $('#surrogate_step8_select_5').hide();
    var s = $(this).val();

    for(var i=0;i<=s;i++){
        $('.surrogate_step8_select_'+i).show();
    }
    for(var k=s+1;k<11;k++){
        $('.surrogate_step8_select_'+k).hide();
    }
});

$('body').on('click', '.add_relation', function (e) {
    e.preventDefault();
    $('.copyMy').clone().appendTo('#surrogate_step8_content').show().removeClass('copyMy');
});

$('body').on('click', '.save_FamilyMemberInformation', function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    $.ajax({
        url: '/surrogate/save-family-member-information',
        method: 'GET',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            form.find('.idFamilyMemberInformation').val(response);
            form.find('.delete_FamilyMemberInformation').attr('data-id', response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.delete_FamilyMemberInformation', function (e) {
    e.preventDefault();
    var parent = $(this).closest('.education_step8_2');
    $.ajax({
        url: '/surrogate/delete-family-member-information',
        method: 'GET',
        data: {id: $(this).attr('data-id')},
        success: function (response) {
            parent.remove();
        },
        error: function () {

        }
    });
});

$('body').on('click', '.btn-next-step.form_step', function (e) {
    e.preventDefault();
    $('form.form_step').submit();
});

$('body').on('click', '.add_relation_step9', function (e) {
    e.preventDefault();
    $('.copyMy').find('input[name="HealthDetails[type]"]').val($(this).attr('data-id'));
    var html = $('.copyMy').html();
    var parent = $(this).closest('.row');
    $(html).appendTo(parent).show().removeClass('copyMy');
});

$('body').on('click', '.save_edit_step9', function (e) {
    e.preventDefault();
    var form = $(this).closest('form');
    $.ajax({
        url: '/surrogate/save-health-details',
        method: 'GET',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            form.find('.HealthDetails').val(response);
            form.find('.delete_edit_step9').attr('data-id', response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.delete_edit_step9', function (e) {
    e.preventDefault();
    var parent = $(this).closest('form');
    if($(this).attr('data-id'))
        $.ajax({
            url: '/surrogate/delete-health-details',
            method: 'GET',
            data: {id: $(this).attr('data-id')},
            success: function (response) {
                if(parent.hasClass('parent'))
                    parent.next('.offset-lg-4.offset-md-4').remove();
                else
                    parent.prev('.offset-lg-4.offset-md-4').remove();
                parent.remove();
            },
            error: function () {

            }
        });
});

$('body').on('change', 'input[name="FamilyHealthHistory[you_adopted]"]',function(e){
    if($(this).val()==0)
        $('.desktop_step8').show();
    else
        $('.desktop_step8').hide();
});

$('body').on('change', 'select[name="FamilyHealthHistory[did_you_have]"]',function(e){
    if($(this).val()==0)
        $('.hidden_block').hide();
    else
        $('.hidden_block').show();
});

$('body').on('change', 'select[name="PersonalInfo[country]"]', function (e) {
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('select[name="PersonalInfo[city]"]').html(response);
        },
        error: function () {

        }
    });
});







//Surrogate Step-6
$('body').on('click', '.surrogate_step6_16_1', function () {
    $('#surrogate_step6_lang').show();

});
$('body').on('click', '.surrogate_step6_16_2', function () {
    $('#surrogate_step6_lang').hide();
});



//Surrogate Step-7
$('body').on('change', '#surrogate_select_13', function () {
    if($(this).val() == 'Other') {
        $('#surrogate_tab_13_1').show();
        $('#surrogate_tab_13_2').hide();
    }else if($(this).val() == 'Referral'){
        $('#surrogate_tab_13_2').show();
        $('#surrogate_tab_13_1').hide();
    }else {
        $('#surrogate_tab_13_1').hide();
        $('#surrogate_tab_13_2').hide();
    }
});


//Surrogate Step-8
$('body').on('click', '#surrogate_checked_8_1', function () {
    $('#surrogate_step8_content').hide();
    $('#surrogate_step8_title').hide();
});

$('body').on('click', '#surrogate_checked_8_2', function () {
    $('#surrogate_step8_content').show();
    $('#surrogate_step8_title').show();
});

$('body').on('change', '#surrogate_step8_13', function () {
    // $('#surrogate_step8_select_5').hide();
    var s = $(this).val();
    for(var i=0;i<s;i++){
        $('#surrogate_step8_select_'+i).show();
    }
    for(var k=s;k<11;k++){
        $('#surrogate_step8_select_'+k).hide();
    }

});




//Egg Donor Step3

$('body').on('click', 'input[name="SurrogateAvailability[are_you_willing_to_be_completely]"]', function (e) {

    if($(this).val() == 1) {
        $('#egg_donor_step3_27_1').removeClass('blur').addClass('blur');
        $('#egg_donor_step3_27_2').removeClass('blur').addClass('blur');
        $('#egg_donor_step3_28_1').removeClass('blur').addClass('blur');
        $('#egg_donor_step3_28_2').removeClass('blur').addClass('blur');
        $('#egg_donor_step3_29_1').removeClass('blur').addClass('blur');
        $('#egg_donor_step3_29_2').removeClass('blur').addClass('blur');
    }else{
        $('#egg_donor_step3_27_1').removeClass('blur');
        $('#egg_donor_step3_27_2').removeClass('blur');
        $('#egg_donor_step3_28_1').removeClass('blur');
        $('#egg_donor_step3_28_2').removeClass('blur');
        $('#egg_donor_step3_29_1').removeClass('blur');
        $('#egg_donor_step3_29_2').removeClass('blur');
    }
});


//Egg Donor Step5
$('body').on('click', '#egg_donor_step5_plus_2', function (e) {
    $('#egg_donor_step5_select_2_country').removeClass('blur');
    $('#egg_donor_step5_select_2_percent').removeClass('blur');
});

$('body').on('click', '#egg_donor_step5_plus_3', function (e) {
    $('#egg_donor_step5_select_3_country').removeClass('blur');
    $('#egg_donor_step5_select_3_percent').removeClass('blur');
});

$('body').on('click', '#egg_donor_step5_plus_4', function (e) {
    $('#egg_donor_step5_select_4_country').removeClass('blur');
    $('#egg_donor_step5_select_4_percent').removeClass('blur');
});


// Surrogate AND Egg Donor


$('body').on('click', '#egg_donor_step5_plus_4', function (e) {
    $('#egg_donor_step5_select_4_country').removeClass('blur');
    $('#egg_donor_step5_select_4_percent').removeClass('blur');
});


// Indentend parent egg-donor and surrogate
$('body').on('click', '#indented_donorSurrogate_donor_message', function (e) {

    if($(this).val() == 2){
        if($('#indented_donorSurrogate_surrogate_message:checked').val() == 2){
            $('#indented_parent_message_surrogate_donor').hide();
        }else {
            $('#indented_parent_message_surrogate_donor').show();
        }
    }else{
        $('#indented_parent_message_surrogate_donor').show();
    }
});

$('body').on('click', '#indented_donorSurrogate_surrogate_message', function (e) {

    if($(this).val() == 2){
        if($('#indented_donorSurrogate_donor_message:checked').val() == 2){
            $('#indented_parent_message_surrogate_donor').hide();

        }else {
            $('#indented_parent_message_surrogate_donor').show();
        }
    }else{
        $('#indented_parent_message_surrogate_donor').show();
    }
});


// Indentend parent surrogate
$('body').on('click', '#indented_surrogate_message', function (e) {
    if($(this).val() == 2){
        $('#indented_parent_message_surrogate_donor').hide();
    }else{
        $('#indented_parent_message_surrogate_donor').show();
    }
});


// Indentend egg-donation
$('body').on('click', '#indented_egg_donation_message', function (e) {
    if($(this).val() == 2){
        $('#indented_parent_message_surrogate_donor').hide();
    }else{
        $('#indented_parent_message_surrogate_donor').show();
    }
});

$('body').on('click', '.search_surrogates', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/surrogate-database',
        method: 'POST',
        data: $('form#w0').serialize(),
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.search_eg_donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/egg-donor-database',
        method: 'POST',
        data: $('form#egg-donor-database').serialize(),
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.ClearSearches', function (e) {
    e.preventDefault();
    $("#w0")[0].reset();
});

$('body').on('click', '.New_and_Updated', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.View_All_Surrogates', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-surrogates',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.New_and_Updated_eg_donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated-eg-donor',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.View_All_Surrogates_eg_donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-surrogates-eg-donor',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.New_and_UpdatedIntendedParent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated-intended-parent',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.View_All_IntendedParent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-surrogates-intended-parent',
        method: 'GET',
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.search_intended_parent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/intended-parent-database',
        method: 'POST',
        data: $('form#w0').serialize(),
        success: function (response) {
            $('.database-profile-wrap').html(response);
        },
        error: function () {

        }
    });
});



$('body').on('click', '.write_other_currency', function (e) {

    $('.other_compensation_currency').show();
    $('#compensation_currency').hide();
    $(this).hide();
    $('.select_current_currency').show();
});

$('body').on('click', '.select_current_currency', function (e) {

    $('.other_compensation_currency').hide();
    $('#compensation_currency').show();
    $(this).hide();
    $('.write_other_currency').show();
});



$('body').on('click', '.write_other_currency_egg_donor', function (e) {

    $('.other_compensation_currency_egg_donor').show();
    $('#compensation_currency_egg_donor').hide();
    $(this).hide();
    $('.select_current_currency_egg_donor').show();
});

$('body').on('click', '.select_current_currency_egg_donor', function (e) {

    $('.other_compensation_currency_egg_donor').hide();
    $('#compensation_currency_egg_donor').show();
    $(this).hide();
    $('.write_other_currency_egg_donor').show();
});


/////////////////////////////////////////////////////////////////////////////////////////////


$('body').on('change', 'input[name="UserEducation[do_you_know_any_language]"]', function (e) {
    if($(this).val() == 1)
        $('#surrogate_step6_lang').show();
    else
        $('#surrogate_step6_lang').hide();
});
