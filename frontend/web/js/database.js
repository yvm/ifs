$("body").on('click', '.empty-item', function () {
    var favorite_user_id = $(this).attr('data-user-id');
    var user_id = $(this).attr('data-user');
    if(user_id == ""){
        alert('Чтобы добавить пользователь в избранное нужно войти!');
    }else {
        $.ajax({
            url: "/profile/favorite",
            dataType: "json",
            data: {favorite_user_id: favorite_user_id},
            method: "post",
            success: function (data) {
                if(data.status){
                    $('.measurement-item'+favorite_user_id).html('<img src="/images/heart-fill.svg" alt="heart-fill">');
                    $(this).toggleClass('empty__active');
                }else{
                    $('.measurement-item'+favorite_user_id).html('<img src="/images/heart.svg" alt="heart">');
                    $(this).toggleClass('empty__active');
                }
                //alert(data.text);
            },
            error: function () {
                alert('Упс, что-то пошло не так!');
            }
        });
    }
});



$('body').on('click', '.ClearSearches', function (e) {
    e.preventDefault();
    $("#w0")[0].reset();
});




// surrogate database
$('body').on('click', '.search_surrogates', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/surrogate-database',
        method: 'POST',
        data: $('form#w0').serialize(),
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});




$('body').on('click', '.My_Favorites', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/my-favorites',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});


$('body').on('click', '.New_and_Updated', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});


$('body').on('click', '.View_All_Surrogates', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-surrogates',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});



// Egg Donor Database
$('body').on('click', '.search_egg_donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/egg-donor-database',
        method: 'POST',
        data: $('form#w0').serialize(),
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});



$('body').on('click', '.My_Favorites_Egg_Donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/my-favorites-egg-donor',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});


$('body').on('click', '.New_and_Updated_Egg_Donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated-egg-donor',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.View_All_Egg_Donor', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-egg-donor',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});







// Intended Database
$('body').on('click', '.search_parent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/intended-parent-database',
        method: 'POST',
        data: $('form#w0').serialize(),
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});



$('body').on('click', '.My_Favorites_Parent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/my-favorites-parent',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});


$('body').on('click', '.New_and_Updated_Parent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/new-and-updated-parent',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.View_All_Parent', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/database/view-all-parent',
        method: 'GET',
        success: function (response) {
            $('.search_result').html(response);
        },
        error: function () {

        }
    });
});