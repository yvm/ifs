$('body').on('change', '.country_modal select[name="GetAConsultation[country]"]', function (e) {
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('.country_modal select[name="GetAConsultation[city]"]').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('change', '.country_modal select[name="IfsForm[country]"]', function (e) {
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('.country_modal select[name="IfsForm[city]"]').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('click', '.send_GET_A_CONSULTATION', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/form-submission/get-a-consultation',
        method: 'POST',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response != 1) {
                $('.errors-get-a-consultation').show();
                $('.errors-get-a-consultation').html(response);
            }
            else {
                $('.success-get-a-consultation').show();
                $('.errors-get-a-consultation').hide();
            }
        },
        error: function () {

        }
    });
});

$('body').on('change', 'select[name="BiotransportationForm[country]"]', function (e) {
    if($(this).val() == 152)
        $('.state').show();
    else
        $('.state').hide();
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('select[name="BiotransportationForm[city]"]').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('change', 'select[name="BiotransportationForm[country_deliver]"]', function (e) {
    if($(this).val() == 152)
        $('.state_deliver').show();
    else
        $('.state_deliver').hide();
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('select[name="BiotransportationForm[city_deliver]"]').html(response);
        },
        error: function () {

        }
    });
});

$('body').on('change', 'select[name="BiotransportationForm[country_transportation]"]', function (e) {
    if($(this).val() == 152)
        $('.state_transportation').show();
    else
        $('.state_transportation').hide();
    $.ajax({
        url: '/registration/city',
        method: 'GET',
        data: {id: $(this).val()},
        success: function (response) {
            $('select[name="BiotransportationForm[city_transportation]"]').html(response);
        },
        error: function () {

        }
    });
});


$('body').on('click', '.send_ifs_form', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/form-submission/ifs-form',
        method: 'POST',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response != 1) {
                $('.errors-ifs-form').show();
                $('.errors-ifs-form').html(response);
            }
            else {
                $('.success-ifs-form').show();
                $('.errors-ifs-form').hide();
            }
        },
        error: function () {

        }
    });
});

$('body').on('click', '.attach-file.ifs-form', function (e) {
    e.preventDefault();
    $('.uploadImgInputIfsForm').click();
});

var photo;
var data_photo = new FormData();

$('body').on('change', '.uploadImgInputIfsForm',function(e){
    photo = this.files;

    $.each( photo, function( key, value ){
        data_photo.append( 'Files['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/form-submission/ifs-form-files',
        data: data_photo,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $(response).appendTo('.modal-files.ifs-form');
            data_photo = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('change', 'input[name="GetAConsultation[role]"]', function (e) {
    if($(this).val() != 1)
        $('input[name="GetAConsultation[type][]"]').removeAttr('checked');
});

$('body').on('change', 'input[name="IfsForm[role]"]', function (e) {
    if($(this).val() != 1)
        $('input[name="IfsForm[type][]"]').removeAttr('checked');
});

$('body').on('click', '.send_biotransportation_registr', function (e) {
    e.preventDefault();
    $.ajax({
        url: '/form-submission/biotransportation-registr',
        method: 'POST',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response != 1) {
                $('.errors-biotransportation-registr-form').show();
                $('.errors-biotransportation-registr-form').html(response);
            }
            else {
                $('.success-biotransportation-registr-form').show();
                $('.errors-biotransportation-registr-form').hide();
            }
        },
        error: function () {

        }
    });
});

$('body').on('click', '.modal.fade.show', function (e) {
    $('.modal.fade.show').removeAttr('show').hide();
});

// $( function() {
//     $( "#searchbox" ).autocomplete({
//         source: availableTags
//     });
// });

$(document).ready(function() {
    $( "#searchbox" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/autocomplete/index",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.id
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        focus: function(event, ui) {
            $('#searchbox').val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            $('.post_id_hidden').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });

    $( ".gsc-input" ).autocomplete({
        source: function( request, response ) {
            $.ajax({
                url: "/autocomplete/index2",
                dataType: "jsonp",
                data: {
                    featureClass: "P",
                    style: "full",
                    maxRows: 12,
                    name_startsWith: request.term
                },

                success: function(data) {
                    response($.map(data.users, function(user) {
                        return {
                            label: user.title,
                            value: user.id
                        }
                    }));
                }
            });
        },
        minLength: 2,
        mustMatch: true,
        focus: function(event, ui) {
            $('.gsc-input').val(ui.item.label);
            return false;
        },
        select: function(event, ui) {
            $('.post_id_hidden').val(ui.item.value);
            return false;
        },
        open: function() {
            $(this).removeClass("ui-corner-all").addClass("ui-corner-top");
        },
        close: function() {
            $(this).removeClass("ui-corner-top").addClass("ui-corner-all");
        }
    });
});

$('body').on('click', '.ui-menu-item', function (e) {
    window.location.href = '/search/index?text='+$('#searchbox').val();
});

$('body').on('keyup', '.gsc-input', function (e) {
    // var arr = [];
    // $('.gsq_a td span').each(function () {
    //     arr.push($(this).html());
    // });
    // $('.gstl_50').html('<tbody><tr><td class="gssb_f"></td><td class="gssb_e" style="width: 100%;"><table cellspacing="0" cellpadding="0" class="gsc-completion-container" style="width: 100%;"><tbody><tr class=""><td class="gssb_a " dir="ltr" style="text-align: left;"><div class="gsq_a"><table cellspacing="0" cellpadding="0" style="width: 100%;"><tbody><tr><td style="width: 100%;"><span>пе<b>резагрузить</b></span></td></tr></tbody></table></div></td></tr><tr class=""><td class="gssb_a " dir="ltr" style="text-align: left;"><div style="text-align: right;"><img src="http://www.google.com/cse/static/images/1x/googlelogo_lightgrey_46x16dp.png" srcset="http://www.google.com/cse/static/images/2x/googlelogo_lightgrey_46x16dp.png 2x" style="vertical-align: middle; padding-right: 1px;"><span style="color: rgb(181, 181, 181); font-size: 12px; vertical-align: middle;">Система пользовательского поиска</span></div></td></tr></tbody></table></td></tr></tbody>');
    // $('.gstl_50').attr('style', 'width: 1452px; top: 145px; position: absolute; text-align: left; left: 0px; display: block;');
    // var val = $(this).val();
    // $.ajax({
    //     url: "/autocomplete/index2",
    //     method: 'GET',
    //     data: {q: val},
    //     dataType: 'JSON',
    //     success: function (response) {
    //         console.log(response);
    //     },
    //     error: function () {
    //
    //     }
    // });
});


