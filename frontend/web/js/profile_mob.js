
var photo;
var data_photo = new FormData();
var edit_photo = 0;

$('body').on('click', '.uploadImg_mob',function(e){
    edit_photo= 0;
    $('.uploadImgInput_mob').click();
});

$('body').on('click', '#addPhoto',function(e){
    edit_photo= 0;
    $('.uploadImgInput_mob').click();
});

$('body').on('click', '.edit_img_profile_mob',function(e){
    edit_photo = $(this).attr('data-id');
    $('.uploadImgInput_mob').click();
});

$('body').on('change', '.uploadImgInput_mob',function(e){
    photo = this.files;

    $.each( photo, function( key, value ){
        data_photo.append( 'UserPhoto['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?edit_photo='+edit_photo+'&view=mob',
        data: data_photo,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            if(edit_photo == 0)
                $('#profile_photo_mob').html(response);
            else
                $('.mob-uploaded-img[data-id="'+edit_photo+'"]').html(response);
            data_photo = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_photo_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type'),  view: 'mob'},
        method: 'GET',
        success: function (response) {
            $('#profile_photo_mob').html(response);
        },
        error: function () {

        }
    });
});

var documents;
var data_documents = new FormData();

$('body').on('click', '#addDoc',function(e){
    edit_photo= 0;
    $('.uploadImgInputDocuments_mob').click();
});

$('body').on('click', '.uploadImgDocuments_doc',function(e){
    $('.uploadImgInputDocuments_mob').click();
});

$('body').on('change', '.uploadImgInputDocuments_mob',function(e){
    documents = this.files;

    $.each( documents, function( key, value ){
        data_documents.append( 'UserDocuments['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?view=mob',
        data: data_documents,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_doc_mob').html(response);
            data_documents = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_documents_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type'), view: 'mob'},
        method: 'GET',
        success: function (response) {
            $('#profile_doc_mob').html(response);
        },
        error: function () {

        }
    });
});

var video;
var data_video= new FormData();

$('body').on('click', '.uploadImgVideo_mob',function(e){
    $('.uploadImgInputVideo_mob').click();
});

$('body').on('click', '#addVideo',function(e){
    $('.uploadImgInputVideo_mob').click();
});



$('body').on('change', '.uploadImgInputVideo_mob',function(e){
    video = this.files;

    $.each( video, function( key, value ){
        data_video.append( 'UserVideo['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?view=mob',
        data: data_video,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_video_mob').html(response);
            data_video = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_video_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type'), view: 'mob'},
        method: 'GET',
        success: function (response) {

            $('#profile_video_mob').html(response);
            $('.uploadImgVideo_mob.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});

var HandwritingSample;
var data_HandwritingSample= new FormData();

$('body').on('click', '#addHandSample',function(e){
    $('.uploadImgInputHandwritingSample_mob').click();
});

$('body').on('click', '.uploadImgHandwritingSample_mob',function(e){
    $('.uploadImgInputHandwritingSample_mob').click();
});

$('body').on('change', '.uploadImgInputHandwritingSample_mob',function(e){
    HandwritingSample = this.files;

    $.each( HandwritingSample, function( key, value ){
        data_HandwritingSample.append( 'UserHandwritingSample['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?view=mob',
        data: data_HandwritingSample,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_handwriting_mob').html(response);
            data_HandwritingSample = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_HandwritingSample_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type'), view: 'mob'},
        method: 'GET',
        success: function (response) {
            $('#profile_handwriting_mob').html(response);
            $('.uploadImgHandwritingSample_mob.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});

var VoiceMessage;
var data_VoiceMessage= new FormData();


$('body').on('click', '#addVoiceMessage',function(e){
    $('.uploadImgInputVoiceMessage_mob').click();
});

$('body').on('click', '.uploadImgVoiceMessage_mob',function(e){
    $('.uploadImgInputVoiceMessage_mob').click();
});

$('body').on('change', '.uploadImgInputVoiceMessage_mob',function(e){
    VoiceMessage = this.files;

    $.each( VoiceMessage, function( key, value ){
        data_VoiceMessage.append( 'UserVoiceMessage['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?view=mob',
        data: data_VoiceMessage,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_voice_message_mob').html(response);
            data_VoiceMessage = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_VoiceMessage_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type'), view: 'mob'},
        method: 'GET',
        success: function (response) {
            $('#profile_voice_message_mob').html(response);
            $('.uploadImgVoiceMessage_mob.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});



$('body').on('click', '.delete_favorite_user_mob',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/delete-favorite',
        data: {favorite_user_id: $(this).attr('data-user-id'),' view':'mob'},
        method: 'GET',
        success: function (response) {
            $('#profile_favorites_mob').html(response);
        },
        error: function () {

        }
    });
});




$('body').on('click', '#profile_status_edit_mob',function(e){
    $('#profile_status_save_mob').show();
    $('#profile_status_text_mob').hide();
    $('#profile_status_edit_mob').hide();
    $('#profile_status_textarea_mob').show();
});




$('body').on('click', '#profile_status_save_mob',function(e){

    $.ajax({
        type: 'POST',
        url: '/profile/update-status',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $('#profile_status_text_mob').html($('#profile_status_textarea_mob').val());
                $('#profile_status_save_mob').hide();
                $('#profile_status_text_mob').show();
                $('#profile_status_edit_mob').show();
                $('#profile_status_textarea_mob').hide();
            }else{
                alert('Something is error..');
            }
        },
        error: function () {

        }
    });
});



