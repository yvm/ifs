// jquery mask
$(document).ready(function(){
	
  $('.date').mask('00/00/0000');
  $('.time').mask('00:00:00');
  $('.date_time').mask('00/00/0000 00:00:00');
  $('.cep').mask('00000-000');
  $('.phone').mask('0000-0000');
  $('.phone_with_ddd').mask('(00) 0000-0000');
  $('.phone_us').mask('(000) 000-0000');
  $('.mixed').mask('AAA 000-S0S');
  $('.cpf').mask('000.000.000-00', {reverse: true});
  $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
  $('.money1').mask('000.000.000.000.000,00', {reverse: true});
  $('.money2').mask("#.##0,00", {reverse: true});
  $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', {
    translation: {
      'Z': {
        pattern: /[0-9]/, optional: true
      }
    }
  });
  $('.ip_address').mask('099.099.099.099');
  $('.percent').mask('##0,00%', {reverse: true});
  $('.clear-if-not-match').mask("00/00/0000", {clearIfNotMatch: true});
  $('.placeholder').mask("00/00/0000", {placeholder: "__/__/____"});
  $('.fallback').mask("00r00r0000", {
      translation: {
        'r': {
          pattern: /[\/]/,
          fallback: '/'
        },
        placeholder: "__/__/____"
      }
    });
  $('.selectonfocus').mask("00/00/0000", {selectOnFocus: true});
});
// jquery mask end


$(window).scroll(function(){
	if ($(window).scrollTop() > 400) { 
		$('.scroll-up-ifs').css('transform', 'translateY(0)');
	}
	else {
		$('.scroll-up-ifs').css('transform', 'translateY(200%)');
	}
});

// b scripts
$('.ths-popup-link.biotransportation-form-success').on('click', function (e) {
    $('.price-biotransportation').val($(this).attr('data-price'));
});

$(document).ready(function(){
	
	$(".scroll-up-ifs").on("click", function (event) {
		event.preventDefault();
		var id  = $(this).attr('href'),
			top = $(id).offset().top;
		$('body,html').animate({scrollTop: top}, 800);
	});
});

// sf menu
$(document).ready(function () {
  $('ul.sf-menu').superfish({
    pathClass: 'current',
    cssArrows: false,
    delay: '200',
    animation: { opacity: 'show', height: 'show' },
    speed: '70',
    autoArrows: false,
    dropShadows: false,
  });
});
// end sf menu

// login sign up popup
$(function () {
    $('.ths-popup-link').on('click', function (e) {
        /*e.preventDefault();*/
        var thsAttr = $(this).attr('href');
        $('.ths-modal-popup').hide();
        $(thsAttr).show();
    });

    $('.modal-close').on('click', function (e) {

        e.preventDefault();

        $(this).closest('.ths-modal-popup').hide();

    });
})
var eyeField = document.getElementById("toggle");
var inputState = document.getElementById("password");
var eyeLogin = document.getElementsByClassName("eye-login");

eyeField.onclick = function(inputState)  {


    if (document.getElementById("password").getAttribute("type") == "password") {
        console.log("work");
        document.getElementsByClassName("eye-login")[0].classList.toggle("eye-login-fill");
        document.getElementsByClassName("eye-login")[1].classList.toggle("eye-login-fill");
    }
    else {
        console.log("else");
        document.getElementsByClassName("eye-login")[0].classList.toggle("eye-login-fill");
        document.getElementsByClassName("eye-login")[1].classList.toggle("eye-login-fill");
    }


};
// end login sign up popup


// profile btn
$(document).ready(function() {

  $('.profile__btn').click(function(e) {

    e.preventDefault();
	  
	$('#header').toggleClass('header-active');

    var href = $(this).attr('href');
    $(href).slideToggle(300);
	  
	$(document).mouseup(function (e) {
      var div = $(".profile__btn");
      if (!div.is(e.target)
          && div.has(e.target).length === 0) {
        $(href).slideUp(300);
      }
    });
    
  });

});
// profile btn end


$(document).ready(function () {
  // personal weight
  $(".personal-weight-item.ot input").on("change paste keyup", function () {
    var id = $(this).val();
    $('.personal-weight-item.ot input').val(id);
  });
    $(".personal-weight-item.do input").on("change paste keyup", function () {
        var id = $(this).val();
        $('.personal-weight-item.do input').val(id);
    });
  // end personal weight

  // personal weight
  $(".personal-height-item.ot input").on("change paste keyup", function () {
    var id = $(this).val();
    $('.personal-height-item.ot input').val(id);
  });
    $(".personal-height-item.do input").on("change paste keyup", function () {
        var id = $(this).val();
        $('.personal-height-item.do input').val(id);
    });
  // end personal weight
});



// main-slide owl
$(document).ready(function () {
  var owlMain = $('.main-slide-wrap');

  owlMain.owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    mouseDrag: false,
    responsiveClass: true,
    responsive: {
      992: {
		  dots: true
      },
      0: {
		  dots: false
      }
    }
  });

  $('.arrow-next').click(function (e) {
    e.preventDefault();
    owlMain.trigger('next.owl.carousel');
  })
  // Go to the previous item
  $('.arrow-prev').click(function (e) {
    e.preventDefault();
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    owlMain.trigger('prev.owl.carousel', [300]);
  })

});
// end main-slide owl


// hamburger btn
$(document).ready(function () {

	$('.hamburger').click(function (e) {

		$(this).toggleClass('is-active');
		$(this).parent().toggleClass('hamburger-wrap-active');
		$(this).find('.line').toggleClass('line-active');
		$('.mobile-menu').slideToggle();

	});

	$('.mobile-menu li').on('click', function() {

		$('.mobile-submenu').hide();
		$('.mobile-menu li').removeClass('menu-active__link');
		$(this).find('.mobile-submenu').slideToggle();
		$('.mobile-menu li::before').css('transform', 'rotate(360deg)');

		if ($(this).find('.mobile-submenu').is(':visible')) {
			$(this).addClass('menu-active__link');
		}

	});

});
// end hamburger btn


// header desktop search
$(document).ready(function () {

  $('.search').find('a').on('click', function (e) {
    e.preventDefault();
    var searchAttr = $(this).attr('href');
    $(searchAttr).slideDown(300);

    $(document).mouseup(function (e) {
      var div = $("#search-desktop-active");
      if (!div.is(e.target)
          && div.has(e.target).length === 0) {
        div.slideUp(300);
      }
    });
  });

});
// end header desktop search

// mobile-right-menu-btn
$(document).ready(function () {

  $('.mobile-right-menu-btn-wrap').on('click', function (e) {
    e.preventDefault();
    $(this).parent().toggleClass('mobile-right-menu-btn-active');
	$('.mobile-right-menu-items').toggleClass('d-block');
    $('.mobile-log-in').toggleClass('mobile-login-active');
  });

  $('.mobile-right-menu-items').find('a').on('click', function (e) {
    e.preventDefault();

    var items = $(this).attr('href');

    $('.i-active').not($(items)).slideUp();
    $(items).slideToggle(300);

  });

  $('.location-active').find('a').on('click', function () {
    $('.location-item-active').removeClass('location-item-active');
    $(this).addClass('location-item-active');
    $(this).parent().hide();
  });

});
// end mobile-right-menu-btn 


// differences tabs
$(document).ready(function ($) {
  $('.differences-bottom div').hide();
  $('.differences-bottom div:first').show();
  $('.differences-top div a:first').addClass('differences-active');
  $('.differences-top div').click(function (event) {
    event.preventDefault()
    $('.differences-top div a').removeClass('differences-active');
    $(this).find('a').addClass('differences-active');
    $('.differences-bottom div').hide();

    var selectTab = $(this).find('a').attr("href");

    $(selectTab).fadeIn();
  });
});
// end differences tabs


// medical slide owl
$(document).ready(function () {
  var medicalOwl = $('.medical-team-slide')
  medicalOwl.owlCarousel({
    loop: true,
	items: 5,
	autoWidth: true,
  });

  $('.medical-arrow-right').click(function (e) {
    e.preventDefault();
    medicalOwl.trigger('next.owl.carousel');
  })

  // Go to the previous item
  $('.medical-arrow-left').click(function (e) {
    e.preventDefault();
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    medicalOwl.trigger('prev.owl.carousel', [300]);
  })
});
// end medical slide owl


// team owl
$(document).ready(function () {
  var teamOwl = $('.team-slide')
  teamOwl.owlCarousel({
    items: 5,
    loop: true,
    autoWidth: true
  });

  $('.team-arrow-right').click(function (e) {
    e.preventDefault();
    teamOwl.trigger('next.owl.carousel');
  })

  // Go to the previous item
  $('.team-arrow-left').click(function (e) {
    e.preventDefault();
    // With optional speed parameter
    // Parameters has to be in square bracket '[]'
    teamOwl.trigger('prev.owl.carousel', [300]);
  })
});
// end team owl


// search scripts
$(document).ready(function () {

  var $searchTrigger = $('[data-ic-class="search-trigger"]'),
      $searchInput = $('[data-ic-class="search-input"]'),
      $searchClear = $('[data-ic-class="search-clear"]');
  $searchTrigger.click(function () {
    var $this = $('[data-ic-class="search-trigger"]');
    $this.addClass('active');
    $searchInput.focus();
  });

  $searchInput.blur(function () {
    if ($searchInput.val().length > 0) {

      return false;

    } else {
      $searchTrigger.removeClass('active');
    }
  });

  $searchClear.click(function () {
    $searchInput.val('');
  });

  $searchInput.focus(function () {
    $searchTrigger.addClass('active');
  });

});
// end search scripts

// personal collapse
$(function () {
  var title = $('.personal-right-accordion').find('.personal-title');
  var wrap = $('.personal-right-accordion').find('.personal-right-accordion-wrap');

  wrap.hide();
  title.on('click', function () {
    $(this).next('.personal-right-accordion-wrap').slideToggle();
    $(this).toggleClass('personal-title-active');
  })
});
// end personal collapse

// personal collapse left
$(function () {
  var title = $('.personal-left').find('.personal-title');
  var wrap = $('.personal-left').find('.personal-left-accordion-wrap');

  title.on('click', function () {
    $(this).next('.personal-left-accordion-wrap').slideToggle();
    $(this).toggleClass('personal-left-title-active');
  })
});
// end personal collapse left


// ifs inner content mobile owl
$(function () {
  var owl = $('.inner-content-mobile');
  owl.owlCarousel({
    items: 1,
	autoHeight: true
  });
});
// end ifs inner content mobile owl


// services mobile owl
$(function () {
  var owl = $('.services-mobile-wrap');
  owl.owlCarousel({
    items: 1,
	autoHeight: true
  });
});
// end services mobile owl

// biotransportation facts content mobile
$(function () {
  var owl = $('.biotransportation-facts-content-mobile');

  owl.owlCarousel({
    items: 1,
    dots: true,
  });
});
// end biotransportation facts content mobile


// end favourites click

// end b scripts



// a scripts

$(document).ready(function () {

  $('.aniimated-thumbnials').lightGallery({
    thumbnail: true,
    animateThumb: false,
    showThumbByDefault: false
  });
  $('.video-gallery').lightGallery();
});
$(document).ready(function () {
  $(".lightgallery").lightGallery();
});
$(document).ready(function () {
  $(".collapse-text").on('click', function () {
    $(this).find(".collapse-arrow").toggleClass("rotate-img");
  });
});

$(document).ready(function () {

  // surrogate owl
  $('.owl-surrogate').owlCarousel({
    loop: true,
    margin: 10,
    responsiveClass: true,
    responsive: {
      0: {
        items: 1,
        nav: true
      },
      600: {
        items: 2,
        nav: false
      },
      1000: {
        items: 3,
        nav: true,
        loop: false
      }
    }
  })
  $('.mobile-owl').owlCarousel({
    loop: true,
    margin: 10,
    dots: true,
    dotsEach: true,
    responsiveClass: true,
	autoHeight: true,
    responsive: {
      0: {
        items: 1,

      },
      600: {
        items: 1,
        nav: false
      },

    }

  })

});
// end a scripts


// s scripts
$(document).ready(function () {
	
  var process = $('.process');
  var surrogacyCosts = $('.surrogacy-costs-mob');
  var photos = $('.egg-donor-xzoom-photos');

  process.owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: true,
    nav: false
  });

  surrogacyCosts.owlCarousel({
    items: 1,
    loop: true,
    nav: false,
    dots: true,
    mouseDrag: true,
    nav: false,
	autoHeight: true
  });

  photos.owlCarousel({
    items: 3,
    loop: true,
    nav: true,
    dots: false,
    mouseDrag: true,
    navText: ["<img src='../images/nav-prev.png'>", "<img src='../images/nav-next.png'>"]
  });

});

$('.xzoom, .xzoom-gallery').xzoom({ position: 'lens', lensShape: 'circle', bg: true, sourceClass: 'xzoom-hidden' });


$(document).ready(function(){

	$('ul.programs-tab li').click(function(){
		var tab_id = $(this).attr('data-tabs');

		$('ul.programs-tab li').removeClass('current-active');
		$('.program-content').removeClass('current-active');

		$(this).addClass('current-active');
		$("#"+tab_id).addClass('current-active');
	});

});

$(document).ready(function(){

	$('ul.general-info-tabs li').click(function(){
		var tab_id = $(this).attr('data-general-tab');

		$('ul.general-info-tabs li').removeClass('general-current');
		$('.general-info-content').removeClass('general-current');

		$(this).addClass('general-current');
		$("#"+tab_id).addClass('general-current');
	});

});


$(document).ready(function () {

  $('ul.tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('ul.tabs li').removeClass('current');
    $('.tab-content').removeClass('current');

    $(this).addClass('current');
    $("#" + tab_id).addClass('current');
  })

})

$(document).ready(function () {
  $("#file").change(function (e) {
    var img = e.target.files[0];

    if (!iEdit.open(img, true, function (res) {
      $("#result").attr("src", res);
    })) {
      alert("Whoops! That is not an image!");
    }

  });
});

// end s scripts