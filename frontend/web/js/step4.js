$('body').on('change', 'input[name="PersonalHistory[good_health]"]', function (e) {
    if($(this).val() == 0)
        $('.good_health').show();
    else
        $('.good_health').hide();
});

$('body').on('change', 'input[name="PersonalHistory[have_you_ever]"]', function (e) {
    if($(this).val() == 1)
        $('.have_you_ever').show();
    else
        $('.have_you_ever').hide();
});

$('body').on('change', 'input[name="PersonalHistory[you_currently]"]', function (e) {
    if($(this).val() == 1)
        $('.you_currently').show();
    else
        $('.you_currently').hide();
});

$('body').on('change', 'input[name="PersonalHistory[have_you_taken]"]', function (e) {
    if($(this).val() == 1)
        $('.have_you_taken').show();
    else
        $('.have_you_taken').hide();
});

$('body').on('change', 'input[name="PersonalHistory[ablation_procedure]"]', function (e) {
    if($(this).val() == 1)
        $('.ablation_procedure').show();
    else
        $('.ablation_procedure').hide();
});

$('body').on('change', 'input[name="PersonalHistory[recreational_drugs]"]', function (e) {
    if($(this).val() == 1)
        $('.recreational_drugs').show();
    else
        $('.recreational_drugs').hide();
});

$('body').on('change', 'input[name="PersonalHistory[alcohol_abuse]"]', function (e) {
    if($(this).val() == 1)
        $('.alcohol_abuse').show();
    else
        $('.alcohol_abuse').hide();
});

$('body').on('change', 'input[name="PersonalHistory[alcohol_addiction]"]', function (e) {
    if($(this).val() == 1)
        $('.alcohol_addiction').show();
    else
        $('.alcohol_addiction').hide();
});

$('body').on('change', 'input[name="PersonalHistory[sexual_abuse]"]', function (e) {
    if($(this).val() == 1)
        $('.sexual_abuse').show();
    else
        $('.sexual_abuse').hide();
});

$('body').on('change', 'input[name="PersonalHistory[had_any_cancer]"]', function (e) {
    if($(this).val() == 1)
        $('.had_any_cancer').show();
    else
        $('.had_any_cancer').hide();
});

$('body').on('change', 'input[name="PersonalHistory[blood_donor]"]', function (e) {
    if($(this).val() == 1)
        $('.blood_donor').show();
    else
        $('.blood_donor').hide();
});

$('body').on('change', 'input[name="PersonalHistory[hospitalizations]"]', function (e) {
    if($(this).val() == 1)
        $('.hospitalizations').show();
    else
        $('.hospitalizations').hide();
});

$('body').on('change', 'input[name="PersonalHistory[recreational_drugs]"]', function (e) {
    if($(this).val() == 1)
        $('.recreational_drugs').show();
    else
        $('.recreational_drugs').hide();
});

$('body').on('change', 'select[name="PersonalInfo[country]"]', function (e) {
    if($(this).val() == 152)
        $('.country_state').show();
    else
        $('.country_state').hide();
});

$('body').on('change', 'input[name="MentalHealth[health_professional]"]', function (e) {
    if($(this).val() == 1)
        $('.health_professional').show();
    else
        $('.health_professional').hide();
});

$('body').on('change', 'input[name="PersonalHistory[depo_provera]"]', function (e) {
    if($(this).val() == 1)
        $('.depo_provera').show();
    else
        $('.depo_provera').hide();
});

$('body').on('change', 'input[name="PersonalHistory[birth_control]"]', function (e) {
    if($(this).val() == 1)
        $('.birth_control').show();
    else
        $('.birth_control').hide();
});

$('body').on('change', 'input[name="PersonalHistory[fragile_x]"]', function (e) {
    if($(this).val() == 1)
        $('.fragile_x').show();
    else
        $('.fragile_x').hide();
});

$('body').on('change', 'input[name="PersonalHistory[tay_sachs]"]', function (e) {
    if($(this).val() == 1)
        $('.tay_sachs').show();
    else
        $('.tay_sachs').hide();
});

$('body').on('change', 'input[name="PersonalHistory[sickle_cell]"]', function (e) {
    if($(this).val() == 1)
        $('.sickle_cell').show();
    else
        $('.sickle_cell').hide();
});

$('body').on('change', 'input[name="PersonalHistory[cystic_fibrosis]"]', function (e) {
    if($(this).val() == 1)
        $('.cystic_fibrosis').show();
    else
        $('.cystic_fibrosis').hide();
});

$('body').on('change', 'input[name="PersonalHistory[sma]"]', function (e) {
    if($(this).val() == 1)
        $('.sma').show();
    else
        $('.sma').hide();
});

$('body').on('change', 'input[name="PersonalHistory[tattoos]"]', function (e) {
    if($(this).val() == 1)
        $('.tattoos').show();
    else
        $('.tattoos').hide();
});

$('body').on('change', 'input[name="PersonalHistory[body_piercings]"]', function (e) {
    if($(this).val() == 1)
        $('.body_piercings').show();
    else
        $('.body_piercings').hide();
});