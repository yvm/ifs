
var photo;
var data_photo = new FormData();
var edit_photo = 0;

$('body').on('click', '.uploadImg',function(e){
     edit_photo= 0;
    $('.uploadImgInput').click();
});

$('body').on('click', '.edit_img_profile',function(e){
    edit_photo = $(this).attr('data-id');
    $('.uploadImgInput').click();
});

$('body').on('change', '.uploadImgInput',function(e){
    photo = this.files;

    $.each( photo, function( key, value ){
        data_photo.append( 'UserPhoto['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo?edit_photo='+edit_photo,
        data: data_photo,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            if(edit_photo == 0)
                $('#profile_photo').html(response);
            else
                $('.photo-place[data-id="'+edit_photo+'"]').html(response);
            data_photo = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_photo',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        success: function (response) {
            $('#profile_photo').html(response);
        },
        error: function () {

        }
    });
});

var documents;
var data_documents = new FormData();

$('body').on('click', '.uploadImgDocuments',function(e){
    $('.uploadImgInputDocuments').click();
});

$('body').on('change', '.uploadImgInputDocuments',function(e){
    documents = this.files;

    $.each( documents, function( key, value ){
        data_documents.append( 'UserDocuments['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo',
        data: data_documents,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_document').html(response);
            data_documents = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_documents',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        success: function (response) {
            $('#profile_document').html(response);
        },
        error: function () {

        }
    });
});

var video;
var data_video= new FormData();

$('body').on('click', '.uploadImgVideo',function(e){
    $('.uploadImgInputVideo').click();
});

$('body').on('change', '.uploadImgInputVideo',function(e){
    video = this.files;

    $.each( video, function( key, value ){
        data_video.append( 'UserVideo['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo',
        data: data_video,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_video').html(response);
            data_video = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_video',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        success: function (response) {
            // var html = ' <div class="photo-place">\n' +
            //     '            <button type="button" name="uploadImg" class="uploadImgVideo">\n' +
            //     '                <img src="/images/addPlus.png">\n' +
            //     '            </button>\n' +
            //     '        </div>';
            $('#profile_video').html(response);
            $('.uploadImgVideo.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});

var HandwritingSample;
var data_HandwritingSample= new FormData();

$('body').on('click', '.uploadImgHandwritingSample',function(e){
    $('.uploadImgInputHandwritingSample').click();
});

$('body').on('change', '.uploadImgInputHandwritingSample',function(e){
    HandwritingSample = this.files;

    $.each( HandwritingSample, function( key, value ){
        data_HandwritingSample.append( 'UserHandwritingSample['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo',
        data: data_HandwritingSample,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_handwriting').html(response);
            data_HandwritingSample = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_HandwritingSample',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        success: function (response) {
            // var html = '<div class="add-video-block">\n' +
            //         '<div class="title-info">\n'+
            //             '<h3>Select File:</h3>\n'+
            //             '<p>(support only *.png, *.jpg, *.jpeg, *.pdf, maximum size: 5mb, maximum 1 file)</p>\n'+
            //             '<button type="button" name="addVideo" class="uploadImgHandwritingSample add">Add file</button>\n'+
            //         '</div>\n'+
            //     '</div>\n'+
            //     ' <div class="photo-place">\n' +
            //     '            <button type="button" name="uploadImg" class="uploadImgHandwritingSample">\n' +
            //     '                <img src="/images/addPlus.png">\n' +
            //     '            </button>\n' +
            //     '        </div>';
            $('#profile_handwriting').html(response);
            $('.uploadImgHandwritingSample.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});

var VoiceMessage;
var data_VoiceMessage= new FormData();

$('body').on('click', '.uploadImgVoiceMessage',function(e){
    $('.uploadImgInputVoiceMessage').click();
});

$('body').on('change', '.uploadImgInputVoiceMessage',function(e){
    VoiceMessage = this.files;

    $.each( VoiceMessage, function( key, value ){
        data_VoiceMessage.append( 'UserVoiceMessage['+key+']', value );
    });

    e.stopPropagation(); // Остановка происходящего
    e.preventDefault();  // Полная остановка происходящего

    $.ajax({
        url: '/profile/photo',
        data: data_VoiceMessage,
        cache: false,
        contentType: false,
        processData: false,
        method: 'POST',
        type: 'POST',
        success: function (response) {
            $('#profile_voice_message').html(response);
            data_VoiceMessage = new FormData();
        },
        error: function () {
            alert('Ошибка');
        }
    });
});

$('body').on('click', '.delete_img_profile_VoiceMessage',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/deleteimg',
        data: {id: $(this).attr('data-id'), type: $(this).attr('data-type')},
        method: 'GET',
        success: function (response) {
            // var html = ' <div class="photo-place">\n' +
            //     '            <button type="button" name="uploadImg" class="uploadImgVoiceMessage">\n' +
            //     '                <img src="/images/addPlus.png">\n' +
            //     '            </button>\n' +
            //     '        </div>';
            $('#profile_voice_message').html(response);
            $('.uploadImgVoiceMessage.add').removeAttr('disabled');
        },
        error: function () {

        }
    });
});

$('body').on('click', '.Got_It',function(e){
    e.preventDefault();
    var val = $(this).attr('data-id');
    $.ajax({
        url: '/profile/got-it',
        data: {id: $(this).attr('data-id')},
        method: 'GET',
        dataType: 'JSON',
        success: function (response) {
            if(response.all != 0){
                $('.sum_no_got_it.all').html(response.all);
                $('.sum_no_got_it.id' + response.id).html(response.countid);
                $('.li_program_stage.id'+val).removeAttr('style');
                $('.Got_It[data-id="'+val+'"]').hide();
            }
            else
                $('.sum_no_got_it').remove();
        },
        error: function () {

        }
    });
});


$('body').on('click', '.delete_favorite_user',function(e){
    e.preventDefault();
    $.ajax({
        url: '/profile/delete-favorite',
        data: {favorite_user_id: $(this).attr('data-user-id')},
        method: 'GET',
        success: function (response) {
            $('.my-profile-favorites').html(response);
        },
        error: function () {

        }
    });
});




$('body').on('click', '#profile_status_edit',function(e){
    $('#profile_status_save').show();
    $('#profile_status_text').hide();
    $('#profile_status_edit').hide();
    $('#profile_status_textarea').show();
});



$('body').on('click', '#profile_status_save',function(e){

    $.ajax({
        type: 'POST',
        url: '/profile/update-status',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $('#profile_status_text').html($('#profile_status_textarea').val());
                $('#profile_status_save').hide();
                $('#profile_status_text').show();
                $('#profile_status_edit').show();
                $('#profile_status_textarea').hide();
            }else{
               alert('Something is error..');
            }
        },
        error: function () {

        }
    });
});





$('body').on('click', '.btn-setting-save-parent', function (e) {
    $.ajax({
        type: 'POST',
        url: '/profile/edit-parent',
        // dataType: 'json',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){

                swal("Your changes", "has been saved successfully", "success");
            }else{
                swal("Error", response, "error");
            }
        },
        error: function () {
        }
    });
});


$('body').on('click', '.btn-setting-save-surrogate-donor', function (e) {
    $.ajax({
        type: 'POST',
        url: '/profile/edit',
        // dataType: 'json',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){

                swal("Your changes", "has been saved successfully", "success");
            }else{
                swal("Error", response, "error");
            }
        },
        error: function () {
        }
    });
});


$('body').on('click', '.change_pass', function (e) {
    $.ajax({
        type: 'POST',
        url: '/profile/update-password',
        // dataType: 'json',
        data: $(this).closest('form').serialize(),
        success: function (response) {
            if(response == 1){
                $('input[name="User[password_repeat]"]').val("");
                $('input[name="User[password]"]').val("");
                $('input[name="User[old_password]"]').val("");
                swal("Your changes ", "has been saved successfully ", "success");
            }else{
                swal("Error", response, "error");
            }
        },
        error: function () {
        }
    });
});