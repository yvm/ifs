<?php

    use common\modules\user\models\User;
    use yii\helpers\Html;

    /* @var $this yii\web\View */
    /* @var $user User */

    $confirmLink = Yii::$app->urlManager->createAbsoluteUrl(['/registration-modal/confirm', 'token' => $user->access_token]);
    if($user->role == 1)
        $role = "Intended Parent";
    if($user->role == 2)
        $role = "Surrogate";
    if($user->role == 3)
        $role = "an Egg Donor";
    if($user->role == 4)
        $role = "Surrogate & Egg Donor";
    if($user->role == 5)
        $role = "Biotransportation Client";
?>
<div class="signup-confirm">
    <p><em>Hi</em> <em>dear</em><em> <?=$role?>, &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </em></p>

    <p><em>Greetings!</em></p>

    <p><em>Thank you for your interest in becoming a </em><em><?=$role?></em><em>. <u><?= Html::a(Html::encode('Click here'), $confirmLink) ?></u> to start your registration as a </em><em><?=$role?></em><em>.</em></p>

    <p><em>Once you complete your registration, we will review it and let you know if you qualify to complete your registration and become a </em><em><?=$role?></em><em>. Thank you for deciding to take this step.</em></p>

    <p><em>Sincerely, </em></p>

    <p><em>IFS Team</em></p>
</div>
