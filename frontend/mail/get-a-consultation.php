<div>
    First Name: <?=$model->firstname?>
</div>
<div>
    Last Name: <?=$model->lastname?>
</div>
<div>
    I am:
    <?
        if($model->role == 1)
            echo "Intended Parent";
        if($model->role == 2)
            echo "Surrogate";
        if($model->role == 3)
            echo "Egg Donor";
        if($model->role == 4)
            echo "Surrogate & Egg Donor";
        if($model->role == 5)
            echo "Biotransportation Client";
    ?>
</div>
<?if($model->type){?>
<div>
    Interested in:
    <?
        if($model->type == 1)
            echo "Surrogacy";
        if($model->type == 2)
            echo "Egg Donation";
        if($model->type == 3)
            echo "Surrogacy & Egg Donation";
    ?>
</div>
<?}?>
<div>
    Mobile Phone Number: <?=$model->mobile_phone_number?>
</div>
<div>
    Email: <?=$model->email?>
</div>
<div>
    Telegram: <?=$model->telegram?>
</div>
<div>
    Country: <?
                foreach($countries as $v){
                    if($v->id == $model->country) {
                        echo $v->name;
                        break;
                    }
                }
             ?>
</div>
<div>
    WatsApp: <?=$model->WatsApp?>
</div>
<div>
    Skype: <?=$model->Skype?>
</div>
<div>
    City: <?
            foreach($city as $v){
                if($v->id == $model->city) {
                    echo $v->name;
                    break;
                }
            }
          ?>
</div>
<div>
    Desired Consult Location: <?
                                foreach($DesiredConsultLocation as $v){
                                    if($v->id == $model->DesiredConsultLocation) {
                                        echo $v->name;
                                        break;
                                    }
                                }
                              ?>
</div>
<div>
    Desired Consult Date: <?=$model->data?>
</div>
<div>
    Desired Consult Time: <?=$model->time?>
</div>
<div>
    Desired Consult GMT: <?=$model->GMT?>
</div>
<div>
    Questions & Comments: <?=$model->QuestionsComments?>
</div>

