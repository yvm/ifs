<?php

namespace frontend\controllers;

use common\models\DonorCompensation;
use common\models\EthnicOrigin;
use common\models\EthnicOriginPerson;
use common\models\Miscellaneous;
use common\models\City;
use common\models\Countries;
use common\models\Countrys;
use common\models\Region;
use common\models\RegistrationText;
use common\models\UserHandwritingSample;
use common\models\UserVoiceMessage;
use common\models\AditionalInformation;
use common\models\Biotranssub;
use common\models\Consultation;
use common\models\FamilyHealthHistory;
use common\models\FamilyMemberInformation;
use common\models\HealthDetails;
use common\models\Ifssub;
use common\models\LoginForm;
use common\models\MentalHealth;
use common\models\Parentsub;
use common\models\PersonalHistory;
use common\models\PersonalInfo;
use common\models\PregnanciesAndSurrogacies;
use common\models\Profiles;
use common\models\RegistrationStep1;
use common\models\SexualHistory;
use common\models\SurrogateAvailability;
use common\models\User;
use common\models\UserAgree;
use common\models\UserCharacteristic;
use common\models\UserDocuments;
use common\models\UserEducation;
use common\models\UserPhoto;
use common\models\UserVideo;
use common\models\Valuta;
use frontend\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

class EggDonorController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
    public function actionStep20()
    {
		 $this->redirect('/egg-donor/step4');
	}
	
    public function Step($step)
    {
        if(!Yii::$app->session['reg_user'])
            return $this->goHome();

        $user = User::findOne(Yii::$app->session['reg_user']);

        if($user->step < $step)
            $this->redirect('/egg-donor/step'.$user->step);

        if($user->role != 3){
            if($user->role == 1)
                $this->redirect('/registration/step'.$user->step);
            if($user->role == 2)
                $this->redirect('/surrogate/step'.$user->step);
            if($user->role == 4)
                $this->redirect('/surrogate-and-egg-donor/step'.$user->step);
            if($user->role == 5)
                $this->redirect('/biotransportation-client/step'.$user->step);
        }

        return $user;
    }

    public function actionStep1()
    {
        $user = $this->Step(1);

        $model = RegistrationStep1::findOne(Yii::$app->session['reg_user']);
        if(!$model)
            $model = new RegistrationStep1;

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if($user->step == 1) {
                $user->step = 2;
                $user->save(false);
            }
            return $this->redirect('step2');
        }

        $text_step1 = RegistrationText::findOne(['type' => 4, 'role' => 3]);

        return $this->render('step1', compact('model', 'text_step1'));
    }

    public function actionStep2()
    {
        $user = $this->Step(2);

        $profile = Profiles::findOne(Yii::$app->session['reg_user']);

        $PersonalInfo = PersonalInfo::findOne(Yii::$app->session['reg_user']);
        if(!$PersonalInfo)
            $PersonalInfo = new PersonalInfo();
        $PersonalInfo->scenario = PersonalInfo::step2;

        if ($profile->load(Yii::$app->request->post()) && $profile->save()) {
            if ($PersonalInfo->load(Yii::$app->request->post())) {
                $PersonalInfo->date_of_birth = date("Y-m-d", strtotime($_POST['PersonalInfo']['date_of_birth']));
                $PersonalInfo->user_id = Yii::$app->session['reg_user'];
                if($PersonalInfo->save()) {
                    if($user->step == 2) {
                        $user->step = 3;
                        $user->save(false);
                    }
                    $this->redirect('/egg-donor/step3');
                }
            }
        }

        $countries = Countrys::find()->all();
        $region = Region::findAll(['country_id' => 152]);
        $city = City::findAll(['region_id' => 0]);

        $countries_arr = [];
        foreach ($countries as $v){
            $countries_arr[$v->id] = $v->name;
        }

        $region_arr = [];
        foreach ($region as $v){
            $region_arr[$v->id] = $v->name;
        }

        $city_arr = [];
        foreach ($city as $v){
            $city_arr[$v->id] = $v->name;
        }

        return $this->render('step2', compact('profile', 'PersonalInfo', 'countries_arr', 'region_arr', 'city_arr'));
    }

     public function actionStep3()
    {
        $user = $this->Step(3);

        $PersonalInfo = PersonalInfo::findOne(Yii::$app->session['reg_user']);
        if($PersonalInfo) {
            $PersonalInfo->scenario = PersonalInfo::eggDonor;

            $SurrogateAvailability = SurrogateAvailability::findOne(['user_id' => Yii::$app->session['reg_user']]);
            if (!$SurrogateAvailability)
                $SurrogateAvailability = new SurrogateAvailability();
            $SurrogateAvailability->scenario = SurrogateAvailability::eggDonor;

            $PregnanciesAndSurrogacies = PregnanciesAndSurrogacies::findOne(Yii::$app->session['reg_user']);
            if (!$PregnanciesAndSurrogacies)
                $PregnanciesAndSurrogacies = new PregnanciesAndSurrogacies();
            $PregnanciesAndSurrogacies->scenario = PregnanciesAndSurrogacies::eggDonor;

            if ($PersonalInfo->load(Yii::$app->request->post())) {
                $PersonalInfo->user_id = Yii::$app->session['reg_user'];
                if ($PersonalInfo->save())
                    if ($SurrogateAvailability->load(Yii::$app->request->post())) {
                        $SurrogateAvailability->user_id = Yii::$app->session['reg_user'];
                        if ($SurrogateAvailability->save())
                            if ($PregnanciesAndSurrogacies->load(Yii::$app->request->post())) {

                                if($PregnanciesAndSurrogacies->number_pregnancies == 0){
                                    $PregnanciesAndSurrogacies->how_many_children = 0;
                                    $PregnanciesAndSurrogacies->children_now = 0;
                                    $PregnanciesAndSurrogacies->number_miscarriages = 0;
                                    $PregnanciesAndSurrogacies->number_abortions = 0;
                                    $PregnanciesAndSurrogacies->number_stillbirths = 0;
                                    $PregnanciesAndSurrogacies->number_live_births = 0;

                                    $PregnanciesAndSurrogacies->list_each = '';
                                    $PregnanciesAndSurrogacies->if_any_of_your = '';
                                    $PregnanciesAndSurrogacies->please_list = '';
                                }

                                if ($PregnanciesAndSurrogacies->number_pregnancies == 0 && $PregnanciesAndSurrogacies->have_you_ever == 0) {
                                    $PregnanciesAndSurrogacies->scenario = PregnanciesAndSurrogacies::surrogate_numberMiscarriage_nameOfClinic;
                                }

                                if ($PregnanciesAndSurrogacies->number_pregnancies == 1 && $PregnanciesAndSurrogacies->have_you_ever == 0) {
                                    $PregnanciesAndSurrogacies->scenario = PregnanciesAndSurrogacies::surrogate_number_of_Miscarriage;
                                }

                                if ($PregnanciesAndSurrogacies->number_pregnancies == 0 && $PregnanciesAndSurrogacies->have_you_ever == 1) {
                                    $PregnanciesAndSurrogacies->scenario = PregnanciesAndSurrogacies::surrogate_name_of_clinic;
                                }
                                $PregnanciesAndSurrogacies->user_id = Yii::$app->session['reg_user'];
                                if ($PregnanciesAndSurrogacies->save()) {
                                    if ($user->step == 3) {
                                        $user->step = 4;
                                        $user->save(false);
                                    }
                                    $this->redirect('/egg-donor/step4');
                                }
                            }
                    }
            }

            return $this->render('step3', compact('SurrogateAvailability', 'PersonalInfo', 'PregnanciesAndSurrogacies'));
        }
    }

    public function actionStep4()
    {
        $user = $this->Step(4);

        $PersonalHistory = PersonalHistory::findOne(Yii::$app->session['reg_user']);
        if(!$PersonalHistory)
            $PersonalHistory = new PersonalHistory();
        else {
            $PersonalHistory->please_indicate = unserialize($PersonalHistory->please_indicate);
            $PersonalHistory->method_birth_control = unserialize($PersonalHistory->method_birth_control);
        }

        $PersonalHistory->scenario = PersonalHistory::EggDonor;

        $MentalHealth = MentalHealth::findOne(Yii::$app->session['reg_user']);
        if(!$MentalHealth)
            $MentalHealth = new MentalHealth();

        $SexualHistory = SexualHistory::findOne(Yii::$app->session['reg_user']);
        if(!$SexualHistory)
            $SexualHistory = new SexualHistory();

        if ($PersonalHistory->load(Yii::$app->request->post()) && $MentalHealth->load(Yii::$app->request->post()) && $SexualHistory->load(Yii::$app->request->post())) {
            $PersonalHistory->please_indicate = serialize($PersonalHistory->please_indicate);
            $PersonalHistory->method_birth_control = serialize($PersonalHistory->method_birth_control);

            $PersonalHistory->user_id = Yii::$app->session['reg_user'];
            if($PersonalHistory->save()){
                $MentalHealth->user_id = Yii::$app->session['reg_user'];
                if($MentalHealth->save()) {
                    $SexualHistory->user_id = Yii::$app->session['reg_user'];
                    if ($SexualHistory->save()) {
                        if ($user->step == 4) {
                            $user->step = 5;
                            $user->save(false);
                        }
                        $this->redirect('/egg-donor/step5');
                    }
                }
            }
        }

        return $this->render('step4', compact('PersonalHistory', 'MentalHealth', 'SexualHistory'));
    }
	
	public function actionStep5(){

        $user = $this->Step(5);

        $userCharacteristic = UserCharacteristic::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$userCharacteristic)
            $userCharacteristic = new UserCharacteristic();
        $userCharacteristic->scenario = UserCharacteristic::eggDonor;

        $EthnicOrigin= EthnicOrigin::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$EthnicOrigin)
            $EthnicOrigin = new EthnicOrigin();

        if ($userCharacteristic->load(Yii::$app->request->post())) {
            $userCharacteristic->user_id = Yii::$app->session['reg_user'];
            if($userCharacteristic->save()) {
                if ($EthnicOrigin->load(Yii::$app->request->post())) {
                    $EthnicOrigin->user_id = Yii::$app->session['reg_user'];
                    if($EthnicOrigin->save()) {
                        if($user->step == 5) {
                            $user->step = 6;
                            $user->save(false);
                        }
                        $this->redirect('/egg-donor/step6');
                    }
                }
            }
        }

        $ethnicA = EthnicOriginPerson::find()->orderBy('sort ASC')->all();

        return $this->render('step5',compact('userCharacteristic','EthnicOrigin', 'ethnicA'));
    }


    public function actionStep6(){

        $user = $this->Step(6);

        $userEducation = UserEducation::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$userEducation)
            $userEducation = new UserEducation();
        $userEducation->scenario = UserEducation::eggDonor;

        if ($userEducation->load(Yii::$app->request->post())) {
            $userEducation->user_id = Yii::$app->session['reg_user'];
            if($userEducation->do_you_know_any_language == 2){
                $userEducation->foreign_language = "";
                $userEducation->scenario = UserEducation::eggDonorForeignLanguage;
            }
            if($userEducation->save()) {
                if($user->step == 6) {
                    $user->step = 7;
                    $user->save(false);
                }
                $this->redirect('/egg-donor/step7');
            }

        }
        return $this->render('step6',compact('userEducation'));
    }



    public function actionStep7(){

        $user = $this->Step(7);
        $valuta = Valuta::findOne(['status'=>1]);
        $compensation = DonorCompensation::find()->one();

        $Miscellaneous = Miscellaneous::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$Miscellaneous)
            $Miscellaneous = new Miscellaneous();
        $Miscellaneous->scenario = Miscellaneous::eggDonor;

        if ($Miscellaneous->load(Yii::$app->request->post())) {
            $Miscellaneous->user_id = Yii::$app->session['reg_user'];
            if($Miscellaneous->save()) {
                if($user->step == 7) {
                    $user->step = 8;
                    $user->save(false);
                }
                $this->redirect('/egg-donor/step8');
            }

        }

        return $this->render('step7',compact('Miscellaneous','valuta','compensation'));
    }

    public function actionStep8()
    {
        $user = $this->Step(8);

        $FamilyMemberInformation = FamilyMemberInformation::findAll(['user_id' => Yii::$app->session['reg_user']]);
        if(!$FamilyMemberInformation)
            $FamilyMemberInformation = new FamilyMemberInformation();

        $FamilyHealthHistory = FamilyHealthHistory::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$FamilyHealthHistory)
            $FamilyHealthHistory = new FamilyHealthHistory();

        $FamilyHealthHistory->scenario = FamilyHealthHistory::role1;

        if ($FamilyHealthHistory->load(Yii::$app->request->post())) {
            $FamilyHealthHistory->user_id = Yii::$app->session['reg_user'];
            if($FamilyHealthHistory->save()) {
                if($user->step == 8) {
                    $user->step = 9;
                    $user->save(false);
                }
                $this->redirect('/egg-donor/step9');
            }
        }

        return $this->render('step8', compact('FamilyMemberInformation', 'FamilyHealthHistory'));
    }

    public function actionSaveFamilyMemberInformation()
    {
        $FamilyMemberInformation = new FamilyMemberInformation();

        if($_GET['id'])
            $FamilyMemberInformation = FamilyMemberInformation::findOne($_GET['id']);

        if ($FamilyMemberInformation->load(Yii::$app->request->get())) {
            $FamilyMemberInformation->user_id = Yii::$app->session['reg_user'];
            if($FamilyMemberInformation->save())
                return $FamilyMemberInformation->id;
        }
    }

    public function actionDeleteFamilyMemberInformation()
    {
        FamilyMemberInformation::deleteAll(['id' => $_GET['id']]);
    }

    public function actionStep9()
    {
        $user = $this->Step(9);

        if (isset($_POST['end_step'])){
            if($user->step == 9) {
                $user->step = 10;
                $user->save(false);
            }
            $this->redirect('/egg-donor/step10');
        }

        $FamilyHealthHistory = FamilyHealthHistory::findOne(['user_id' => Yii::$app->session['reg_user']]);
        if(!$FamilyHealthHistory)
            $FamilyHealthHistory = new FamilyHealthHistory();

        $HealthDetails = HealthDetails::findAll(['user_id' => Yii::$app->session['reg_user']]);

        return $this->render('step9', compact('FamilyHealthHistory', 'HealthDetails'));
    }

    public function actionSaveHealthDetails()
    {
        $HealthDetails = new HealthDetails();

        if($_GET['id'])
            $HealthDetails = HealthDetails::findOne($_GET['id']);

        if ($HealthDetails->load(Yii::$app->request->get())) {
            $HealthDetails->user_id = Yii::$app->session['reg_user'];
            if($HealthDetails->save())
                return $HealthDetails->id;
        }
    }

    public function actionDeleteHealthDetails()
    {
        HealthDetails::deleteAll(['id' => $_GET['id']]);
    }

    public function actionStep10()
    {
        if(!Yii::$app->request->isAjax)
            $user = $this->Step(10);
        $error = '';

        if(isset($_FILES['UserPhoto'])){
            $files = $this->actionUpdateaimg($_FILES['UserPhoto'], 'UserPhoto', 'userphoto');
            $UserPhoto = UserPhoto::findAll(['user_id' => Yii::$app->session['reg_user']]);
            $array_ext = ['jpg', 'jpeg', 'png'];

            foreach($files['UserPhoto'] as $k => $v){
                $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->session['reg_user'].'/'.$v, PATHINFO_EXTENSION);
                if(count($files['UserPhoto']) + count($UserPhoto) > 10){
                    $error .= 'You have exceeded the number of photos allowed for uploading';
                }elseif(ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userphoto/'.Yii::$app->session['reg_user'].'/'.$v)/1000) > 5000 || !in_array($ext, $array_ext)) {
                    $error .= 'Your file is more than 5mb or inappropriate extension';
                }else {
                    $model = new UserPhoto();
                    $model->photo = $v;
                    $model->user_id = Yii::$app->session['reg_user'];
                    if (!$model->save()) {
                        $error = "";
                        $errors = $model->getErrors();
                        foreach ($errors as $v) {
                            $error .= $v[0] . '<br />';
                        }
                    }
                }
            }
            $UserPhoto = UserPhoto::findAll(['user_id' => Yii::$app->session['reg_user']]);

            return $this->renderPartial('UserPhoto', compact('UserPhoto', 'error'));
        }

        if(isset($_FILES['UserDocuments'])){
            $files = $this->actionUpdateaimg($_FILES['UserDocuments'], 'UserDocuments', 'userdocuments');
            $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);
            $array_ext = ['jpg', 'jpeg', 'png', 'pdf'];

            foreach($files['UserDocuments'] as $k => $v){
                $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v, PATHINFO_EXTENSION);
                if(count($files['UserDocuments']) + count($UserDocuments) > 10){
                    $error .= 'You have exceeded the number of photos allowed for uploading';
                }elseif(ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v)/1000) > 5000 || !in_array($ext, $array_ext)) {
                    $error .= 'Your file is more than 5mb or inappropriate extension';
                }else {
                    $model = new UserDocuments();
                    $model->img = $v;
                    $model->user_id = Yii::$app->session['reg_user'];
                    if (!$model->save()) {
                        $error = "";
                        $errors = $model->getErrors();
                        foreach ($errors as $v) {
                            $error .= $v[0] . '<br />';
                        }
                    }
                }
            }
            $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);

            return $this->renderPartial('UserDocuments', compact('UserDocuments', 'error'));
        }

        if(isset($_FILES['UserVoiceMessage'])){
            $files = $this->actionUpdateaimg($_FILES['UserVoiceMessage'], 'UserVoiceMessage', 'uservoicemessage');
            $UserVoiceMessage = UserVoiceMessage::findOne(['user_id' => Yii::$app->session['reg_user']]);
            $array_ext = ['mp4', '3gp', 'ogg'];

            foreach($files['UserVoiceMessage'] as $k => $v){
                $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/uservoicemessage/'.Yii::$app->session['reg_user'].'/'.$v, PATHINFO_EXTENSION);
                if(count($files['UserVoiceMessage']) + count($UserVoiceMessage) > 1){
                    $error .= 'Your file is more than 5mb or inappropriate extension.';
                }elseif(ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/uservoicemessage/'.Yii::$app->session['reg_user'].'/'.$v)/1000) > 5000 || !in_array($ext, $array_ext)) {
                    $error .= 'Your file is more than 5mb or inappropriate extension.';
                }else {
                    $model = new UserVoiceMessage();
                    $model->video = $v;
                    $model->user_id = Yii::$app->session['reg_user'];
                    if (!$model->save()) {
                        $error = "";
                        $errors = $model->getErrors();
                        foreach ($errors as $v) {
                            $error .= $v[0] . '<br />';
                        }
                    }
                }
            }

            $UserVoiceMessage = UserVoiceMessage::findOne(['user_id' => Yii::$app->session['reg_user']]);

            return $this->renderPartial('UserVoiceMessage', compact('UserVoiceMessage', 'error'));
        }

        if(isset($_FILES['UserHandwritingSample'])){
            $files = $this->actionUpdateaimg($_FILES['UserHandwritingSample'], 'UserHandwritingSample', 'userhandwritingsample');
            $UserHandwritingSample = UserHandwritingSample::findOne(['user_id' => Yii::$app->session['reg_user']]);
            $array_ext = ['mp4', '3gp', 'ogg'];

            foreach($files['UserHandwritingSample'] as $k => $v){
                $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userhandwritingsample/'.Yii::$app->session['reg_user'].'/'.$v, PATHINFO_EXTENSION);
                if(count($files['UserHandwritingSample']) + count($UserHandwritingSample) > 1){
                    $error .= 'Your file is more than 5mb or inappropriate extension.';
                }elseif(ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userhandwritingsample/'.Yii::$app->session['reg_user'].'/'.$v)/1000) > 5000) {
                    $error .= 'Your file is more than 5mb or inappropriate extension.';
                }else {
                    $model = new UserHandwritingSample();
                    $model->video = $v;
                    $model->user_id = Yii::$app->session['reg_user'];
                    if (!$model->save()) {
                        $error = "";
                        $errors = $model->getErrors();
                        foreach ($errors as $v) {
                            $error .= $v[0] . '<br />';
                        }
                    }
                }
            }

            $UserHandwritingSample = UserHandwritingSample::findOne(['user_id' => Yii::$app->session['reg_user']]);

            return $this->renderPartial('UserHandwritingSample', compact('UserHandwritingSample', 'error'));
        }

        $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);
        $UserVideo = UserVideo::findOne(['user_id' => Yii::$app->session['reg_user']]);
        $UserPhoto = UserPhoto::findAll(['user_id' => Yii::$app->session['reg_user']]);
        $UserVoiceMessage = UserVoiceMessage::findOne(['user_id' => Yii::$app->session['reg_user']]);
        $UserHandwritingSample = UserHandwritingSample::findOne(['user_id' => Yii::$app->session['reg_user']]);

        return $this->render('step10', compact('user', 'UserDocuments', 'error', 'UserVideo', 'UserPhoto', 'UserVoiceMessage', 'UserHandwritingSample'));
    }

    public function actionUpdateaimg($FILES, $type, $dir)
    {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/backend/web/images/'.$dir.'/'.Yii::$app->session['reg_user'];

        if(!is_dir($uploaddir))
            mkdir($uploaddir, 0777);

        $array = [];
        foreach( $FILES as $k => $files ){
            foreach($files as $k1 => $file) {
                if (!empty($FILES['tmp_name'][$k1])) {
                    if (move_uploaded_file($FILES['tmp_name'][$k1], $uploaddir . '/' . $type . time() . '_' . basename($FILES['name'][$k1]))) {
                        $array[$type][$k1] = $type . time() . '_' . basename($FILES['name'][$k1]);
                    }
                }
            }
        }

        return $array;
    }

    public function actionRegisterEnd()
    {
        $user_documents = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);
        if(empty($user_documents))
            return 'Загрузите документ(ы)';
        else{
            $user = User::findOne(Yii::$app->session['reg_user']);
            $user->status_edit_profile = 1;
            $user->active = 1;
            $user->step = 20;
            $user->save(false);
            Yii::$app->user->login($user, 3600 * 24 * 30);
            Yii::$app->session['reg_user'] = 0;
            return 1;
        }
    }
}
