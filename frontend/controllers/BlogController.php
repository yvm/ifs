<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2019
 * Time: 10:27
 */

namespace frontend\controllers;


use common\models\Banner;
use common\models\Blog;
use common\models\Menu;
use common\models\Translation;
use DateTime;
use Yii;

class BlogController extends FrontendController
{

    public function actionIndex($month = null){

        $model = Menu::find()->where('url = "/blog/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $main = $banner[10];
        if($month == null){
            $blog = Blog::getAll();
        }else{
            $blog = Blog::getBlogByMonth($month);
        }

        $translation = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('index',compact('blog','main','translation'));
    }

    public function actionView($id){

        $menu = Menu::find()->where('url = "/blog/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $blog = Blog::findOne($id);
        $this->setMeta($blog->metaName, $blog->metaKey, $blog->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $main = $banner[10];
        $archives = Blog::getArchives();
        $translation = Translation::find()->where('page_id='.$menu->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();


        return $this->render('view',compact('blog','main','archives','translation'));
    }

}