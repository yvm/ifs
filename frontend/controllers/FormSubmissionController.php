<?php

namespace frontend\controllers;

use common\models\BiotransportationForm;
use common\models\IfsForm;
use common\models\AdminEmail;
use common\models\City;
use common\models\Countries;
use common\models\Countrys;
use common\models\DesiredConsultLocation;
use common\models\GetAConsultation;
use common\models\IfsFormFiles;
use common\models\Region;
use common\models\AditionalInformation;
use common\models\Biotranssub;
use common\models\Consultation;
use common\models\Ifssub;
use common\models\LoginForm;
use common\models\Parentsub;
use common\models\PersonalInfo;
use common\models\Profiles;
use common\models\RegistrationStep1;
use common\models\User;
use common\models\UserAgree;
use common\models\UserDocuments;
use common\models\UserPhoto;
use common\models\UserVideo;
use frontend\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

class FormSubmissionController extends FrontendController
{
    public function actionGetAConsultation()
    {
        $model = new GetAConsultation;

        if ($model->load(Yii::$app->request->post())) {
            if(count($model->type) == 2)
                $model->type = 3;
            else
                $model->type = $model->type[0];
            if($model->save()) {
                $this->EmailGetAConsultation($model);
                return 1;
            }
            else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0] . '<br />';
                }
                return $error;
            }
        }
    }

    public function EmailGetAConsultation($model)
    {
        $admins_email = AdminEmail::find()->all();

        $countries = Countrys::find()->all();
        $city = City::find()->all();
        $DesiredConsultLocation = DesiredConsultLocation::find()->all();

        foreach ($admins_email as $v) {
            \Yii::$app->mailer
                ->compose('get-a-consultation', ['model' => $model, 'countries' => $countries, 'city' => $city, 'DesiredConsultLocation' => $DesiredConsultLocation])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject('Заявка на консультацию ' . \Yii::$app->name)
                ->send();
        }

        \Yii::$app->mailer
            ->compose('get-a-consultation', ['model' => $model, 'countries' => $countries, 'city' => $city, 'DesiredConsultLocation' => $DesiredConsultLocation])
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name.' robot'])
            ->setTo($model->email)
            ->setSubject('Заявка на консультацию '.\Yii::$app->name)
            ->send();
    }

    public function actionIfsForm()
    {
        $model = new IfsForm;

        if ($model->load(Yii::$app->request->post())) {
            if(count($model->type) == 2)
                $model->type = 3;
            else
                $model->type = $model->type[0];
            if($model->save()) {
                foreach($_POST['files'] as $v){
                    $file = IfsFormFiles::findOne($v);
                    $file->parent_id = $model->id;
                    $file->save(false);
                }
                $this->EmailIfsForm($model);
                return 1;
            }
            else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0] . '<br />';
                }
                return $error;
            }
        }
    }

    public function actionIfsFormFiles()
    {
        if(isset($_FILES['Files'])){
            $files = $this->actionUpdateaimg($_FILES['Files'], 'Files', 'ifs-modal');

            foreach($files['Files'] as $k => $v){
                $model = new IfsFormFiles();
                $model->file = $v;
                $model->save();
            }

            $id = $model->id;

            return $this->renderAjax('IfsFormFiles', compact('id'));
        }
    }

    public function EmailIfsForm($model)
    {
        $admins_email = AdminEmail::find()->all();

        $countries = Countrys::find()->all();
        $city = City::find()->all();

        foreach ($admins_email as $v) {
            \Yii::$app->mailer
                ->compose('get-ifs-form', ['model' => $model, 'countries' => $countries, 'city' => $city])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject('Заявка на консультацию ' . \Yii::$app->name)
                ->send();
        }

        \Yii::$app->mailer
            ->compose('get-ifs-form', ['model' => $model, 'countries' => $countries, 'city' => $city])
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name.' robot'])
            ->setTo($model->email)
            ->setSubject('Заявка на консультацию '.\Yii::$app->name)
            ->send();
    }

    public function actionUpdateaimg($FILES, $type, $dir)
    {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/backend/web/images/ifs-modal/'.Yii::$app->session['reg_user'];

        if(!is_dir($uploaddir))
            mkdir($uploaddir, 0777);

        $array = [];
        foreach( $FILES as $k => $files ){
            foreach($files as $k1 => $file) {
                if (!empty($FILES['tmp_name'][$k1])) {
                    if (move_uploaded_file($FILES['tmp_name'][$k1], $uploaddir . '/' . $type . time() . '_' . basename($FILES['name'][$k1]))) {
                        $array[$type][$k1] = $type . time() . '_' . basename($FILES['name'][$k1]);
                    }
                }
            }
        }

        return $array;
    }

    public function actionBiotransportationRegistr()
    {
        $model = new BiotransportationForm();

        if ($model->load(Yii::$app->request->post())) {
            if($model->save()) {
                $this->EmailBiotransportationRegistr($model);
                return 1;
            }
            else {
                $error = "";
                $errors = $model->getErrors();
                foreach ($errors as $v) {
                    $error .= $v[0] . '<br />';
                }
                return $error;
            }
        }
    }

    public function EmailBiotransportationRegistr($model)
    {
        $admins_email = AdminEmail::find()->all();

        $countries = Countrys::find()->all();
        $city = City::find()->all();
        $region = Region::findAll(['country_id' => 152]);

        foreach ($admins_email as $v) {
            \Yii::$app->mailer
                ->compose('get-biotransportation-registr', ['model' => $model, 'countries' => $countries, 'city' => $city, 'region' => $region])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject('Заявка на консультацию ' . \Yii::$app->name)
                ->send();
        }

        \Yii::$app->mailer
            ->compose('get-biotransportation-registr', ['model' => $model, 'countries' => $countries, 'city' => $city, 'region' => $region])
            ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name.' robot'])
            ->setTo($model->email)
            ->setSubject('Заявка на консультацию '.\Yii::$app->name)
            ->send();
    }
}

