<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2019
 * Time: 10:27
 */

namespace frontend\controllers;

use common\models\Autocomlete;
use common\models\Menu;
use common\models\Surrogacycost;

class SearchController extends FrontendController
{
    public $inc = 1;
    public $count_page = 2;
    public $count_result = 0;

    public function actionIndex($text)//яйцеклеток
    {
        $metaName = 'Поиск';
        $metaKey = 'Поиск';
        $metaDesc = 'Поиск';
        $this->setMeta($metaName, $metaKey, $metaDesc);
        $array = [];

        $result = Menu::find()->where('text like "%'.$text.'%"')->all();
        $array = $this->result($result, $array, $text, 'url', 'text','text');

        $result = Surrogacycost::find()->where('content like "%'.$text.'%"')->all();
        $array = $this->result($result, $array, $text, 'geturl', 'getname','content');

        $result = $array;
        $count = $this->count_result;

        $i = 1;
        $array_str = [];
        if($result)
            foreach($result as $val){
                foreach($val as $v){
                    $text = explode(' ', trim(strip_tags($v['text'])));
                    foreach ($text as $v){
                        $v = str_replace(':', '', $v);
                        if(!in_array($v, $array_str) && !is_numeric($v)) {
                            $pattern = "/[а-яА-я]/i";
                            if (preg_match($pattern, $v, $matches))
                                $array_str[] = $v;
                        }
                    }
                    $i++;
                }
//                if($i == 3)
//                    break;
            }

        if($array_str) {
            //$this->actionXml($array_str);
        }

        return $this->render('index',compact('result', 'count'));
    }

    public function result($result, $array, $text, $url, $name, $text_result)
    {
        foreach ($result as $v){
            $this->count_result++;
            $rez = str_replace($text, '<span>'.$text.'</span>', $v->$text_result);
            $array[$this->inc][] = ['url' => $v->$url, 'name' => $v->$name, 'text' => $this->html_cut($rez, 200)];
            if(count($array[$this->inc]) % $this->count_page == 0)
                $this->inc++;
        }

        return $array;
    }

    public function actionXml($array_str)
    {
        $filename = $_SERVER['DOCUMENT_ROOT'].'/xml/autocomplete.xml';

        $xml = new \SimpleXMLElement('<?xml version="1.0"?><Autocompletions total="'.count($array_str).'"/>');
        $i = 1;
        foreach ($array_str as $v) {
            if(is_int($v))
                continue;
//             if($i == 0){
//                 break;
//             }
            $model = \common\models\Autocomlete::findOne(['name' => html_entity_decode($v)]);
            if(!$model) {
                $model = new Autocomlete;
                $model->name = html_entity_decode($v);
                $model->save(false);
            }
            $track = $xml->addChild('Autocompletion');
            $track->addAttribute('term', html_entity_decode($v));
            $track->addAttribute('type', 1);
            $track->addAttribute('match', 1);
            $i++;
        }

        Header('Content-type: text/xml');
        $fd = fopen($filename, 'w') or die("не удалось создать файл");
        $str = $xml->asXML();
        fwrite($fd, $str);
        fclose($fd);
//        print($xml->asXML());die;
    }

    public function html_cut($text, $max_length)
    {
        $tags   = array();
        $result = "";

        $is_open   = false;
        $grab_open = false;
        $is_close  = false;
        $in_double_quotes = false;
        $in_single_quotes = false;
        $tag = "";

        $i = 0;
        $stripped = 0;

        $stripped_text = strip_tags($text);

        while ($i < strlen($text) && $stripped < strlen($stripped_text) && $stripped < $max_length)
        {
            $symbol  = $text{$i};
            $result .= $symbol;

            switch ($symbol)
            {
                case '<':
                    $is_open   = true;
                    $grab_open = true;
                    break;

                case '"':
                    if ($in_double_quotes)
                        $in_double_quotes = false;
                    else
                        $in_double_quotes = true;

                    break;

                case "'":
                    if ($in_single_quotes)
                        $in_single_quotes = false;
                    else
                        $in_single_quotes = true;

                    break;

                case '/':
                    if ($is_open && !$in_double_quotes && !$in_single_quotes)
                    {
                        $is_close  = true;
                        $is_open   = false;
                        $grab_open = false;
                    }

                    break;

                case ' ':
                    if ($is_open)
                        $grab_open = false;
                    else
                        $stripped++;

                    break;

                case '>':
                    if ($is_open)
                    {
                        $is_open   = false;
                        $grab_open = false;
                        array_push($tags, $tag);
                        $tag = "";
                    }
                    else if ($is_close)
                    {
                        $is_close = false;
                        array_pop($tags);
                        $tag = "";
                    }

                    break;

                default:
                    if ($grab_open || $is_close)
                        $tag .= $symbol;

                    if (!$is_open && !$is_close)
                        $stripped++;
            }

            $i++;
        }

        while ($tags)
            $result .= "</".array_pop($tags).">";

        return $result." […]";
    }
}