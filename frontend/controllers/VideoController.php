<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.04.2019
 * Time: 16:16
 */

namespace frontend\controllers;


use common\models\Banner;
use common\models\Menu;
use common\models\Translation;
use common\models\Video;
use common\models\Videotype;
use Yii;

class VideoController extends FrontendController
{

    public function actionIndex(){

        $model = Menu::find()->where('url = "/video/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $video['main'] = $banner[12];
        $video['data'] = Video::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('index',compact('video'));
    }

    public function actionView($id){

        $menu = Menu::find()->where('url = "/video/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $model = Video::findOne($id);
        $this->setMeta($model->name);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $main = $banner[12];
        $main->bigTitle = $model->name;
        $video = Videotype::getAll(1,$id);
        $translation = Translation::find()->where('page_id='.$menu->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('view',compact('video','main','translation','model'));
    }

}