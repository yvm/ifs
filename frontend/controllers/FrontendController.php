<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 22.07.2018
 * Time: 2:02
 */

namespace frontend\controllers;

use common\models\AdminEmail;
use common\models\Admission;
use common\models\Buttons;
use common\models\Contact;
use common\models\Country;
use common\models\Donationcost;
use common\models\Guideline;
use common\models\Profiles;;
use common\models\Language;
use common\models\Menu;
use common\models\Pagenotfound;
use common\models\Preselect;
use common\models\Surrogacycost;
use common\models\Translation;
use common\models\TranslationMessage;
use common\models\TranslationModal;
use Yii;
use yii\web\Controller;

class FrontendController extends Controller
{

    public function init()
    {

        $preselect = Preselect::find()->one();
        if(!isset(Yii::$app->session["country"])){
            Yii::$app->session->set('country',$preselect->country_id);
        }

        if(!isset(Yii::$app->session["lang"])){
            Yii::$app->session->set('lang',$preselect->language_id);
        }



        $menu = Menu::find()->with('menuItems')->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'].' AND status=1')->orderBy('sort ASC')->all();
        $surrogacyCost = Surrogacycost::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();
        $donationCost = Donationcost::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();
        $contact = Contact::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->one();
        $buttons = Buttons::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();
        $county = Country::find()->where('status=1')->orderBy('sort ASC')->all();
        $pageNotFound = Pagenotfound::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->one();
        $translation = Translation::find()->where('page_id=0 AND country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();
        $translationModal = TranslationModal::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();
        $translationMessage = TranslationMessage::find()->where('country_id='.Yii::$app->session['country'].' AND language_id='.Yii::$app->session['lang'])->all();

        Yii::$app->view->params['country'] = $county;
        Yii::$app->view->params['menu'] = $menu;
        Yii::$app->view->params['surrogacyCost'] = $surrogacyCost;
        Yii::$app->view->params['donationCost'] = $donationCost;
        Yii::$app->view->params['contact'] = $contact;
        Yii::$app->view->params['buttons'] = $buttons;
        Yii::$app->view->params['activeLanguage'] =  Language::getActiveLanguage();
        Yii::$app->view->params['404'] = $pageNotFound;
        Yii::$app->view->params['translation'] = $translation;
        Yii::$app->view->params['translationModal'] = $translationModal;
        Yii::$app->view->params['translationMessage'] = $translationMessage;

    }

    protected function setMeta($title = null, $keywords = null, $description = null){
        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);
    }


    public function SendEmailRegistration($step)
    {
        $admins_email = AdminEmail::find()->all();

        foreach ($admins_email as $v){
            \Yii::$app->mailer
                ->compose('get-step-email', ['step' => $step])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject( Yii::$app->view->params['translationMessage'][0]->name.' '.Yii::$app->session['reg_user'].' ' . \Yii::$app->name)
                ->send();
        }
    }
	
	protected function sendUpdatePasswordInstruction($name, $email, $link) {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
//            ->setSubject('Update password')

            ->setHtmlBody("<p>".Yii::$app->view->params['translationMessage'][1]->name." $name, </p>
                                 <p>".Yii::$app->view->params['translationMessage'][2]->name." $link ".Yii::$app->view->params['translationMessage'][3]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][4]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][5]->name."</p>");
        return $emailSend->send();

    }

    protected function sendNewPassword($name, $email, $password) {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
//            ->setSubject('Update password')

            ->setHtmlBody("<p>".Yii::$app->view->params['translationMessage'][1]->name." $name, </p>
                                 <p>".Yii::$app->view->params['translationMessage'][6]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][7]->name." $password</p>
                                 <p>".Yii::$app->view->params['translationMessage'][4]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][5]->name."</p>");
        return $emailSend->send();

    }



    protected function sendUpdatedNewPassword($name, $email, $password) {
        $emailSend = Yii::$app->mailer->compose()
            ->setFrom(Yii::$app->params['adminEmail'])
            ->setTo($email)
//            ->setSubject('Update password')

            ->setHtmlBody("<p>".Yii::$app->view->params['translationMessage'][1]->name." $name, </p>
                                 <p>".Yii::$app->view->params['translationMessage'][8]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][7]->name." $password</p>
                                 <p>".Yii::$app->view->params['translationMessage'][4]->name."</p>
                                 <p>".Yii::$app->view->params['translationMessage'][5]->name."</p>");
        return $emailSend->send();

    }
}