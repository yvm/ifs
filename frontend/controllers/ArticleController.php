<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 10.04.2019
 * Time: 11:28
 */

namespace frontend\controllers;


use common\models\Article;
use common\models\Banner;
use common\models\Menu;
use common\models\Translation;
use Yii;

class ArticleController extends FrontendController
{

    public function actionIndex($month = null){

        $model = Menu::find()->where('url = "/article/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $main = $banner[13];
        if($month == null){
            $article = Article::getAll();
        }else{
            $article = Article::getBlogByMonth($month);
        }
        $translation = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('index',compact('article','main','translation'));
    }

    public function actionView($id){

        $menu = Menu::find()->where('url = "/article/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $article = Article::findOne($id);
        $this->setMeta($article->metaName, $article->metaKey, $article->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $main = $banner[10];
        $archives = Article::getArchives();
        $translation = Translation::find()->where('page_id='.$menu->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('view',compact('article','main','archives','translation'));
    }

}