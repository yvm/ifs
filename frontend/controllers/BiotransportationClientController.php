<?php

namespace frontend\controllers;

use common\models\City;
use common\models\Countries;
use common\models\Countrys;
use common\models\DesiredConsultLocation;
use common\models\Region;
use common\models\AditionalInformation;
use common\models\Biotranssub;
use common\models\Consultation;
use common\models\Ifssub;
use common\models\LoginForm;
use common\models\Parentsub;
use common\models\PersonalInfo;
use common\models\Profiles;
use common\models\RegistrationStep1;
use common\models\User;
use common\models\UserAgree;
use common\models\UserDocuments;
use common\models\UserPhoto;
use common\models\UserVideo;
use frontend\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\RegistrationText;

class BiotransportationClientController extends FrontendController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
	
    public function actionStep20()
    {
		 $this->redirect('/biotransportation-client/step4');
	}
	
    public function Step($step)
    {
        if(!Yii::$app->session['reg_user'])
            return $this->goHome();

        $user = User::findOne(Yii::$app->session['reg_user']);

        if($user->step < $step)
            $this->redirect('/biotransportation-client/step'.$user->step);

        if($user->role != 5){
            if($user->role == 1)
                $this->redirect('/registration/step'.$user->step);
            if($user->role == 2)
                $this->redirect('/surrogate/step'.$user->step);
            if($user->role == 3)
                $this->redirect('/egg-donor/step'.$user->step);
            if($user->role == 4)
                $this->redirect('/surrogate-and-egg-donor/step'.$user->step);
        }

        return $user;
    }

    public function actionStep1()
    {
        $user = $this->Step(1);

        $model = RegistrationStep1::findOne(Yii::$app->session['reg_user']);
        if(!$model)
            $model = new RegistrationStep1;

        if($model->load(Yii::$app->request->post()) && $model->save()) {
            if($user->step == 1) {
                $user->step = 2;
                $user->save(false);
            }
            return $this->redirect('step2');
        }

        $text_step1 = RegistrationText::findOne(['type' => 4, 'role' => 5]);

        return $this->render('step1', compact('model', 'text_step1'));
    }

    public function actionStep2()
    {
        $user = $this->Step(2);

        $profile = Profiles::findOne(Yii::$app->session['reg_user']);

        $PersonalInfo = PersonalInfo::findOne(Yii::$app->session['reg_user']);
        if(!$PersonalInfo)
            $PersonalInfo = new PersonalInfo();

        $PersonalInfo->scenario = PersonalInfo::biotrans;

        if ($profile->load(Yii::$app->request->post()) && $profile->save()) {
            if ($PersonalInfo->load(Yii::$app->request->post())) {
                $PersonalInfo->date_of_birth = date("Y-m-d", strtotime($_POST['PersonalInfo']['date_of_birth']));
                $PersonalInfo->user_id = Yii::$app->session['reg_user'];
                if($PersonalInfo->save()) {
                    if($user->step == 2) {
                        $user->step = 3;
                        $user->save(false);
                    }
                    $this->redirect('/biotransportation-client/step3');
                }
            }
        }

        $countries = Countrys::find()->all();
        $region = Region::findAll(['country_id' => 152]);
        $city = City::findAll(['region_id' => 0]);

        $countries_arr = [];
        foreach ($countries as $v){
            $countries_arr[$v->id] = $v->name;
        }

        $region_arr = [];
        foreach ($region as $v){
            $region_arr[$v->id] = $v->name;
        }

        $city_arr = [];
        foreach ($city as $v){
            $city_arr[$v->id] = $v->name;
        }

        return $this->render('step2', compact('profile', 'PersonalInfo', 'countries_arr', 'region_arr', 'city_arr'));
    }

    public function actionStep3()
    {
        $user = $this->Step(3);

        $desiredConsultation = DesiredConsultLocation::find()->orderBy('sort ASC')->all();

        $AditionalInformation = AditionalInformation::findOne(Yii::$app->session['reg_user']);
        if(!$AditionalInformation)
            $AditionalInformation = new AditionalInformation();
        $AditionalInformation->scenario = AditionalInformation::biotrans;

        $Consultation = Consultation::findOne(Yii::$app->session['reg_user']);
        if(!$Consultation)
            $Consultation = new Consultation();
        $Consultation->scenario = Consultation::biotrans;

        if ($AditionalInformation->load(Yii::$app->request->post())) {
            $AditionalInformation->user_id = Yii::$app->session['reg_user'];
            if($AditionalInformation->save())
                if ($Consultation->load(Yii::$app->request->post())) {
                    $Consultation->time_date = date("Y-m-d", strtotime($_POST['Consultation']['time_date']));
                    $Consultation->user_id = Yii::$app->session['reg_user'];
                    if($Consultation->save()) {
                        if($user->step == 3) {
                            $user->step = 4;
                            $user->save(false);
                        }
                        $this->redirect('/biotransportation-client/step4');
                    }
                }
        }

        return $this->render('step3',compact('AditionalInformation', 'Consultation', 'desiredConsultation'));
    }

    public function actionStep4()
    {
        if(!Yii::$app->request->isAjax)
            $user = $this->Step(4);

        $user = User::findOne(Yii::$app->session['reg_user']);
        $error = '';

        if(isset($_FILES['UserDocuments'])){
            $files = $this->actionUpdateaimg($_FILES['UserDocuments'], 'UserDocuments', 'userdocuments');
            $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);
            $array_ext = ['jpg', 'jpeg', 'png', 'pdf'];

            foreach($files['UserDocuments'] as $k => $v){
                $ext = pathinfo($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v, PATHINFO_EXTENSION);
                if(count($files['UserDocuments']) + count($UserDocuments) > 10){
                    $error .= 'You have exceeded the number of photos allowed for uploading';
                }elseif(ceil(filesize($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$v)/1000) > 5000 || !in_array($ext, $array_ext)) {
                    $error .= 'Your file is more than 5mb or inappropriate extension';
                }else {
                    $model = new UserDocuments();
                    $model->img = $v;
                    $model->user_id = Yii::$app->session['reg_user'];
                    if (!$model->save()) {
                        $error = "";
                        $errors = $model->getErrors();
                        foreach ($errors as $v) {
                            $error .= $v[0] . '<br />';
                        }
                    }
                }
            }
            $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);

            return $this->renderPartial('UserDocuments', compact('UserDocuments', 'error'));
        }


        $UserDocuments = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);


        return $this->render('step4', compact('user', 'UserDocuments', 'error'));
    }

    public function actionUpdateaimg($FILES, $type, $dir)
    {
        $uploaddir = $_SERVER['DOCUMENT_ROOT'].'/backend/web/images/'.$dir.'/'.Yii::$app->session['reg_user'];

        if(!is_dir($uploaddir))
            mkdir($uploaddir, 0777);

        $array = [];
        foreach( $FILES as $k => $files ){
            foreach($files as $k1 => $file) {
                if (!empty($FILES['tmp_name'][$k1])) {
                    if (move_uploaded_file($FILES['tmp_name'][$k1], $uploaddir . '/' . $type . time() . '_' . basename($FILES['name'][$k1]))) {
                        $array[$type][$k1] = $type . time() . '_' . basename($FILES['name'][$k1]);
                    }
                }
            }
        }

        return $array;
    }

    public function actionDeleteimg()
    {
        if($_GET['type'] == "UserDocuments") {
            $model = UserDocuments::findOne($_GET['id']);
            UserDocuments::deleteAll(['id' => $_GET['id']]);
            unlink($_SERVER['DOCUMENT_ROOT'].'/backend/web/images/userdocuments/'.Yii::$app->session['reg_user'].'/'.$model->img);
        }

        $array = ['type' => $_GET['type'], 'id' => $_GET['id']];

        return json_encode($array);
    }

    public function actionRegisterEnd()
    {
        $user_documents = UserDocuments::findAll(['user_id' => Yii::$app->session['reg_user']]);
        if(empty($user_documents))
            return 'Загрузите документ(ы)';
        else{
            $user = User::findOne(Yii::$app->session['reg_user']);
            $user->status_edit_profile = 1;
            $user->active = 1;
            $user->step = 20;
            $user->save(false);
            Yii::$app->user->login($user, 3600 * 24 * 30);
            Yii::$app->session['reg_user'] = 0;
            return 1;
        }
    }
}
