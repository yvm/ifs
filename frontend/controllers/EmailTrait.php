<?php
namespace frontend\controllers;

use Yii;
use common\models\AdminEmail;

trait EmailTrait
{
    public function actionEmailGotIT($profile, $program)
    {
        $admins_email = AdminEmail::find()->all();

        foreach ($admins_email as $v){
            Yii::$app->mailer
                ->compose('GotIT', ['profile' => $profile, 'program' => $program])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject('Пользователь ознакомился с новой информацией по программе.')
                ->send();
        }
    }

    public function actionNewProgramStages($profile)
    {
        $admins_email = AdminEmail::find()->all();

        foreach ($admins_email as $v){
            Yii::$app->mailer
                ->compose('NewProgramStages', ['profile' => $profile])
                ->setFrom([\Yii::$app->params['adminEmail'] => \Yii::$app->name . ' robot'])
                ->setTo($v->name)
                ->setSubject('Новая программа на сайте '.\Yii::$app->params['sait'])
                ->send();
        }
    }
}