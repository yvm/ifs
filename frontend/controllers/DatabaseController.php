<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.04.2019
 * Time: 20:36
 */

namespace frontend\controllers;


use common\models\Admission;
use common\models\Age;
use common\models\Banner;
use common\models\Countrys;
use common\models\Height;
use common\models\Menu;
use common\models\PersonalInfo;
use common\models\User;
use common\models\UserVideo;
use common\models\UserFavorites;
use common\models\Weight;
use Yii;
use yii\data\Pagination;

class DatabaseController extends FrontendController

{
    private $paginationCount = 9;

    public function beforeAction($action)
    {
        if(Yii::$app->user->isGuest){
            return $this->goHome();
        }

        $admission_susProfile_surrogate = Admission::findOne(['role'=>2]);
        $admission_susProfile_donor = Admission::findOne(['role'=>3]);
        $admission_susProfile_parent = Admission::findOne(['role'=>1]);
        
        $admission_surrogate = Admission::findOne(['role'=>2]);
        $admission_donor = Admission::findOne(['role'=>3]);
        $admission_parent = Admission::findOne(['role'=>1]);

        Yii::$app->view->params['admission_surrogate'] = $admission_surrogate;
        Yii::$app->view->params['admission_donor'] = $admission_donor;
        Yii::$app->view->params['admission_parent'] = $admission_parent;

        Yii::$app->view->params['admission_susProfile_surrogate'] = $admission_susProfile_surrogate->visibility;
        Yii::$app->view->params['admission_susProfile_donor'] = $admission_susProfile_donor->visibility;
        Yii::$app->view->params['admission_susProfile_parent'] = $admission_susProfile_parent->visibility;
        return parent::beforeAction($action); // TODO: Change the autogenerated stub
    }

    public function actionSurrogateDatabase(){


        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }


        $model = Menu::find()->where('url = "" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[7];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        if(isset($_POST['Ot'])){
            $date = ' AND DATEDIFF(CURRENT_DATE(),personal_info.date_of_birth) / 365.25 Between '.$_POST['Ot'].' And '.$_POST['Do'];

            $country = '';
            if($_POST['country'] != '')
                $country = ' AND personal_info.country = '.$_POST['country'];

            $marital = '';
            if($_POST['marital'])
                $marital = ' AND personal_info.marital_status = '.$_POST['marital'];

            $video = '';
            if($_POST['video'])
                $video = ' AND user_video.user_id = user.id';

            $new = '';
            if($_POST['new']) {
                $time = time() - 3600 * 24 * 15;
                $new = ' AND user.created_at >= '.$time;
            }

            $active = '';
            if($_POST['available_to_begin']) {
                $active = ' AND user.active = '.$_POST['available_to_begin'];
            }


            $previous = '';
            if($_POST['previous_surrogate']) {
                $previous = ' AND (miscellaneous.have_been_eggdonor = '.$_POST['previous_surrogate']. ' OR user.profi = 1)';
            }

            $favorites = '';
            if($_POST['my_favourites']) {
                $favorites = ' AND (user_favorites.user_id = '.Yii::$app->user->identity->id.')';
            }

            if(Yii::$app->view->params['admission_susProfile_surrogate']){
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history','miscellaneous','favorites'])
                    ->where('(user.role = 2 OR user.role = 4) AND user.step = 20 AND user.status = 1'.$date.$country.$marital.$new.$video.$active.$previous.$favorites);
            }else{
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history','miscellaneous','favorites'])
                    ->where('(user.role = 2 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND user.status_profile = 1'.$date.$country.$marital.$new.$video.$active.$previous.$favorites);
            }

            $count = $user->count();
            $pagination = new Pagination(['totalCount' => $count, 'pageSize'=> $this->paginationCount]);
            $users = $user->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            $model['data'] = $users;
            $model['pagination'] = $pagination;

            return $this->renderAjax('search',compact('model','fav','admission'));
        }

        $countries = Countrys::find()->all();
        $model = User::getSurrogateUsersWithPagination($this->paginationCount);
        $age = Age::findOne(['role'=>2]);

        return $this->render('surrogate-database',compact('surDatabase', 'model', 'countries','fav','age'));
    }



    public function actionSurrogateDatabaseSearch()
    {

		
        $model = Menu::find()->where('url = "" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[7];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        $countries = Countrys::find()->all();
        $id = $_GET['search'];
        if(Yii::$app->view->params['admission_susProfile_surrogate']) {
            $user = User::find()->where('(role = 2 OR role = 4) AND step = 20 AND status = 1')->all();
        }else{
            $user = User::find()->where('(role = 2 OR role = 4) AND step = 20 AND status = 1 AND status_profile = 1')->all();
        }
        foreach ($user as $v){
            $user_id = $v->getShortRoleName().'-'.$v->profile->getCreatedDateWithoutDot().''.$v->getOrderID();
            if($user_id == $id){
                $model = User::getSurrogateSearchUsersWithPagination($v->id, $this->paginationCount);
                break;
            }
        }

        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        return $this->render('surrogate-database',compact('surDatabase', 'model', 'countries_arr', 'countries','fav'));
    }

    public function actionNewAndUpdated()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }
        $time = time() - 3600 * 24 * 15;
        if(Yii::$app->view->params['admission_susProfile_surrogate']) {
            $user = User::find()->where('created_at >= ' . $time . ' AND (role = 2 OR role = 4) AND step = 20 AND status = 1');
        }else{
            $user = User::find()->where('created_at >= ' . $time . ' AND (role = 2 OR role = 4) AND step = 20 AND status_profile = 1 AND status = 1');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;
        return $this->renderAjax('search',compact('model','fav'));
    }


    public function actionMyFavorites()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_surrogate']) {
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('(user.role = 2 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }else{
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('(user.role = 2 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND user.status_profile = 1 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search',compact('model','fav'));
    }

    public function actionViewAllSurrogates()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_surrogate']) {
            $user = User::find()->where('(role = 2 OR role = 4) AND step = 20 AND status = 1');
        }else{
            $user = User::find()->where('(role = 2 OR role = 4) AND step = 20 AND status = 1 AND status_profile = 1');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search',compact('model','fav'));
    }














    public function actionEggDonorDatabase(){

        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        $model = Menu::find()->where('url = "" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[8];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        if(isset($_POST['Ot'])){
            $date = ' AND DATEDIFF(CURRENT_DATE(),personal_info.date_of_birth) / 365.25 Between '.$_POST['Ot'].' And '.$_POST['Do'];

            $country = '';
            if($_POST['country'] != '')
                $country = ' AND personal_info.country = '.$_POST['country'];

            $marital = '';
            if($_POST['marital'])
                $marital = ' AND personal_info.marital_status = '.$_POST['marital'];

            $video = '';
            if($_POST['video'])
                $video = ' AND user_video.user_id = user.id';

            $new = '';
            if($_POST['new']) {
                $time = time() - 3600 * 24 * 15;
                $new = ' AND user.created_at >= '.$time;
            }

            $active = '';
            if($_POST['available_to_begin']) {
                $active = ' AND user.active = '.$_POST['available_to_begin'];
            }


            $previous = '';
            if($_POST['previous_surrogate']) {
                $previous = ' AND (miscellaneous.have_been_eggdonor = '.$_POST['previous_surrogate']. ' OR user.profi = 1)';
            }

            $favorites = '';
            if($_POST['my_favourites']) {
                $favorites = ' AND (user_favorites.user_id = '.Yii::$app->user->identity->id.')';
            }

            $height = '';
            if($_POST['height_Ot'] && $_POST['height_Do']) {
                $height = ' AND personal_info.height >= '.$_POST['height_Ot'].' AND personal_info.height <= '.$_POST['height_Do'];
            }

            $weight = '';
            if($_POST['weight_Ot'] && $_POST['weight_Do']) {
                $weight = ' AND personal_info.weight >= '.$_POST['weight_Ot'].' AND personal_info.weight <= '.$_POST['weight_Do'];
            }

            $eye_color = '';
            if($_POST['eye_color']) {
                $eye_color = ' AND personal_info.eye_color = '.$_POST['eye_color'];
            }


            $hair_color = '';
            if($_POST['hair_color']) {
                $hair_color = ' AND personal_info.natural_hair_color_now = '.$_POST['hair_color'];
            }


            $ethnic_origin = '';
            if($_POST['ethnic']) {
                $ethnic_origin = ' AND ((ethnic_origin.ethnicA = '.$_POST['ethnic'].' AND ethnic_origin.percentA >= 20) OR'.
                    '(ethnic_origin.ethnicB = '.$_POST['ethnic'].' AND ethnic_origin.percentB >= 20) OR'.
                    '(ethnic_origin.ethnicC = '.$_POST['ethnic'].' AND ethnic_origin.percentC >= 20) OR'.
                    '(ethnic_origin.ethnicD = '.$_POST['ethnic'].' AND ethnic_origin.percentD >= 20))';
            }


            $education = '';
            if($_POST['education']) {
                $education = ' AND user_education.educational_level = '.$_POST['education'];
            }

            $blood_group = '';
            if($_POST['blood_group']) {
                $blood_group = ' AND personal_history.blood_group = '.'"'.$_POST['blood_group'].'"';
            }

            if(Yii::$app->view->params['admission_susProfile_donor']) {
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history', 'miscellaneous', 'favorites'])
                    ->where('(user.role = 3 OR user.role = 4) AND user.step = 20 AND user.status = 1' . $date . $country . $marital . $new . $video .
                        $active . $previous . $favorites . $height . $weight . $eye_color . $hair_color . $ethnic_origin . $education . $blood_group);
            }else{
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history', 'miscellaneous', 'favorites'])
                    ->where('(user.role = 3 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND user.status_profile = 1' . $date . $country . $marital . $new . $video .
                        $active . $previous . $favorites . $height . $weight . $eye_color . $hair_color . $ethnic_origin . $education . $blood_group);
            }
            $count = $user->count();
            $pagination = new Pagination(['totalCount' => $count, 'pageSize'=> $this->paginationCount]);
            $users = $user->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            $model['data'] = $users;
            $model['pagination'] = $pagination;

            return $this->renderAjax('search-egg-donor',compact('model','fav'));
        }

        $countries = Countrys::find()->all();
        $model = User::getEggDonorUsersWithPagination($this->paginationCount);
        $age = Age::findOne(['role'=>3]);
        $weight = Weight::findOne(['role'=>3]);
        $height = Height::findOne(['role'=>3]);

        return $this->render('egg-donor-database',compact('surDatabase', 'model', 'countries','fav','age','weight','height'));
    }

    public function actionEggDonorDatabaseSearch()
    {

        $model = Menu::find()->where('url = "" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[8];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        $countries = Countrys::find()->all();
        $id = $_GET['search'];
        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $user = User::find()->where('(role = 3 OR role = 4) AND step = 20 AND status = 1')->all();
        }else{
            $user = User::find()->where('(role = 3 OR role = 4) AND step = 20 AND status = 1 AND status_profile = 1')->all();
        }
        foreach ($user as $v){
            $user_id = $v->getShortRoleName().'-'.$v->profile->getCreatedDateWithoutDot().''.$v->getOrderID();
            if($user_id == $id){
                $model = User::getEggDonorSearchUsersWithPagination($v->id, $this->paginationCount);
                break;
            }
        }

        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        return $this->render('egg-donor-database',compact('surDatabase', 'model', 'countries_arr', 'countries','fav'));
    }

    public function actionNewAndUpdatedEggDonor()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }
        $time = time() - 3600 * 24 * 15;
        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $user = User::find()
                ->where('created_at >= ' . $time . ' AND (role = 3 OR role = 4) AND step = 20 AND status = 1');
        }else{
            $user = User::find()
                ->where('created_at >= ' . $time . ' AND (role = 3 OR role = 4) AND step = 20 AND status = 1  AND status_profile = 1');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-egg-donor',compact('model','fav'));
    }


    public function actionMyFavoritesEggDonor()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('(user.role = 3 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }else{
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('(user.role = 3 OR user.role = 4) AND user.step = 20 AND user.status = 1 AND user.status_profile = 1 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-egg-donor',compact('model','fav'));
    }

    public function actionViewAllEggDonor()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_donor']) {
            $user = User::find()
                ->where('(role = 3 OR role = 4) AND step = 20 AND status = 1');
        }else{
            $user = User::find()
                ->where('(role = 3 OR role = 4) AND step = 20 AND status = 1 AND status_profile = 1');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-egg-donor',compact('model','fav'));
    }















    public function actionIntendedParentDatabase()
    {

        if (!Yii::$app->user->isGuest) {
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id' => Yii::$app->user->identity->id]);
            foreach ($favorite as $v) {
                array_push($fav, $v->favorite_user_id);
            }
        }

        $model = Menu::find()->where('url = "" AND country_id=' . Yii::$app->session["country"] . ' AND language_id=' . Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id=' . Yii::$app->session["country"] . ' AND language_id=' . Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[9];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        if (isset($_POST['Ot'])) {
            $date = ' AND DATEDIFF(CURRENT_DATE(),personal_info.date_of_birth) / 365.25 Between ' . $_POST['Ot'] . ' And ' . $_POST['Do'];

            $country = '';
            if ($_POST['country'] != '')
                $country = ' AND personal_info.country = ' . $_POST['country'];

            $marital = '';
            if ($_POST['marital'])
                $marital = ' AND personal_info.marital_status = ' . $_POST['marital'];

            $video = '';
            if ($_POST['video'])
                $video = ' AND user_video.user_id = user.id';

            $new = '';
            if ($_POST['new']) {
                $time = time() - 3600 * 24 * 15;
                $new = ' AND user.created_at >= ' . $time;
            }

            $active = '';
            if ($_POST['available_to_begin']) {
                $active = ' AND user.active = ' . $_POST['available_to_begin'];
            }


            $favorites = '';
            if ($_POST['my_favourites']) {
                $favorites = ' AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')';
            }

            if(Yii::$app->view->params['admission_susProfile_parent']) {
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history', 'miscellaneous', 'favorites'])
                    ->where('user.role = 1 AND user.step = 20 AND user.status = 1' . $date . $country . $marital . $new . $video . $active . $favorites);
            }else{
                $user = User::find()
                    ->joinWith(['personal_info', 'video', 'education', 'ethnic_origin', 'personal_history', 'miscellaneous', 'favorites'])
                    ->where('user.role = 1 AND user.step = 20 AND user.status_profile = 1  AND user.status = 1' . $date . $country . $marital . $new . $video . $active . $favorites);
            }
            $count = $user->count();
            $pagination = new Pagination(['totalCount' => $count, 'pageSize' => $this->paginationCount]);
            $users = $user->offset($pagination->offset)
                ->limit($pagination->limit)
                ->all();
            $model['data'] = $users;
            $model['pagination'] = $pagination;

            return $this->renderAjax('search-parent', compact('model', 'fav'));
        }

        $countries = Countrys::find()->all();
        $model = User::getIndentedParentUsersWithPagination($this->paginationCount);
        $age = Age::findOne(['role'=>1]);

        return $this->render('intended-parent-database', compact('surDatabase', 'model', 'countries', 'fav','age'));
    }

    public function actionIntendedParentDatabaseSearch()
    {

        $model = Menu::find()->where('url = "" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surDatabase['main'] = $banner[9];
        $this->setMeta($surDatabase['main']->bigTitle, $model->metaKey, $model->metaDesc);

        $countries = Countrys::find()->all();
        $id = $_GET['search'];
        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $user = User::find()->where('role = 1 AND status = 1 AND step = 20')->all();
        }else{
            $user = User::find()->where('role = 1 AND status = 1 AND status_profile = 1 AND step = 20')->all();
        }
        foreach ($user as $v){
            $user_id = $v->getShortRoleName().'-'.$v->profile->getCreatedDateWithoutDot().''.$v->getOrderID();
            if($user_id == $id){
                $model = User::getIndentedParentSearchUsersWithPagination($v->id, $this->paginationCount);
                break;
            }
        }

        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        return $this->render('intended-parent-database',compact('surDatabase', 'model', 'countries_arr', 'countries','fav'));
    }

    public function actionNewAndUpdatedParent()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }
        $time = time() - 3600 * 24 * 15;
        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $user = User::find()
                ->where('created_at >= ' . $time . ' AND role = 1 AND status = 1 AND step = 20');
        }else{
            $user = User::find()
                ->where('created_at >= ' . $time . ' AND role = 1 AND status = 1 AND status_profile = 1 AND step = 20');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-parent',compact('model','fav'));
    }


    public function actionMyFavoritesParent()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('user.role = 1 AND user.status = 1 AND user.step = 20 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }else{
            $user = User::find()
                ->joinWith(['favorites'])
                ->where('user.role = 1 AND user.status_profile = 1 AND user.status = 1 AND user.step = 20 AND (user_favorites.user_id = ' . Yii::$app->user->identity->id . ')');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-parent',compact('model','fav'));
    }

    public function actionViewAllParent()
    {
        if(!Yii::$app->user->isGuest){
            $fav = array();
            $favorite = UserFavorites::findAll(['user_id'=>Yii::$app->user->identity->id]);
            foreach ($favorite as $v){
                array_push($fav,$v->favorite_user_id);
            }
        }

        if(Yii::$app->view->params['admission_susProfile_parent']) {
            $user = User::find()
                ->where('role = 1 AND status = 1 AND step = 20');
        }else{
            $user = User::find()
                ->where('role = 1 AND status_profile = 1 AND status = 1 AND step = 20');
        }
        $count = $user->count();
        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$this->paginationCount]);
        $users = $user->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();
        $model['data'] = $users;
        $model['pagination'] = $pagination;;
        return $this->renderAjax('search-parent',compact('model','fav'));
    }





}