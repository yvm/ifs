<?php

namespace frontend\controllers;


use common\models\Banner;
use common\models\Biotranscon;
use common\models\Biotranscost;
use common\models\Biotransfact;
use common\models\Biotransprocess;
use common\models\Biotransprocessbutton;
use common\models\Donationcost;
use common\models\Donationcostcon;
use common\models\Donationcoststage;
use common\models\Donationhowstart;
use common\models\Donationlast;
use common\models\Donationprogramcon;
use common\models\Donationprogramtype;
use common\models\Donorabout;
use common\models\Donorbeneft;
use common\models\Donorcomp;
use common\models\Donorfaq;
use common\models\Donorforeigncon;
use common\models\Donorforeigntype;
use common\models\Donorhowallwork;
use common\models\Donorhowstart;
use common\models\Donorrequire;
use common\models\Donorstage;
use common\models\Donorstory;
use common\models\Ifsabout;
use common\models\Ifscontact;
use common\models\Ifsdiff;
use common\models\Ifsfacts;
use common\models\Ifsmedteam;
use common\models\Ifsservice;
use common\models\Ifsteam;
use common\models\Ifstrip;
use common\models\LoginForm;
use common\models\Parentbenefit;
use common\models\Parentdoctor;
use common\models\Parentfaq;
use common\models\Parenthowallwork;
use common\models\Parenthowstart;
use common\models\Parentlaw;
use common\models\Parentprogram;
use common\models\Parentstory;
use common\models\Parentwhykazak;
use common\models\Profiles;
use common\models\Submenu;
use common\models\Surrogacycost;
use common\models\Surrogacycostcon;
use common\models\Surrogacycoststage;
use common\models\Surrogacycosttype;
use common\models\Surrogacydatabase;
use common\models\Surrogacyprocess;
use common\models\Surrogacyprogramcon;
use common\models\Surrogacyprogramtype;
use common\models\Surrogacyschool;
use common\models\Surrogacyservice;
use common\models\Surrogateabout;
use common\models\Surrogatebeneft;
use common\models\Surrogatecomp;
use common\models\Surrogatefaq;
use common\models\Surrogateforeigncon;
use common\models\Surrogateforeigntype;
use common\models\Surrogatehowallwork;
use common\models\Surrogatehowstart;
use common\models\Surrogatepsyc;
use common\models\Surrogaterequire;
use common\models\Surrogateschool;
use common\models\Surrogateservice;
use common\models\Surrogatestage;
use common\models\Surrogatestagecon;
use common\models\Surrogatestory;
use common\models\Translation;
use common\models\User;
use frontend\models\SignupForm;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\log\Target;
use yii\web\Controller;
use common\models\Buttons;
use common\models\Contact;
use common\models\Mainprogress;
use common\models\Mainslide;
use common\models\Menu;


class SiteController extends FrontendController
{


    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'logout' => ['post'],
//                ],
//            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		//Yii::$app->session->destroy();var_dump($_SESSION);die;
        $captcha = new SignupForm();
        $captcha = $captcha->actionVipCapcha(8);

        $model = Menu::find()->where('url = "/" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $mainSlides = Mainslide::find()->where('status=1 AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('sort ASC')->all();
        $mainProgress = Mainprogress::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('index',compact('mainProgress','mainSlides', 'captcha'));
    }

    public function actionIfs()
    {
        $model = Menu::find()->where('url = "/site/ifs" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['main'] = $banner[0];
        $ifs['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['about'] = Ifsabout::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $ifs['fact'] = Ifsfacts::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['service'] = Ifsservice::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['diff'] = Ifsdiff::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['medteam'] = Ifsmedteam::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['team'] = Ifsteam::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $ifs['contact'] = Ifscontact::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();

        return $this->render('ifs',compact('ifs'));
    }




    public function actionParent(){

        if(!Yii::$app->user->isGuest){
            $check = User::find()->where('id='.Yii::$app->user->identity->id.' && role=1')->one();
        }else{
            $check = false;
        }
        $model = Menu::find()->where('url = "/site/parent" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['main'] = $banner[1];
        $parent['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['howAllWorks'] = Parenthowallwork::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $parent['programs'] = Parentprogram::find()->where('status =1 AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['benefits'] = Parentbenefit::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['whyKazakhstan'] = Parentwhykazak::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['doctors'] = Parentdoctor::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['laws'] = Parentlaw::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $parent['storyAndTesti'] = Parentstory::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['faq'] = Parentfaq::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['howStart'] = Parenthowstart::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $parent['translation'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('parent',compact('parent','check'));
    }

    public function actionSurrogacy(){

        $model = Menu::find()->where('url = "/site/surrogacy" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogacy['main'] = $banner[2];
        $surrogacy['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogacy['programcon'] = Surrogacyprogramcon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['programtype'] = Surrogacyprogramtype::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogacy['process'] = Surrogacyprocess::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogacy['costContent'] = Surrogacycostcon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['cost'] = Surrogacycost::find()->with('types')->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogacy['costTypes'] = Surrogacycosttype::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['banner'] = Surrogacydatabase::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['school'] = Surrogacyschool::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['service'] = Surrogacyservice::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogacy['translation'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('surrogacy',compact('surrogacy'));
    }

    public function actionSurrogacyCost($id){

        $model = Menu::find()->where('url = "/site/surrogacy" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $cost = Surrogacycost::findOne($id);
        $stages = Surrogacycoststage::find()->where('cost_id='.$id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $translation = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('surrogacy-cost',compact('cost','stages','translation'));
    }

    public function actionEggDonation(){
        $model = Menu::find()->where('url = "/site/egg-donation" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $eggdonation['main'] = $banner[3];
        $eggdonation['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $eggdonation['programcon'] = Donationprogramcon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $eggdonation['programtype'] = Donationprogramtype::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $eggdonation['howStart'] = Donationhowstart::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $eggdonation['cost'] = Donationcost::find()->with('types')->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $eggdonation['content'] = Donationcostcon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $eggdonation['banner'] = Donationlast::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $eggdonation['translation'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('egg-donation',compact('eggdonation'));
    }

    public function actionEggDonationCost($id){
        $model = Menu::find()->where('url = "/site/egg-donation" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $translation = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $cost = Donationcost::findOne($id);
        $stages = Donationcoststage::find()->where('cost_id='.$id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('egg-donation-cost',compact('translation','cost','stages'));
    }


    public function actionBiotransportation()
    {
        if(!Yii::$app->user->isGuest){
            $check = User::find()->where('id='.Yii::$app->user->identity->id.' && role=5')->one();
        }else{
            $check = false;
        }

        $model = Menu::find()->where('url = "/site/biotransportation" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $biotrans['main']  = $banner[4];
        $biotrans['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $biotrans['con'] = Biotranscon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $biotrans['fact'] = Biotransfact::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $biotrans['process'] = Biotransprocess::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $biotrans['processButton'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $biotrans['cost'] = Biotranscost::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('biotransportation',compact('biotrans','check'));
    }


    public function actionSurrogate(){

        if(!Yii::$app->user->isGuest){
            $check = User::find()->where('id='.Yii::$app->user->identity->id.' && role=2')->one();
        }else{
            $check = false;
        }
        $model = Menu::find()->where('url = "/site/surrogate" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['main'] = $banner[5];
        $surrogate['sub'] = Submenu::find()->where('menu_id ='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['howAllWorks'] = Surrogatehowallwork::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['about'] = Surrogateabout::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['benefit'] = Surrogatebeneft::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['requirement'] = Surrogaterequire::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['foreigncon'] = Surrogateforeigncon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['foreigntype'] = Surrogateforeigntype::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['stages'] = Surrogatestage::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['stagesContent'] = Surrogatestagecon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['compensation'] = Surrogatecomp::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['school'] = Surrogateschool::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['service'] = Surrogateservice::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['support'] = Surrogatepsyc::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $surrogate['story'] = Surrogatestory::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['faq'] = Surrogatefaq::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['howStart'] = Surrogatehowstart::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $surrogate['translation'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('surrogate',compact('surrogate','check'));
    }


    public function actionEggDonor(){

        if(!Yii::$app->user->isGuest){
            $check = User::find()->where('id='.Yii::$app->user->identity->id.' && role=3')->one();
        }else{
            $check = false;
        }

        $model = Menu::find()->where('url = "/site/egg-donor" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['main'] = $banner[6];
        $donor['sub'] = Submenu::find()->where('menu_id = '.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['howAllWorks'] = Donorhowallwork::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $donor['about'] = Donorabout::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $donor['benefit'] = Donorbeneft::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['requirement'] = Donorrequire::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['foreigncon'] = Donorforeigncon::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $donor['foreigntype'] = Donorforeigntype::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['stages'] = Donorstage::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['compensation'] = Donorcomp::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $donor['story'] = Donorstory::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['faq'] = Donorfaq::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['howStart'] = Donorhowstart::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $donor['translation'] = Translation::find()->where('page_id='.$model->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('egg-donor',compact('donor','check'));
    }

    public function actionSiteMap(){

        $model = Menu::find()->where('url = "/site/site-map" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $siteMap['main'] = $banner[14];

        return $this->render('siteMap',compact('siteMap'));
    }

    public function actionTripAlmaty(){

        $model = Submenu::find()->where('url = "trip-almaty" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->name, '', '');
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $trip['main'] = $banner[15];
        $trip['data'] = Ifstrip::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->with('tripImages')->all();


        return $this->render('trip-to-almaty',compact('trip'));
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->login()) {
                $user = User::findOne(Yii::$app->user->id);
                $user->scenario = User::GotIt;
                $user->got_it = 1;
                $user->save(false);

                return 1;
            }else{
                $error = "";
                $errors = $model->getErrors();
                $m=0;
                foreach($errors as $v){
                    $m++;
                    if(count($errors) ==$m){
                        $error .= $v[0];
                    }

                }

                return $error;
            }
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }


    public function actionUpdatePasswordByEmail(){
        $user = new User();
        $user->scenario = User::update_password_by_email;

        if ($user->load(Yii::$app->request->post())){
            if($user->email != null){
                if($user->validate()){
                    $check = User::findByEmail($user->email);
                    if($check){
                        $check->generateToken();
                        if($check->save(false)){
                            $profile = Profiles::findOne(['user_id'=>$check->id]);
                            $link = Yii::$app->request->hostInfo . '/site/confirm?token='.$check->access_token;
                            $this->sendUpdatePasswordInstruction($profile->first_name, $check->email, $link);
                            $arr = 1;
                            return $arr;
                        }else{
                            $error = "";
                            $errors = $user->getErrors();
                            foreach($errors as $v){
                                $error .= $v[0].'<br />';
                            }
                            return $error;
                        }

                    }else{
                        $arr = 2;
                        return $arr;
                    }

                } else {
                    $arr = 3;
                    return $arr;
                }
            }else{
                $arr = 4;
                return $arr;
            }
        }
    }


    public function actionConfirm($token)
    {
        if (empty($token) || !is_string($token) || null === $user = User::findByToken($token)) {
            echo 'Ошибка.';
        } else {
            $user->removeToken();
            $newPassword = $user->randomPassword();
            $user->setPassword($newPassword);
            if($user->save(false)){
                $profile = Profiles::findOne(['user_id'=>$user->id]);
                Yii::$app->session['modal-pass-reset'] = 1;
                Yii::$app->session['first_name'] = $profile->first_name;
                $this->sendNewPassword($profile->first_name,$user->email,$newPassword);
                return $this->redirect('/');
            }else{
                echo 'Oops, something is error!';
            }

//            \Yii::$app->session->addFlash('success', \Yii::t('main-message', 'Email успешно подтвержден. Вы можете войти на сайт.'));
        }
    }


}
