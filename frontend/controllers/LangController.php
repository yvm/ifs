<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 14.02.2019
 * Time: 11:00
 */

namespace frontend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class LangController extends FrontendController
{
    public function actionSetCountry($country){

        Yii::$app->session->set('country',$country);
        Yii::$app->session->set('lang',1);
        
        $url = $_SERVER['HTTP_REFERER'];
        $check = '';
        $m = 0;
        for($i=0;$i<strlen($url);$i++){
            if($url[$i] == '/') $m++;
            if($m == 3){
                $check = substr($url,$i+1,5);
                break;
            }
        }
        if($check == 'video') $this->redirect('/video/index');
        elseif($check == 'photo') $this->redirect('/photo/index');
        elseif($check == 'blog/') $this->redirect('/blog/index');
        elseif($check == 'artic') $this->redirect('/article/index');
        else $this->redirect($_SERVER['HTTP_REFERER']);
    }


    public function actionSetLang($lang){


        Yii::$app->session->set('lang',$lang);
        $url = $_SERVER['HTTP_REFERER'];
        $check = '';
        $m = 0;
        for($i=0;$i<strlen($url);$i++){
            if($url[$i] == '/') $m++;
            if($m == 3){
                $check = substr($url,$i+1,5);
                break;
            }
        }
        if($check == 'video') $this->redirect('/video/index');
        elseif($check == 'photo') $this->redirect('/photo/index');
        elseif($check == 'blog/') $this->redirect('/blog/index');
        elseif($check == 'artic') $this->redirect('/article/index');
        else $this->redirect($_SERVER['HTTP_REFERER']);

    }
}