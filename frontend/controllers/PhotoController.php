<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 09.04.2019
 * Time: 16:16
 */

namespace frontend\controllers;


use common\models\Banner;
use common\models\Menu;
use common\models\Photo;
use common\models\Phototype;
use common\models\Translation;
use Yii;

class PhotoController extends FrontendController
{

    public function actionIndex(){

        $model = Menu::find()->where('url = "/photo/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $this->setMeta($model->metaName, $model->metaKey, $model->metaDesc);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        $photo['main'] = $banner[11];
        $photo['data'] = Photo::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();

        return $this->render('index',compact('photo'));
    }

    public function actionView($id){

        $menu = Menu::find()->where('url = "/photo/index" AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->one();
        $model = Photo::findOne($id);
        $this->setMeta($model->name);
        $banner = Banner::find()->where('country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->orderBy('id DESC')->all();
        $main = $banner[11];
        $main->bigTitle = $model->name;
        $photo = Phototype::getAll(2,$id);
        $translation = Translation::find()->where('page_id='.$menu->id.' AND country_id='.Yii::$app->session["country"].' AND language_id='.Yii::$app->session["lang"])->all();
        return $this->render('view',compact('photo','main','translation','model'));
    }





}