<?php
/**
 * Created by PhpStorm.
 * User: Yuriy
 * Date: 22.07.2018
 * Time: 2:02
 */

namespace frontend\controllers;

use yii\web\Controller;

class AutocompleteController extends Controller
{
    public function actionIndex()
    {
        $q = urlencode($_GET['name_startsWith']);
        //$content = file_get_contents('https://ru.wikipedia.org/w/api.php?action=opensearch&limit=10&format=json&search='.$q);
        $content = file_get_contents('https://www.google.com/complete/search?q='.$q.'&cp=5&client=psy-ab&xssi=t&gs_ri=gws-wiz&hl=ru-KZ&authuser=0&pq=u003cb%20php&psi=O3ATXZr8Bq71qwHNwLSYDw.1561555011490&ei=O3ATXZr8Bq71qwHNwLSYDw');

        $content = substr($content, 6);
        $pattern = "/(.*)(,{.*}])/i";
        $replacement = "\${1}";
        $content = preg_replace($pattern, $replacement, $content);
//        echo '<pre>'.print_r(json_decode($content), true).'</pre>';die;
        $array = [];
        $array_isset = [];
        foreach (json_decode($content) as $k => $v){
            $pattern = "/(.*) (.*)/i";
            preg_match($pattern, $q, $matches);
            $v[0] = strip_tags(html_entity_decode($v[0]));
//            if(!$matches) {
//            $pattern = "/^(.+) /i";
//            $replacement = "\${1}";
//            $title = preg_replace($pattern, $replacement, $v[0]);
//            }else
//                $title = $v[0];
            $arr = explode(' ',trim($v[0]));
            if(!in_array($arr[0], $array_isset)) {
                $array_isset[] = $arr[0];
                $array[] = [
                    'id' => 1,
                    'title' => $arr[0]
                ];
                $inc++;
            }
            if($inc == 5)
                break;
//            $array[] = [
//                'id' => 1,
//                'title' => strip_tags(html_entity_decode($v[0]))
//            ];
//            if($k == 4)
//                break;
        }

        $json = json_encode(array('users' => $array));

        if ($_GET['callback']) {
            $callback = $_GET['callback'];
            $json = $callback . '('. $json . ')';
        }

        return $json;
    }

    public function actionIndex2()
    {
        $arr_new = ['перезагрузить', 'перезагрузка'];//В базе данных.
        //foreach($_GET['arr'] as $v) {
            $q = urlencode($_GET['name_startsWith']);
            $content = file_get_contents('https://www.google.com/complete/search?q=' . $q . '&cp=5&client=psy-ab&xssi=t&gs_ri=gws-wiz&hl=ru-KZ&authuser=0&pq=u003cb%20php&psi=O3ATXZr8Bq71qwHNwLSYDw.1561555011490&ei=O3ATXZr8Bq71qwHNwLSYDw');
            $content = substr($content, 6);
            $pattern = "/(.*)(,{.*}])/i";
            $replacement = "\${1}";
            $content = preg_replace($pattern, $replacement, $content);
            $array = [];
            $array_isset = [];
            foreach (json_decode($content) as $k => $v) {
                $pattern = "/(.*) (.*)/i";
                preg_match($pattern, $q, $matches);
                $v[0] = strip_tags(html_entity_decode($v[0]));
                $arr = explode(' ',trim($v[0]));
                if (in_array($arr[0], $arr_new) && !in_array($arr[0], $array_isset)) {
                    $array_isset[] = $arr[0];
                }
            }
        //}
        foreach ($array_isset as $v){
            $array[] = [
                'id' => 1,
                'title' => $v
            ];
        }

        $json = json_encode(array('users' => $array));

        if ($_GET['callback']) {
            $callback = $_GET['callback'];
            $json = $callback . '('. $json . ')';
        }

        return $json;
    }
}